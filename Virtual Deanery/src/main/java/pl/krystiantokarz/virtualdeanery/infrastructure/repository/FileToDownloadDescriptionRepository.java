package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.filetodownload.CustomFileToDownloadDescriptionRepository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FileToDownloadDescriptionRepository extends JpaRepository<FileToDownloadDescriptionEntity,Long>, CustomFileToDownloadDescriptionRepository {

    @Query(value = "SELECT file FROM FileToDownloadDescriptionEntity file WHERE file.fileStatus = :fileStatus AND file.fileType = :fileType")
    Page<FileToDownloadDescription> findAllFileToDownloadDescriptionByFileStatusAndFileTypePage(@Param("fileStatus") FileStatus fileStatus,  @Param("fileType") FileType fileType, Pageable pageable);

    @Query(value = "SELECT file FROM FileToDownloadDescriptionEntity file WHERE file.fileStatus = :fileStatus")
    Page<FileToDownloadDescription> findAllFileToDownloadDescriptionByFileStatusPage(@Param("fileStatus") FileStatus fileStatus, Pageable pageable);

    List<FileReadyToDownload> findByOriginalFileNameContainingIgnoreCaseAndFileTypeAndFileStatus(String originalFileName,FileType fileType,FileStatus fileStatus);

    @Query(value = "SELECT file FROM FileToDownloadDescriptionEntity file WHERE file.id = :fileId AND file.fileType = :fileType AND file.fileStatus = :fileStatus")
    Optional<FileToDownloadDescription> findFileToDownloadDescriptionByFileIdAndFileTypeAndStatus(@Param("fileId") Long fileId, @Param("fileType") FileType fileType, @Param("fileStatus") FileStatus fileStatus);

    @Query("select count(file) > 0 from FileToDownloadDescriptionEntity file where file.originalFileName = :fileName AND file.fileType = :fileType AND file.fileStatus = :fileStatus")
    boolean checkFileToDownloadWithSelectedOriginalNameAndTypeAndStatusExist(@Param("fileName") String fileName, @Param("fileType") FileType fileType, @Param("fileStatus") FileStatus fileStatus);

    FileReadyToDownload save(FileReadyToDownload fileReadyToDownload);

    Optional<FileToDownloadDescription> findById(Long id);

    Optional<FileToDownloadDescription> findByIdAndFileStatus(Long id,FileStatus fileStatus);
}
