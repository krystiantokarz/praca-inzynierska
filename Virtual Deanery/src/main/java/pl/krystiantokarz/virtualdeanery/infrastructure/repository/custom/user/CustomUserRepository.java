package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.user;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

public interface CustomUserRepository{

    User findUserByIdIfExist(Long id) throws UserNotExistException;

    User findUserByEmailIfExist(String email) throws UserNotExistException;

    User findUserByLoginIfExist(String login) throws UserNotExistException;

    Page<User> findAllForSelectedPageAndPageSize(Integer page, Integer pageSize);


}
