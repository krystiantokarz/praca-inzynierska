package pl.krystiantokarz.virtualdeanery.infrastructure.mail.resetpassword;

import org.apache.commons.lang3.StringUtils;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.SendObject;

public class ResetPasswordSendObject extends SendObject{

    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private String token;

    private ResetPasswordSendObject(String to, String login, String password,String token,String firstName, String lastName) {
        super(to);
        this.login = login;
        this.password = password;
        this.token = token;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public static ResetPasswordSendObject from(String to, String login, String password,String token,String firstName, String lastName) {
        validate(to,login,password,token,firstName,lastName);
        return new ResetPasswordSendObject(to,login,password,token,firstName,lastName);
    }

    private static void validate(String to, String login, String password,String token,String firstName, String lastName) {
        if(StringUtils.isBlank(to)){
            throw new IllegalArgumentException("To address cannot be null or empty");
        }
        if(StringUtils.isBlank(firstName)){
            throw new IllegalArgumentException("First name cannot be null or empty");
        }
        if(StringUtils.isBlank(lastName)){
            throw new IllegalArgumentException("Last name cannot be null or empty");
        }
        if(StringUtils.isBlank(password)){
            throw new IllegalArgumentException("Password cannot be null or empty");
        }
        if(StringUtils.isBlank(token)){
            throw new IllegalArgumentException("Token cannot be null or empty");
        }
        if(StringUtils.isBlank(login)){
            throw new IllegalArgumentException("Login cannot be null or empty");
        }
    }


    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResetPasswordSendObject that = (ResetPasswordSendObject) o;

        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        return token != null ? token.equals(that.token) : that.token == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ResetPasswordSendObject{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
