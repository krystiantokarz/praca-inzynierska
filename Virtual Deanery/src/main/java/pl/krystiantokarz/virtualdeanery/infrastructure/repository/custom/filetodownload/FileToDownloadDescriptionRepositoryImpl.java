package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.filetodownload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;
import java.util.Optional;
import org.springframework.data.domain.Pageable;

@Component
public class FileToDownloadDescriptionRepositoryImpl implements CustomFileToDownloadDescriptionRepository {

    @Autowired
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Override
    public FileReadyToDownload findFileReadyToDownloadByFileIdAndFileType(Long fileId, FileType fileType) throws FileNotExistException {
        Optional<FileToDownloadDescription> result = fileToDownloadDescriptionRepository.findFileToDownloadDescriptionByFileIdAndFileTypeAndStatus(fileId, fileType, FileStatus.READY_TO_DOWNLOAD);
        return (FileReadyToDownload) result.orElseThrow(
                () -> new FileNotExistException(String.format("File to download with id = %d not exist ", fileId))
        );
    }

    @Override
    public FileWaitingForAccepted findFileWaitingForAcceptedByFileIdAndFileType(Long fileId, FileType fileType) throws FileNotExistException{
        Optional<FileToDownloadDescription> result = fileToDownloadDescriptionRepository.findFileToDownloadDescriptionByFileIdAndFileTypeAndStatus(fileId, fileType, FileStatus.WAITING_FOR_ACCEPTED);
        return (FileWaitingForAccepted) result.orElseThrow(
                () -> new FileNotExistException(String.format("File to download with id = %d not exist ", fileId))
        );
    }

    @Override
    public FileWaitingForAccepted findFileWaitingForAcceptedByFileId(Long fileId) throws FileNotExistException {
        Optional<FileToDownloadDescription> result = fileToDownloadDescriptionRepository.findByIdAndFileStatus(fileId, FileStatus.WAITING_FOR_ACCEPTED);
        return (FileWaitingForAccepted) result.orElseThrow(
                () -> new FileNotExistException(String.format("File to download with id = %d not exist ", fileId))
        );
    }

    @Override
    public Page<FileReadyToDownload> findAllFileReadyToDownloadPageByFileType(FileType fileType, Pageable pageable){
        Page<FileToDownloadDescription> result = fileToDownloadDescriptionRepository.findAllFileToDownloadDescriptionByFileStatusAndFileTypePage(FileStatus.READY_TO_DOWNLOAD, fileType, pageable);
        return result.map(e -> (FileReadyToDownload) e );
    }

    @Override
    public Page<FileWaitingForAccepted> findAllFileWaitingForAcceptedPage(Pageable pageable){
        Page<FileToDownloadDescription> result = fileToDownloadDescriptionRepository.findAllFileToDownloadDescriptionByFileStatusPage(FileStatus.WAITING_FOR_ACCEPTED,pageable);
        return  result.map(e -> (FileWaitingForAccepted) e );
    }

    @Override
    public FileToDownloadDescription findFileDescriptionByIdIfExist(Long fileId) throws FileNotExistException {
        Optional<FileToDownloadDescription> result = fileToDownloadDescriptionRepository.findById(fileId);
        return result.orElseThrow(
                () -> new FileNotExistException(String.format("File to download with id = %d not exist ", fileId))
        );
    }
}
