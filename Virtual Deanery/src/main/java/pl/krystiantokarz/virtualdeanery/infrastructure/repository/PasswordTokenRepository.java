package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.krystiantokarz.virtualdeanery.domain.passwordresettoken.PasswordResetToken;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.passwordtoken.CustomPasswordTokenRepository;

import java.util.Optional;

@Repository
public interface PasswordTokenRepository extends JpaRepository<PasswordResetToken,Long>, CustomPasswordTokenRepository {

    Optional<PasswordResetToken> findByToken(String token);
}
