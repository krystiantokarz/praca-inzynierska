package pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out;

import java.io.Serializable;

public class UserInGroupDTO implements Serializable {

    private Long userId;

    private String firstName;

    private String lastName;

    private boolean adminAccess;

    public UserInGroupDTO() {
    }

    public UserInGroupDTO(Long userId, String firstName, String lastName, boolean adminAccess) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.adminAccess = adminAccess;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isAdminAccess() {
        return adminAccess;
    }

    public void setAdminAccess(boolean adminAccess) {
        this.adminAccess = adminAccess;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserInGroupDTO that = (UserInGroupDTO) o;

        if (adminAccess != that.adminAccess) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        return lastName != null ? lastName.equals(that.lastName) : that.lastName == null;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (adminAccess ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserInGroupDTO{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", adminAccess=" + adminAccess +
                '}';
    }
}
