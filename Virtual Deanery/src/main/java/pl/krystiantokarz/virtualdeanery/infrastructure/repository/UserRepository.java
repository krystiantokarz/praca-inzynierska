package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.user.CustomUserRepository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long>, CustomUserRepository {

    Optional<User> findById(Long id);

    Optional<User> findByEmail(String email);

    Optional<User> findByLoginDataLogin(String login);

    @Query("select count(u) > 0 from User u where u.email = :email")
    boolean checkUserWithSelectedEmailExist(@Param("email") String email);

    @Query("select count(u) > 0 from User u where u.email = :email and u.id <> :userId")
    boolean checkUserWithSelectedEmailExistWithoutSelectedUser(@Param("email") String email, @Param("userId") Long userId);

    @Query("select count(u) > 0 from User u where u.loginData.login = :login")
    boolean checkUserWithSelectedLoginExist(@Param("login") String login);

    @Query("select count(u) > 0 from User u where u.loginData.login = :login and u.id <> :userId")
    boolean checkUserWithSelectedLoginExistWithoutSelectedUser(@Param("login") String login, @Param("userId") Long userId);

    List<User> findByFirstNameIgnoreCaseAndLastNameIgnoreCase(String firstName, String lastName);

    List<User> findByFirstNameContainingIgnoreCase(String firstName);

    List<User> findByLastNameContainingIgnoreCase(String lastName);

}
