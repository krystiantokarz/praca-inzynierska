package pl.krystiantokarz.virtualdeanery.infrastructure.mail.resetpassword;

import org.thymeleaf.context.Context;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.EmailStrategy;

import java.util.Locale;

public class ResetPasswordSender extends EmailStrategy {

    private ResetPasswordSendObject emailSendObject;

    private static final String HTML_CONTENT = "/email/reset-password";
    private static final String BUNDLE_TOPIC = "reset.password.topic";

    public ResetPasswordSender(ResetPasswordSendObject object) {
        super(object);
        this.emailSendObject = object;
    }

    public Context createMessageContext(Locale locale){
        Context ctx = new Context(locale);
        ctx.setVariable("firstName", emailSendObject.getFirstName());
        ctx.setVariable("lastName", emailSendObject.getLastName());
        ctx.setVariable("password", emailSendObject.getPassword());
        ctx.setVariable("login", emailSendObject.getLogin());
        ctx.setVariable("token", emailSendObject.getToken());
        return ctx;


    }

    @Override
    public String getTemplateName() {
        return HTML_CONTENT;
    }

    @Override
    public String getBundleTopic() {
        return BUNDLE_TOPIC;
    }

}
