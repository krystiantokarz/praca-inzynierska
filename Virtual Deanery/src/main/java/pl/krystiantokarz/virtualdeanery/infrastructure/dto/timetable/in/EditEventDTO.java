package pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalTime;

public class EditEventDTO implements Serializable{

    @NotNull
    private Long eventId;

    @NotEmpty
    @Size(min = 5)
    private String topic;

    @NotEmpty
    @Size(min = 5)
    private String message;

    @NotNull
//    @JsonFormat(pattern = "HH:mm")
    private LocalTime timeFrom;

    @NotNull
//    @JsonFormat(pattern = "HH:mm")
    private LocalTime timeTo;

    public EditEventDTO() {
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalTime getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(LocalTime timeFrom) {
        this.timeFrom = timeFrom;
    }

    public LocalTime getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(LocalTime timeTo) {

        this.timeTo = timeTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EditEventDTO that = (EditEventDTO) o;

        if (eventId != null ? !eventId.equals(that.eventId) : that.eventId != null) return false;
        if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (timeFrom != null ? !timeFrom.equals(that.timeFrom) : that.timeFrom != null) return false;
        return timeTo != null ? timeTo.equals(that.timeTo) : that.timeTo == null;
    }

    @Override
    public int hashCode() {
        int result = eventId != null ? eventId.hashCode() : 0;
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (timeFrom != null ? timeFrom.hashCode() : 0);
        result = 31 * result + (timeTo != null ? timeTo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EditEventDTO{" +
                "eventId=" + eventId +
                ", topic='" + topic + '\'' +
                ", message='" + message + '\'' +
                ", timeFrom=" + timeFrom +
                ", timeTo=" + timeTo +
                '}';
    }
}
