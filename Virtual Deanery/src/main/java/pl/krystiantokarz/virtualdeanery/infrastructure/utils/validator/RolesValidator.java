package pl.krystiantokarz.virtualdeanery.infrastructure.utils.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.utils.constraint.RolesConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class RolesValidator implements ConstraintValidator<RolesConstraint, List<String>> {

    private static final Logger logger = LoggerFactory.getLogger(RolesValidator.class);

    @Override
    public void initialize(RolesConstraint rolesConstraint) {
    }

    @Override
    public boolean isValid(List<String> values, ConstraintValidatorContext constraintValidatorContext) {
        try{
            for (String val: values) {
                RoleEnum.valueOf(val);
            }
            return true;
        }catch (IllegalArgumentException e){
            logger.info("Cannot valid roles array to role enumeration");
            return false;
        }
    }
}
