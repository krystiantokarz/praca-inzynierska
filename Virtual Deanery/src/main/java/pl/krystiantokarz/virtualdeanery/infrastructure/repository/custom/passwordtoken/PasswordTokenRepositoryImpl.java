package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.passwordtoken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.krystiantokarz.virtualdeanery.domain.passwordresettoken.PasswordResetToken;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.PasswordTokenRepository;
import pl.krystiantokarz.virtualdeanery.service.resetpassword.exception.PasswordResetTokenNotExistException;

import java.util.Optional;

@Component
public class PasswordTokenRepositoryImpl implements CustomPasswordTokenRepository {

    @Autowired
    private PasswordTokenRepository passwordTokenRepository;

    @Override
    public PasswordResetToken findByTokenIfExist(String token) throws PasswordResetTokenNotExistException {

        Optional<PasswordResetToken> result = passwordTokenRepository.findByToken(token);
        return result.orElseThrow(
                () -> new PasswordResetTokenNotExistException(String.format("Password reset token not exist for token = %s", token))
        );
    }
}
