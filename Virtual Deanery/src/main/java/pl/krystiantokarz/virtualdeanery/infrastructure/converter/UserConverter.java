package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;

import java.util.List;
import java.util.stream.Collectors;

public class UserConverter {

    public static List<FindUserDTO> convertUsersToDTOs(List<User> users){
        return users.stream()
                .map(UserConverter::convertUserToDTO)
                .collect(Collectors.toList());
    }

    public static FindUserDTO convertUserToDTO(User user){
        return new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );
    }
}
