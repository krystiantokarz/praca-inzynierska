package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescription;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.file.out.FileToDownloadDTO;

import java.util.List;
import java.util.stream.Collectors;

public class FileConverter {

    public static <K extends FileToDownloadDescription> List<FileToDownloadDTO> convertFilesToDTOs(List<K> fileDescriptions){
        return  fileDescriptions.stream()
                .map( e -> new FileToDownloadDTO(
                        e.getId(),
                        e.getOriginalFileName(),
                        FileSizeConverter.convertBytesToMegabytes(e.getFileSize()),
                        FileTypeConverter.convertFileType(e.getFileExtensionType()),
                        e.getFileType().toString()
                )).collect(Collectors.toList());
    }
}
