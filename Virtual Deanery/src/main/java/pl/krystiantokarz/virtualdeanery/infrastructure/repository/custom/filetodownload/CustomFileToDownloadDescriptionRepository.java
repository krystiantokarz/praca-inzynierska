package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.filetodownload;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.krystiantokarz.virtualdeanery.domain.file.FileReadyToDownload;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescription;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.file.FileWaitingForAccepted;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;

public interface CustomFileToDownloadDescriptionRepository {

    FileReadyToDownload findFileReadyToDownloadByFileIdAndFileType(Long fileId, FileType fileType) throws FileNotExistException;

    FileWaitingForAccepted findFileWaitingForAcceptedByFileIdAndFileType(Long fileId, FileType fileType) throws FileNotExistException;

    FileWaitingForAccepted findFileWaitingForAcceptedByFileId(Long fileId) throws FileNotExistException;

    Page<FileReadyToDownload> findAllFileReadyToDownloadPageByFileType(FileType fileType, Pageable pageable);

    Page<FileWaitingForAccepted> findAllFileWaitingForAcceptedPage(Pageable pageable);

    FileToDownloadDescription findFileDescriptionByIdIfExist(Long fileId) throws FileNotExistException;
}
