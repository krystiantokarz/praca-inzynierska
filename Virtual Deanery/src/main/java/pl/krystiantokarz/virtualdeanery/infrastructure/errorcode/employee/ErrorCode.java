package pl.krystiantokarz.virtualdeanery.infrastructure.errorcode.employee;

public enum ErrorCode {
    USER_LOGIN_EXIST(0,"User with selected login exist"),
    USER_EMAIL_EXIST(1,"User with selected email exist"),
    INVALIDATE_DATE(2,"Date is in the past"),
    TIME_LIMIT(3,"Time exceeds the limits"),
    INVALIDATE_TIME(4,"Begin time is not before end time");

    private final int code;
    private final String description;

    ErrorCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", description='" + description + '\'' +
                '}';
    }
}
