package pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

public class MessageDescriptionDTO implements Serializable{

    private Long messageId;

    private String fromFirstName;

    private String fromLastName;

    private String subject;

    private LocalDateTime date;

    private String status;

    public MessageDescriptionDTO() {
    }

    public MessageDescriptionDTO(Long messageId, String fromFirstName, String fromLastName, String subject, LocalDateTime date, String status) {
        this.messageId = messageId;
        this.fromFirstName = fromFirstName;
        this.fromLastName = fromLastName;
        this.subject = subject;
        this.date = date;
        this.status = status;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getFromFirstName() {
        return fromFirstName;
    }

    public void setFromFirstName(String fromFirstName) {
        this.fromFirstName = fromFirstName;
    }

    public String getFromLastName() {
        return fromLastName;
    }

    public void setFromLastName(String fromLastName) {
        this.fromLastName = fromLastName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageDescriptionDTO that = (MessageDescriptionDTO) o;

        if (messageId != null ? !messageId.equals(that.messageId) : that.messageId != null) return false;
        if (fromFirstName != null ? !fromFirstName.equals(that.fromFirstName) : that.fromFirstName != null)
            return false;
        if (fromLastName != null ? !fromLastName.equals(that.fromLastName) : that.fromLastName != null) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        return status != null ? status.equals(that.status) : that.status == null;
    }

    @Override
    public int hashCode() {
        int result = messageId != null ? messageId.hashCode() : 0;
        result = 31 * result + (fromFirstName != null ? fromFirstName.hashCode() : 0);
        result = 31 * result + (fromLastName != null ? fromLastName.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MessageDescriptionDTO{" +
                "messageId=" + messageId +
                ", fromFirstName='" + fromFirstName + '\'' +
                ", fromLastName='" + fromLastName + '\'' +
                ", subject='" + subject + '\'' +
                ", date=" + date +
                ", status='" + status + '\'' +
                '}';
    }
}
