package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.GroupWithAccessDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.GroupDTO;

import java.util.ArrayList;
import java.util.List;

public class GroupConverter {

    public static List<GroupWithAccessDTO> convertGroupsToGroupsWithAccessDTOs(List<Group> groups, List<Group> groupsForUser){
        List<GroupWithAccessDTO> returnList = new ArrayList<>();

        for(Group group: groups){
            GroupWithAccessDTO groupDTO;
            boolean result  = groupsForUser.stream().anyMatch(e -> (e.getId().equals(group.getId())));
            if(result == false){
                groupDTO = new GroupWithAccessDTO(
                        group.getId(),
                        group.getName(),
                        group.getDescription(),
                        false);
            }else{
                groupDTO = new GroupWithAccessDTO(
                        group.getId(),
                        group.getName(),
                        group.getDescription(),
                        true);
            }
            returnList.add(groupDTO);
        }
        return returnList;
    }

    public static GroupDTO convertSingleGroupToDTO(Group group){
        return new GroupDTO(
                group.getId(),
                group.getName(),
                group.getDescription()
        );
    }

    public static List<GroupDTO> convertGroupsToGroupsDTOs(List<Group> allGroups){
        List<GroupDTO> returnList = new ArrayList<>();

        for(Group group: allGroups){
            GroupDTO groupForUserDTO = new GroupDTO(group.getId(), group.getName(), group.getDescription());
            returnList.add(groupForUserDTO);
        }
        return returnList;
    }
}
