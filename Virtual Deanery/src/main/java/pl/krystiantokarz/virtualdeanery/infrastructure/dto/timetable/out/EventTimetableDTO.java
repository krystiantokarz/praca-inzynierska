package pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public class EventTimetableDTO implements Serializable {

    private Long eventId;

    private Long userId;

    private String topic;

    private String userFirstName;

    private String userLastName;

    private LocalDate date;

    private LocalTime timeFrom;

    private LocalTime timeTo;

    public EventTimetableDTO() {
    }

    public EventTimetableDTO(Long eventId, Long userId, String topic, String userFirstName, String userLastName, LocalDate date, LocalTime timeFrom, LocalTime timeTo) {
        this.eventId = eventId;
        this.userId = userId;
        this.topic = topic;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.date = date;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(LocalTime timeFrom) {
        this.timeFrom = timeFrom;
    }

    public LocalTime getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(LocalTime timeTo) {
        this.timeTo = timeTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventTimetableDTO that = (EventTimetableDTO) o;

        if (eventId != null ? !eventId.equals(that.eventId) : that.eventId != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;
        if (userFirstName != null ? !userFirstName.equals(that.userFirstName) : that.userFirstName != null)
            return false;
        if (userLastName != null ? !userLastName.equals(that.userLastName) : that.userLastName != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (timeFrom != null ? !timeFrom.equals(that.timeFrom) : that.timeFrom != null) return false;
        return timeTo != null ? timeTo.equals(that.timeTo) : that.timeTo == null;
    }

    @Override
    public int hashCode() {
        int result = eventId != null ? eventId.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (userFirstName != null ? userFirstName.hashCode() : 0);
        result = 31 * result + (userLastName != null ? userLastName.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (timeFrom != null ? timeFrom.hashCode() : 0);
        result = 31 * result + (timeTo != null ? timeTo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EventTimetableDTO{" +
                "eventId=" + eventId +
                ", userId=" + userId +
                ", topic='" + topic + '\'' +
                ", userFirstName='" + userFirstName + '\'' +
                ", userLastName='" + userLastName + '\'' +
                ", date=" + date +
                ", timeFrom=" + timeFrom +
                ", timeTo=" + timeTo +
                '}';
    }
}
