package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.Optional;

@Component
public class UserRepositoryImpl implements CustomUserRepository {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findUserByIdIfExist(Long id) throws UserNotExistException {
        Optional<User> result = userRepository.findById(id);
        if(result.isPresent()){
            return result.get();
        }else{
            throw new UserNotExistException(String.format("User with id = %s not exist ", id));
        }
    }

    @Override
    public User findUserByEmailIfExist(String email) throws UserNotExistException {
        Optional<User> result = userRepository.findByEmail(email);
        if(result.isPresent()){
            return result.get();
        }else{
            throw new UserNotExistException(String.format("User with email = %s not exist ", email));
        }
    }

    @Override
    public User findUserByLoginIfExist(String login) throws UserNotExistException {
        Optional<User> result = userRepository.findByLoginDataLogin(login);
        if(result.isPresent()){
            return result.get();
        }else{
            throw new UserNotExistException(String.format("User with login = %s not exist ", login));
        }
    }

    @Override
    public Page<User> findAllForSelectedPageAndPageSize(Integer page, Integer pageSize){
        return userRepository.findAll(new PageRequest(page, pageSize));
    }

}
