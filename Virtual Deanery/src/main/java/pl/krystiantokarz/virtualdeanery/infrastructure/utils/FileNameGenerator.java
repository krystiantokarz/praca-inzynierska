package pl.krystiantokarz.virtualdeanery.infrastructure.utils;

import java.util.UUID;

public abstract class FileNameGenerator {

    public static String generateFileIdentity(){
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
