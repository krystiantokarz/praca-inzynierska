package pl.krystiantokarz.virtualdeanery.infrastructure.mail;

import org.apache.commons.lang3.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Locale;

@Component
public class EmailSender {

    private static final String from = "Virtual-Deanery";

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private MessageSource messageSource;

    @Async(value = "threadPoolTaskExecutor")
    public void sendEmail(EmailStrategy emailStrategy, Locale locale){
        Context context = emailStrategy.createMessageContext(locale);
        MimeMessage mail = javaMailSender.createMimeMessage();
        String messageContext = templateEngine.process(emailStrategy.getTemplateName(), context);
        SendObject object = emailStrategy.getObject();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, CharEncoding.UTF_8);
            helper.setFrom(from);
            helper.setTo(object.getTo());
            helper.setSubject(messageSource.getMessage(emailStrategy.getBundleTopic(), new Object[0], locale));
            helper.setText(messageContext, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        javaMailSender.send(mail);
    }
}
