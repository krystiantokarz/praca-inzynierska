package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.mailmessage;

import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailAttachmentFileDescriptionNotExist;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailMessageNotExistException;

public interface CustomMailMessageRepository {

    MailMessageEntity findMailMessageByIdForSelectedRecipientIfExist(Long messageId, User user) throws MailMessageNotExistException;

    MailAttachmentFileDescription findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(Long attachmentId, User user) throws MailAttachmentFileDescriptionNotExist;

}
