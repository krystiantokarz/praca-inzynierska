package pl.krystiantokarz.virtualdeanery.infrastructure.mail.changeuserstatus;

import org.thymeleaf.context.Context;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.EmailStrategy;

import java.util.Locale;

public class ChangeUserStatusSender extends EmailStrategy {

    private ChangeUserStatusSendObject emailSendObject;

    private static final String HTML_CONTENT = "/email/change-user-status";
    private static final String BUNDLE_TOPIC = "account.status.topic";

    public ChangeUserStatusSender(ChangeUserStatusSendObject object) {
        super(object);
        this.emailSendObject = object;
    }

    @Override
    protected Context createMessageContext(Locale locale) {
        Context ctx = new Context(locale);
        ctx.setVariable("firstName", emailSendObject.getFirstName());
        ctx.setVariable("lastName", emailSendObject.getLastName());
        ctx.setVariable("login", emailSendObject.getLogin());
        ctx.setVariable("enabled", emailSendObject.isDisable());
        return ctx;
    }

    @Override
    public String getTemplateName() {
        return HTML_CONTENT;
    }

    @Override
    public String getBundleTopic() {
        return BUNDLE_TOPIC;
    }
}
