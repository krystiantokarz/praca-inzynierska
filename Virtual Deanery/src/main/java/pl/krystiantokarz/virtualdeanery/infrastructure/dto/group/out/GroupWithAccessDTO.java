package pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out;

import java.io.Serializable;

public class GroupWithAccessDTO implements Serializable {

    private Long id;

    private String name;

    private String description;

    private boolean access;

    public GroupWithAccessDTO() {
    }

    public GroupWithAccessDTO(Long id, String name, String description, boolean access) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.access = access;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAccess() {
        return access;
    }

    public void setAccess(boolean access) {
        this.access = access;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupWithAccessDTO that = (GroupWithAccessDTO) o;

        if (access != that.access) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (access ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "GroupWithAccessDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", access=" + access +
                '}';
    }
}
