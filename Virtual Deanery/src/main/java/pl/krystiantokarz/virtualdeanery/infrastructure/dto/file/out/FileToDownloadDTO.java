package pl.krystiantokarz.virtualdeanery.infrastructure.dto.file.out;

import java.io.Serializable;

public class FileToDownloadDTO implements Serializable {

    private Long id;

    private String fileName;

    private int fileSize;

    private String fileExtensionType;

    private String fileType;

    public FileToDownloadDTO() {
    }

    public FileToDownloadDTO(Long id, String fileName, int fileSize, String fileExtensionType, String fileType) {
        this.id = id;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileExtensionType = fileExtensionType;
        this.fileType = fileType;
    }

    public Long getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public int getFileSize() {
        return fileSize;
    }

    public String getFileExtensionType() {
        return fileExtensionType;
    }

    public String getFileType() {
        return fileType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileToDownloadDTO that = (FileToDownloadDTO) o;

        if (fileSize != that.fileSize) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (fileName != null ? !fileName.equals(that.fileName) : that.fileName != null) return false;
        if (fileExtensionType != null ? !fileExtensionType.equals(that.fileExtensionType) : that.fileExtensionType != null)
            return false;
        return fileType != null ? fileType.equals(that.fileType) : that.fileType == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        result = 31 * result + fileSize;
        result = 31 * result + (fileExtensionType != null ? fileExtensionType.hashCode() : 0);
        result = 31 * result + (fileType != null ? fileType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FileToDownloadDTO{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                ", fileExtensionType='" + fileExtensionType + '\'' +
                ", fileType='" + fileType + '\'' +
                '}';
    }
}
