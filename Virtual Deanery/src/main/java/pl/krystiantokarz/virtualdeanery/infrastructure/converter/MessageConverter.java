package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out.AttachmentDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out.MessageContentDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out.MessageDescriptionDTO;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MessageConverter {

    public static List<MessageDescriptionDTO> convertMessagesToMessageDescriptionDTOs(List<MailMessageEntity> messages){
       return messages.stream().map(
                e -> new MessageDescriptionDTO(
                        e.getId(),
                        e.getSender().getFirstName(),
                        e.getSender().getLastName(),
                        e.getSubject(),
                        e.getDate(),
                        e.getMessageStatus().toString()
                )).collect(Collectors.toList());
    }

    public static MessageContentDTO convertMessageToMessageContentDTO(MailMessageEntity message){
        return new MessageContentDTO(
                message.getSender().getFirstName(),
                message.getSender().getLastName(),
                message.getSubject(),
                message.getMessage(),
                message.getDate(),
                message.getMessageStatus().toString(),
                convertMessageToAttachmentDTOs(message)
        );
    }

    private static List<AttachmentDTO> convertMessageToAttachmentDTOs(MailMessageEntity message){
        if(message.getAttachments() != null){
            return message.getAttachments().stream()
                    .map(e -> new AttachmentDTO(
                            e.getId(),
                            e.getOriginalFileName()
                    )).collect(Collectors.toList());
        }else{
            return Collections.emptyList();
        }
    }
}
