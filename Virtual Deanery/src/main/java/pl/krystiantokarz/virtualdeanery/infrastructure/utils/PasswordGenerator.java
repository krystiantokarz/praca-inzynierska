package pl.krystiantokarz.virtualdeanery.infrastructure.utils;

import org.apache.commons.text.RandomStringGenerator;

public abstract class PasswordGenerator {

    public static String generatePassword(int passwordLength){
        RandomStringGenerator generator = new RandomStringGenerator.Builder().
                withinRange('a', 'z').build();
        return generator.generate(passwordLength);
    }
}
