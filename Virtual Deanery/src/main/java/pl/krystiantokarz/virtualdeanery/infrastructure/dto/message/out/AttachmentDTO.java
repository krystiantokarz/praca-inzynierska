package pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out;

import java.io.Serializable;

public class AttachmentDTO implements Serializable{

    private Long attachmentId;

    private String attachmentName;

    public AttachmentDTO() {
    }

    public AttachmentDTO(Long attachmentId, String attachmentName) {
        this.attachmentId = attachmentId;
        this.attachmentName = attachmentName;
    }

    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttachmentDTO that = (AttachmentDTO) o;

        if (attachmentId != null ? !attachmentId.equals(that.attachmentId) : that.attachmentId != null) return false;
        return attachmentName != null ? attachmentName.equals(that.attachmentName) : that.attachmentName == null;
    }

    @Override
    public int hashCode() {
        int result = attachmentId != null ? attachmentId.hashCode() : 0;
        result = 31 * result + (attachmentName != null ? attachmentName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AttachmentDTO{" +
                "attachmentId=" + attachmentId +
                ", attachmentName='" + attachmentName + '\'' +
                '}';
    }
}
