package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.timetable.CustomTimetableEventRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface TimetableEventRepository extends JpaRepository<TimetableEvent,Long>, CustomTimetableEventRepository {

    Optional<TimetableEvent> findById(Long id);

    List<TimetableEvent> findByCreatingUser(User user);

    List<TimetableEvent> findByEventDate(LocalDate eventDate);

    List<TimetableEvent> findByEventDateAndCreatingUser(LocalDate eventDate, User user);

}
