package pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.in;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class CreateMessageDTO implements Serializable{

    @NotNull
    private Long idUserForSendMessage;

    private List<MultipartFile> files;

    @NotNull
    @Size(min = 3)
    private String subject;

    private String message;

    public CreateMessageDTO() {
    }

    public CreateMessageDTO(Long idUserForSendMessage, List<MultipartFile> files, String subject, String message) {
        this.idUserForSendMessage = idUserForSendMessage;
        this.files = files;
        this.subject = subject;
        this.message = message;
    }

    public Long getIdUserForSendMessage() {
        return idUserForSendMessage;
    }

    public void setIdUserForSendMessage(Long idUserForSendMessage) {
        this.idUserForSendMessage = idUserForSendMessage;
    }

    public List<MultipartFile> getFiles() {
        return files;
    }

    public void setFiles(List<MultipartFile> files) {
        this.files = files;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreateMessageDTO that = (CreateMessageDTO) o;

        if (idUserForSendMessage != null ? !idUserForSendMessage.equals(that.idUserForSendMessage) : that.idUserForSendMessage != null)
            return false;
        if (files != null ? !files.equals(that.files) : that.files != null) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        return message != null ? message.equals(that.message) : that.message == null;
    }

    @Override
    public int hashCode() {
        int result = idUserForSendMessage != null ? idUserForSendMessage.hashCode() : 0;
        result = 31 * result + (files != null ? files.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CreateMessageDTO{" +
                "idUserForSendMessage=" + idUserForSendMessage +
                ", files=" + files +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
