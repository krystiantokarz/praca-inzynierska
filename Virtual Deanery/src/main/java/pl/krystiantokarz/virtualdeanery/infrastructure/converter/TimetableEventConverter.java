package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventDescriptionDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventTimetableDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventCalendarDTO;

import java.util.List;
import java.util.stream.Collectors;

public class TimetableEventConverter {


    public static List<EventCalendarDTO> convertEventsToCalendarEventDTOs(List<TimetableEvent> timetables){
        return timetables.stream()
                .map(e -> new EventCalendarDTO(
                        e.getId(),
                        e.getTopic(),
                        e.getCreatingUser().getId(),
                        e.getEventDate()
                )).collect(Collectors.toList());
    }


    public static List<EventTimetableDTO> convertEventsToTimetableEventDTOs(List<TimetableEvent> timetables){
        return timetables.stream()
                .map(e -> new EventTimetableDTO(
                        e.getId(),
                        e.getCreatingUser().getId(),
                        e.getTopic(),
                        e.getCreatingUser().getFirstName(),
                        e.getCreatingUser().getLastName(),
                        e.getEventDate(),
                        e.getTimeFrom(),
                        e.getTimeTo()
                ))
                .collect(Collectors.toList());
    }

    public static EventDescriptionDTO convertEventToTimetableEventDescriptionDTO(TimetableEvent timetableEvent){
        return new EventDescriptionDTO(
                timetableEvent.getId(),
                timetableEvent.getCreatingUser().getId(),
                timetableEvent.getTopic(),
                timetableEvent.getMessage(),
                timetableEvent.getCreatingUser().getFirstName(),
                timetableEvent.getCreatingUser().getLastName(),
                timetableEvent.getEventDate(),
                timetableEvent.getTimeFrom(),
                timetableEvent.getTimeTo()
        );
    }
}
