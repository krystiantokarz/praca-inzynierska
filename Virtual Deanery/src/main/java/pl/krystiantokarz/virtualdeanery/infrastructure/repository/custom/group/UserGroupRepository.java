package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.group;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import java.util.List;
import java.util.Optional;

@Repository
interface UserGroupRepository extends JpaRepository<UserGroup,Long> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "delete from UserGroup ug where ug.group = :group")
    void deleteUserGroupForSelectedGroup(@Param("group") Group group);

    List<UserGroup> findByUserId(Long userId);

    List<UserGroup> findByGroupId(Long groupId);

    Optional<UserGroup> findByUserIdAndGroupId(Long userId, Long groupId);

    Optional<UserGroup> findByUserAndGroup(User user, Group group);

}
