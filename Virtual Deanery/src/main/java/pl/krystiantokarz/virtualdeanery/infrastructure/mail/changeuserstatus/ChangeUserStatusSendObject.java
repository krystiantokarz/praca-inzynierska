package pl.krystiantokarz.virtualdeanery.infrastructure.mail.changeuserstatus;

import org.apache.commons.lang3.StringUtils;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.SendObject;

public class ChangeUserStatusSendObject extends SendObject {

    private String firstName;
    private String lastName;
    private String login;
    private boolean disable;

    private ChangeUserStatusSendObject(String to, String firstName, String lastName, String login, boolean disable) {
        super(to);
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.disable = disable;
    }

    public static ChangeUserStatusSendObject from(String to, String firstName, String lastName,String login, boolean disable) {
        validate(to,firstName, lastName,login,disable);
        return new ChangeUserStatusSendObject(to,firstName, lastName,login,disable);
    }

    private static void validate(String to,String firstName, String lastName, String login,Boolean disable) {
        if(StringUtils.isBlank(to)){
            throw new IllegalArgumentException("To address cannot be null or empty");
        }
        if(StringUtils.isBlank(firstName)){
            throw new IllegalArgumentException("First name cannot be null or empty");
        }
        if(StringUtils.isBlank(lastName)){
            throw new IllegalArgumentException("Last name cannot be null or empty");
        }
        if(StringUtils.isBlank(login)){
            throw new IllegalArgumentException("Login cannot be null or empty");
        }
        if(disable == null){
            throw new IllegalArgumentException("Disable status cannot be null");
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public boolean isDisable() {
        return disable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChangeUserStatusSendObject that = (ChangeUserStatusSendObject) o;

        if (disable != that.disable) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        return login != null ? login.equals(that.login) : that.login == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (disable ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ChangeUserStatusSendObject{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", disable=" + disable +
                '}';
    }
}
