package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.mailmessage.CustomMailMessageRepository;

import java.util.Optional;

@Repository
public interface MailMessageRepository extends JpaRepository<MailMessageEntity,Long>, CustomMailMessageRepository {

    @Query(value = "SELECT msg FROM MailMessageEntity msg WHERE msg.recipient = :user")
    Page<MailMessageEntity> findMessagesForSelectedUser(@Param("user") User user, Pageable pageable);

    @Query(value = "SELECT msg From MailMessageEntity msg WHERE msg.id = :messageId AND msg.recipient = :recipient")
    Optional<MailMessageEntity> findMessageByIdForSelectedRecipient(@Param("messageId") Long messageId, @Param("recipient") User user);


}
