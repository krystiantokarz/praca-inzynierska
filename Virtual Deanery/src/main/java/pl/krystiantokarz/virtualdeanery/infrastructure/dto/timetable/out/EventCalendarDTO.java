package pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDate;

public class EventCalendarDTO implements Serializable{

    private Long eventId;

    private String topic;

    private Long userId;

    private LocalDate date;

    public EventCalendarDTO() {
    }

    public EventCalendarDTO(Long eventId, String topic, Long userId, LocalDate date) {
        this.eventId = eventId;
        this.topic = topic;
        this.userId = userId;
        this.date = date;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventCalendarDTO that = (EventCalendarDTO) o;

        if (eventId != null ? !eventId.equals(that.eventId) : that.eventId != null) return false;
        if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        return date != null ? date.equals(that.date) : that.date == null;
    }

    @Override
    public int hashCode() {
        int result = eventId != null ? eventId.hashCode() : 0;
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
