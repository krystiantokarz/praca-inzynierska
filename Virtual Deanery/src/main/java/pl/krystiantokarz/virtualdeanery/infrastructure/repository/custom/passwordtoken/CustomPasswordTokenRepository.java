package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.passwordtoken;

import pl.krystiantokarz.virtualdeanery.domain.passwordresettoken.PasswordResetToken;
import pl.krystiantokarz.virtualdeanery.service.resetpassword.exception.PasswordResetTokenNotExistException;

public interface CustomPasswordTokenRepository {

    PasswordResetToken findByTokenIfExist(String token) throws PasswordResetTokenNotExistException;
}
