package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.timetable;

import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.List;

public interface CustomTimetableEventRepository {

    TimetableEvent findTimetableEventByIdIfExist(Long id) throws TimetableEventNotExistException;

    List<TimetableEvent> findAllByEventDate(Integer day, Integer month, Integer year);

    List<TimetableEvent> findAllByEventDateForSelectedUser(Integer day, Integer month, Integer year, User user);
}
