package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.UserInGroupDTO;

import java.util.List;
import java.util.stream.Collectors;

public class UserGroupConverter {

    public static List<UserInGroupDTO> convertUserGroupToUserInGroupDTO(List<UserGroup> userGroups){
        return userGroups.stream()
                .map(e -> {
                    boolean isAdmin = false;
                    if(e.getMemberType() == MemberType.ADMIN_MEMBER) {
                        isAdmin = true;
                    }
                    return new UserInGroupDTO(
                            e.getUser().getId(),
                            e.getUser().getFirstName(),
                            e.getUser().getLastName(),
                            isAdmin
                    );
                })
                .collect(Collectors.toList());
    }
}
