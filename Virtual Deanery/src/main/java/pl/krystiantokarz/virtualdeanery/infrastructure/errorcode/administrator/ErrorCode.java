package pl.krystiantokarz.virtualdeanery.infrastructure.errorcode.administrator;

public enum ErrorCode {
    USER_LOGIN_EXIST(0,"User with selected login exist"),
    USER_EMAIL_EXIST(1,"User with selected email exist");

    private final int code;
    private final String description;

    ErrorCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", description='" + description + '\'' +
                '}';
    }
}
