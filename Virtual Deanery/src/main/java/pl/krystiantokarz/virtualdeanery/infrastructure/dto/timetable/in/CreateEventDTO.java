package pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public class CreateEventDTO implements Serializable{

    @NotEmpty
    @Size(min = 5)
    private String topic;

    @NotEmpty
    @Size(min = 5)
    private String message;

    @NotNull
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @NotNull
//    @JsonFormat(pattern = "HH:mm")
    private LocalTime eventTimeFrom;

    @NotNull
//    @JsonFormat(pattern = "HH:mm")
    private LocalTime eventTimeTo;

    public CreateEventDTO() {
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getEventTimeFrom() {
        return eventTimeFrom;
    }

    public void setEventTimeFrom(LocalTime eventTimeFrom) {
        this.eventTimeFrom = eventTimeFrom;
    }

    public LocalTime getEventTimeTo() {
        return eventTimeTo;
    }

    public void setEventTimeTo(LocalTime eventTimeTo) {
        this.eventTimeTo = eventTimeTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreateEventDTO that = (CreateEventDTO) o;

        if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (eventTimeFrom != null ? !eventTimeFrom.equals(that.eventTimeFrom) : that.eventTimeFrom != null) return false;
        return eventTimeTo != null ? eventTimeTo.equals(that.eventTimeTo) : that.eventTimeTo == null;
    }

    @Override
    public int hashCode() {
        int result = topic != null ? topic.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (eventTimeFrom != null ? eventTimeFrom.hashCode() : 0);
        result = 31 * result + (eventTimeTo != null ? eventTimeTo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CreateEventDTO{" +
                "topic='" + topic + '\'' +
                ", message='" + message + '\'' +
                ", date=" + date +
                ", eventTimeFrom=" + eventTimeFrom +
                ", eventTimeTo=" + eventTimeTo +
                '}';
    }
}
