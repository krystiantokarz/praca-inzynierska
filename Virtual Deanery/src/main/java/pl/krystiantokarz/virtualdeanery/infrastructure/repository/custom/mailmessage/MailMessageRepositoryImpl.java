package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.mailmessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailMessageNotExistException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailAttachmentFileDescriptionNotExist;

import java.util.Optional;

@Component
public class MailMessageRepositoryImpl implements  CustomMailMessageRepository{

    @Autowired
    private MailMessageRepository mailMessageRepository;

    @Autowired
    private MessageAttachmentFileDescriptionRepository messageAttachmentFileDescriptionRepository;

    @Override
    public MailMessageEntity findMailMessageByIdForSelectedRecipientIfExist(Long messageId, User user) throws MailMessageNotExistException {
        Optional<MailMessageEntity> result = mailMessageRepository.findMessageByIdForSelectedRecipient(messageId, user);
        return result.orElseThrow(
                () -> new MailMessageNotExistException(String.format("Mail message with id = %d for user id =  %d not exist ", messageId, user.getId()))
        );

    }

    @Override
    public MailAttachmentFileDescription findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(Long attachmentId, User user) throws MailAttachmentFileDescriptionNotExist{
        Optional<MailAttachmentFileDescription> result = messageAttachmentFileDescriptionRepository.findMessageAttachmentFileByIdForSelectedUser(attachmentId, user);
        return result.orElseThrow(
                () -> new MailAttachmentFileDescriptionNotExist(String.format("Mail message attachment file with id = %d for user id = %d not exist", attachmentId,user.getId()))
        );
    }
}
