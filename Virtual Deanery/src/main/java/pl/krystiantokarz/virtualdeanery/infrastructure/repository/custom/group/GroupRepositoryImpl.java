package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;

import java.util.List;
import java.util.Optional;

@Component
public class GroupRepositoryImpl implements CustomGroupRepository {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;


    @Override
    public Group findGroupByIdIfExist(Long id) throws GroupNotExistException {
        Optional<Group> result = groupRepository.findById(id);
        if(result.isPresent()) {
            return result.get();
        }else{
            throw new GroupNotExistException(String.format("Group with id = %d not exist ", id));
        }
    }

    @Override
    public List<UserGroup> findUserGroupsByGroupId(Long groupId) {
        return userGroupRepository.findByGroupId(groupId);
    }

    @Override
    public List<UserGroup> findUserGroupsByUserId(Long userId) {
        return userGroupRepository.findByUserId(userId);
    }

    @Override
    public UserGroup findUserGroupByUserAndGroup(User user, Group group) throws UserGroupNotExistException {
        Optional<UserGroup> result = userGroupRepository.findByUserAndGroup(user, group);
        if(result.isPresent()) {
            return result.get();
        }else{
            throw new UserGroupNotExistException(String.format("User Group for user with id = %d and group with id = %d", user.getId(), user.getId()));
        }
    }

    @Override
    public UserGroup findUserGroupForAdminMember(User groupAdmin, Group group) throws UserGroupNotExistException, PrivilegeException {
        UserGroup result = findUserGroupByUserAndGroup(groupAdmin, group);
        if(result.getMemberType() == MemberType.ADMIN_MEMBER){
            return result;
        }else{
            throw new PrivilegeException(String.format("Selected user with id = %d is not a admin member in selected group with id = %d", groupAdmin.getId(),group.getId()));
        }
    }

    @Override
    public UserGroup saveUserGroup(UserGroup userGroup) {
        UserGroup result = userGroupRepository.save(userGroup);
//        userGroupRepository.flush();
        return result;
    }

    @Override
    public void deleteUserGroup(UserGroup userGroup) {
        userGroupRepository.delete(userGroup);
    }

    @Override
    public void deleteAllUserGroupsForSelectedGroup(Group group) {
        userGroupRepository.deleteUserGroupForSelectedGroup(group);
    }

    @Override
    public boolean checkLoggedUserIsAdminInSelectedGroup(Long userId,Long groupId) {
        Optional<UserGroup> userGroupOptional = userGroupRepository.findByUserIdAndGroupId(userId, groupId);
        if(userGroupOptional.isPresent()){
            UserGroup userGroup = userGroupOptional.get();
            return userGroup.getMemberType() == MemberType.ADMIN_MEMBER;
        }else{
            return false;
        }
    }
}
