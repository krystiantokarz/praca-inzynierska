package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import pl.krystiantokarz.virtualdeanery.domain.group.chat.ChatMessage;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.chat.out.OutputChatMessageDTO;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ChatMessageConverter {

    public static  OutputChatMessageDTO convertChatMessageToOutputDTO(ChatMessage chatMessage){
        return new OutputChatMessageDTO(
                chatMessage.getMessage(),
                chatMessage.getSender().getFirstName(),
                chatMessage.getSender().getLastName(),
                chatMessage.getDate(),
                chatMessage.getSender().getId()
        );
    }

    public static  List<OutputChatMessageDTO> convertChatMessageListToOutputDTOList(List<ChatMessage> chatMessages){
        return chatMessages.stream()
                .map(e -> new OutputChatMessageDTO(
                            e.getMessage(),
                            e.getSender().getFirstName(),
                            e.getSender().getLastName(),
                            e.getDate(),
                            e.getSender().getId()
                ))
                .sorted(Comparator.comparing(OutputChatMessageDTO::getDate))
                .collect(Collectors.toList());
    }
}
