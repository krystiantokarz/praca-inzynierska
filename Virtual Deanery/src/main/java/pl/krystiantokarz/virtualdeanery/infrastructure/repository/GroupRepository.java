package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.group.CustomGroupRepository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<Group,Long>, CustomGroupRepository{

    Optional<Group> findById(Long id);

    List<Group> findByNameContainingIgnoreCase(String name);

    @Query(value = "SELECT g FROM Group g, UserGroup ug WHERE g = ug.group AND ug.user.id = :userId")
    Page<Group> findGroupsForSelectedUser(@Param("userId") Long userId, Pageable pageable);

    @Query(value = "SELECT g FROM Group g, UserGroup ug WHERE g = ug.group AND ug.user.id = :userId")
    List<Group> findGroupsForSelectedUser(@Param("userId") Long userId);

    @Query("select count(g) > 0 from Group g where g.name = :name AND g.id <> :groupId")
    boolean checkGroupWithSelectedNameExistWithoutSelectedGroup(@Param("name") String name, @Param("groupId") Long actuallyGroupId);

    @Query("select count(g) > 0 from Group g where g.name = :name")
    boolean checkGroupWithSelectedNameExist(@Param("name") String name);
}
