package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import pl.krystiantokarz.virtualdeanery.domain.user.role.Role;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;

import java.util.List;
import java.util.stream.Collectors;


public class RolesConverter {

    public static List<String> convertUserRolesToStringList(List<Role> roles){
        return roles.stream()
                .map(Role::getRole)
                .map(Enum::toString)
                .collect(Collectors.toList());

    }

    public static List<Role> convertRolesStringToObjects(List<String> roles){
        return roles.stream()
                .map(e -> Role.from(RoleEnum.valueOf(e)))
                .collect(Collectors.toList());
    }
}
