package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.timetable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.TimetableEventRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class TimetableEventRepositoryImpl implements CustomTimetableEventRepository {

    @Autowired
    private TimetableEventRepository timetableEventRepository;


    @Override
    public TimetableEvent findTimetableEventByIdIfExist(Long id) throws TimetableEventNotExistException {
        Optional<TimetableEvent> result = timetableEventRepository.findById(id);
        return result.orElseThrow(
                () -> new TimetableEventNotExistException(String.format("Timetable event with id = %d not exist",id))
        );
    }

    @Override
    public List<TimetableEvent> findAllByEventDate(Integer day, Integer month, Integer year) {
        LocalDate localDate = LocalDate.of(year, month, day);
        return timetableEventRepository.findByEventDate(localDate);
    }

    @Override
    public List<TimetableEvent> findAllByEventDateForSelectedUser(Integer day, Integer month, Integer year, User user) {
        LocalDate localDate = LocalDate.of(year, month, day);
        return timetableEventRepository.findByEventDateAndCreatingUser(localDate,user);
    }
}
