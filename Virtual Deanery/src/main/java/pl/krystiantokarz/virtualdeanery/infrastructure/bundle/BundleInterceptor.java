package pl.krystiantokarz.virtualdeanery.infrastructure.bundle;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
public class BundleInterceptor {


    private String fileAcceptTopic = "file.accept.topic";

    private String fileRejectTopic = "file.reject.topic";

    private String fileAcceptDesc = "file.accept.message";

    private String fileRejectDesc = "file.reject.message";

    public String getFileAcceptTopic() {
        return fileAcceptTopic;
    }

    public String getFileRejectTopic() {
        return fileRejectTopic;
    }

    public String getFileAcceptDesc() {
        return fileAcceptDesc;
    }

    public String getFileRejectDesc() {
        return fileRejectDesc;
    }
}
