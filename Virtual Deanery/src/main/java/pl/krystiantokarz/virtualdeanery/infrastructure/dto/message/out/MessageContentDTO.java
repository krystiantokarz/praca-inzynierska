package pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class MessageContentDTO implements Serializable{

    private String fromFirstName;

    private String fromLastName;

    private String subject;

    private String message;

    private LocalDateTime date;

    private String status;

    private List<AttachmentDTO> attachments;

    public MessageContentDTO() {
    }

    public MessageContentDTO(String fromFirstName, String fromLastName, String subject, String message, LocalDateTime date, String status, List<AttachmentDTO> attachments) {
        this.fromFirstName = fromFirstName;
        this.fromLastName = fromLastName;
        this.message = message;
        this.subject = subject;
        this.date = date;
        this.status = status;
        this.attachments = attachments;
    }

    public String getFromFirstName() {
        return fromFirstName;
    }

    public void setFromFirstName(String fromFirstName) {
        this.fromFirstName = fromFirstName;
    }

    public String getFromLastName() {
        return fromLastName;
    }

    public void setFromLastName(String fromLastName) {
        this.fromLastName = fromLastName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<AttachmentDTO> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDTO> attachments) {
        this.attachments = attachments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageContentDTO that = (MessageContentDTO) o;

        if (fromFirstName != null ? !fromFirstName.equals(that.fromFirstName) : that.fromFirstName != null)
            return false;
        if (fromLastName != null ? !fromLastName.equals(that.fromLastName) : that.fromLastName != null) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        return attachments != null ? attachments.equals(that.attachments) : that.attachments == null;
    }

    @Override
    public int hashCode() {
        int result = fromFirstName != null ? fromFirstName.hashCode() : 0;
        result = 31 * result + (fromLastName != null ? fromLastName.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (attachments != null ? attachments.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MessageContentDTO{" +
                "fromFirstName='" + fromFirstName + '\'' +
                ", fromLastName='" + fromLastName + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", date=" + date +
                ", status='" + status + '\'' +
                ", attachments=" + attachments +
                '}';
    }
}
