package pl.krystiantokarz.virtualdeanery.infrastructure.mail;

import org.thymeleaf.context.Context;

import java.util.Locale;

public abstract class EmailStrategy<T extends SendObject> {

    protected T object;

    public EmailStrategy(T object) {
        this.object = object;
    }

    protected abstract Context createMessageContext(Locale locale);

    public T getObject() {
        return object;
    }

    public abstract String getTemplateName();

    public abstract String getBundleTopic();


}
