package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.mailmessage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import java.util.Optional;

interface MessageAttachmentFileDescriptionRepository extends JpaRepository<MailAttachmentFileDescription,Long> {


    @Query(value = "SELECT msgAtt FROM MailAttachmentFileDescription msgAtt, MailMessageEntity msg WHERE msgAtt.id = :attachmentId " +
            " AND msg.recipient = :recipient AND msgAtt member msg.attachments")
    Optional<MailAttachmentFileDescription> findMessageAttachmentFileByIdForSelectedUser(@Param("attachmentId") Long attachmentId, @Param("recipient") User user);
}
