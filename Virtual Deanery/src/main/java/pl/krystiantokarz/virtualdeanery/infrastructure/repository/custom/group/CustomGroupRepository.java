package pl.krystiantokarz.virtualdeanery.infrastructure.repository.custom.group;

import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;

import java.util.List;

public interface CustomGroupRepository {

    Group findGroupByIdIfExist(Long groupId) throws GroupNotExistException;

    List<UserGroup> findUserGroupsByGroupId(Long groupId);

    List<UserGroup> findUserGroupsByUserId(Long userId);

    UserGroup findUserGroupByUserAndGroup(User user, Group group) throws UserGroupNotExistException;

    UserGroup findUserGroupForAdminMember(User groupAdmin, Group group) throws UserGroupNotExistException, PrivilegeException;

    UserGroup saveUserGroup(UserGroup userGroup);

    void deleteUserGroup(UserGroup userGroup);

    void deleteAllUserGroupsForSelectedGroup(Group group);

    boolean checkLoggedUserIsAdminInSelectedGroup(Long userId,Long groupId);




}
