package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

public class FileSizeConverter {

    private static final int KILOBYTES_SIZE = 1024;
    private static final int MEGABYTE_SIZE = 1024;

    public static int convertBytesToMegabytes(int bytes){
        return (int) Math.ceil((double)bytes / (KILOBYTES_SIZE*MEGABYTE_SIZE));
    }
}
