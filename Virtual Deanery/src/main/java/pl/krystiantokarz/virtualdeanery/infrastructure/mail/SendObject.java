package pl.krystiantokarz.virtualdeanery.infrastructure.mail;

public abstract class SendObject {

    private String to;

    public SendObject(String to) {
        this.to = to;
    }

    public String getTo() {
        return to;
    }

}
