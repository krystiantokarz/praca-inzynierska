package pl.krystiantokarz.virtualdeanery.infrastructure.mail.createaccount;

import org.apache.commons.lang3.StringUtils;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.SendObject;

public class NewAccountSendObject extends SendObject{

    private String firstName;
    private String lastName;
    private String password;
    private String login;

    private NewAccountSendObject(String to, String firstName, String lastName, String password, String login) {
        super(to);
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.login = login;
    }


    public static NewAccountSendObject from(String to, String firstName, String lastName, String password, String login) {
        validate(to,firstName, lastName, password, login);
        return new NewAccountSendObject(to,firstName, lastName, password, login);
    }

    private static void validate(String to, String firstName, String lastName, String password, String login) {
        if(StringUtils.isBlank(to)){
            throw new IllegalArgumentException("To address cannot be null or empty");
        }
        if(StringUtils.isBlank(firstName)){
            throw new IllegalArgumentException("First name cannot be null or empty");
        }
        if(StringUtils.isBlank(lastName)){
            throw new IllegalArgumentException("Last name cannot be null or empty");
        }
        if(StringUtils.isBlank(password)){
            throw new IllegalArgumentException("Password cannot be null or empty");
        }
        if(StringUtils.isBlank(login)){
            throw new IllegalArgumentException("Login cannot be null or empty");
        }
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewAccountSendObject that = (NewAccountSendObject) o;

        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        return login != null ? login.equals(that.login) : that.login == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewAccountSendObject{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", login='" + login + '\'' +
                '}';
    }
}
