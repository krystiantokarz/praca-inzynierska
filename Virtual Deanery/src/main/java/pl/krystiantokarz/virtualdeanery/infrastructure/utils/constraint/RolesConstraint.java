package pl.krystiantokarz.virtualdeanery.infrastructure.utils.constraint;

import pl.krystiantokarz.virtualdeanery.infrastructure.utils.validator.RolesValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = RolesValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface RolesConstraint {
    String message() default "pl.krystiantokarz.virtualdeanery.infrastructure.utils.constraint.RolesConstraint";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
