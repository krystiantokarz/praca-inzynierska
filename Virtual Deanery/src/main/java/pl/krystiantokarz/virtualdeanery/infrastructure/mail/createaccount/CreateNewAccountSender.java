package pl.krystiantokarz.virtualdeanery.infrastructure.mail.createaccount;

import org.thymeleaf.context.Context;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.EmailStrategy;
import java.util.Locale;


public class CreateNewAccountSender extends EmailStrategy{


    private NewAccountSendObject emailSendObject;

    private static final String HTML_CONTENT = "/email/create-account";
    private static final String BUNDLE_TOPIC = "account.create.topic ";

    public CreateNewAccountSender(NewAccountSendObject object) {
        super(object);
        this.emailSendObject = object;
    }

    @Override
    public Context createMessageContext(Locale locale) {
        Context ctx = new Context(locale);
        ctx.setVariable("firstName", emailSendObject.getFirstName());
        ctx.setVariable("lastName", emailSendObject.getLastName());
        ctx.setVariable("password", emailSendObject.getPassword());
        ctx.setVariable("login", emailSendObject.getLogin());
        return ctx;

    }

    @Override
    public String getTemplateName() {
        return HTML_CONTENT;
    }

    @Override
    public String getBundleTopic() {
        return BUNDLE_TOPIC;
    }
}
