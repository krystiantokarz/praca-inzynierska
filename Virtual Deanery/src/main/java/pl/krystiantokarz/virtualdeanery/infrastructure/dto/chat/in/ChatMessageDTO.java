package pl.krystiantokarz.virtualdeanery.infrastructure.dto.chat.in;

import java.io.Serializable;

public class ChatMessageDTO implements Serializable {

    private Long groupId;

    private String message;

    public ChatMessageDTO() {
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChatMessageDTO that = (ChatMessageDTO) o;

        if (groupId != null ? !groupId.equals(that.groupId) : that.groupId != null) return false;
        return message != null ? message.equals(that.message) : that.message == null;
    }

    @Override
    public int hashCode() {
        int result = groupId != null ? groupId.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ChatMessageDTO{" +
                "groupId=" + groupId +
                ", message='" + message + '\'' +
                '}';
    }
}
