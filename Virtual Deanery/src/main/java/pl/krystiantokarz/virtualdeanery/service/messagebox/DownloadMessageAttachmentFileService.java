package pl.krystiantokarz.virtualdeanery.service.messagebox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.DownloadFileException;
import pl.krystiantokarz.virtualdeanery.service.file.DownloadFileService;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.DownloadMessageAttachmentFileException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailAttachmentFileDescriptionNotExist;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.io.OutputStream;

@Service
public class DownloadMessageAttachmentFileService {

    private static final Logger logger = LoggerFactory.getLogger(DownloadMessageAttachmentFileService.class);

    private DownloadFileService downloadFileService;

    private MailMessageRepository mailMessageRepository;

    private UserRepository userRepository;

    @Value("${file.repository.fileattachment}")
    private String repositoryPath;

    @Autowired
    public DownloadMessageAttachmentFileService(DownloadFileService downloadFileService, MailMessageRepository mailMessageRepository, UserRepository userRepository) {
        this.downloadFileService = downloadFileService;
        this.mailMessageRepository = mailMessageRepository;
        this.userRepository = userRepository;
    }


    public void downloadFile(Long attachmentFileId, Long userId, OutputStream outputStream) throws DownloadMessageAttachmentFileException, UserNotExistException, MailAttachmentFileDescriptionNotExist {
        User user = userRepository.findUserByIdIfExist(userId);
        MailAttachmentFileDescription messageAttachmentFileDescription = mailMessageRepository.findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(attachmentFileId, user);
        String savedFileName = messageAttachmentFileDescription.getSavedFileName();
        try {
            downloadFileService.downloadFile(repositoryPath, savedFileName, outputStream);
            logger.info("Download mail attachment file with saved file = {}", savedFileName);
        } catch (DownloadFileException ex) {
            logger.warn("Cannot download message attachment file {} ",messageAttachmentFileDescription.getSavedFileName(),ex);
            throw new DownloadMessageAttachmentFileException(String.format("Download message attachment file = %s is impossible",messageAttachmentFileDescription.getSavedFileName()));
        }

    }

}
