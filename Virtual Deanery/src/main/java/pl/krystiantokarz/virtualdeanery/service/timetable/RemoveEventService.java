package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.TimetableEventRepository;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;

@Service
public class RemoveEventService {

    private static final Logger logger = LoggerFactory.getLogger(EditEventService.class);

    private TimetableEventRepository timetableEventRepository;
    private PrivilegeService privilegeService;

    @Autowired
    public RemoveEventService(TimetableEventRepository timetableEventRepository,PrivilegeService privilegeService) {
        this.timetableEventRepository = timetableEventRepository;
        this.privilegeService = privilegeService;
    }

    public void removeSelectedEventByUser(Long eventId, Long userId) throws TimetableEventNotExistException, PrivilegeException {
        TimetableEvent timetableEvent = timetableEventRepository.findTimetableEventByIdIfExist(eventId);
        privilegeService.checkUserHasAccessToEditEvent(timetableEvent, userId);
        timetableEventRepository.delete(timetableEvent);
    }


}
