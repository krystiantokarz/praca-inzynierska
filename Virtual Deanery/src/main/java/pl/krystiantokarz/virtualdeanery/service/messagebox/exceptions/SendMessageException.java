package pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions;

public class SendMessageException extends Exception {

    public SendMessageException() {
    }

    public SendMessageException(String message) {
        super(message);
    }
}
