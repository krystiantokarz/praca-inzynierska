package pl.krystiantokarz.virtualdeanery.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FindUserService {

    private UserRepository userRepository;

    @Autowired
    public FindUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findUserById(Long id) throws UserNotExistException {
        return userRepository.findUserByIdIfExist(id);
    }

    public Page<User> getUsersByPageAndPageSize(Integer page, Integer size){
        return userRepository.findAllForSelectedPageAndPageSize(page, size);
    }

    public List<User> searchUsers(String searchedString) {
        String[] splitSearchString = searchedString.trim().split("\\s+");
        if (splitSearchString.length == 1) {
            List<User> firstResult = userRepository.findByFirstNameContainingIgnoreCase(splitSearchString[0]);
            List<User> secondResult = userRepository.findByLastNameContainingIgnoreCase(splitSearchString[0]);
            return returnOneUserListWithDistinctOperation(firstResult, secondResult);
        } else if (splitSearchString.length == 2) {
            List<User> firstResult = userRepository.findByFirstNameIgnoreCaseAndLastNameIgnoreCase(splitSearchString[0], splitSearchString[1]);
            List<User> secondResult = userRepository.findByFirstNameIgnoreCaseAndLastNameIgnoreCase(splitSearchString[1], splitSearchString[0]);
            return returnOneUserListWithDistinctOperation(firstResult, secondResult);
        } else {
            return Collections.emptyList();
        }
    }


    private List<User> returnOneUserListWithDistinctOperation(List<User> firstList, List<User> secondList) {
        List<User> resultList = new ArrayList<>();
        resultList.addAll(firstList);
        resultList.addAll(secondList);
        return resultList.stream().distinct().collect(Collectors.toList());

    }
}
