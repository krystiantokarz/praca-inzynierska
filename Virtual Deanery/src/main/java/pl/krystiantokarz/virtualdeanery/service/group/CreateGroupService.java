package pl.krystiantokarz.virtualdeanery.service.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroupFactory;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.CreateGroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupWithSelectedNameExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@Service
public class CreateGroupService {

    private static final Logger logger = LoggerFactory.getLogger(CreateGroupService.class);

    private GroupRepository groupRepository;

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    @Autowired
    public CreateGroupService(GroupRepository groupRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void createGroup(CreateGroupDTO createGroupDTO,Long userId) throws UserNotExistException, GroupWithSelectedNameExistException {

        checkGroupWithSelectedNameNotExist(createGroupDTO.getName());
        User user = userRepository.findUserByIdIfExist(userId);
        Group newGroup = createGroupWithEncodePassword(createGroupDTO);

        groupRepository.save(newGroup);
        createAndPersistUserGroups(user,newGroup);
        logger.info("Group with id = {} was created",newGroup.getId());
    }

    private void checkGroupWithSelectedNameNotExist(String groupName) throws GroupWithSelectedNameExistException {
        boolean result = groupRepository.checkGroupWithSelectedNameExist(groupName);
        if(result){
            logger.debug("Group with name = {} cannot be created. Name is not unique",groupName);
            throw new GroupWithSelectedNameExistException(String.format("Cannot create new group. Selected group name = %s is not unique",groupName));
        }
    }

    private Group createGroupWithEncodePassword(CreateGroupDTO createGroupDTO){
        String encodePassword = encodePassword(createGroupDTO.getPassword());
        return Group.from(
                createGroupDTO.getName(),
                encodePassword,
                createGroupDTO.getDescription()
        );
    }

    private String encodePassword(String password){
        return  passwordEncoder.encode(password);
    }

    private void createAndPersistUserGroups(User user,Group group){
        UserGroup userGroup = UserGroupFactory.createUserGroupForAdministratorMember(user, group);
        groupRepository.saveUserGroup(userGroup);

    }
}
