package pl.krystiantokarz.virtualdeanery.service.group.exceptions;

public class PrivilegeException extends Exception {

    public PrivilegeException() {
    }

    public PrivilegeException(String message) {
        super(message);
    }
}
