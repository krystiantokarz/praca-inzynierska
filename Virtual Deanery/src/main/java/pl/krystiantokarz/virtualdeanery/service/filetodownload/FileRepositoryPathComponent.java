package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;

@Component
public class FileRepositoryPathComponent {

    private static final Logger logger = LoggerFactory.getLogger(FileRepositoryPathComponent.class);

    @Value("${file.repository.filetodownload.archive}")
    private String archiveRepositoryPath;

    @Value("${file.repository.filetodownload.form}")
    private String formRepositoryPath;

    @Value("${file.repository.filetodownload.tmp}")
    private String tmpRepositoryPath;

    public String getRepositoryPath(FileStatus fileStatus, FileType fileType){
        if(fileStatus == FileStatus.WAITING_FOR_ACCEPTED){
            return tmpRepositoryPath;
        }
        if(fileStatus == FileStatus.READY_TO_DOWNLOAD){
            return gerRepositoryPathForFileToDownload(fileType);
        }

        logger.warn("File status = {} is not found",fileStatus);
        throw new IllegalArgumentException("FileStatus is not found");
    }


    private String gerRepositoryPathForFileToDownload(FileType fileType){
        if (fileType == FileType.ARCHIVE) {
            return archiveRepositoryPath;
        }
        if( fileType == FileType.FORM) {
            return formRepositoryPath;
        }

        logger.warn("File type = {} is not found",fileType);
        throw new IllegalArgumentException("FileType is not found");
    }
}
