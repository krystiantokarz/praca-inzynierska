package pl.krystiantokarz.virtualdeanery.service.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.krystiantokarz.virtualdeanery.infrastructure.utils.FileNameGenerator;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.SaveFileException;

import java.io.*;

@Service
public class SaveFileService {

    private static final Logger logger = LoggerFactory.getLogger(SaveFileService.class);

    public String saveFile(String repositoryPath,String filePrefix, MultipartFile file) throws SaveFileException {

        try {
            String savedFileName = filePrefix + FileNameGenerator.generateFileIdentity();
            File fsFile = new File(repositoryPath,savedFileName);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fsFile));
            stream.write(file.getBytes());
            stream.close();
            return savedFileName;
        } catch (IOException ex) {
            logger.warn("Cannot save file {} to repository = {}",file.getOriginalFilename(),repositoryPath,ex);
            throw new SaveFileException(String.format("Save file with name = %s is impossible",file.getOriginalFilename()));

        }

    }

}
