package pl.krystiantokarz.virtualdeanery.service.group.exceptions;

public class UserGroupNotExistException extends Exception {

    public UserGroupNotExistException() {
    }

    public UserGroupNotExistException(String message) {
        super(message);
    }
}
