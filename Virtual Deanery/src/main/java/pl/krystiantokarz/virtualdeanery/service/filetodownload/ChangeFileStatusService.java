package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.infrastructure.bundle.BundleInterceptor;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.service.file.ChangeFileRepositoryService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.RemoveFileException;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.ChangeFileToDownloadTypeException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.SendMessageService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.Locale;

@Service
public class ChangeFileStatusService {

    private static final Logger logger = LoggerFactory.getLogger(ChangeFileStatusService.class);

    private DeleteFileToDownloadService deleteFileToDownloadService;

    private ChangeFileRepositoryService changeFileRepositoryService;

    private FileRepositoryPathComponent fileRepositoryPathComponent;

    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    private SendMessageService sendMessageService;

    private BundleInterceptor bundleInterceptor;

    private MessageSource messageSource;

    @Autowired
    public ChangeFileStatusService(DeleteFileToDownloadService deleteFileToDownloadService, ChangeFileRepositoryService changeFileRepositoryService, FileRepositoryPathComponent fileRepositoryPathComponent, FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository, SendMessageService sendMessageService, BundleInterceptor bundleInterceptor, MessageSource messageSource) {
        this.deleteFileToDownloadService = deleteFileToDownloadService;
        this.changeFileRepositoryService = changeFileRepositoryService;
        this.fileRepositoryPathComponent = fileRepositoryPathComponent;
        this.fileToDownloadDescriptionRepository = fileToDownloadDescriptionRepository;
        this.sendMessageService = sendMessageService;
        this.bundleInterceptor = bundleInterceptor;
        this.messageSource = messageSource;
    }

    public void acceptFile(Long fileId,Locale locale) throws FileNotExistException, ChangeFileToDownloadTypeException, UserNotExistException, FileWithSelectedNameExistException {
        FileWaitingForAccepted fileWaitingForAcceptedDescription = fileToDownloadDescriptionRepository.findFileWaitingForAcceptedByFileId(fileId);
        checkOriginalFileNameIsNotExistInRepositoryForSelectedFileType(fileWaitingForAcceptedDescription);

        String oldRepositoryPath = fileRepositoryPathComponent.getRepositoryPath(fileWaitingForAcceptedDescription.getFileStatus(), fileWaitingForAcceptedDescription.getFileType());
        FileReadyToDownload fileReadyToDownload = fileWaitingForAcceptedDescription.acceptFile();
        fileToDownloadDescriptionRepository.save(fileReadyToDownload);
        String newRepositoryPath = fileRepositoryPathComponent.getRepositoryPath(fileReadyToDownload.getFileStatus(), fileReadyToDownload.getFileType());
        changeFileRepositoryService.changeFileRepository(oldRepositoryPath,newRepositoryPath,fileReadyToDownload.getSavedFileName());

        sendInformationMessage(bundleInterceptor.getFileAcceptTopic(), bundleInterceptor.getFileAcceptDesc(), fileWaitingForAcceptedDescription, locale);
        logger.info("File to download with id = {} is accepted", fileId);

    }

    private void checkOriginalFileNameIsNotExistInRepositoryForSelectedFileType(FileWaitingForAccepted fileWaitingForAcceptedDescription) throws FileWithSelectedNameExistException {
        String fileName = fileWaitingForAcceptedDescription.getOriginalFileName();
        FileType fileType = fileWaitingForAcceptedDescription.getFileType();
        boolean result = fileToDownloadDescriptionRepository.checkFileToDownloadWithSelectedOriginalNameAndTypeAndStatusExist(fileName,fileType, FileStatus.READY_TO_DOWNLOAD);
        if(result){
            logger.debug("Change file status to ready to download for file is not impossible. File name = {} is not unique in file type = {}", fileName, fileType);
            throw new FileWithSelectedNameExistException(String.format("File to download with name = %s and type = %s is already exist", fileName,fileType));
        }
    }

    public void rejectFile(Long fileId,Locale locale) throws FileNotExistException, RemoveFileException, UserNotExistException {
        FileWaitingForAccepted fileWaitingForAcceptedDescription = fileToDownloadDescriptionRepository.findFileWaitingForAcceptedByFileId(fileId);
        deleteFileToDownloadService.deleteFile(fileId);
        sendInformationMessage(bundleInterceptor.getFileRejectTopic(), bundleInterceptor.getFileRejectDesc(), fileWaitingForAcceptedDescription, locale);
        logger.info("File to download with id = {} is rejected", fileId);
    }


    private void sendInformationMessage(String subject, String message, FileToDownloadDescription file, Locale locale) throws UserNotExistException {
        sendMessageService.sendMessageFromSystemUser(
                messageSource.getMessage(subject, new Object[0], locale),
                messageSource.getMessage(message, new Object[]{file.getOriginalFileName()}, locale),
                file.getAddingUser().getId());
    }

}
