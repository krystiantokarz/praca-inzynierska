package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.EditEventDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.TimetableEventRepository;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.*;

import java.time.LocalTime;

@Service
public class EditEventService {

    private static final Logger logger = LoggerFactory.getLogger(EditEventService.class);

    private static final int EVENT_START_TIME = 8;
    private static final int EVENT_END_TIME = 21;

    private TimetableEventRepository timetableEventRepository;
    private PrivilegeService privilegeService;

    @Autowired
    public EditEventService(TimetableEventRepository timetableEventRepository,PrivilegeService privilegeService) {
        this.timetableEventRepository = timetableEventRepository;
        this.privilegeService = privilegeService;
    }

    public void editSelectedEventByUser(EditEventDTO editEventDTO, Long userId) throws TimetableEventNotExistException, EventTimeLimitException, ValidEventTimeException, PrivilegeException {
        checkCorrectDates(editEventDTO.getTimeFrom(), editEventDTO.getTimeTo());
        TimetableEvent timetableEvent = timetableEventRepository.findTimetableEventByIdIfExist(editEventDTO.getEventId());
        privilegeService.checkUserHasAccessToEditEvent(timetableEvent, userId);
        timetableEvent.changeEventData(
                editEventDTO.getTimeFrom(),
                editEventDTO.getTimeTo(),
                editEventDTO.getTopic(),
                editEventDTO.getMessage()
        );

        timetableEventRepository.save(timetableEvent);
        logger.info("User with id = {} change timetable event data with id = {} ", userId, timetableEvent.getId());
    }


    private void checkCorrectDates(LocalTime startDate, LocalTime endDate) throws EventTimeLimitException, ValidEventTimeException {
        if(startDate.isAfter(endDate) || startDate.equals(endDate)){
            logger.debug("Cannot create timetable event - selected start time is after end time");
            throw new ValidEventTimeException("Cannot create timetable event - selected start time is after end time");
        }
        if (startDate.isBefore(LocalTime.of(EVENT_START_TIME, 0)) || endDate.isAfter(LocalTime.of(EVENT_END_TIME,0))) {
            logger.debug("Cannot create timetable event - selected time exceeds the range");
            throw new EventTimeLimitException("Cannot create timetable event - selected time exceeds the range");
        }
    }
}
