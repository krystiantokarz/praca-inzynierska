package pl.krystiantokarz.virtualdeanery.service.group.exceptions;

public class GroupNotExistException extends Exception{

    public GroupNotExistException() {
        super();
    }

    public GroupNotExistException(String message) {
        super(message);
    }
}
