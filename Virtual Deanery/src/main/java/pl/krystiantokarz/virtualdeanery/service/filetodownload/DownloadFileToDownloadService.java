package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescription;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.DownloadFileToDownloadException;
import pl.krystiantokarz.virtualdeanery.service.file.DownloadFileService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.DownloadFileException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;

import java.io.OutputStream;

@Service
public class DownloadFileToDownloadService {

    private static final Logger logger = LoggerFactory.getLogger(DownloadFileToDownloadService.class);

    private DownloadFileService downloadFileService;

    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Autowired
    public DownloadFileToDownloadService(DownloadFileService downloadFileService, FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository, FileRepositoryPathComponent fileRepositoryPathComponent) {
        this.downloadFileService = downloadFileService;
        this.fileToDownloadDescriptionRepository = fileToDownloadDescriptionRepository;
        this.fileRepositoryPathComponent = fileRepositoryPathComponent;
    }


    public void downloadFile(Long fileId,OutputStream outputStream) throws DownloadFileToDownloadException, FileNotExistException {
        FileToDownloadDescription fileToDownloadDescription = null;
        try {
            fileToDownloadDescription = fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId);
            String savedFileName = fileToDownloadDescription.getSavedFileName();
            String repositoryPath = fileRepositoryPathComponent.getRepositoryPath(fileToDownloadDescription.getFileStatus()
                    ,fileToDownloadDescription.getFileType());
            downloadFileService.downloadFile(repositoryPath,savedFileName,outputStream);
            logger.info("Downloaded file to download with id = {}",fileId);
        } catch (DownloadFileException ex) {
            logger.warn("Cannot download file to download with saved file name = {} and id = {}",fileToDownloadDescription.getSavedFileName(),fileToDownloadDescription.getId(),ex);
            throw new DownloadFileToDownloadException(String.format("Download file to download with saved file name = %s and id = %d is impossible",fileToDownloadDescription.getSavedFileName(),fileToDownloadDescription.getId()));
        }

    }

}
