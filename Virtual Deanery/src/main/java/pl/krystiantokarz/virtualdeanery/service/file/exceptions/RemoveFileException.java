package pl.krystiantokarz.virtualdeanery.service.file.exceptions;

public class RemoveFileException extends Exception {

    public RemoveFileException() {
    }

    public RemoveFileException(String message) {
        super(message);
    }
}
