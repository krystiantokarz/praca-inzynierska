package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileIsEmptyException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileUploadException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;
import pl.krystiantokarz.virtualdeanery.service.file.SaveFileService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.SaveFileException;

@Service
public class UploadFileToDownloadService {

    private static final Logger logger = LoggerFactory.getLogger(UploadFileToDownloadService.class);

    private static final String ARCHIVE_FILE_PREFIX = "archive_";
    private static final String FORM_FILE_PREFIX = "form_";

    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    private SaveFileService saveFileService;

    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Autowired
    public UploadFileToDownloadService(FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository, SaveFileService saveFileService, FileRepositoryPathComponent fileRepositoryPathComponent) {
        this.fileToDownloadDescriptionRepository = fileToDownloadDescriptionRepository;
        this.saveFileService = saveFileService;
        this.fileRepositoryPathComponent = fileRepositoryPathComponent;
    }


    public void uploadFileToDownload(MultipartFile file, User user, FileStatus fileStatus,FileType fileType) throws FileIsEmptyException, FileWithSelectedNameExistException, FileUploadException {

        try {
            checkFileIsNotEmpty(file);
            checkOriginalFileNameIsNotExistInSelectedFileTypeRepository(file.getOriginalFilename(),fileType,fileStatus);
            String filePrefix = getCorrectFilePrefix(fileType);
            String repositoryPath = fileRepositoryPathComponent.getRepositoryPath(fileStatus,fileType);
            String savedFileName = saveFileService.saveFile(repositoryPath, filePrefix, file);
            saveFileDescription(file, user, savedFileName,fileStatus,fileType);
        } catch (SaveFileException e) {
            logger.warn("Cannot upload file to download", e);
            throw new FileUploadException(String.format("Upload form file %s is not possible",file.getName()));
        }
    }

    private void checkFileIsNotEmpty(MultipartFile file) throws FileIsEmptyException {
        if (file == null || file.getSize() == 0){
            logger.debug("Cannot upload file = {} to download because is empty",file);
            throw new FileIsEmptyException("Cannot upload file to download because file is empty");
        }

    }

    private String getCorrectFilePrefix(FileType fileType){
        if (fileType == FileType.ARCHIVE)
            return ARCHIVE_FILE_PREFIX;
        if( fileType == FileType.FORM)
            return FORM_FILE_PREFIX;
        else{
            logger.warn("File type = {} is not found",fileType);
            throw new IllegalArgumentException(String.format("File type = %s is not exist", fileType));
        }
    }

    private void checkOriginalFileNameIsNotExistInSelectedFileTypeRepository(String fileName, FileType fileType,FileStatus fileStatus) throws FileWithSelectedNameExistException {
        boolean result = fileToDownloadDescriptionRepository.checkFileToDownloadWithSelectedOriginalNameAndTypeAndStatusExist(fileName,fileType,fileStatus);
        if(result){
            logger.debug("Upload file name = {} is not unique for file type = {}", fileName, fileType);
            throw new FileWithSelectedNameExistException(String.format("File with name = %s and type = %s is already exist", fileName,fileType));
        }
    }

    private void saveFileDescription(MultipartFile multipartFile, User user, String savedName, FileStatus fileStatus, FileType fileType) {
        Integer fileSize = (int) multipartFile.getSize();
        FileToDownloadDescriptionEntity fileDescription = FileToDownloadDescriptionEntity.from(
                multipartFile.getOriginalFilename(),
                savedName,
                fileSize,
                multipartFile.getContentType(),
                fileType,
                fileStatus,
                user
        );
        fileToDownloadDescriptionRepository.save(fileDescription);
        logger.info("Uploaded file to download with id = {}", fileDescription.getId());
    }

}
