package pl.krystiantokarz.virtualdeanery.service.file.exceptions;

public class SaveFileException extends Exception {

    public SaveFileException() {
    }

    public SaveFileException(String message) {
        super(message);
    }
}
