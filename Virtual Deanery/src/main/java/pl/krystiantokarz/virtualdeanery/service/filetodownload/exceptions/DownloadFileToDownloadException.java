package pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions;

public class DownloadFileToDownloadException extends Exception {
    public DownloadFileToDownloadException() {
    }

    public DownloadFileToDownloadException(String message) {
        super(message);
    }
}
