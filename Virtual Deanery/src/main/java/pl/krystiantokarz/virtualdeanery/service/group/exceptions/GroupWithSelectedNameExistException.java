package pl.krystiantokarz.virtualdeanery.service.group.exceptions;

public class GroupWithSelectedNameExistException extends Exception{

    public GroupWithSelectedNameExistException() {
    }

    public GroupWithSelectedNameExistException(String message) {
        super(message);
    }
}
