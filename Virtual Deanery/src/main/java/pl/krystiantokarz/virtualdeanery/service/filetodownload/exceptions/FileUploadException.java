package pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions;

public class FileUploadException extends Exception {

    public FileUploadException() {
    }

    public FileUploadException(String message) {
        super(message);
    }
}
