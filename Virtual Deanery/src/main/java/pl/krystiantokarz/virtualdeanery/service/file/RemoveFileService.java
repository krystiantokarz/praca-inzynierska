package pl.krystiantokarz.virtualdeanery.service.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.RemoveFileException;

import java.io.File;

@Service
public class RemoveFileService {

    private static final Logger logger = LoggerFactory.getLogger(RemoveFileService.class);

    public void removeFileFromRepository(String repositoryPath, String fileName) throws RemoveFileException {
        File file = new File(repositoryPath,fileName);
        boolean result = file.delete();
        if(!result){
            logger.warn("Cannot remove file {} from repository = {}",fileName,repositoryPath);
            throw new RemoveFileException(String.format("Remove file = %s from repository = %s is impossible",fileName,repositoryPath));
        }
    }
}
