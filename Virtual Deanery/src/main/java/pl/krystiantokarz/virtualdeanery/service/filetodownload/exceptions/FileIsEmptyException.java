package pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions;

public class FileIsEmptyException extends Exception {

    public FileIsEmptyException() {
    }

    public FileIsEmptyException(String message) {
        super(message);
    }
}
