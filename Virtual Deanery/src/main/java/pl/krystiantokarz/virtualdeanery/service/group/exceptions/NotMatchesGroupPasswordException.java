package pl.krystiantokarz.virtualdeanery.service.group.exceptions;

public class NotMatchesGroupPasswordException extends Exception {

    public NotMatchesGroupPasswordException() {
    }

    public NotMatchesGroupPasswordException(String message) {
        super(message);
    }
}
