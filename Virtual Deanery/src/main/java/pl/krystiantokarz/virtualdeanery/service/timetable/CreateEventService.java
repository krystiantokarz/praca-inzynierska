package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.CreateEventDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.TimetableEventRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.EventTimeLimitException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventDateException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventTimeException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.time.LocalDate;
import java.time.LocalTime;

@Service
public class CreateEventService {

    private static final Logger logger = LoggerFactory.getLogger(CreateEventService.class);

    private static final int EVENT_START_TIME = 8;
    private static final int EVENT_END_TIME = 21;

    private UserRepository userRepository;

    private TimetableEventRepository timetableEventRepository;

    @Autowired
    public CreateEventService(UserRepository userRepository, TimetableEventRepository timetableEventRepository) {
        this.userRepository = userRepository;
        this.timetableEventRepository = timetableEventRepository;
    }

    public void createEvent(CreateEventDTO createEventDTO, Long userId) throws UserNotExistException, EventTimeLimitException, ValidEventTimeException, ValidEventDateException {
        User user = userRepository.findUserByIdIfExist(userId);
        checkCreationDateIsInFuture(createEventDTO.getDate());
        checkCorrectDates(createEventDTO.getEventTimeFrom(), createEventDTO.getEventTimeTo());
        TimetableEvent timetableEvent = TimetableEvent.from(
                createEventDTO.getDate(),
                createEventDTO.getEventTimeFrom(),
                createEventDTO.getEventTimeTo(),
                createEventDTO.getTopic(),
                createEventDTO.getMessage(),
                user
        );
        timetableEventRepository.save(timetableEvent);
        logger.info("Timetable event was created. Event id = {}", timetableEvent.getId());
    }


    private void checkCreationDateIsInFuture(LocalDate eventDate) throws ValidEventDateException {
        LocalDate now = LocalDate.now();
        if(eventDate.isBefore(now)){
            logger.debug("Cannot create timetable event - selected date is in past");
            throw new ValidEventDateException("Cannot create timetable event - selected date is in past");
        }
    }

    private void checkCorrectDates(LocalTime eventStartTime, LocalTime eventEndTime) throws EventTimeLimitException, ValidEventTimeException {
        if(eventStartTime.isAfter(eventEndTime) || eventStartTime.equals(eventEndTime)){
            logger.debug("Cannot create timetable event - selected start time is after end time");
            throw new ValidEventTimeException("Cannot create timetable event - selected start time is after end time");
        }
        if (eventStartTime.isBefore(LocalTime.of(EVENT_START_TIME, 0)) || eventEndTime.isAfter(LocalTime.of(EVENT_END_TIME,0))) {
            logger.debug("Cannot create timetable event - selected time exceeds the range");
            throw new EventTimeLimitException("Cannot create timetable event - selected time exceeds the range");
        }
    }
}
