package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.service.file.RemoveFileService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.RemoveFileException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;

@Service
public class DeleteFileToDownloadService {

    private static final Logger logger = LoggerFactory.getLogger(DeleteFileToDownloadService.class);

    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    private FileRepositoryPathComponent fileRepositoryPathComponent;

    private RemoveFileService removeFileService;

    @Autowired
    public DeleteFileToDownloadService(FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository, FileRepositoryPathComponent fileRepositoryPathComponent, RemoveFileService removeFileService) {
        this.fileToDownloadDescriptionRepository = fileToDownloadDescriptionRepository;
        this.fileRepositoryPathComponent = fileRepositoryPathComponent;
        this.removeFileService = removeFileService;
    }

    public void deleteFile(Long fileId) throws FileNotExistException, RemoveFileException {
        FileToDownloadDescription fileDescription = fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId);
        String repositoryPath = fileRepositoryPathComponent.getRepositoryPath(fileDescription.getFileStatus(), fileDescription.getFileType());
        removeFileService.removeFileFromRepository(repositoryPath, fileDescription.getSavedFileName());
        fileToDownloadDescriptionRepository.delete(fileId);
        logger.info("Deleted file to download with id = {}",fileId);
    }





}
