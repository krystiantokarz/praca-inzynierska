package pl.krystiantokarz.virtualdeanery.service.timetable.exceptions;

public class EventTimeLimitException extends Exception {

    public EventTimeLimitException() {
    }

    public EventTimeLimitException(String message) {
        super(message);
    }
}
