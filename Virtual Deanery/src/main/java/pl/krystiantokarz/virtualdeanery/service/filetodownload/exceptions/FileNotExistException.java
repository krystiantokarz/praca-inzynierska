package pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions;

public class FileNotExistException extends Exception {

    public FileNotExistException() {
    }

    public FileNotExistException(String message) {
        super(message);
    }
}
