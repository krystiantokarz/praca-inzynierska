package pl.krystiantokarz.virtualdeanery.service.file.exceptions;

public class DownloadFileException extends Exception {

    public DownloadFileException() {
    }

    public DownloadFileException(String message) {
        super(message);
    }
}
