package pl.krystiantokarz.virtualdeanery.service.messagebox;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.mail.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailAttachmentFileDescriptionNotExist;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailMessageNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@Service
public class FindMessageService {

    private final UserRepository userRepository;

    private final MailMessageRepository mailMessageRepository;

    @Autowired
    public FindMessageService(UserRepository userRepository, MailMessageRepository mailMessageRepository) {
        this.userRepository = userRepository;
        this.mailMessageRepository = mailMessageRepository;
    }

    public Page<MailMessageEntity> findMessagesForUserByPageAndPageSize(Long userId, int page, int size) throws UserNotExistException {
        User user = userRepository.findUserByIdIfExist(userId);
        return mailMessageRepository.findMessagesForSelectedUser(user, new PageRequest(page, size));
    }

    public MailMessageEntity findMessageByIdForSelectedUserAndChangeMessageStatusIfNotReadYet(Long messageId, Long userId) throws MailMessageNotExistException, UserNotExistException {
        User user = userRepository.findUserByIdIfExist(userId);
        MailMessageEntity message = mailMessageRepository.findMailMessageByIdForSelectedRecipientIfExist(messageId,user);
        changeMessageStatusIfItIsNotRead(message);
        return message;
    }

//    public MailAttachmentFileDescription findMessageAttachmentFileDescriptionForUser(Long attachmentFileId, Long userId) throws MailAttachmentFileDescriptionNotExist, UserNotExistException {
//        User user = userRepository.findUserByIdIfExist(userId);
//        return mailMessageRepository.findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(attachmentFileId,user);
//    }

    private void changeMessageStatusIfItIsNotRead(MailMessageEntity message) {
        if(message.getMessageStatus() == MailMessageStatus.DELIVERED){
            message.changeStatusToRead();
            mailMessageRepository.save(message);
        }
    }




}
