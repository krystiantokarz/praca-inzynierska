package pl.krystiantokarz.virtualdeanery.service.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.domain.user.UserFactory;
import pl.krystiantokarz.virtualdeanery.domain.user.role.Role;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.CreateUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.EmailSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.createaccount.CreateNewAccountSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.createaccount.NewAccountSendObject;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.RolesConverter;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import pl.krystiantokarz.virtualdeanery.infrastructure.utils.PasswordGenerator;

import java.util.List;
import java.util.Locale;

@Service
public class CreateUserService {

    private static final Logger logger = LoggerFactory.getLogger(CreateUserService.class);

    private static final int PASSWORD_LENGTH = 9;

    @Value("${mail.sender.enabled}")
    private boolean mailEnabled;

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private EmailSender emailSender;

    @Autowired
    public CreateUserService(UserRepository userRepository, PasswordEncoder passwordEncoder, EmailSender emailSender) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailSender = emailSender;
    }

    public void createNewUser(CreateUserDTO createUserDTO, Locale locale) throws UserWithSelectedEmailExistException {
        validateEmail(createUserDTO);
        String generatedPassword = PasswordGenerator.generatePassword(PASSWORD_LENGTH);
        User user = createUserWithGeneratedPassword(createUserDTO,generatedPassword);
        userRepository.save(user);
        sendEmail(user,generatedPassword, locale);
        logger.info("Created new user with id = {}", user.getId());
    }

    private void validateEmail(CreateUserDTO createUserDTO) throws UserWithSelectedEmailExistException {
        String email = createUserDTO.getEmail();
        boolean result = userRepository.checkUserWithSelectedEmailExist(email);
        if(result){
            throw new UserWithSelectedEmailExistException(String.format("Cannot create new user. Selected email = %s is not unique",email));
        }
    }

    private User createUserWithGeneratedPassword(CreateUserDTO createUserDTO,String password){
        List<Role> userRoles = createUserRoles(createUserDTO);
        return UserFactory.build(
                createUserDTO.getFirstName(),
                createUserDTO.getLastName(),
                createUserDTO.getEmail(),
                createUserDTO.getEmail(),
                passwordEncoder.encode(password),
                userRoles
        );
    }

    private List<Role> createUserRoles(CreateUserDTO createUserDTO){
        return RolesConverter.convertRolesStringToObjects(createUserDTO.getRoles());
    }


    private void sendEmail(User user, String password, Locale locale){
        if(mailEnabled){
            NewAccountSendObject objectToSend = NewAccountSendObject.from(
                    user.getEmail(),
                    user.getFirstName(),
                    user.getLastName(),
                    password,
                    user.getLoginData().getLogin()

            );
            CreateNewAccountSender createNewAccountSender = new CreateNewAccountSender(objectToSend);
            emailSender.sendEmail(createNewAccountSender,locale);
            logger.trace("Send email for user with id = {} with created account information", user.getId());
        }
    }


}
