package pl.krystiantokarz.virtualdeanery.service.user.exceptions;

public class UserNotExistException extends Exception{

    public UserNotExistException() {
        super();
    }

    public UserNotExistException(String message) {
        super(message);
    }
}
