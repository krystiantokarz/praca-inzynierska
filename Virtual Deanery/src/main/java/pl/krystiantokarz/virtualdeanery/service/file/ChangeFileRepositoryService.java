package pl.krystiantokarz.virtualdeanery.service.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.ChangeFileToDownloadTypeException;

import java.io.File;

@Service
public class ChangeFileRepositoryService {

    private static final Logger logger = LoggerFactory.getLogger(ChangeFileRepositoryService.class);

    public void changeFileRepository(String oldRepositoryPath, String newRepositoryPath, String fileName) throws ChangeFileToDownloadTypeException {
        File file = new File(oldRepositoryPath,fileName);
        File fileToRename = new File(newRepositoryPath, fileName);
        boolean result = file.renameTo(fileToRename);
        if(!result){
            logger.warn("Cannot change file repository for file name = {} from old repository = {} to new repository = {}",fileName,oldRepositoryPath, newRepositoryPath);
            throw new ChangeFileToDownloadTypeException(String.format("Change file to download repository for file name = %s is impossible",fileName));
        }else{
            logger.info("File with name = {} is corrected changed repository", fileName);
        }
    }
}
