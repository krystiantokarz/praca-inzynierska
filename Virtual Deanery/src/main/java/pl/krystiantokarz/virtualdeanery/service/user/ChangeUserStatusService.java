package pl.krystiantokarz.virtualdeanery.service.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.EmailSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.changeuserstatus.ChangeUserStatusSendObject;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.changeuserstatus.ChangeUserStatusSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.Locale;

@Service
public class ChangeUserStatusService {

    private static final Logger logger = LoggerFactory.getLogger(ChangeUserStatusService.class);

    @Value("${mail.sender.enabled}")
    private boolean mailEnabled;

    private UserRepository userRepository;

    private EmailSender emailSender;

    @Autowired
    public ChangeUserStatusService(UserRepository userRepository, EmailSender emailSender) {
        this.userRepository = userRepository;
        this.emailSender = emailSender;
    }

    public void disableUser(Long userId, Locale locale) throws UserNotExistException {
        User user = userRepository.findUserByIdIfExist(userId);
        user.disableUser();
        userRepository.save(user);
        sendEmail(user, false,locale);
        logger.info("Disabled user with id = {}", userId);
    }

    public void enableUser(Long userId,Locale locale) throws UserNotExistException {
        User user = userRepository.findUserByIdIfExist(userId);
        user.enableUser();
        userRepository.save(user);
        sendEmail(user, true,locale);
        logger.info("Enabled user with id = {}", userId);
    }

    private void sendEmail(User user,boolean enabled, Locale locale){
        if(mailEnabled){
            ChangeUserStatusSendObject objectToSend = ChangeUserStatusSendObject.from(
                    user.getEmail(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getLoginData().getLogin(),
                    enabled

            );
            ChangeUserStatusSender createNewAccountSender = new ChangeUserStatusSender(objectToSend);
            emailSender.sendEmail(createNewAccountSender,locale);
            logger.trace("Send email for user with id = {} with enable/disable information", user.getId());
        }
    }

}
