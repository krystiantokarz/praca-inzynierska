package pl.krystiantokarz.virtualdeanery.service.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroupFactory;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.NotMatchesGroupPasswordException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.List;

@Service
public class ManagedAccessGroupService {

    private static final Logger logger = LoggerFactory.getLogger(ManagedAccessGroupService.class);

    private PasswordEncoder passwordEncoder;

    private GroupRepository groupRepository;

    private UserRepository userRepository;

    @Autowired
    public ManagedAccessGroupService(PasswordEncoder passwordEncoder, GroupRepository groupRepository, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
    }

    public void joinIntoSelectedGroup(Long groupId, Long userId, String enteredPassword) throws UserNotExistException, GroupNotExistException, NotMatchesGroupPasswordException {
        User user = userRepository.findUserByIdIfExist(userId);
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        checkEnteredGroupPasswordIsCorrect(group,userId,enteredPassword);
        UserGroup userGroup = createUserGroup(user, group);
        groupRepository.saveUserGroup(userGroup);
        logger.info("User with id = {} join into group with id = {}",userId, groupId);
    }

    public void leaveFromSelectedGroup(Long groupId, Long userId) throws UserNotExistException, GroupNotExistException, UserGroupNotExistException {
        User user = userRepository.findUserByIdIfExist(userId);
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        UserGroup userGroup = groupRepository.findUserGroupByUserAndGroup(user, group);
        groupRepository.deleteUserGroup(userGroup);
        deleteGroupWithoutMembers(group);
        logger.info("User with id = {} leave from group with id = {}",userId, groupId);
    }

    private void checkEnteredGroupPasswordIsCorrect(Group group, Long userId,String enteredPassword) throws NotMatchesGroupPasswordException {
        boolean matches = passwordEncoder.matches(enteredPassword, group.getPassword());
        if(!matches){
            logger.debug("Cannot join into group with id = {} by user with id = {}. Entered group password = {} was incorrect",group.getId(),userId,enteredPassword);
            throw new NotMatchesGroupPasswordException(String.format("Cannot join into group with id = %d. Entered group password = %s was incorrect",group.getId(),enteredPassword));
        }
    }

    private UserGroup createUserGroup(User user, Group group){
        return UserGroupFactory.createUserGroupForNormalMember(
                user,
                group);

    }

    private void deleteGroupWithoutMembers(Group group){
        List<UserGroup> userGroups = groupRepository.findUserGroupsByGroupId(group.getId());
        if(userGroups.isEmpty()){
            groupRepository.delete(group);
            logger.info("Group with id = {} was deleted", group.getId());
        }
    }




}
