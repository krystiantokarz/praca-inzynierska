package pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions;

public class DownloadMessageAttachmentFileException extends Exception {
    public DownloadMessageAttachmentFileException() {
    }

    public DownloadMessageAttachmentFileException(String message) {
        super(message);
    }
}
