package pl.krystiantokarz.virtualdeanery.service.group.exceptions;

public class NotMatchesPasswordsException extends Exception {
    public NotMatchesPasswordsException() {
    }

    public NotMatchesPasswordsException(String message) {
        super(message);
    }
}
