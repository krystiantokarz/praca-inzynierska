package pl.krystiantokarz.virtualdeanery.service.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditAccountUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedLoginExistException;

@Service
public class EditAccountService {

    private static final Logger logger = LoggerFactory.getLogger(EditAccountService.class);

    private final UserRepository userRepository;

    @Autowired
    public EditAccountService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User editSelectedUser(Long userId, EditAccountUserDTO editAccountUserDTO) throws UserNotExistException, UserWithSelectedEmailExistException, UserWithSelectedLoginExistException {
        User user = userRepository.findUserByIdIfExist(userId);

        validateEmail(editAccountUserDTO, userId);
        validateLogin(editAccountUserDTO, userId);

        user.changeUserData(
                editAccountUserDTO.getFirstName(),
                editAccountUserDTO.getLastName(),
                editAccountUserDTO.getEmail(),
                editAccountUserDTO.getLogin()
        );

        User editedUser = userRepository.save(user);

        logger.info("Edit user account data by user with id = {}", userId);
        return editedUser;
    }

    private void validateEmail(EditAccountUserDTO editAccountUserDTO, Long userId) throws UserWithSelectedEmailExistException {
        String email = editAccountUserDTO.getEmail();
        boolean result = userRepository.checkUserWithSelectedEmailExistWithoutSelectedUser(email, userId);
        if(result){
            logger.debug("Cannot edit user account data for entered email = {} for user with id = {}. Selected email is not unique", userId, email);
            throw new UserWithSelectedEmailExistException(String.format("Cannot edit account for user with id = %d. Selected email = %s is not unique",userId,email));
        }

    }

    private void validateLogin(EditAccountUserDTO editAccountUserDTO, Long userId) throws UserWithSelectedLoginExistException {
        String login = editAccountUserDTO.getLogin();
        boolean result =  userRepository.checkUserWithSelectedLoginExistWithoutSelectedUser(login, userId);
        if(result){
            logger.debug("Cannot edit user account data for entered login = {} for user with id = {}. Selected login is not unique", userId, login);
            throw new UserWithSelectedLoginExistException(String.format("Cannot edit account for user with id = %d. Selected login = %s is not unique",userId,login));
        }
    }
}
