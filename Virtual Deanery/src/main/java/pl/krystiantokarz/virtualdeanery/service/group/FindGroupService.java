package pl.krystiantokarz.virtualdeanery.service.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.List;

@Service
public class FindGroupService {

    private final GroupRepository groupRepository;

    private final UserRepository userRepository;

    @Autowired
    public FindGroupService(GroupRepository groupRepository, UserRepository userRepository) {
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
    }

    public Group findGroupById(Long id) throws GroupNotExistException {
        return groupRepository.findGroupByIdIfExist(id);
    }

    public Page<Group> getGroupsByPageAndSize(int page, int size){
        return groupRepository.findAll(new PageRequest(page, size));
    }

    public Page<Group> getAllGroupsForUserWithPageAndSize(Long userId, int page, int pageSize){
        return groupRepository.findGroupsForSelectedUser(userId,new PageRequest(page,pageSize));
    }

    public List<Group> getAllGroupsForUser(Long userId){

        return groupRepository.findGroupsForSelectedUser(userId);
    }
    public List<Group> searchGroups(String searchString){
        String searchGroupsNameString = searchString.trim();
        return groupRepository.findByNameContainingIgnoreCase(searchGroupsNameString);
    }

    public UserGroup findUserGroup(Long userId, Long groupId) throws UserNotExistException, GroupNotExistException, UserGroupNotExistException {
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        User user = userRepository.findUserByIdIfExist(userId);
        return groupRepository.findUserGroupByUserAndGroup(user,group);
    }

    public List<UserGroup> findUserGroupsForSelectedGroup(Long groupId){
        return groupRepository.findUserGroupsByGroupId(groupId);
    }

    public boolean checkLoggedUserIsAdminInSelectedGroup(Long loggedUserId, Long groupId) {
         return groupRepository.checkLoggedUserIsAdminInSelectedGroup(loggedUserId, groupId);
    }
}
