package pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions;

public class FileWithSelectedNameExistException extends Exception {

    public FileWithSelectedNameExistException() {
    }

    public FileWithSelectedNameExistException(String message) {
        super(message);
    }
}
