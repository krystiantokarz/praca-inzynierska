package pl.krystiantokarz.virtualdeanery.service.user.exceptions;

public class UserWithSelectedEmailExistException extends Exception{

    public UserWithSelectedEmailExistException() {
        super();
    }

    public UserWithSelectedEmailExistException(String message) {
        super(message);
    }
}
