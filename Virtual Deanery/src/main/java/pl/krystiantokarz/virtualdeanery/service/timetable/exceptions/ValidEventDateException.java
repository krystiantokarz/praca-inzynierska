package pl.krystiantokarz.virtualdeanery.service.timetable.exceptions;

public class ValidEventDateException extends Exception {

    public ValidEventDateException() {
    }

    public ValidEventDateException(String message) {
        super(message);
    }
}
