package pl.krystiantokarz.virtualdeanery.service.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupPasswordDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.*;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@Service
public class EditGroupService {

    private static final Logger logger = LoggerFactory.getLogger(EditGroupService.class);

    private PasswordEncoder passwordEncoder;

    private GroupRepository groupRepository;

    private UserRepository userRepository;

    @Autowired
    public EditGroupService(PasswordEncoder passwordEncoder, GroupRepository groupRepository, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
    }

    public void editGroupData(Long groupId, Long userId, EditGroupDTO editGroupDTO) throws GroupNotExistException, PrivilegeException, UserNotExistException, UserGroupNotExistException, GroupWithSelectedNameExistException {
        validateEditGroupName(editGroupDTO, groupId);
        User user = userRepository.findUserByIdIfExist(userId);
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        groupRepository.findUserGroupForAdminMember(user,group);
        group.changeGroupData(editGroupDTO.getName(), editGroupDTO.getDescription());
        groupRepository.save(group);
        logger.info("Group with id = {} has changed data by user with id = {}", groupId,userId);
    }

    private void validateEditGroupName(EditGroupDTO editGroupDTO,Long groupId) throws  GroupWithSelectedNameExistException {
        String groupName = editGroupDTO.getName();
        boolean result = groupRepository.checkGroupWithSelectedNameExistWithoutSelectedGroup(groupName,groupId);
        if(result){
            logger.debug("Group with id = {} cannot be edit group name = {}. Name is not unique",groupId,groupName);
            throw new GroupWithSelectedNameExistException(String.format("Cannot edit group with id = %d. Selected new group name = %s is not unique",groupId, groupName));
        }
    }

    public void editGroupPassword(Long groupId, Long userId, EditGroupPasswordDTO editGroupPasswordDTO) throws GroupNotExistException, PrivilegeException, NotMatchesPasswordsException, UserNotExistException, UserGroupNotExistException {
        User user = userRepository.findUserByIdIfExist(userId);
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        groupRepository.findUserGroupForAdminMember(user, group);
        String oldPassword = group.getPassword();
        String enteredOldPassword = editGroupPasswordDTO.getOldPassword();
        boolean result = passwordEncoder.matches(enteredOldPassword,oldPassword);
        if(result){
            group.changeGroupPassword(passwordEncoder.encode(editGroupPasswordDTO.getNewPassword()));
            groupRepository.save(group);
        }else{
            logger.debug("Cannot edit group with id = {} password. Entered password = {} is not matches", groupId, enteredOldPassword);
            throw new NotMatchesPasswordsException(String.format("Not matches group with id = %d entered password = %s",groupId, oldPassword));
        }
    }
}
