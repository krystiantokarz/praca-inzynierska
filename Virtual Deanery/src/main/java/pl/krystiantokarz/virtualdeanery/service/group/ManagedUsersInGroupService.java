package pl.krystiantokarz.virtualdeanery.service.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@Service
public class ManagedUsersInGroupService {

    private static final Logger logger = LoggerFactory.getLogger(ManagedAccessGroupService.class);

    private GroupRepository groupRepository;

    private UserRepository userRepository;

    @Autowired
    public ManagedUsersInGroupService(GroupRepository groupRepository, UserRepository userRepository) {
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
    }

    public void addAdminAccess(Long userId, Long groupId,Long loggedUserId) throws UserGroupNotExistException, UserNotExistException, PrivilegeException, GroupNotExistException {

        validateLoggedUserPrivilege(loggedUserId, groupId);
        User user = userRepository.findUserByIdIfExist(userId);
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        UserGroup userGroup = groupRepository.findUserGroupByUserAndGroup(user, group);
        userGroup.changeMemberTypeToGroupAdministrator();
        groupRepository.saveUserGroup(userGroup);
        logger.info("Member type for user with id = {} was changed to group administrator in group with id = {}",userId,groupId);
    }

    public void removeAdminAccess(Long userId, Long groupId, Long loggedUserId) throws UserGroupNotExistException, UserNotExistException, PrivilegeException, GroupNotExistException {
        validateLoggedUserPrivilege(loggedUserId, groupId);
        User user = userRepository.findUserByIdIfExist(userId);
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        UserGroup userGroup = groupRepository.findUserGroupByUserAndGroup(user, group);
        userGroup.changeMemberTypeToNormalMember();
        groupRepository.saveUserGroup(userGroup);
        logger.info("Member type for user with id = {} was changed from group administrator to normal member in group with id = {}",userId,groupId);
    }

    public void removeUserFromGroup(Long userId, Long groupId,Long loggedUserId) throws UserGroupNotExistException, UserNotExistException, PrivilegeException, GroupNotExistException {
        validateLoggedUserPrivilege(loggedUserId, groupId);
        User user = userRepository.findUserByIdIfExist(userId);
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        UserGroup userGroup = groupRepository.findUserGroupByUserAndGroup(user, group);
        groupRepository.deleteUserGroup(userGroup);
        logger.info("User with id = {} was removed from group with id = {}", userId, groupId);
    }

    private void validateLoggedUserPrivilege(Long loggedUserId, Long groupId) throws PrivilegeException {
        boolean result = groupRepository.checkLoggedUserIsAdminInSelectedGroup(loggedUserId, groupId);
        if(!result){
            logger.debug("Logged user with id = {} cannot managed group with id = {} because not have privilege to do it ", loggedUserId, groupId);
            throw new PrivilegeException(String.format("Cannot managed group with id = %d by user with id = %d. This user not have privilege", groupId, loggedUserId));
        }
    }
}
