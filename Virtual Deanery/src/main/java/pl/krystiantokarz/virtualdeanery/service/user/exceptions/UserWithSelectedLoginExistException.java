package pl.krystiantokarz.virtualdeanery.service.user.exceptions;

public class UserWithSelectedLoginExistException extends Exception{

    public UserWithSelectedLoginExistException() {
        super();
    }

    public UserWithSelectedLoginExistException(String message) {
        super(message);
    }

}
