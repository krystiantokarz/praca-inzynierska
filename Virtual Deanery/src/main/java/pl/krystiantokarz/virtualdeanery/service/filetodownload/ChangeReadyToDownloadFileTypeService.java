package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.file.FileReadyToDownload;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.service.file.ChangeFileRepositoryService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.ChangeFileToDownloadTypeException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;

@Service
public class ChangeReadyToDownloadFileTypeService {

    private static final Logger logger = LoggerFactory.getLogger(ChangeReadyToDownloadFileTypeService.class);

    private FileRepositoryPathComponent fileRepositoryPathComponent;

    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    private ChangeFileRepositoryService changeFileRepositoryService;

    @Autowired
    public ChangeReadyToDownloadFileTypeService(FileRepositoryPathComponent fileRepositoryPathComponent, FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository, ChangeFileRepositoryService changeFileRepositoryService) {
        this.fileRepositoryPathComponent = fileRepositoryPathComponent;
        this.fileToDownloadDescriptionRepository = fileToDownloadDescriptionRepository;
        this.changeFileRepositoryService = changeFileRepositoryService;
    }

    public void changeFileFromArchiveToFormFileType(Long fileId) throws FileNotExistException, ChangeFileToDownloadTypeException, FileWithSelectedNameExistException {
        changeFileType(fileId, FileType.ARCHIVE, FileType.FORM);
        logger.info("Changed file type from archive to form for file to download with id = {}", fileId);
    }

    public void changeFileFromFormToArchiveFileType(Long fileId) throws FileNotExistException, ChangeFileToDownloadTypeException, FileWithSelectedNameExistException {
        changeFileType(fileId, FileType.FORM, FileType.ARCHIVE);
        logger.info("Changed file type from form to archive for file to download with id = {}", fileId);
    }

    private void changeFileType(Long fileId,FileType oldFileType, FileType newFileType) throws FileNotExistException, ChangeFileToDownloadTypeException, FileWithSelectedNameExistException {

        FileReadyToDownload file = (FileReadyToDownload) fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId);
        checkOriginalFileNameIsNotExistInNewFileTypeRepository(file.getOriginalFileName(), newFileType);
        FileReadyToDownload changedFile = file.changeFileType(newFileType);
        fileToDownloadDescriptionRepository.save(changedFile);
        changeFileRepository(file, oldFileType, newFileType);

    }

    private void checkOriginalFileNameIsNotExistInNewFileTypeRepository(String fileName, FileType fileType) throws FileWithSelectedNameExistException {
        boolean result = fileToDownloadDescriptionRepository.checkFileToDownloadWithSelectedOriginalNameAndTypeAndStatusExist(fileName,fileType, FileStatus.READY_TO_DOWNLOAD);
        if(result){
            logger.debug("Cannot change file type. File with name = {} is not unique for new file type = {}", fileName, fileType);
            throw new FileWithSelectedNameExistException(String.format("File to download with name = %s and type = %s is already exist", fileName,fileType));
        }
    }



    private void changeFileRepository(FileReadyToDownload file, FileType oldFileType, FileType newFileType) throws ChangeFileToDownloadTypeException {
        String oldRepositoryPath = fileRepositoryPathComponent.getRepositoryPath(file.getFileStatus(), oldFileType);
        String newRepositoryPath = fileRepositoryPathComponent.getRepositoryPath(file.getFileStatus(), newFileType);
        changeFileRepositoryService.changeFileRepository(oldRepositoryPath,newRepositoryPath,file.getSavedFileName());
        logger.debug("Changed file old repository path = {} to new repository path = {}", oldRepositoryPath, newRepositoryPath);
    }
}
