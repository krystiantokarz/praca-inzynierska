package pl.krystiantokarz.virtualdeanery.service.resetpassword.exception;

public class PasswordResetTokenNotExistException extends Exception {

    public PasswordResetTokenNotExistException() {
    }

    public PasswordResetTokenNotExistException(String message) {
        super(message);
    }
}
