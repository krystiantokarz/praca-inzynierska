package pl.krystiantokarz.virtualdeanery.service.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@Service
public class DeleteGroupService {

    private static final Logger logger = LoggerFactory.getLogger(DeleteGroupService.class);

    private GroupRepository groupRepository;

    private UserRepository userRepository;

    @Autowired
    public DeleteGroupService(GroupRepository groupRepository, UserRepository userRepository) {
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void deleteGroupBySystemAdministrator(Long groupId) throws GroupNotExistException {
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        deleteGroupWithUserGroups(group);
    }

    @Transactional
    public void deleteGroupByGroupAdministrator(Long userId, Long groupId) throws UserNotExistException, GroupNotExistException, UserGroupNotExistException, PrivilegeException {
        User user = userRepository.findUserByIdIfExist(userId);
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        UserGroup userGroupForAdminMember = groupRepository.findUserGroupForAdminMember(user, group);
        deleteGroupWithUserGroups(userGroupForAdminMember.getGroup());
    }

    private void deleteGroupWithUserGroups(Group group) throws GroupNotExistException {
        try {
            groupRepository.deleteAllUserGroupsForSelectedGroup(group);
            groupRepository.delete(group);
        } catch (EmptyResultDataAccessException e){
            logger.warn("Cannot delete group with id = {}", group.getId(), e);
            throw new GroupNotExistException(String.format("Group with id %d not exist", group.getId()));

        }
    }
}
