package pl.krystiantokarz.virtualdeanery.service.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.DownloadFileException;

import java.io.*;

@Service
public class DownloadFileService {

    private static final Logger logger = LoggerFactory.getLogger(DownloadFileService.class);

    private static final int BUFFER_SIZE = 4096;

    public void downloadFile(String repositoryPath, String fileName, OutputStream outputStream) throws DownloadFileException {
        try {
            FileInputStream inputStream = new FileInputStream(new File(repositoryPath, fileName));
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            inputStream.close();
            outputStream.close();
        } catch (Exception ex) {
            logger.warn("Cannot download file {} from repository = {}",fileName,repositoryPath,ex);
            throw new DownloadFileException(String.format("Download file = %s is impossible - IOException",fileName));
        }
    }
}
