package pl.krystiantokarz.virtualdeanery.service.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.RolesConverter;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedLoginExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@Service
public class EditUserService {

    private static final Logger logger = LoggerFactory.getLogger(EditUserService.class);

    private UserRepository userRepository;

    @Autowired
    public EditUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User editSelectedUser(Long userId, EditUserDTO editUserDTO) throws UserNotExistException, UserWithSelectedEmailExistException, UserWithSelectedLoginExistException {
        User user = userRepository.findUserByIdIfExist(userId);

        validateEmail(editUserDTO, userId);
        validateLogin(editUserDTO, userId);

        user.changeUserData(
                editUserDTO.getFirstName(),
                editUserDTO.getLastName(),
                editUserDTO.getEmail(),
                editUserDTO.getLogin(),
                RolesConverter.convertRolesStringToObjects(editUserDTO.getRoles())
        );
        User savedUser = userRepository.save(user);
        logger.info("Edit user account data for user with id = {}", user.getId());
        return savedUser;
    }

    private void validateEmail(EditUserDTO editUserDTO, Long userId) throws UserWithSelectedEmailExistException {
        String email = editUserDTO.getEmail();
        boolean result = userRepository.checkUserWithSelectedEmailExistWithoutSelectedUser(email,userId);
        if(result){
            logger.debug("Cannot edit user data for entered email = {}. Selected email is not unique", email);
            throw new UserWithSelectedEmailExistException(String.format("Cannot edit user with id = %d. Selected email = %s is not unique",userId,email));
        }
    }

    private void validateLogin(EditUserDTO editUserDTO, Long userId) throws UserWithSelectedLoginExistException {
        String login = editUserDTO.getLogin();
        boolean result =  userRepository.checkUserWithSelectedLoginExistWithoutSelectedUser(login,userId);
        if(result){
            logger.debug("Cannot edit user data for entered login = {}. Selected login is not unique", login);
            throw new UserWithSelectedLoginExistException(String.format("Cannot edit user with id = %d. Selected login = %s is not unique",userId,login));
        }
    }
}
