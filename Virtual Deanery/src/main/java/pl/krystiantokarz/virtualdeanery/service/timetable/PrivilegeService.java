package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.PrivilegeException;

@Service
public class PrivilegeService {

    private static final Logger logger = LoggerFactory.getLogger(PrivilegeService.class);

    public void checkUserHasAccessToEditEvent(TimetableEvent timetable, Long userId) throws PrivilegeException {
        if(!timetable.getCreatingUser().getId().equals(userId)){
            logger.debug("User with id = {} have not access to change timetable event data", userId);
            throw new PrivilegeException(String.format("User with id = %d has not privilege to edit timetable event with id = %d", userId, timetable.getId()));
        }
    }
}
