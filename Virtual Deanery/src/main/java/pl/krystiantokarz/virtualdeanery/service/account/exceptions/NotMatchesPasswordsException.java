package pl.krystiantokarz.virtualdeanery.service.account.exceptions;

public class NotMatchesPasswordsException extends Exception {
    public NotMatchesPasswordsException() {
    }

    public NotMatchesPasswordsException(String message) {
        super(message);
    }
}
