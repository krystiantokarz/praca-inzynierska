package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;

import java.util.List;

@Service
public class FindFileToDownloadService {

    private static final Logger logger = LoggerFactory.getLogger(FindFileToDownloadService.class);

    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Autowired
    public FindFileToDownloadService(FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository) {
        this.fileToDownloadDescriptionRepository = fileToDownloadDescriptionRepository;
    }

    public Page<FileReadyToDownload> getFileReadyToDownloadDescriptionsByFileTypeAndPageAndPageSize(FileType fileType, Integer page, Integer size) {
        return fileToDownloadDescriptionRepository.findAllFileReadyToDownloadPageByFileType(fileType,new PageRequest(page, size));
    }

    public Page<FileWaitingForAccepted> getFileWaitingForAcceptedDescriptionsByPageAndPageSize(Integer page, Integer size) {
        return fileToDownloadDescriptionRepository.findAllFileWaitingForAcceptedPage(new PageRequest(page, size));
    }

    public List<FileReadyToDownload> searchFilesReadyToDownloadBySearchedStringOriginalNameContainingIgnoreCaseAndFileType(String searchString, FileType fileType){
        String fileNameSearchString = searchString.trim();
        return fileToDownloadDescriptionRepository.findByOriginalFileNameContainingIgnoreCaseAndFileTypeAndFileStatus(fileNameSearchString,fileType,FileStatus.READY_TO_DOWNLOAD);
    }



}
