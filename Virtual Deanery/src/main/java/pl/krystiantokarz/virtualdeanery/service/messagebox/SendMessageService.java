package pl.krystiantokarz.virtualdeanery.service.messagebox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageFactory;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.in.CreateMessageDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.file.SaveFileService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.SaveFileException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.SendMessageException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.ArrayList;
import java.util.List;

@Service
public class SendMessageService {

    private static final Logger logger = LoggerFactory.getLogger(SendMessageService.class);

    private static final String FILE_PREFIX = "attachment_";

    @Value("${file.repository.fileattachment}")
    private String repositoryPath;

    @Value("${application.user.email}")
    private String applicationEmail;

    private UserRepository userRepository;

    private MailMessageRepository mailMessageRepository;

    private SaveFileService saveFileService;

    @Autowired
    public SendMessageService(UserRepository userRepository, MailMessageRepository mailMessageRepository, SaveFileService saveFileService) {
        this.userRepository = userRepository;
        this.mailMessageRepository = mailMessageRepository;
        this.saveFileService = saveFileService;
    }

    public void sendMessage(CreateMessageDTO messageDTO, Long userFromId) throws UserNotExistException, SendMessageException {

        User userTo = userRepository.findUserByIdIfExist(messageDTO.getIdUserForSendMessage());
        User userFrom = userRepository.findUserByIdIfExist(userFromId);

        MailMessageEntity message;

        if(checkAttachmentFilesIsNotEmpty(messageDTO.getFiles())){
            List<MailAttachmentFileDescription> fileDescriptions = saveAttachmentsAndCreateAttachmentDescriptions(messageDTO.getFiles());
            message = MailMessageFactory.createWithAttachments(
                    messageDTO.getSubject(),
                    messageDTO.getMessage(),
                    fileDescriptions,
                    userFrom,
                    userTo);
        }else{
            message = MailMessageFactory.createWithoutAttachments(
                    messageDTO.getSubject(),
                    messageDTO.getMessage(),
                    userFrom,
                    userTo);
        }
        mailMessageRepository.save(message);
        logger.info("Message to user with id = {} from user with id = {} was send", userTo, userFrom);
    }


    public void sendMessageFromSystemUser(String subject, String messageContent, Long toUserId) throws UserNotExistException {

        User userTo = userRepository.findUserByIdIfExist(toUserId);
        User appUser = userRepository.findUserByEmailIfExist(applicationEmail);
        MailMessageEntity message = MailMessageFactory.createWithoutAttachments(
                subject,
                messageContent,
                appUser,
                userTo);

        mailMessageRepository.save(message);
        logger.info("Message to user with id = {} was send by application", toUserId);
    }

    private boolean checkAttachmentFilesIsNotEmpty(List<MultipartFile> multipartFiles){
        if(multipartFiles == null || multipartFiles.isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    private List<MailAttachmentFileDescription> saveAttachmentsAndCreateAttachmentDescriptions(List<MultipartFile> multipartFiles) throws SendMessageException {

        List<MailAttachmentFileDescription> messageAttachmentFileDescriptions = new ArrayList<>(multipartFiles.size());
        try {
            for (MultipartFile file : multipartFiles) {
                String savedFileName = saveFileService.saveFile(repositoryPath, FILE_PREFIX, file);

                messageAttachmentFileDescriptions.add(MailAttachmentFileDescription.from(
                        file.getOriginalFilename(),
                        savedFileName,
                        (int) file.getSize(),
                        file.getContentType()
                ));
            }
            return messageAttachmentFileDescriptions;
        }catch (SaveFileException e) {
            logger.warn("Cannot save attachment files for message");
            throw new SendMessageException("Send message with attachments file is not possible");
        }
    }



}
