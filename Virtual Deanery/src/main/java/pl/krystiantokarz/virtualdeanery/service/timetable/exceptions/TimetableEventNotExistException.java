package pl.krystiantokarz.virtualdeanery.service.timetable.exceptions;

public class TimetableEventNotExistException extends Exception {

    public TimetableEventNotExistException() {
    }

    public TimetableEventNotExistException(String message) {
        super(message);
    }
}
