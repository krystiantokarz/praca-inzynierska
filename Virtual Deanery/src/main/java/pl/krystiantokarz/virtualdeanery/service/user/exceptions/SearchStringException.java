package pl.krystiantokarz.virtualdeanery.service.user.exceptions;

public class SearchStringException extends Exception{

    public SearchStringException() {
        super();
    }

    public SearchStringException(String message) {
        super(message);
    }

}
