package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.TimetableEventRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.time.LocalDate;
import java.util.List;

@Service
public class FindEventService {

    private static final Logger logger = LoggerFactory.getLogger(FindEventService.class);

    private TimetableEventRepository timetableEventRepository;
    private UserRepository userRepository;
    private PrivilegeService privilegeService;

    @Autowired
    public FindEventService(TimetableEventRepository timetableEventRepository, UserRepository userRepository,PrivilegeService privilegeService) {
        this.timetableEventRepository = timetableEventRepository;
        this.userRepository = userRepository;
        this.privilegeService = privilegeService;
    }

    public List<TimetableEvent> findAllTimetableEvents(){
        return timetableEventRepository.findAll();
    }

    public TimetableEvent findTimetableEventById(Long id) throws TimetableEventNotExistException {
        return timetableEventRepository.findTimetableEventByIdIfExist(id);
    }

    public TimetableEvent getTimetableEventToEdit(Long eventId, Long userId) throws TimetableEventNotExistException, PrivilegeException {
        TimetableEvent timetableEvent = timetableEventRepository.findTimetableEventByIdIfExist(eventId);
        privilegeService.checkUserHasAccessToEditEvent(timetableEvent, userId);
        return timetableEvent;
    }

    public List<TimetableEvent> findAllTimetableEventsForSelectedUser(Long userId) throws UserNotExistException {
        User user = userRepository.findUserByIdIfExist(userId);
        return timetableEventRepository.findByCreatingUser(user);
}
    public List<TimetableEvent> findAllTimetableEventsByEventDate(Integer day, Integer month, Integer year) {
        LocalDate localDate = LocalDate.of(year, month, day);
        return timetableEventRepository.findByEventDate(localDate);
    }
    public List<TimetableEvent> findAllTimetableEventsByEventDateForSelectedUser(Integer day, Integer month, Integer year, Long userId) throws UserNotExistException {
        User user = userRepository.findUserByIdIfExist(userId);
        LocalDate localDate = LocalDate.of(year, month, day);
        return timetableEventRepository.findByEventDateAndCreatingUser(localDate,user);
    }
}
