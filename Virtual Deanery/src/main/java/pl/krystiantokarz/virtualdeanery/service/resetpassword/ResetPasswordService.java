package pl.krystiantokarz.virtualdeanery.service.resetpassword;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.passwordresettoken.PasswordResetToken;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.EmailSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.resetpassword.ResetPasswordSendObject;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.resetpassword.ResetPasswordSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.PasswordTokenRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.utils.PasswordGenerator;
import pl.krystiantokarz.virtualdeanery.service.resetpassword.exception.CreatedTokenExpiryDateException;
import pl.krystiantokarz.virtualdeanery.service.resetpassword.exception.PasswordResetTokenNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.UUID;

@Service
public class ResetPasswordService {

    private static final Logger logger = LoggerFactory.getLogger(ResetPasswordService.class);

    @Value("${mail.sender.enabled}")
    private boolean mailEnabled;

    private final UserRepository userRepository;

    private final PasswordTokenRepository passwordTokenRepository;

    private final PasswordEncoder passwordEncoder;

    private final EmailSender emailSender;

    private static final int PASSWORD_LENGTH = 10;

    private static final int correctHoursTokenExpiry = 24;

    @Autowired
    public ResetPasswordService(UserRepository userRepository, PasswordTokenRepository passwordTokenRepository, PasswordEncoder passwordEncoder, EmailSender emailSender) {
        this.userRepository = userRepository;
        this.passwordTokenRepository = passwordTokenRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailSender = emailSender;
    }

    public void generatePasswordTokenForUser(String email, Locale locale) throws UserNotExistException, MessagingException {
        User user = userRepository.findUserByEmailIfExist(email);
        String token = UUID.randomUUID().toString();
        String newPassword = PasswordGenerator.generatePassword(PASSWORD_LENGTH);
        PasswordResetToken userToken = PasswordResetToken.from(
                token,
                passwordEncoder.encode(newPassword),
                user);
        passwordTokenRepository.save(userToken);
        sendEmailWithToken(user, token, newPassword,locale);
        logger.info("New password token was generated for user with id = {}", user.getId());
    }

    public void changePasswordWithToken(String token) throws PasswordResetTokenNotExistException, CreatedTokenExpiryDateException {
        PasswordResetToken passwordResetToken = passwordTokenRepository.findByTokenIfExist(token);
        validateExpiryDate(passwordResetToken);
        User user = passwordResetToken.getUser();
        user.changeUserPassword(passwordResetToken.getPassword());
        userRepository.save(user);
        logger.info("Generated token random password was changed for user with id = {}", user.getId());
    }

    private void validateExpiryDate(PasswordResetToken resetToken) throws CreatedTokenExpiryDateException {
        LocalDateTime expiryTokenDate = resetToken.getExpiryDate();
        if(expiryTokenDate.plusHours(correctHoursTokenExpiry).isBefore(LocalDateTime.now())){
            logger.debug("Cannot reset password token for user with id = {}. Last token is expired", resetToken.getUser().getId());
            throw new CreatedTokenExpiryDateException(String.format("Reset password token with id = %d and token value = %s is expired",resetToken.getId(), resetToken.getToken()));
        }
    }

    private void sendEmailWithToken(User user, String token ,String newPassword,Locale locale) throws MessagingException {
        if(mailEnabled){
            ResetPasswordSendObject resetPasswordSendObject = ResetPasswordSendObject.from(
                    user.getEmail(),
                    user.getLoginData().getLogin(),
                    newPassword,
                    token,
                    user.getFirstName(),
                    user.getLastName()
            );
            ResetPasswordSender resetPasswordSender = new ResetPasswordSender(resetPasswordSendObject);
            emailSender.sendEmail(resetPasswordSender,locale);
        }
    }
}
