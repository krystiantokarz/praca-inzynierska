package pl.krystiantokarz.virtualdeanery.service.timetable.exceptions;

public class ValidEventTimeException extends Exception {

    public ValidEventTimeException() {
    }

    public ValidEventTimeException(String message) {
        super(message);
    }
}
