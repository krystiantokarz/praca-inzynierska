package pl.krystiantokarz.virtualdeanery.service.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.account.exceptions.NotMatchesPasswordsException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@Service
public class ChangePasswordService {

    private static final Logger logger = LoggerFactory.getLogger(ChangePasswordService.class);

    private PasswordEncoder passwordEncoder;

    private UserRepository userRepository;

    @Autowired
    public ChangePasswordService(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    public void changePassword(Long userId, String enteredOldPassword, String enteredNewPassword) throws NotMatchesPasswordsException, UserNotExistException {

        User user = userRepository.findUserByIdIfExist(userId);
        String actuallyUserPassword = user.getLoginData().getPassword();

        boolean result = passwordEncoder.matches(enteredOldPassword,actuallyUserPassword);
        if(result){
            String newEncodePassword = passwordEncoder.encode(enteredNewPassword);
            user.changeUserPassword(newEncodePassword);
            userRepository.save(user);
            logger.info("Changed password for user with id = {}", userId);
        }else{
            logger.info("Cannot change password because entered old password = {} is not matches for user with id = {}", enteredNewPassword,userId);
            throw new NotMatchesPasswordsException(String.format("Cannot change password. Not matches entered old password = %s for user with id = %d",enteredNewPassword, userId));
        }

    }

}
