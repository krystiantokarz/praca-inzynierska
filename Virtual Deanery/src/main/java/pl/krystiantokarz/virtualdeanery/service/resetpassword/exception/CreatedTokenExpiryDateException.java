package pl.krystiantokarz.virtualdeanery.service.resetpassword.exception;

public class CreatedTokenExpiryDateException extends Exception {

    public CreatedTokenExpiryDateException() {
    }

    public CreatedTokenExpiryDateException(String message) {
        super(message);
    }
}
