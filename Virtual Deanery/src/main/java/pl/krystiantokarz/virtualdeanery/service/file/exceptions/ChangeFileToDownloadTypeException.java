package pl.krystiantokarz.virtualdeanery.service.file.exceptions;

public class ChangeFileToDownloadTypeException extends Exception {

    public ChangeFileToDownloadTypeException() {
    }

    public ChangeFileToDownloadTypeException(String message) {
        super(message);
    }
}
