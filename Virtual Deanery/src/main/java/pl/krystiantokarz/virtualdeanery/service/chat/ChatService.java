package pl.krystiantokarz.virtualdeanery.service.chat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.krystiantokarz.virtualdeanery.domain.group.chat.ChatMessage;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.chat.in.ChatMessageDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.ChatMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@Service
public class ChatService {

    private static final Logger logger = LoggerFactory.getLogger(ChatService.class);

    private GroupRepository groupRepository;

    private UserRepository userRepository;

    private ChatMessageRepository chatMessageRepository;

    private static final int pageSize = 15;

    @Autowired
    public ChatService(GroupRepository groupRepository, UserRepository userRepository, ChatMessageRepository chatMessageRepository) {
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
        this.chatMessageRepository = chatMessageRepository;
    }

    public ChatMessage addMessage(Long userId, Long groupId, ChatMessageDTO chatMessageDTO) throws UserNotExistException, GroupNotExistException, UserGroupNotExistException {
        User user = userRepository.findUserByIdIfExist(userId);
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        UserGroup userGroup = groupRepository.findUserGroupByUserAndGroup(user, group);

        ChatMessage chatMessage = ChatMessage.from(
                chatMessageDTO.getMessage(),
                group,
                user
        );

        chatMessageRepository.save(chatMessage);
        logger.debug("Added message by user with id = {} and group with id = {}", userId, groupId);
        return chatMessage;
    }

    public Page<ChatMessage> findChatMessagesForGroupByPage(Long groupId, Integer page) throws GroupNotExistException {
        Group group = groupRepository.findGroupByIdIfExist(groupId);
        return chatMessageRepository.findByGroupOrderByDateDesc(group,new PageRequest(page,pageSize));
    }



}
