package pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions;

public class MailMessageNotExistException extends Exception{

    public MailMessageNotExistException() {
    }

    public MailMessageNotExistException(String message) {
        super(message);
    }
}
