package pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions;

public class MailAttachmentFileDescriptionNotExist extends Exception {

    public MailAttachmentFileDescriptionNotExist() {
    }

    public MailAttachmentFileDescriptionNotExist(String message) {
        super(message);
    }
}
