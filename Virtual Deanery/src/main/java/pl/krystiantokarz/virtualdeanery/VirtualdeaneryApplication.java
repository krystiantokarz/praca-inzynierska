package pl.krystiantokarz.virtualdeanery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class VirtualdeaneryApplication {

	public static void main(String[] args) {

		SpringApplication.run(VirtualdeaneryApplication.class, args);

	}

}
