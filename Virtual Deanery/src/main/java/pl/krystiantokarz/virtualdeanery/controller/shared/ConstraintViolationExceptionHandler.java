package pl.krystiantokarz.virtualdeanery.controller.shared;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class ConstraintViolationExceptionHandler {

    @ExceptionHandler(value = { javax.validation.ConstraintViolationException.class })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public void handleResourceNotFoundException() {

    }
}
