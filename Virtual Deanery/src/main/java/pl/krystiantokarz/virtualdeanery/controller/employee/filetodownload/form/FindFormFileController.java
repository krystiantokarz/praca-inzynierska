package pl.krystiantokarz.virtualdeanery.controller.employee.filetodownload.form;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.domain.file.FileReadyToDownload;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.FileConverter;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.FindFileToDownloadService;

import java.util.List;


@RequestMapping(BaseURL.employeePath + EmployeeURL.filesPath + EmployeeURL.formFilePath)
@Controller("findFormFileForEmployeeController")
public class FindFormFileController {

    private static final Logger logger = LoggerFactory.getLogger(FindFormFileController.class);

    @Autowired
    private FindFileToDownloadService findFileToDownloadService;

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getFormFilesToDownloadBySelectedPage(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page, Model model){
        int pageSize = 10;
        Page<FileReadyToDownload> filesPage = findFileToDownloadService.getFileReadyToDownloadDescriptionsByFileTypeAndPageAndPageSize(FileType.FORM,page, pageSize);
        model.addAttribute("files", FileConverter.convertFilesToDTOs(filesPage.getContent()));
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("previousPage", page - 1);
        if(page == 0){
            model.addAttribute("blockPreviousPage", true);
        }
        int totalPageFromZero = filesPage.getTotalPages()-1;
        if(page == totalPageFromZero || totalPageFromZero < 0){
            model.addAttribute("blockNextPage", true);
        }
        return "/employee/file/form/files";
    }

    @GetMapping(value = "/search")
    public String searchFileDescriptions(@RequestParam String searchString, Model model){
        List<FileReadyToDownload> result = findFileToDownloadService.searchFilesReadyToDownloadBySearchedStringOriginalNameContainingIgnoreCaseAndFileType(searchString, FileType.FORM);
        if(result.isEmpty()){
            return "/employee/file/form/fragment/search-files-fragment :: not-found-result";
        }else{
            model.addAttribute("files", FileConverter.convertFilesToDTOs(result));
            return "/employee/file/form/fragment/search-files-fragment :: correct-result";
        }
    }



}
