package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.UserConverter;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;

import java.util.List;

@RequestMapping(BaseURL.administratorPath +  AdministratorURL.usersPath)
@Controller("findUseAdministratorController")
@SessionAttributes("logged-user-id")
public class FindUserController {

    @Autowired
    private FindUserService findUserService;

    private static final int DEFAULT_PAGE_SIZE = 10;

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getUsersBySelectedPage(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                         @ModelAttribute("logged-user-id") Long loggedUserId,
                                         Model model){
        Page<User> usersPage = findUserService.getUsersByPageAndPageSize(page, DEFAULT_PAGE_SIZE);
        model.addAttribute("users", UserConverter.convertUsersToDTOs(usersPage.getContent()));
        model.addAttribute("loggedUserId", loggedUserId);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("previousPage", page - 1);
        if(page == 0){
            model.addAttribute("blockPreviousPage", true);
        }
        int totalPageFromZero = usersPage.getTotalPages()-1;
        if(page == totalPageFromZero || totalPageFromZero < 0){
            model.addAttribute("blockNextPage", true);
        }
        return "/administrator/user/users";
    }


    @GetMapping(value = "/search")
    public String findUserInSystem(@RequestParam(value = "searchString") String searchString,
                                   @ModelAttribute("logged-user-id") Long loggedUserId,
                                   Model model){
        List<User> users = findUserService.searchUsers(searchString);
        if(users.isEmpty()){
            return "/administrator/user/fragment/search-users-fragment :: not-found-result";
        }else{
            model.addAttribute("users", UserConverter.convertUsersToDTOs(users));
            model.addAttribute("loggedUserId", loggedUserId);
            return "/administrator/user/fragment/search-users-fragment :: correct-result";
        }
    }




}
