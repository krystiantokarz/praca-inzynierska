package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.form;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.ChangeFileToDownloadTypeException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.ChangeReadyToDownloadFileTypeService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.filePath + AdministratorURL.formFilePath)
@Controller
public class ChangeFormTypeFileController {

    private static final Logger logger = LoggerFactory.getLogger(ChangeFormTypeFileController.class);

    @Autowired
    private ChangeReadyToDownloadFileTypeService changeFileTypeService;

    @PutMapping("/status/{fileId}")
    public ResponseEntity changeFormFileToArchiveFileStatus(@PathVariable("fileId") Long fileId){
        try {
            changeFileTypeService.changeFileFromFormToArchiveFileType(fileId);
            return ResponseEntity.ok().build();
        } catch (FileNotExistException e) {
            logger.info("Cannot remove form file - file with id = {} not exist",e);
            return ResponseEntity.notFound().build();
        } catch (ChangeFileToDownloadTypeException e) {
            logger.warn("Cannot change type form file with id = {}", fileId,e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (FileWithSelectedNameExistException e) {
            logger.info("Cannot change type form file with id = {} - selected file name = {} is not unique in archive repository", fileId, e);
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }
}
