package pl.krystiantokarz.virtualdeanery.controller.employee.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.domain.group.chat.ChatMessage;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.ChatMessageConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.chat.in.ChatMessageDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.chat.out.OutputChatMessageDTO;
import pl.krystiantokarz.virtualdeanery.service.chat.ChatService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;

import java.util.List;

@RequestMapping(BaseURL.employeePath + EmployeeURL.chat)
@Controller
@SessionAttributes("logged-user-id")
public class ChatController {

    @Autowired
    private ChatService chatService;


    @MessageMapping("/chat/{groupId}")
    @SendTo("/topic/messages")
    public OutputChatMessageDTO processChatMessages(@DestinationVariable("groupId") Long groupId,
                                                    ChatMessageDTO chatMessageDTO,
                                                    SimpMessageHeaderAccessor headerAccessor) throws Exception{

        Long userId = (Long) headerAccessor.getSessionAttributes().get("logged-user-id");
        ChatMessage chatMessage = chatService.addMessage(userId, groupId, chatMessageDTO);
        return ChatMessageConverter.convertChatMessageToOutputDTO(chatMessage);
    }

    @GetMapping(value = "/message/{groupId}")
    public @ResponseBody List<OutputChatMessageDTO> findOutputChatMessage(@PathVariable("groupId") Long groupId,
                                                                          @RequestParam("page") Integer page,
                                                                          @ModelAttribute("logged-user-id") Long loggedUserId) throws GroupNotExistException {
        Page<ChatMessage> chatMessagePage = chatService.findChatMessagesForGroupByPage(groupId, page);
        return ChatMessageConverter.convertChatMessageListToOutputDTOList(chatMessagePage.getContent());
    }
}
