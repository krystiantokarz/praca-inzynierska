package pl.krystiantokarz.virtualdeanery.controller.administrator.messagebox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.controller.shared.PrepareDownloadFileComponent;
import pl.krystiantokarz.virtualdeanery.service.messagebox.DownloadMessageAttachmentFileService;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.DownloadMessageAttachmentFileException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailAttachmentFileDescriptionNotExist;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.mailPath)
@Controller("downloadMessageAttachmentFileForAdministratorController")
@SessionAttributes("logged-user-id")
public class DownloadMessageAttachmentFileController {

    private static final Logger logger = LoggerFactory.getLogger(DownloadMessageAttachmentFileController.class);

    @Autowired
    private DownloadMessageAttachmentFileService downloadMessageAttachmentService;

    @Autowired
    private PrepareDownloadFileComponent prepareDownloadFileComponent;

    @GetMapping(value = "/attachment")
    public ResponseEntity downloadMessageAttachmentFile(@RequestParam("id") Long attachmentFileId,
                                                        @ModelAttribute("logged-user-id") Long loggedUserId,
                                                        HttpServletResponse response) throws IOException {
        try {
            HttpServletResponse httpServletResponse = prepareDownloadFileComponent.prepareResponseForMessageAttachment(response,attachmentFileId,loggedUserId);
            downloadMessageAttachmentService.downloadFile(attachmentFileId, loggedUserId, httpServletResponse.getOutputStream());
            return ResponseEntity.ok().build();
        } catch (MailAttachmentFileDescriptionNotExist e) {
            logger.info("Cannot download message attachment file by administrator -  file attachment with id = {} not exits", attachmentFileId,e);
            return ResponseEntity.notFound().build();
        } catch (DownloadMessageAttachmentFileException e) {
            logger.warn("Cannot download message attachment file with id = {} by administrator", attachmentFileId,e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot download message attachment file with id = {}. Administrator with id = {} not exist",loggedUserId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

}
