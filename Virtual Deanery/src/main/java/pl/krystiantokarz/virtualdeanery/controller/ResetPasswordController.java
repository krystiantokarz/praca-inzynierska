package pl.krystiantokarz.virtualdeanery.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.service.resetpassword.ResetPasswordService;
import pl.krystiantokarz.virtualdeanery.service.resetpassword.exception.CreatedTokenExpiryDateException;
import pl.krystiantokarz.virtualdeanery.service.resetpassword.exception.PasswordResetTokenNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.mail.MessagingException;
import java.util.Locale;

@Controller(value = "/")
public class ResetPasswordController {

    @Autowired
    private ResetPasswordService mainService;

    @PutMapping("password/reset")
    public ResponseEntity generateResetPasswordTokenForSelectedUserEmail(@RequestParam("email") String email){
        try {
            Locale locale = LocaleContextHolder.getLocale();
            mainService.generatePasswordTokenForUser(email,locale);
            return ResponseEntity.ok().build();
        } catch (UserNotExistException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (MessagingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }


    @GetMapping("password/reset")
    public String acceptChangePasswordUsingToken(@RequestParam("token") String token, Model model){
        try {
            mainService.changePasswordWithToken(token);
            model.addAttribute("token",true);
            return "/information-page";
        } catch (PasswordResetTokenNotExistException | CreatedTokenExpiryDateException e) {
            model.addAttribute("token",false);
            return "/information-page";
        }
    }
}
