package pl.krystiantokarz.virtualdeanery.controller.employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.LoginData;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.domain.user.role.Role;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RequestMapping(BaseURL.employeePath)
@Controller
@SessionAttributes("logged-user-id")
public class MainEmployeeController {

    final Logger logger = LoggerFactory.getLogger(MainEmployeeController.class);

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getHomeAdministratorPage(){
        return "/employee/home";
    }

    @GetMapping("/loggedUser")
    public @ResponseBody Long getActuallyLoggedUserId(@ModelAttribute("logged-user-id") Long loggedUserId){
        return loggedUserId;
    }

}
