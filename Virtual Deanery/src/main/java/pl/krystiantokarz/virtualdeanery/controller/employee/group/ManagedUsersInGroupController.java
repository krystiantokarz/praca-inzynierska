package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.service.group.ManagedUsersInGroupService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@RequestMapping(BaseURL.employeePath + EmployeeURL.groupPath)
@Controller
@SessionAttributes("logged-user-id")
public class ManagedUsersInGroupController {

    private static final Logger logger = LoggerFactory.getLogger(ManagedUsersInGroupController.class);

    @Autowired
    private ManagedUsersInGroupService managedUsersInGroupService;

    @PutMapping(value = "/{groupId}/user/{userId}/access/admin")
    public ResponseEntity addAdminAccess(@PathVariable("groupId") Long groupId,
                                         @PathVariable("userId") Long userId,
                                         @ModelAttribute("logged-user-id") Long loggedUserId) {
        try {
            managedUsersInGroupService.addAdminAccess(userId, groupId,loggedUserId);
            return ResponseEntity.ok().build();
        } catch (UserGroupNotExistException e) {
            logger.info("Cannot add admin access for user in group with id = {} - user is not a group member",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (GroupNotExistException e) {
            logger.info("Cannot add admin access for user in group with id = {} - group not exist",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot add admin access for user with id = {} in group with id = {} - user not exist",userId,groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (PrivilegeException e) {
            logger.info("Cannot add admin access for user with id = {} in group with id = {}. Logged user with id = {} not have a privilege to do it",userId,groupId,loggedUserId,e);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @PutMapping(value = "/{groupId}/user/{userId}/access/member")
    public ResponseEntity removeAdminAccess(@PathVariable("groupId") Long groupId,
                                            @PathVariable("userId") Long userId,
                                            @ModelAttribute("logged-user-id") Long loggedUserId) {
        try {
            managedUsersInGroupService.removeAdminAccess(userId, groupId,loggedUserId);
            return ResponseEntity.ok().build();
        } catch (UserGroupNotExistException e) {
            logger.info("Cannot remove admin access for user in group with id = {} - user is not a group member",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (GroupNotExistException e) {
            logger.info("Cannot remove admin access for user in group with id = {} - group not exist",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot remove admin access for user with id = {} in group with id = {} - user not exist",userId,groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (PrivilegeException e) {
            logger.info("Cannot remove admin access for user with id = {} in group with id = {}. Logged user with id = {} not have a privilege to do it ",userId,groupId,loggedUserId,e);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @DeleteMapping(value = "/{groupId}/user/{userId}")
    public ResponseEntity removeUserFromGroup(@PathVariable("groupId") Long groupId,
                                              @PathVariable("userId") Long userId,
                                              @ModelAttribute("logged-user-id") Long loggedUserId) {
        try {
            managedUsersInGroupService.removeUserFromGroup(userId, groupId,loggedUserId);
            return ResponseEntity.ok().build();
        } catch (UserGroupNotExistException e) {
            logger.info("Cannot remove user from group with id = {} - user is not a group member",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (GroupNotExistException e) {
            logger.info("Cannot remove user from group with id = {} - group not exist",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot remove user from group with id = {} in group with id = {} - user not exist",userId,groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (PrivilegeException e) {
            logger.info("Cannot remove user from group with id = {} in group with id = {}. Logged user with id = {} not have a privilege to do it",userId,groupId,loggedUserId,e);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
