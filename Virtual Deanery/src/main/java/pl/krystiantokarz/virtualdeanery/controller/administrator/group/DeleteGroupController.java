package pl.krystiantokarz.virtualdeanery.controller.administrator.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.service.group.DeleteGroupService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.groupPath)
@Controller
public class DeleteGroupController {

    private static final Logger logger = LoggerFactory.getLogger(DeleteGroupController.class);

    @Autowired
    private DeleteGroupService deleteGroupService;

    @DeleteMapping(value = "/{groupId}")
    public ResponseEntity deleteGroupBySelectedGroupId(@PathVariable("groupId") Long groupId){
        try {
            deleteGroupService.deleteGroupBySystemAdministrator(groupId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (GroupNotExistException e) {
            logger.info("Cannot delete group by administrator - group with id {} not exist",e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
