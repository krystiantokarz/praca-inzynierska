package pl.krystiantokarz.virtualdeanery.controller.employee.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.UserConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserInformationDTO;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.validation.constraints.Size;
import java.util.List;

@RequestMapping(BaseURL.employeePath)
@Controller("findUserEmployeeController")
@SessionAttributes("logged-user-id")
public class FindUserController {

    private static final Logger logger = LoggerFactory.getLogger(FindUserController.class);

    private static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private FindUserService findUserService;

    @GetMapping(value = EmployeeURL.usersPath, produces = MediaType.TEXT_HTML_VALUE)
    public String getUsersBySelectedPage(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page, Model model, @ModelAttribute("logged-user-id") Long loggedUserId){
        Page<User> usersPage = findUserService.getUsersByPageAndPageSize(page, DEFAULT_PAGE_SIZE);
        model.addAttribute("users", UserConverter.convertUsersToDTOs(usersPage.getContent()));
        model.addAttribute("loggedUserId", loggedUserId);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("previousPage", page - 1);
        if(page == 0){
            model.addAttribute("blockPreviousPage", true);
        }
        int totalPageFromZero = usersPage.getTotalPages()-1;
        if(page == totalPageFromZero || totalPageFromZero < 0){
            model.addAttribute("blockNextPage", true);
        }
        return "/employee/user/users";
    }


    @GetMapping(value = EmployeeURL.userPath + "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findUser(@PathVariable(value = "userId") Long userId){
        try {
            User user = findUserService.findUserById(userId);
            FindUserInformationDTO result = new FindUserInformationDTO(user.getId(), user.getFirstName(), user.getLastName(), user.getEmail());
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (UserNotExistException e) {
            logger.info("Cannot find user with id = {}", userId, e);
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping(value = EmployeeURL.usersPath + "/search")
    public String findUsersInSystem(@RequestParam(value = "searchString") @Size(min = 3) String searchString, Model model){
        List<User> users = findUserService.searchUsers(searchString);
        if(users.isEmpty()){
            return "/employee/user/fragment/search-users-fragment :: not-found-result";
        }else{
            model.addAttribute("users", UserConverter.convertUsersToDTOs(users));
            return "/employee/user/fragment/search-users-fragment :: correct-result";
        }
    }
}
