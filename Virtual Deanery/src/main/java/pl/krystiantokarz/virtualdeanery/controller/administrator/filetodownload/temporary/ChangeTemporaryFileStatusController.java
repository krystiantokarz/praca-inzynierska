package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.temporary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.ChangeFileToDownloadTypeException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.ChangeFileStatusService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.RemoveFileException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.Locale;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.filePath)
@Controller
@SessionAttributes("logged-user-id")
public class ChangeTemporaryFileStatusController {

    private static final Logger logger = LoggerFactory.getLogger(ChangeTemporaryFileStatusController.class);

    @Autowired
    private ChangeFileStatusService changeFileStatusService;

    @PutMapping("/{fileId}")
    public ResponseEntity acceptFile(@PathVariable("fileId") Long fileId,@ModelAttribute("logged-user-id") Long loggedUserId){
        try {
            Locale locale = LocaleContextHolder.getLocale();
            changeFileStatusService.acceptFile(fileId,locale);
            return ResponseEntity.ok().build();
        } catch (FileNotExistException e) {
            logger.info("Cannot accept temporary file - file with id {} not exist",e);
            return ResponseEntity.notFound().build();
        } catch (ChangeFileToDownloadTypeException e){
            logger.warn("Cannot accept temporary file with id = {}",fileId,e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot accept temporary file with id = {} - user with id {} not exist",fileId, loggedUserId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (FileWithSelectedNameExistException e) {
            logger.info("Cannot accept temporary file with id = [} - file with with selected name exist in repository", fileId,e);
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @DeleteMapping("/{fileId}")
    public ResponseEntity rejectFile(@PathVariable("fileId") Long fileId,@ModelAttribute("logged-user-id") Long loggedUserId){
        try {
            Locale locale = LocaleContextHolder.getLocale();
            changeFileStatusService.rejectFile(fileId,locale);
            return ResponseEntity.ok().build();
        } catch (FileNotExistException e) {
            logger.info("Cannot reject temporary file - file with id {} not exist",e);
            return ResponseEntity.notFound().build();
        } catch (RemoveFileException e){
            logger.warn("Cannot rejected temporary file with id = {}",fileId,e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot reject temporary file with id = {} - user with id {} not exist", fileId,loggedUserId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
