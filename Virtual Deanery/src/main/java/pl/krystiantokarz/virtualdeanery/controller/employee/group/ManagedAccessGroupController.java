package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.controller.shared.ConstraintViolationExceptionHandler;
import pl.krystiantokarz.virtualdeanery.service.group.ManagedAccessGroupService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.NotMatchesGroupPasswordException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@RequestMapping(BaseURL.employeePath + EmployeeURL.groupPath)
@Controller
@SessionAttributes("logged-user-id")
@Validated
public class ManagedAccessGroupController extends ConstraintViolationExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ManagedAccessGroupController.class);

    @Autowired
    private ManagedAccessGroupService managedAccessGroupService;


    @PutMapping("/access/{groupId}")
    public ResponseEntity joinIntoSelectedGroup(@PathVariable("groupId") Long groupId,
                                                @NotNull @Size(min = 5) @RequestParam("pass") String password,
                                                @ModelAttribute("logged-user-id") Long loggedUserId){
        try {
            managedAccessGroupService.joinIntoSelectedGroup(groupId,loggedUserId,password);
            return ResponseEntity.ok().build();
        } catch (UserNotExistException e) {
            logger.info("Cannot join into group with id = {} access - user with id = {} not exist", groupId,loggedUserId, e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (GroupNotExistException e) {
            logger.info("Cannot join into group - group with id = {} not exist", groupId, e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (NotMatchesGroupPasswordException e) {
            logger.info("Cannot join into group - not matches group password", groupId, e);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        }
    }


    @DeleteMapping("/access/{groupId}")
    public ResponseEntity leaveSelectedGroup(@PathVariable("groupId") Long groupId, @ModelAttribute("logged-user-id") Long loggedUserId){
        try {
            managedAccessGroupService.leaveFromSelectedGroup(groupId,loggedUserId);
            return ResponseEntity.ok().build();
        } catch (GroupNotExistException e) {
            logger.info("Cannot leave from group with id = {} - group not exist",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot leave from group with id = {} - user with id = {} not exist",groupId,loggedUserId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserGroupNotExistException e) {
            logger.info("Cannot leave from group with id = {} - user with id = {} is not a group members",groupId,loggedUserId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
