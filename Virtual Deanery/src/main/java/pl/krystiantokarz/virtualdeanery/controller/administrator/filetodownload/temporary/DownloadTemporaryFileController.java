package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.temporary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.controller.shared.PrepareDownloadFileComponent;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.DownloadFileToDownloadService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.DownloadFileToDownloadException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.filePath)
@Controller
public class DownloadTemporaryFileController {

    private static final Logger logger = LoggerFactory.getLogger(DownloadTemporaryFileController.class);

    @Autowired
    private DownloadFileToDownloadService downloadFileToDownloadService;

    @Autowired
    private PrepareDownloadFileComponent prepareDownloadFileComponent;

    @GetMapping()
    public ResponseEntity downloadTemporaryFile(@RequestParam("fileId") Long fileId, HttpServletResponse response) throws IOException {

        try {
            HttpServletResponse httpServletResponse = prepareDownloadFileComponent.prepareResponseForFileToDownload(response, fileId);
            downloadFileToDownloadService.downloadFile(fileId, httpServletResponse.getOutputStream());
            return ResponseEntity.ok().build();
        } catch (FileNotExistException e) {
            logger.info("Cannot download temporary file - file with id {} not exist",e);
            return ResponseEntity.notFound().build();
        } catch (DownloadFileToDownloadException e) {
            logger.warn("Cannot download temporary file with id {}",fileId,e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
