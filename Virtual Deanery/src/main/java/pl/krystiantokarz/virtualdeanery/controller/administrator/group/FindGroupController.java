package pl.krystiantokarz.virtualdeanery.controller.administrator.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.GroupConverter;
import pl.krystiantokarz.virtualdeanery.service.group.FindGroupService;

import java.util.List;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.groupsPath)
@Controller(value = "adminFindGroupController")
public class FindGroupController {

    @Autowired
    private FindGroupService findGroupService;


    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getGroupsBySelectedPage(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page, Model model){
        int pageSize = 10;
        Page<Group> groups = findGroupService.getGroupsByPageAndSize(page, pageSize);
        model.addAttribute("groups", GroupConverter.convertGroupsToGroupsDTOs(groups.getContent()));
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("previousPage", page - 1);
        if(page == 0){
            model.addAttribute("blockPreviousPage", true);
        }
        int totalPageFromZero = groups.getTotalPages()-1;
        if(page == totalPageFromZero || totalPageFromZero < 0){
            model.addAttribute("blockNextPage", true);
        }
        return "/administrator/group/groups";
    }

    @GetMapping(value = "/search")
    public String findGroupsInSystem(@RequestParam(value = "searchString") String searchString, Model model){
        List<Group> groups = findGroupService.searchGroups(searchString);
        if(groups.isEmpty()){
            return "/administrator/group/fragment/search-groups-fragment :: not-found-result";
        }else{
            model.addAttribute("groups", GroupConverter.convertGroupsToGroupsDTOs(groups));
            return "/administrator/group/fragment/search-groups-fragment :: correct-result";
        }
    }
}
