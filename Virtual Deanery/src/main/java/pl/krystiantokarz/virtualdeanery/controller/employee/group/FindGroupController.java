package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.GroupConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.UserGroupConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.GroupDTO;
import pl.krystiantokarz.virtualdeanery.service.group.FindGroupService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.List;

@RequestMapping(BaseURL.employeePath)
@Controller(value = "employeeFindGroupController")
@SessionAttributes("logged-user-id")
public class FindGroupController {

    private static final Logger logger = LoggerFactory.getLogger(FindGroupController.class);

    private static final int DEFAULT_PAGE_SIZE = 10;

    @Autowired
    private FindGroupService findGroupService;

    @GetMapping(value = EmployeeURL.groupsPath, produces = MediaType.TEXT_HTML_VALUE)
    public String getGroupsBySelectedPage(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                          @ModelAttribute("logged-user-id") Long loggedUserId,
                                          Model model){
        Page<Group> groupsPage = findGroupService.getGroupsByPageAndSize(page, DEFAULT_PAGE_SIZE);
        List<Group> userGroups = findGroupService.getAllGroupsForUser(loggedUserId);
        model.addAttribute("groups", GroupConverter.convertGroupsToGroupsWithAccessDTOs(groupsPage.getContent(),userGroups));
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("previousPage", page - 1);
        if(page == 0){
            model.addAttribute("blockPreviousPage", true);
        }
        int totalPageFromZero = groupsPage.getTotalPages()-1;
        if(page == totalPageFromZero || totalPageFromZero < 0){
            model.addAttribute("blockNextPage", true);
        }
        return "/employee/group/groups";
    }

    @GetMapping(value = EmployeeURL.groupsPath + "/user", produces = MediaType.TEXT_HTML_VALUE)
    public String getGroupsForUserBySelectedPage(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                 @ModelAttribute("logged-user-id") Long loggedUserId,
                                                 Model model){
        Page<Group> groups = findGroupService.getAllGroupsForUserWithPageAndSize(loggedUserId,page,DEFAULT_PAGE_SIZE);
        model.addAttribute("groups", GroupConverter.convertGroupsToGroupsDTOs(groups.getContent()));
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("previousPage", page - 1);
        if(page == 0){
            model.addAttribute("blockPreviousPage", true);
        }
        int totalPageFromZero = groups.getTotalPages()-1;
        if(page == totalPageFromZero || totalPageFromZero < 0){
            model.addAttribute("blockNextPage", true);
        }

        return "/employee/group/groups-for-user";
    }

    @GetMapping(value = EmployeeURL.groupPath + "/{groupId}/description")
    public ResponseEntity findGroupById(@PathVariable("groupId") Long groupId) {
        try {
            Group groupById = findGroupService.findGroupById(groupId);
            GroupDTO result = GroupConverter.convertSingleGroupToDTO(groupById);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (GroupNotExistException e) {
            logger.info("Cannot find group with id = {}", groupId, e);
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping(value = EmployeeURL.groupsPath + "/search")
    public String findGroupInSystem(@RequestParam(value = "searchString") String searchString, Model model, @ModelAttribute("logged-user-id") Long loggedUserId){
        List<Group> groups = findGroupService.searchGroups(searchString);
        List<Group> groupsForEmployee = findGroupService.getAllGroupsForUser(loggedUserId);
        if(groups.isEmpty()){
            return "/employee/group/fragment/search-groups-fragment :: not-found-result";
        }else{
            model.addAttribute("groups", GroupConverter.convertGroupsToGroupsWithAccessDTOs(groups,groupsForEmployee));
            return "/employee/group/fragment/search-groups-fragment :: correct-result";
        }
    }


    @GetMapping(value = EmployeeURL.groupPath + "/{groupId}")
    public String getSelectedGroup(@PathVariable("groupId") Long groupId, Model model,@ModelAttribute("logged-user-id") Long loggedUserId){
        try {
            UserGroup userGroup = findGroupService.findUserGroup(loggedUserId,groupId);
            List<UserGroup> userGroups = findGroupService.findUserGroupsForSelectedGroup(userGroup.getGroup().getId());
            model.addAttribute("name", userGroup.getGroup().getName());
            model.addAttribute("groupId", userGroup.getGroup().getId());
            model.addAttribute("description", userGroup.getGroup().getDescription());
            model.addAttribute("members", UserGroupConverter.convertUserGroupToUserInGroupDTO(userGroups));
            model.addAttribute("loggedUserId", loggedUserId);
            model.addAttribute("adminAccount", findGroupService.checkLoggedUserIsAdminInSelectedGroup(loggedUserId,groupId));
            return "employee/group/selected-group";
        } catch (GroupNotExistException e) {
            logger.info("Cannot find group with id = {} - group not exist", groupId, e);
            return "employee/error/error-page";
        }catch (UserGroupNotExistException e) {
            logger.info("Cannot find group with id = {} - user not with id = {} is not a group member", groupId, loggedUserId, e);
            return "employee/error/error-page";
        } catch (UserNotExistException e) {
            logger.info("Cannot find group with id = {} - user with id = {} not exist", groupId,loggedUserId, e);
            return "employee/error/error-page";
        }
    }
}
