package pl.krystiantokarz.virtualdeanery.controller.administrator.messagebox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.MessageConverter;
import pl.krystiantokarz.virtualdeanery.service.messagebox.FindMessageService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailMessageNotExistException;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.mailPath)
@Controller("findMessageForAdministratorController")
@SessionAttributes("logged-user-id")
public class FindMessageController {

    private static final Logger logger = LoggerFactory.getLogger(FindMessageController.class);

    private static final int DEFAULT_PAGE_SIZE = 20;

    @Autowired
    private FindMessageService findMessageService;

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getPageForMessageBoxForSelectedUser(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page, @ModelAttribute("logged-user-id") Long loggedUserId, Model model) throws UserNotExistException {

        Page<MailMessageEntity> messagesPage = findMessageService.findMessagesForUserByPageAndPageSize(loggedUserId, page, DEFAULT_PAGE_SIZE);
        model.addAttribute("messages", MessageConverter.convertMessagesToMessageDescriptionDTOs(messagesPage.getContent()));
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("previousPage", page - 1);
        if (page == 0) {
            model.addAttribute("blockPreviousPage", true);
        }
        int totalPageFromZero = messagesPage.getTotalPages() - 1;
        if (page == totalPageFromZero || totalPageFromZero < 0) {
            model.addAttribute("blockNextPage", true);
        }
        return "administrator/messagebox/message-box";
    }

    @GetMapping(value = "/{messageId}", produces = MediaType.TEXT_HTML_VALUE)
    public String getMessage(@PathVariable("messageId") Long messageId, @ModelAttribute("logged-user-id") Long loggedUserId, Model model) {

        try {
            MailMessageEntity message = findMessageService.findMessageByIdForSelectedUserAndChangeMessageStatusIfNotReadYet(messageId, loggedUserId);
            model.addAttribute("messageContent", MessageConverter.convertMessageToMessageContentDTO(message));
            return "administrator/messagebox/selected-message";
        } catch (MailMessageNotExistException e) {
            logger.info("Message with id = {} not exits", messageId,e);
            return "/administrator/error/error-page";
        } catch (UserNotExistException e) {
            logger.info("Cannot find mail message with id = {}. Administrator with id = {} not exist",loggedUserId,e);
            return "/administrator/error/error-page";
        }


    }
}
