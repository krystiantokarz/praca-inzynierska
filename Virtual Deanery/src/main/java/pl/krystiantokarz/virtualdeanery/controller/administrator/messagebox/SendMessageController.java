package pl.krystiantokarz.virtualdeanery.controller.administrator.messagebox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.in.CreateMessageDTO;
import pl.krystiantokarz.virtualdeanery.service.messagebox.SendMessageService;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.SendMessageException;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.validation.Valid;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.mailPath)
@Controller("sendMessageForAdministratorController")
@SessionAttributes("logged-user-id")
public class SendMessageController {

    private static final Logger logger = LoggerFactory.getLogger(SendMessageController.class);

    @Autowired
    private FindUserService findUserService;

    @Autowired
    private SendMessageService sendMessageService;


    @GetMapping(value = "/message/{userId}", produces = MediaType.TEXT_HTML_VALUE)
    public String getPageForSendMessageToSelectedUser(@PathVariable("userId") Long userId, Model model){

        try {
            User user = findUserService.findUserById(userId);
            model.addAttribute("firstName",user.getFirstName());
            model.addAttribute("lastName",user.getLastName());
            model.addAttribute("email", user.getEmail());
            model.addAttribute("id", user.getId());
            return "administrator/messagebox/send-message";
        } catch (UserNotExistException e) {
            logger.info("Cannot get page for send message - administrator with id {} not exist",userId,e);
            return "administrator/error/error-page";
        }
    }

    @PostMapping(value = "/message")
    public ResponseEntity sendMessageForSelectedUser(@Valid @ModelAttribute CreateMessageDTO messageDTO, BindingResult result, @ModelAttribute("logged-user-id") Long loggedUserId) {
        if(result.hasErrors()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }else {
            try {
                sendMessageService.sendMessage(messageDTO, loggedUserId);
                return ResponseEntity.ok().build();
            } catch (UserNotExistException e) {
                logger.info("Cannot send message by administrator - user with id {} not exist", e);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } catch (SendMessageException e) {
                logger.warn("Cannot send message by administrator ", e);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        }
    }
}
