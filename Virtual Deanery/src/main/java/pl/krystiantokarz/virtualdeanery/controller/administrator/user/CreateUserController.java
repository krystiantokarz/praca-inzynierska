package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.infrastructure.errorcode.administrator.ErrorCode;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.CreateUserDTO;
import pl.krystiantokarz.virtualdeanery.service.user.CreateUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;

import javax.validation.Valid;
import java.util.Locale;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.userPath)
@Controller
public class CreateUserController {

    private static final Logger logger = LoggerFactory.getLogger(CreateUserController.class);

    @Autowired
    private CreateUserService createUserService;

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getPageToAddNewUser(){
        return "/administrator/user/user-add";
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createNewUser(@RequestBody @Valid CreateUserDTO createUserDTO, BindingResult result){
        if(result.hasErrors()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }else{
            Locale locale = LocaleContextHolder.getLocale();
            return createUser(createUserDTO,locale);
        }
    }

    private ResponseEntity createUser(CreateUserDTO createUserDTO,Locale locale){
        try {
            createUserService.createNewUser(createUserDTO,locale);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (UserWithSelectedEmailExistException e) {
            logger.info("Cannot createWithAttachments user - selected email = {} is occupied",createUserDTO.getEmail(),e);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ErrorCode.USER_EMAIL_EXIST);
        }

    }
}
