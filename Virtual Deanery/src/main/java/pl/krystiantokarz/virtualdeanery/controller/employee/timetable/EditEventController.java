package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.infrastructure.errorcode.employee.ErrorCode;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.TimetableEventConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.EditEventDTO;
import pl.krystiantokarz.virtualdeanery.service.timetable.EditEventService;
import pl.krystiantokarz.virtualdeanery.service.timetable.FindEventService;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.EventTimeLimitException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventTimeException;

import javax.validation.Valid;

@RequestMapping(BaseURL.employeePath + EmployeeURL.timetable)
@Controller
@SessionAttributes("logged-user-id")
public class EditEventController {

    private static final Logger logger = LoggerFactory.getLogger(EditEventController.class);

    @Autowired
    private EditEventService editEventService;

    @Autowired
    private FindEventService findEventService;


    @GetMapping(value = "/event/{eventId}/data")
    public String getPageAndEventDataToEdit(@PathVariable("eventId") Long eventId, Model model, @ModelAttribute("logged-user-id") Long loggedUserId) {

        try {
            TimetableEvent timetableEvent = findEventService.getTimetableEventToEdit(eventId, loggedUserId);
            model.addAttribute("event", TimetableEventConverter.convertEventToTimetableEventDescriptionDTO(timetableEvent));
            return "/employee/timetable/edit-event";
        } catch (PrivilegeException e) {
            logger.info("Cannot get edit timetable event with id = {} by user with id = {}. User not have privilege to do id", eventId, loggedUserId,e);
            return "/employee/error/error-page";
        } catch (TimetableEventNotExistException e) {
            logger.info("Cannot edit timetable event with id = {} - event not exist",eventId,e);
            return "/employee/error/error-page";
        }

    }

    @PutMapping(value = "/event/{eventId}/data")
    public ResponseEntity editSelectedEvent(@RequestBody @Valid EditEventDTO editEventDTO, BindingResult result, @ModelAttribute("logged-user-id") Long loggedUserId) {
        if(result.hasErrors()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }else {
            try {
                editEventService.editSelectedEventByUser(editEventDTO, loggedUserId);
                return ResponseEntity.status(HttpStatus.OK).build();
            } catch (TimetableEventNotExistException e) {
                logger.info("Cannot edit timetable event with id = {} - event not exist",editEventDTO.getEventId(),e);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } catch (ValidEventTimeException e) {
                logger.info("Cannot edit timetable event with id = {} - time is not validate",editEventDTO.getEventId(),e);
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ErrorCode.INVALIDATE_TIME);
            } catch (EventTimeLimitException e) {
                logger.info("Cannot edit timetable event with id = {} - the limit is exceeded",editEventDTO.getEventId(),e);
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ErrorCode.TIME_LIMIT);
            } catch (PrivilegeException e) {
                logger.info("Cannot edit timetable event with id = {} by user with id = {}. User not have privilege to do id" ,editEventDTO.getEventId(), loggedUserId,e);
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
        }
              
    }
}
