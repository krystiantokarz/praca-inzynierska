package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.service.user.ChangeUserStatusService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.Locale;


@RequestMapping(BaseURL.administratorPath + AdministratorURL.userPath)
@Controller
public class ChangeUserStatusController {

    private static final Logger logger = LoggerFactory.getLogger(ChangeUserStatusController.class);

    @Autowired
    private ChangeUserStatusService changeUserStatusService;

    @PutMapping(value = "/{userId}/disable")
    public ResponseEntity disableSelectedUser(@PathVariable("userId") Long userId){
        try {
            Locale locale = LocaleContextHolder.getLocale();
            changeUserStatusService.disableUser(userId,locale);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot disable user - user with id = {} not exist",e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping(value = "/{userId}/enable")
    public ResponseEntity enableSelectedUser(@PathVariable("userId") Long userId){
        try {
            Locale locale = LocaleContextHolder.getLocale();
            changeUserStatusService.enableUser(userId,locale);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot enable user - user with id = {} not exist",userId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
