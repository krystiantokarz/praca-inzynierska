package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.TimetableEventConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventCalendarDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventDescriptionDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventTimetableDTO;
import pl.krystiantokarz.virtualdeanery.service.timetable.FindEventService;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.List;

@RequestMapping(BaseURL.employeePath + EmployeeURL.timetable)
@Controller
@SessionAttributes("logged-user-id")
public class FindTimetableEventController {

    private static final Logger logger = LoggerFactory.getLogger(FindTimetableEventController.class);


    @Autowired
    private FindEventService findEventService;

    @GetMapping(value = "/events", produces = MediaType.TEXT_HTML_VALUE)
    public String getAllEventsPage(){
        return "/employee/timetable/event";
    }

    @GetMapping(value = "/events/user", produces = MediaType.TEXT_HTML_VALUE)
    public String getUserEventsPage(@ModelAttribute("logged-user-id") Long loggedUserId){
        return "/employee/timetable/user-event";
    }
    @GetMapping(value = "event/user/{eventId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findEventDescription(@PathVariable("eventId") Long eventId){
        try {
            TimetableEvent timetableEvent = findEventService.findTimetableEventById(eventId);
            EventDescriptionDTO result = TimetableEventConverter.convertEventToTimetableEventDescriptionDTO(timetableEvent);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (TimetableEventNotExistException e) {
            logger.info("Cannot find timetable event with id = {}", eventId, e);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping(value = "/events/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<EventCalendarDTO> findAllEventCalendarDescriptions() {

        List<TimetableEvent> events = findEventService.findAllTimetableEvents();
        return TimetableEventConverter.convertEventsToCalendarEventDTOs(events);
    }

    @GetMapping(value = "/events/user/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<EventCalendarDTO> findAllEventCalendarDescriptionsForSelectedUser(@ModelAttribute("logged-user-id") Long loggedUserId) {

        List<TimetableEvent> events = null;
        try {
            events = findEventService.findAllTimetableEventsForSelectedUser(loggedUserId);
        } catch (UserNotExistException e) {
            e.printStackTrace();
        }
        return TimetableEventConverter.convertEventsToCalendarEventDTOs(events);
    }



    @GetMapping(value = "/events/date/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<EventTimetableDTO> findAllEventTimetableDescriptions(@RequestParam("day") Integer day,
                                                                                   @RequestParam("month") Integer month,
                                                                                   @RequestParam("year") Integer year,
                                                                                   @ModelAttribute("logged-user-id") Long loggedUserId){
        List<TimetableEvent> events = findEventService.findAllTimetableEventsByEventDate(day, month, year);
        return TimetableEventConverter.convertEventsToTimetableEventDTOs(events);
    }

    @GetMapping(value = "/events/date/user/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<EventTimetableDTO> findAllEventTimetableDescriptionsForSelectedUser(@RequestParam("day") Integer day,
                                                                                                  @RequestParam("month") Integer month,
                                                                                                  @RequestParam("year") Integer year,
                                                                                                  @ModelAttribute("logged-user-id") Long loggedUserId){
        List<TimetableEvent> events = null;
        try {
            events = findEventService.findAllTimetableEventsByEventDateForSelectedUser(day, month, year,loggedUserId);
        } catch (UserNotExistException e) {
            e.printStackTrace();
        }
        return TimetableEventConverter.convertEventsToTimetableEventDTOs(events);
    }


}
