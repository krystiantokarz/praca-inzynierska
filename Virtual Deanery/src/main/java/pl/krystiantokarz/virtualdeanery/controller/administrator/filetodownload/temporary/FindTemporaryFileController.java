package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.temporary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.domain.file.FileWaitingForAccepted;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.FileConverter;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.FindFileToDownloadService;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.filesPath)
@Controller
public class FindTemporaryFileController {


    @Autowired
    private FindFileToDownloadService findFileToDownloadService;

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getFilesToDownloadWaitingForAcceptableBySelectedPage(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page, Model model){
        int pageSize = 10;
        Page<FileWaitingForAccepted> filesPage = findFileToDownloadService.getFileWaitingForAcceptedDescriptionsByPageAndPageSize(page, pageSize);
        model.addAttribute("files", FileConverter.convertFilesToDTOs(filesPage.getContent()));
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("previousPage", page - 1);
        if(page == 0){
            model.addAttribute("blockPreviousPage", true);
        }
        int totalPageFromZero = filesPage.getTotalPages()-1;
        if(page == totalPageFromZero || totalPageFromZero < 0){
            model.addAttribute("blockNextPage", true);
        }
        return "/administrator/file/temporary/files";
    }
}
