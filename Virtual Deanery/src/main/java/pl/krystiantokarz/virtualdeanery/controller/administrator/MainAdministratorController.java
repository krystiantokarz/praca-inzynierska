package pl.krystiantokarz.virtualdeanery.controller.administrator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;

@RequestMapping(BaseURL.administratorPath)
@Controller
@SessionAttributes("logged-user-id")
public class MainAdministratorController {

    final Logger logger = LoggerFactory.getLogger(MainAdministratorController.class);

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getHomeAdministratorPage(){
        return "/administrator/home";
    }



}
