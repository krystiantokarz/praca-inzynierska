package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.infrastructure.errorcode.employee.ErrorCode;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.CreateEventDTO;
import pl.krystiantokarz.virtualdeanery.service.timetable.CreateEventService;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.EventTimeLimitException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventDateException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventTimeException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.validation.Valid;

@RequestMapping(BaseURL.employeePath + EmployeeURL.timetable)
@Controller
@SessionAttributes("logged-user-id")
public class CreateEventController {

    private static final Logger logger = LoggerFactory.getLogger(CreateEventController.class);

    @Autowired
    private CreateEventService createEventService;

    @GetMapping(value = "/event", produces = MediaType.TEXT_HTML_VALUE)
    public String getPageForAddNewEventToTimetable(){
        return "/employee/timetable/add-event";
    }

    @PostMapping(value = "/event", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createTimetableEvent(@RequestBody @Valid CreateEventDTO createEventDTO, BindingResult result,@ModelAttribute("logged-user-id") Long loggedUserId){
        if(result.hasErrors()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }else {
            return createEvent(createEventDTO, loggedUserId);

        }
    }


    private ResponseEntity createEvent(CreateEventDTO createEventDTO, Long loggedUserId){
        try {
            createEventService.createEvent(createEventDTO,loggedUserId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot createWithAttachments timetable event- user with id {} not exist",loggedUserId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (ValidEventTimeException e) {
            logger.info("Cannot createWithAttachments timetable event - time is not validate",e);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ErrorCode.INVALIDATE_TIME);
        } catch (EventTimeLimitException e) {
            logger.info("Cannot createWithAttachments timetable event - the limit is exceeded",e);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ErrorCode.TIME_LIMIT);
        } catch (ValidEventDateException e) {
            logger.info("Cannot createWithAttachments timetable event - date is not validate",e);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ErrorCode.INVALIDATE_DATE);
        }

    }
}
