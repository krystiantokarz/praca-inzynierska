package pl.krystiantokarz.virtualdeanery.controller.administrator.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.errorcode.administrator.ErrorCode;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.RolesConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.UserConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditAccountUserDTO;
import pl.krystiantokarz.virtualdeanery.service.account.EditAccountService;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedLoginExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.validation.Valid;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.accountPath)
@Controller("editAdministratorAccountController")
@SessionAttributes("logged-user-id")
public class EditAccountController {

    private static final Logger logger = LoggerFactory.getLogger(EditAccountController.class);

    @Autowired
    private EditAccountService editAccountService;

    @Autowired
    private FindUserService findUserService;


    @GetMapping(value ="/data", produces = MediaType.TEXT_HTML_VALUE)
    public String getEditAccountPage(Model model, @ModelAttribute("logged-user-id") Long loggedUserId){
        try {
            User user = findUserService.findUserById(loggedUserId);
            model.addAttribute("administrator", UserConverter.convertUserToDTO(user));
            model.addAttribute("roles", RolesConverter.convertUserRolesToStringList(user.getRoles()));
            return "/administrator/account/account-edit";
        } catch (UserNotExistException e) {
            logger.info("Cannot edit administrator password - user with id {} not exist", loggedUserId,e);
            return "/administrator/error/error-page";
        }

    }

    @PutMapping(value = "/data")
    public ResponseEntity editUserAccount(@RequestBody @Valid EditAccountUserDTO editUserDTO, @ModelAttribute("logged-user-id") Long loggedUserId, BindingResult result){
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } else {
            return editUserAccount(loggedUserId, editUserDTO);

        }
    }

    private ResponseEntity editUserAccount(Long userId, EditAccountUserDTO editUserDTO){
        try{
            User user = editAccountService.editSelectedUser(userId, editUserDTO);
            Authentication auth = new UsernamePasswordAuthenticationToken(user.getLoginData().getLogin(), user.getLoginData().getPassword());
            SecurityContextHolder.getContext().setAuthentication(auth);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot edit administrator password - user with id {} not exist",userId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserWithSelectedLoginExistException e) {
            logger.info("Cannot edit administrator password - user with login {} not exist",editUserDTO.getLogin(),e);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ErrorCode.USER_LOGIN_EXIST);
        } catch (UserWithSelectedEmailExistException e) {
            logger.info("Cannot edit administrator password - user with email {} not exist",editUserDTO.getEmail(),e);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ErrorCode.USER_EMAIL_EXIST);
        }
    }
}
