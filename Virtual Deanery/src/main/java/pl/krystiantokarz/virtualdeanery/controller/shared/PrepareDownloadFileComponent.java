package pl.krystiantokarz.virtualdeanery.controller.shared;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescription;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailAttachmentFileDescriptionNotExist;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.servlet.http.HttpServletResponse;

@Component
public class PrepareDownloadFileComponent {

    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String ATTACHMENT_WITH_FILENAME = "attachment; filename=";

    @Autowired
    private MailMessageRepository mailMessageRepository;

    @Autowired
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Autowired
    private UserRepository userRepository;


    public HttpServletResponse prepareResponseForFileToDownload(HttpServletResponse response, Long fileId) throws FileNotExistException {
        FileToDownloadDescription fileDescriptionByIdIfExist = fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId);
        return prepareForFile(response,
                fileDescriptionByIdIfExist.getFileExtensionType(),
                fileDescriptionByIdIfExist.getFileSize(),
                fileDescriptionByIdIfExist.getOriginalFileName()
        );
    }
    public HttpServletResponse prepareResponseForMessageAttachment(HttpServletResponse response, Long attachmentFileId, Long userId) throws UserNotExistException, MailAttachmentFileDescriptionNotExist {
        User user = userRepository.findUserByIdIfExist(userId);
        MailAttachmentFileDescription fileAttachment = mailMessageRepository.findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(attachmentFileId, user);
        return prepareForFile(response,
                fileAttachment.getFileExtensionType(),
                fileAttachment.getFileSize(),
                fileAttachment.getOriginalFileName()
        );
    }

    private HttpServletResponse prepareForFile(HttpServletResponse response, String type, Integer fileSize, String originalFileName){
        response.setContentType(type);
        response.setContentLength(fileSize);
        String headerValue = String.format(ATTACHMENT_WITH_FILENAME + "\"%s\"", originalFileName);
        response.setHeader(CONTENT_DISPOSITION, headerValue);
        return response;
    }

}
