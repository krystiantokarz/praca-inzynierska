package pl.krystiantokarz.virtualdeanery.controller.employee.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.UserConverter;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@RequestMapping(BaseURL.employeePath + EmployeeURL.accountPath)
@Controller("findEmployeeAccountController")
@SessionAttributes("logged-user-id")
public class FindAccountController {

    private static final Logger logger = LoggerFactory.getLogger(FindAccountController.class);

    @Autowired
    private FindUserService findUserService;

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getAccountPage(@ModelAttribute("logged-user-id") Long loggedUserId, Model model){
        try {
            User administrator = findUserService.findUserById(loggedUserId);
            model.addAttribute("user", UserConverter.convertUserToDTO(administrator));
            return "/employee/account/account";
        } catch (UserNotExistException e) {
            logger.info("Cannot find employee account data - user with id {} not exist", loggedUserId,e);
            return "/employee/error/error-page";
        }
    }
}
