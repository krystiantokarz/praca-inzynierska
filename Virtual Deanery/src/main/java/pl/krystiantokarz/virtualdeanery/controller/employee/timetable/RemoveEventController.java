package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.service.timetable.RemoveEventService;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;


@RequestMapping(BaseURL.employeePath + EmployeeURL.timetable)
@Controller
@SessionAttributes("logged-user-id")
public class RemoveEventController {

    private static final Logger logger = LoggerFactory.getLogger(RemoveEventController.class);

    @Autowired
    private RemoveEventService removeEventService;

    @DeleteMapping(value = "/event/{eventId}")
    public ResponseEntity removeSelectedEvent(@PathVariable("eventId") Long eventId, @ModelAttribute("logged-user-id") Long loggedUserId){
        try {
            removeEventService.removeSelectedEventByUser(eventId,loggedUserId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (TimetableEventNotExistException e) {
            logger.info("Cannot delete timetable event with id = {} - event not exist",eventId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (PrivilegeException e) {
            logger.info("Cannot delete timetable event with id = {} by user with id = {}. User not have privilege to do id" ,eventId, loggedUserId,e);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}



