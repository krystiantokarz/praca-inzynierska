package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupPasswordDTO;
import pl.krystiantokarz.virtualdeanery.service.group.EditGroupService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.*;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.validation.Valid;

@RequestMapping(BaseURL.employeePath + EmployeeURL.groupPath)
@Controller
@SessionAttributes("logged-user-id")
public class ManagedGroupController {

    private static final Logger logger = LoggerFactory.getLogger(ManagedGroupController.class);

    @Autowired
    private EditGroupService editGroupService;

    @PutMapping(value = "/{groupId}")
    public ResponseEntity editGroup(@PathVariable("groupId") Long groupId,
                                    @RequestBody @Valid EditGroupDTO editGroupDTO,
                                    @ModelAttribute("logged-user-id") Long loggedUserId,
                                    BindingResult result){
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } else {
            return editGroup(loggedUserId, groupId, editGroupDTO);

        }
    }

    private ResponseEntity editGroup(Long userId, Long groupId,EditGroupDTO editGroupDTO ){
        try {
            editGroupService.editGroupData(groupId,userId,editGroupDTO);
            return ResponseEntity.ok().build();
        } catch (GroupNotExistException e) {
            logger.info("Cannot edit group with id = {} - group not exist",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (PrivilegeException e) {
            logger.info("Cannot edit group with id = {} by user with id = {} - user not have a privilege to do it ", groupId,userId, e);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot edit group with id = {} by user with id = {} - user not exist", groupId,userId, e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserGroupNotExistException e) {
            logger.info("Cannot edit group with id = {} by user with id = {} - user is not a group member", groupId,userId, e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (GroupWithSelectedNameExistException e) {
            logger.info("Cannot edit group with id = {} by user with id = {} - new group name = {} is not unique", groupId,userId, editGroupDTO.getName(), e);
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }
    @PutMapping(value = "/{groupId}/password")
    public ResponseEntity editGroupPassword(@PathVariable("groupId") Long groupId,
                                            @RequestBody @Valid EditGroupPasswordDTO editGroupPasswordDTO,
                                            @ModelAttribute("logged-user-id") Long loggedUserId,
                                            BindingResult result){
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } else {
            return editGroupPassword(loggedUserId, groupId, editGroupPasswordDTO);
        }
    }

    private ResponseEntity editGroupPassword(Long userId, Long groupId,EditGroupPasswordDTO editGroupPasswordDTO ){
        try {
            editGroupService.editGroupPassword(groupId,userId,editGroupPasswordDTO);
            return ResponseEntity.ok().build();
        } catch (GroupNotExistException e) {
            logger.info("Cannot edit group password with id = {} - group not exist",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (PrivilegeException e) {
            logger.info("Cannot edit group password  with id = {} by user with id = {} - user not have a privilege to do it ", groupId,userId, e);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        } catch (NotMatchesPasswordsException e) {
            logger.info("Cannot edit group password with id = {} -  password not matches ",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot edit group password with id = {} by user with id = {} - user not exist", groupId,userId, e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserGroupNotExistException e) {
            logger.info("Cannot edit group password with id = {} by user with id = {} - user is not a group member", groupId,userId, e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }




}
