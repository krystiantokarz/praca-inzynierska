package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.service.group.DeleteGroupService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@RequestMapping(BaseURL.employeePath + EmployeeURL.groupPath)
@Controller
@SessionAttributes("logged-user-id")
public class RemoveGroupController {

    private static final Logger logger = LoggerFactory.getLogger(RemoveGroupController.class);

    @Autowired
    private DeleteGroupService deleteGroupService;

    @DeleteMapping(value = "/{groupId}")
    public ResponseEntity removeGroup(@PathVariable("groupId") Long groupId, @ModelAttribute("logged-user-id") Long loggedUserId){
        try {
            deleteGroupService.deleteGroupByGroupAdministrator(loggedUserId,groupId);
            return ResponseEntity.ok().build();
        } catch (UserGroupNotExistException e) {
            logger.info("Cannot delete group with id = {} - user is not a group member",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (GroupNotExistException e) {
            logger.info("Cannot delete group with id = {} - group not exist",groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot delete group with id = {} in group with id = {} - user not exist",loggedUserId,groupId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (PrivilegeException e) {
            logger.info("Cannot delete group with id = {}. Logged user with id = {} not have a privilege to do it  ",groupId,loggedUserId,e);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
