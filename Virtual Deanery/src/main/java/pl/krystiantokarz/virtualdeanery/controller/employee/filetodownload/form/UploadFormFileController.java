package pl.krystiantokarz.virtualdeanery.controller.employee.filetodownload.form;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.UploadFileToDownloadService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileIsEmptyException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileUploadException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

@RequestMapping(BaseURL.employeePath + EmployeeURL.filePath + EmployeeURL.formFilePath)
@Controller("uploadFormFileForEmployeeController")
@SessionAttributes("logged-user-id")
public class UploadFormFileController {

    private static final Logger logger = LoggerFactory.getLogger(UploadFormFileController.class);

    @Autowired
    private UploadFileToDownloadService uploadFileToDownloadService;

    @Autowired
    private FindUserService findUserService;

    @PostMapping
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file, @ModelAttribute("logged-user-id") Long loggedUserId) {
        try {
            User user = findUserService.findUserById(loggedUserId);
            uploadFileToDownloadService.uploadFileToDownload(file, user, FileStatus.WAITING_FOR_ACCEPTED, FileType.FORM);
            return ResponseEntity.ok().build();
        } catch (FileUploadException e) {
            logger.warn("Cannot upload form file by employee", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot upload form file by employee - user with id {} not exist", loggedUserId, e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (FileIsEmptyException e) {
            logger.info("Cannot upload form file by employee - file is empty", e);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        } catch (FileWithSelectedNameExistException e) {
            logger.info("Cannot upload form file by employee - selected file name = {} is not unique", file.getOriginalFilename(), e);
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }
}
