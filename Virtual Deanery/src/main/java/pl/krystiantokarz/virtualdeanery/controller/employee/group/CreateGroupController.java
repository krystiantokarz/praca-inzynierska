package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.CreateGroupDTO;
import pl.krystiantokarz.virtualdeanery.service.group.CreateGroupService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupWithSelectedNameExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.validation.Valid;

@RequestMapping(BaseURL.employeePath + EmployeeURL.groupPath)
@Controller
@SessionAttributes("logged-user-id")
public class CreateGroupController {

    private static final Logger logger = LoggerFactory.getLogger(CreateGroupController.class);

    @Autowired
    private CreateGroupService createGroupService;

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getPageCreateNewGroup(){
        return "/employee/group/groups-add";
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createNewGroup(@RequestBody @Valid CreateGroupDTO createGroupDTO, BindingResult result, @ModelAttribute("logged-user-id") Long loggedUserId){
        if(result.hasErrors()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }else{
            return createGroup(createGroupDTO,loggedUserId);
        }
    }

    private ResponseEntity createGroup(CreateGroupDTO createGroupDTO, Long userId){
        try {
            createGroupService.createGroup(createGroupDTO,userId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot createWithAttachments group by user with id = {}. User not exist", userId, e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (GroupWithSelectedNameExistException e) {
            logger.info("Cannot createWithAttachments group - selected group name  = {} is occupied", createGroupDTO.getName(), e);
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }
}
