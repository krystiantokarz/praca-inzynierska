package pl.krystiantokarz.virtualdeanery.controller.employee.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.controller.shared.ConstraintViolationExceptionHandler;
import pl.krystiantokarz.virtualdeanery.service.account.ChangePasswordService;
import pl.krystiantokarz.virtualdeanery.service.account.exceptions.NotMatchesPasswordsException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@RequestMapping(BaseURL.employeePath + EmployeeURL.accountPath)
@Controller("changeEmployeePasswordController")
@SessionAttributes("logged-user-id")
@Validated
public class ChangePasswordController extends ConstraintViolationExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ChangePasswordController.class);

    @Autowired
    private ChangePasswordService changePasswordService;


    @GetMapping(value = "/password", produces = MediaType.TEXT_HTML_VALUE)
    public String getChangePasswordPage(){
        return "/employee/account/change-password";
    }

    @PutMapping(value = "/password")
    public ResponseEntity changePassword(@Size(min = 5) @NotNull @RequestParam(value = "oldPassword") String oldPassword,
                                         @Size(min = 5) @NotNull  @RequestParam("newPassword") String newPassword,
                                         @ModelAttribute("logged-user-id") Long loggedUserId) {
        try {
            changePasswordService.changePassword(loggedUserId,oldPassword,newPassword);
            return ResponseEntity.ok().build();
        } catch (NotMatchesPasswordsException e) {
            logger.info("Cannot change employee password - No matches password",e);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot change employee password - user with id {} not exist", loggedUserId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}

