package pl.krystiantokarz.virtualdeanery.controller.employee;

public abstract class EmployeeURL {
    public static final String usersPath = "/users";
    public static final String userPath = "/user";
    public static final String groupsPath = "/groups";
    public static final String groupPath = "/group";
    public static final String filesPath = "/files";
    public static final String filePath = "/file";
    public static final String archiveFilePath = "/archive";
    public static final String formFilePath = "/form";
    public static final String accountPath = "/account";
    public static final String mailPath = "/mail";
    public static final String timetable = "/timetable";
    public static final String chat = "/chat";
}
