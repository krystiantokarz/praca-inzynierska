package pl.krystiantokarz.virtualdeanery.controller.employee.filetodownload.form;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.employee.EmployeeURL;
import pl.krystiantokarz.virtualdeanery.controller.shared.PrepareDownloadFileComponent;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.DownloadFileToDownloadService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.DownloadFileToDownloadException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@RequestMapping(BaseURL.employeePath + EmployeeURL.filePath + EmployeeURL.formFilePath)
@Controller("downloadFormFileForEmployeeController")
public class DownloadFormFileController {

    private static final Logger logger = LoggerFactory.getLogger(DownloadFormFileController.class);

    @Autowired
    private DownloadFileToDownloadService downloadFileToDownloadService;

    @Autowired
    private PrepareDownloadFileComponent prepareDownloadFileComponent;

    @GetMapping
    public ResponseEntity downloadFormFile(@RequestParam("fileId") Long fileId, HttpServletResponse response) throws IOException {

        try {
            HttpServletResponse httpServletResponse = prepareDownloadFileComponent.prepareResponseForFileToDownload(response, fileId);
            downloadFileToDownloadService.downloadFile(fileId, httpServletResponse.getOutputStream());
            return ResponseEntity.ok().build();
        } catch (FileNotExistException e) {
            logger.info("Cannot download archive file by employee - file with id {} not exist",e);
            return ResponseEntity.notFound().build();
        } catch (DownloadFileToDownloadException e) {
            logger.warn("Cannot download form file with id {} by employee", fileId,e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }



}
