package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.archive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.ChangeFileToDownloadTypeException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.ChangeReadyToDownloadFileTypeService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.filePath + AdministratorURL.archiveFilePath)
@Controller
public class ChangeArchiveTypeFileController {

    private static final Logger logger = LoggerFactory.getLogger(ChangeArchiveTypeFileController.class);

    @Autowired
    private ChangeReadyToDownloadFileTypeService changeFileTypeService;

    @PutMapping("/status/{fileId}")
    public ResponseEntity changeArchiveFileToFormFileStatus(@PathVariable("fileId") Long fileId){
        try {
            changeFileTypeService.changeFileFromArchiveToFormFileType(fileId);
            return ResponseEntity.ok().build();
        } catch (FileNotExistException e) {
            logger.info("Cannot remove archive file - file with id = {} not exist",e);
            return ResponseEntity.notFound().build();
        } catch (ChangeFileToDownloadTypeException e) {
            logger.warn("Cannot change type archive file with id = {}", fileId,e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (FileWithSelectedNameExistException e) {
            logger.info("Cannot change type archive file with id = {} - selected file name = {} is not unique in form repository", fileId, e);
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }
}
