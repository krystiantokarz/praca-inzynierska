package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.errorcode.administrator.ErrorCode;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.RolesConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.UserConverter;
import pl.krystiantokarz.virtualdeanery.service.user.EditUserService;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedLoginExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.validation.Valid;

@RequestMapping(BaseURL.administratorPath +  AdministratorURL.userPath)
@Controller
public class EditUserController {

    private static final Logger logger = LoggerFactory.getLogger(CreateUserController.class);

    @Autowired
    private EditUserService editUserService;

    @Autowired
    private FindUserService findUserService;

    @GetMapping(value = "/{userId}", produces = MediaType.TEXT_HTML_VALUE)
    public String getPageAndUserDataToEdit(@PathVariable("userId") Long userId, Model model) {

        try {
            User user = findUserService.findUserById(userId);
            model.addAttribute("user", UserConverter.convertUserToDTO(user));
            model.addAttribute("roles", RolesConverter.convertUserRolesToStringList(user.getRoles()));
            return "/administrator/user/user-edit";
        } catch (UserNotExistException e) {
            logger.info("User with id {} not exist",e);
            return "/administrator/error/error-page";
        }

    }

    @PutMapping(value = "/{userId}")
    public ResponseEntity editSelectedUser(@PathVariable("userId") Long userId, @RequestBody @Valid EditUserDTO editUserDTO, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } else {
            return editUser(userId, editUserDTO);
        }
    }

    private ResponseEntity editUser(Long userId, EditUserDTO editUserDTO){
        try {
            editUserService.editSelectedUser(userId, editUserDTO);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (UserNotExistException e) {
            logger.info("Cannot edit user - user with id {} not exist",userId,e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (UserWithSelectedLoginExistException e) {
            logger.info("Cannot edit user - selected login = {} is occupied",editUserDTO.getLogin(),e);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ErrorCode.USER_LOGIN_EXIST);
        } catch (UserWithSelectedEmailExistException e) {
            logger.info("Cannot edit user - selected email = {} is occupied",editUserDTO.getEmail(),e);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ErrorCode.USER_EMAIL_EXIST);
        }
    }
}
