package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.archive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.krystiantokarz.virtualdeanery.controller.BaseURL;
import pl.krystiantokarz.virtualdeanery.controller.administrator.AdministratorURL;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.DeleteFileToDownloadService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.RemoveFileException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;

@RequestMapping(BaseURL.administratorPath + AdministratorURL.filePath + AdministratorURL.archiveFilePath)
@Controller
public class DeleteArchiveFileController {

    private static final Logger logger = LoggerFactory.getLogger(DeleteArchiveFileController.class);

    @Autowired
    private DeleteFileToDownloadService deleteFileToDownloadService;

    @DeleteMapping("/{fileId}")
    public ResponseEntity deleteSelectedArchiveFileToDownload(@PathVariable("fileId") Long fileId){
        try {
            deleteFileToDownloadService.deleteFile(fileId);
            return ResponseEntity.ok().build();
        } catch (FileNotExistException e) {
            logger.info("Cannot delete archive file - file with id {} not exist",e);
            return ResponseEntity.notFound().build();
        } catch (RemoveFileException e) {
            logger.warn("Cannot delete archive file with id {}", fileId,e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
