package pl.krystiantokarz.virtualdeanery.domain.user;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Embeddable
public class LoginData {

    private String password;

    private String login;

    public LoginData() {
    }

    private LoginData(String password, String login) {
        this.password = password;
        this.login = login;
    }

    public static LoginData from(String password, String login){
        validate(password,login);
        return new LoginData(password, login);
    }

    private static void validate(String password, String login){
        if(StringUtils.isBlank(password)){
            throw new IllegalArgumentException("Password cannot be empty");
        }
        if(StringUtils.isBlank(login)){
            throw new IllegalArgumentException("Login cannot be empty");
        }
    }

    void changePassword(String password) {
        if(StringUtils.isBlank(password)){
            throw new IllegalArgumentException("Password cannot be empty");
        }
        this.password = password;
    }

    void changeLogin(String login) {
        if(StringUtils.isBlank(login)){
            throw new IllegalArgumentException("Login cannot be empty");
        }
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoginData loginData = (LoginData) o;

        if (password != null ? !password.equals(loginData.password) : loginData.password != null) return false;
        return login != null ? login.equals(loginData.login) : loginData.login == null;
    }

    @Override
    public int hashCode() {
        int result = password != null ? password.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LoginData{" +
                "password='" + password + '\'' +
                ", login='" + login + '\'' +
                '}';
    }
}
