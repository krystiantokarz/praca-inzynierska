package pl.krystiantokarz.virtualdeanery.domain.passwordresettoken;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "PASSWORD_RESET_TOKEN")
public class PasswordResetToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String token;

    private String password;

    @OneToOne
    @JoinColumn(nullable = false, name = "USER_ID", referencedColumnName = "id")
    private User user;

    @Column(name = "EXPIRY_DATE")
    @CreatedDate
    private LocalDateTime expiryDate;

    public PasswordResetToken() {
    }

    private PasswordResetToken(String token, String password, User user) {
        this.token = token;
        this.password = password;
        this.user = user;
    }

    public static PasswordResetToken from(String token, String password, User user){
        validate(token, password, user);
        return new PasswordResetToken(token, password, user);
    }

    private static void validate(String token,String password, User user){
        if(StringUtils.isBlank(token)){
            throw new IllegalArgumentException("Token cannot be empty or null");
        }
        if(StringUtils.isBlank(password)){
            throw new IllegalArgumentException("Password cannot be empty or null");
        }
        if(user == null){
            throw new IllegalArgumentException("User cannot be null");
        }
    }

    public Long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PasswordResetToken that = (PasswordResetToken) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (token != null ? !token.equals(that.token) : that.token != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        return expiryDate != null ? expiryDate.equals(that.expiryDate) : that.expiryDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (token != null ? token.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (expiryDate != null ? expiryDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PasswordResetToken{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", password='" + password + '\'' +
                ", user=" + user +
                ", expiryDate=" + expiryDate +
                '}';
    }
}
