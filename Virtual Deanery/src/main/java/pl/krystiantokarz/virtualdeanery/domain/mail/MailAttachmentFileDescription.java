package pl.krystiantokarz.virtualdeanery.domain.mail;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Entity
@Table(name = "MAIL_ATTACHMENT_FILE_DESCRIPTION")
public class MailAttachmentFileDescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ORIGINAL_FILE_NAME")
    private String originalFileName;

    @Column(name = "SAVED_FILE_NAME")
    private String savedFileName;

    @Column(name = "FILE_SIZE")
    private Integer fileSize;

    @Column(name = "FILE_EXTENSION_TYPE")
    private String fileExtensionType;

    public MailAttachmentFileDescription() {
    }

    private MailAttachmentFileDescription(String originalFileName, String savedFileName, Integer fileSize, String fileExtensionType) {
        this.originalFileName = originalFileName;
        this.savedFileName = savedFileName;
        this.fileSize = fileSize;
        this.fileExtensionType = fileExtensionType;

    }

    public static MailAttachmentFileDescription from(String originalFileName, String savedFileName, Integer fileSize, String fileExtensionType) {
        validate(originalFileName,savedFileName,fileSize,fileExtensionType);
        return new MailAttachmentFileDescription(originalFileName, savedFileName, fileSize, fileExtensionType);
    }

    private static void validate(String originalFileName, String savedFileName, Integer fileSize, String fileExtensionType) {
        if(StringUtils.isBlank(originalFileName)){
            throw new IllegalArgumentException("Original file name cannot be empty");
        }
        if(StringUtils.isBlank(savedFileName)){
            throw new IllegalArgumentException("Saved file name cannot be empty");
        }
        if(fileSize == null){
            throw new IllegalArgumentException("File size cannot be null");
        }
        if(StringUtils.isBlank(fileExtensionType)){
            throw new IllegalArgumentException("File extension type cannot be empty");
        }

    }

    public Long getId() {
        return id;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public String getSavedFileName() {
        return savedFileName;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public String getFileExtensionType() {
        return fileExtensionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MailAttachmentFileDescription that = (MailAttachmentFileDescription) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (originalFileName != null ? !originalFileName.equals(that.originalFileName) : that.originalFileName != null)
            return false;
        if (savedFileName != null ? !savedFileName.equals(that.savedFileName) : that.savedFileName != null)
            return false;
        if (fileSize != null ? !fileSize.equals(that.fileSize) : that.fileSize != null) return false;
        return fileExtensionType != null ? fileExtensionType.equals(that.fileExtensionType) : that.fileExtensionType == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (originalFileName != null ? originalFileName.hashCode() : 0);
        result = 31 * result + (savedFileName != null ? savedFileName.hashCode() : 0);
        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
        result = 31 * result + (fileExtensionType != null ? fileExtensionType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MessageAttachmentFileDescription{" +
                "id=" + id +
                ", originalFileName='" + originalFileName + '\'' +
                ", savedFileName='" + savedFileName + '\'' +
                ", fileSize=" + fileSize +
                ", fileExtensionType='" + fileExtensionType + '\'' +
                '}';
    }
}
