package pl.krystiantokarz.virtualdeanery.domain.file;

public enum  FileStatus {

    WAITING_FOR_ACCEPTED,
    READY_TO_DOWNLOAD
}
