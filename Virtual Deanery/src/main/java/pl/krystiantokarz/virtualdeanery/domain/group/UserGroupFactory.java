package pl.krystiantokarz.virtualdeanery.domain.group;

import pl.krystiantokarz.virtualdeanery.domain.user.User;

public class UserGroupFactory {

    public static UserGroup createUserGroupForNormalMember(User user, Group group){
        return UserGroup.from(
                user,
                group,
                MemberType.NORMAL_MEMBER
        );
    }

    public static UserGroup createUserGroupForAdministratorMember(User user, Group group){
        return UserGroup.from(
                user,
                group,
                MemberType.ADMIN_MEMBER
        );
    }
}
