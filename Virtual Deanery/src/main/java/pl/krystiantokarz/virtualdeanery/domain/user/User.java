package pl.krystiantokarz.virtualdeanery.domain.user;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import pl.krystiantokarz.virtualdeanery.domain.user.role.Role;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

@Entity
public class User{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "EMAIL")
    private String email;

    private Boolean enabled;

    @Embedded
    private LoginData loginData;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    private List<Role> roles;

    public User() {
    }

    private User(String firstName, String lastName, String email, LoginData loginData, Boolean enabled, List<Role> roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.loginData = loginData;
        this.roles = roles;
        this.enabled = enabled;
    }

    static User from(String firstName, String lastName, String email, LoginData loginData, Boolean enabled, List<Role> roles) {
        validate(firstName,lastName,email,loginData, enabled,roles );
        return new User(firstName,lastName,email, loginData,enabled, roles);
    }

    private static void validate(String firstName, String lastName, String email, LoginData loginData, Boolean enabled, List<Role> roles) {
        if(StringUtils.isBlank(firstName)){
            throw new IllegalArgumentException("First name cannot be empty");
        }
        if(StringUtils.isBlank(lastName)){
            throw new IllegalArgumentException("Last name cannot be empty");
        }
        if(StringUtils.isBlank(email)){
            throw new IllegalArgumentException("Email cannot be empty");
        }
        if(loginData == null){
            throw new IllegalArgumentException("LoginData cannot be null");
        }
        if(enabled == null){
            throw new IllegalArgumentException("Enabled cannot be null");
        }
        if(CollectionUtils.isEmpty(roles)){
            throw new IllegalArgumentException("Roles cannot be empty or null");
        }
    }


    public void changeUserData(String firstName, String lastName, String email, String login, List<Role> roles){
        validate(firstName,lastName,email,login,roles);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.loginData.changeLogin(login);
        this.roles.clear();
        this.roles.addAll(roles);
    }

    private static void validate(String firstName, String lastName, String email, String login ,List<Role> roles) {
        validate(firstName, lastName, email, login);
        if(CollectionUtils.isEmpty(roles)){
            throw new IllegalArgumentException("Roles cannot be empty or null");
        }
    }

    public void changeUserData(String firstName, String lastName, String email, String login){
        validate(firstName,lastName,email,login);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.loginData.changeLogin(login);
    }


    private static void validate(String firstName, String lastName, String email, String login) {
        if(StringUtils.isBlank(firstName)){
            throw new IllegalArgumentException("First name cannot be empty");
        }
        if(StringUtils.isBlank(lastName)){
            throw new IllegalArgumentException("Last name cannot be empty");
        }
        if(StringUtils.isBlank(email)){
            throw new IllegalArgumentException("Email cannot be empty");
        }
        if(StringUtils.isBlank(login)){
            throw new IllegalArgumentException("Login cannot be empty");
        }

    }


    public void changeUserPassword(String password){
        if (StringUtils.isBlank(password)) {
            throw new IllegalArgumentException("Password cannot be empty");
        }
        this.loginData.changePassword(password);
    }

    public void enableUser(){
        this.enabled = true;
    }

    public void disableUser(){
        this.enabled = false;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public LoginData getLoginData() {
        return loginData;
    }

    public List<Role> getRoles() {
        return Collections.unmodifiableList(roles);
    }

    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (loginData != null ? !loginData.equals(user.loginData) : user.loginData != null) return false;
        if (roles != null ? !roles.equals(user.roles) : user.roles != null) return false;
        return enabled != null ? enabled.equals(user.enabled) : user.enabled == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (loginData != null ? loginData.hashCode() : 0);
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", loginData=" + loginData +
                ", roles=" + roles +
                ", enabled=" + enabled +
                '}';
    }
}

