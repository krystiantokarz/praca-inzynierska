package pl.krystiantokarz.virtualdeanery.domain.file;

public interface FileReadyToDownload extends FileToDownloadDescription {

    FileReadyToDownload changeFileType(FileType fileType);
}
