package pl.krystiantokarz.virtualdeanery.domain.user.role;

import pl.krystiantokarz.virtualdeanery.domain.user.User;

import javax.persistence.*;

@Entity
@Table(name = "ROLE")
public class Role{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "USER_ROLE")
    private RoleEnum role;

    public Role() {
    }

    private Role(RoleEnum role) {
        this.role = role;
    }

    public static Role from(RoleEnum role){
        validate(role);
        return new Role(role);
    }

    private static void validate(RoleEnum role){
        if(role == null){
            throw new IllegalArgumentException("role cannot be empty");
        }
    }

    public Long getId() {
        return id;
    }

    public RoleEnum getRole() {
        return role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role1 = (Role) o;

        if (id != null ? !id.equals(role1.id) : role1.id != null) return false;
        return role == role1.role;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role=" + role +
                '}';
    }
}
