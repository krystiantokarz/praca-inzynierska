package pl.krystiantokarz.virtualdeanery.domain.mail;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Table(name = "MAIL_MESSAGE")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class MailMessageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String subject;

    private String message;

    @Column(name = "MESSAGE_STATUS")
    @Enumerated(EnumType.STRING)
    private MailMessageStatus messageStatus;

    @ManyToOne
    @JoinColumn(name = "SENDER_ID", referencedColumnName = "id")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "RECIPIENT_ID", referencedColumnName = "id")
    private User recipient;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "MESSAGE_ID", referencedColumnName = "id")
    private List<MailAttachmentFileDescription> attachments;

    @Column(name = "CREATION_DATE")
    @CreatedDate
    private LocalDateTime date;

    public MailMessageEntity() {
    }

    private MailMessageEntity(String subject, String message, MailMessageStatus messageStatus, List<MailAttachmentFileDescription> attachments, User sender, User recipient) {
        this.subject = subject;
        this.message = message;
        this.messageStatus = messageStatus;
        this.attachments = attachments;
        this.sender = sender;
        this.recipient = recipient;
    }

    static MailMessageEntity from(String subject, String message, MailMessageStatus messageStatus, List<MailAttachmentFileDescription> attachments, User sender, User recipient){
        validate(subject, messageStatus, sender, recipient);
        return new MailMessageEntity(subject, message, messageStatus, attachments, sender, recipient);
    }

    private static void validate(String subject, MailMessageStatus messageStatus, User sender, User recipient){
        if(StringUtils.isBlank(subject)){
            throw new IllegalArgumentException("Subject cannot be empty or null");
        }
        if(messageStatus == null){
            throw new IllegalArgumentException("Message status cannot be null");
        }
        if(sender == null){
            throw new IllegalArgumentException("User sender cannot be null");
        }
        if(recipient == null){
            throw new IllegalArgumentException("User recipient cannot be null");
        }
    }


    public void changeStatusToRead() {
        messageStatus = MailMessageStatus.READ;
    }

    public Long getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public MailMessageStatus getMessageStatus() {
        return messageStatus;
    }

    public User getSender() {
        return sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public List<MailAttachmentFileDescription> getAttachments() {
        return attachments;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MailMessageEntity that = (MailMessageEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (messageStatus != that.messageStatus) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (attachments != null ? !attachments.equals(that.attachments) : that.attachments != null) return false;
        if (sender != null ? !sender.equals(that.sender) : that.sender != null) return false;
        return recipient != null ? recipient.equals(that.recipient) : that.recipient == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (messageStatus != null ? messageStatus.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (attachments != null ? attachments.hashCode() : 0);
        result = 31 * result + (sender != null ? sender.hashCode() : 0);
        result = 31 * result + (recipient != null ? recipient.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MessageEntity{" +
                "id=" + id +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", messageStatus=" + messageStatus +
                ", date=" + date +
                ", attachments=" + attachments +
                ", sender=" + sender +
                ", recipient=" + recipient +
                '}';
    }
}
