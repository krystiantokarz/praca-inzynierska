package pl.krystiantokarz.virtualdeanery.domain.group;

public enum MemberType {
    ADMIN_MEMBER,
    NORMAL_MEMBER
}
