package pl.krystiantokarz.virtualdeanery.domain.group;

import org.apache.commons.lang3.StringUtils;
import pl.krystiantokarz.virtualdeanery.domain.group.chat.ChatMessage;
import javax.persistence.*;
import java.util.List;

@Table(name="\"GROUP\"")
@Entity
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String password;

    private String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "group")
    private List<ChatMessage> messages;

    public Group() {
    }

    private Group(String name, String password, String description) {
        this.name = name;
        this.password = password;
        this.description = description;
    }

    public static Group from(String name, String password, String description){
        validate(name,password);
        return new Group(name,password,description);
    }

    private static void validate(String name,String password){
        if(StringUtils.isBlank(name)){
            throw new IllegalArgumentException("Group name cannot be empty");
        }
        if(StringUtils.isBlank(password)){
            throw new IllegalArgumentException("Password cannot be empty");
        }
    }


    public void changeGroupData(String name, String description){
        if(StringUtils.isBlank(name)){
            throw new IllegalArgumentException("Group name cannot be empty or null");
        }
        this.name = name;
        this.description = description;
    }

    public void changeGroupPassword(String password){
        if(StringUtils.isBlank(password)){
            throw new IllegalArgumentException("Group password cannot be empty or null");
        }
        this.password = password;
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getDescription() {
        return description;
    }


    public List<ChatMessage> getMessages() {
        return messages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group groups = (Group) o;

        if (id != null ? !id.equals(groups.id) : groups.id != null) return false;
        if (name != null ? !name.equals(groups.name) : groups.name != null) return false;
        if (password != null ? !password.equals(groups.password) : groups.password != null) return false;
        if (description != null ? !description.equals(groups.description) : groups.description != null) return false;
        return messages != null ? messages.equals(groups.messages) : groups.messages == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (messages != null ? messages.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Groups{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", description='" + description + '\'' +
                ", messages=" + messages +
                '}';
    }
}
