package pl.krystiantokarz.virtualdeanery.domain.file;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.CreatedDate;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "FILE_TO_DOWNLOAD_DESCRIPTION")
public class FileToDownloadDescriptionEntity implements FileWaitingForAccepted, FileReadyToDownload {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ORIGINAL_FILE_NAME")
    private String originalFileName;

    @Column(name = "SAVED_FILE_NAME")
    private String savedFileName;

    @Column(name = "FILE_SIZE")
    private Integer fileSize;

    @Column(name = "FILE_EXTENSION_TYPE")
    private String fileExtensionType;

    @Column(name = "FILE_TYPE")
    @Enumerated(EnumType.STRING)
    private FileType fileType;

    @Column(name = "FILE_STATUS")
    @Enumerated(EnumType.STRING)
    private FileStatus fileStatus;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "id")
    private User addingUser;

    @CreatedDate
    private LocalDateTime date;

    public FileToDownloadDescriptionEntity() {
    }

    private FileToDownloadDescriptionEntity(String originalFileName, String savedFileName, Integer fileSize, String fileExtensionType, FileType fileType, FileStatus fileStatus,User user) {
        this.originalFileName = originalFileName;
        this.savedFileName = savedFileName;
        this.fileSize = fileSize;
        this.fileExtensionType = fileExtensionType;
        this.fileType = fileType;
        this.fileStatus = fileStatus;
        this.addingUser = user;
    }

    public static FileToDownloadDescriptionEntity from(String originalFileName, String savedFileName, Integer fileSize, String fileExtensionType, FileType fileType, FileStatus fileStatus,User user){
        validate(originalFileName,savedFileName,fileSize,fileExtensionType, fileType,fileStatus,user );
        return new FileToDownloadDescriptionEntity(originalFileName, savedFileName, fileSize, fileExtensionType, fileType,fileStatus,user);
    }

    private static void validate(String originalFileName, String savedFileName, Integer fileSize, String fileExtensionType, FileType fileType,FileStatus fileStatus,User user){
        if(StringUtils.isBlank(originalFileName)){
            throw new IllegalArgumentException("Original file name cannot be empty or null");
        }
        if(StringUtils.isBlank(savedFileName)){
            throw new IllegalArgumentException("Saved file name cannot be empty or null");
        }
        if(fileSize == null){
            throw new IllegalArgumentException("File size cannot be null");
        }
        if(StringUtils.isBlank(fileExtensionType)){
            throw new IllegalArgumentException("File extension type cannot be empty or null");
        }
        if(fileType == null){
            throw new IllegalArgumentException("File type cannot be null");
        }
        if(fileStatus == null){
            throw new IllegalArgumentException("File status cannot be null");
        }
        if(user == null){
            throw new IllegalArgumentException("Adding user cannot be null");
        }
    }

    @Override
    public FileReadyToDownload acceptFile() {
        this.fileStatus = FileStatus.READY_TO_DOWNLOAD;
        return this;
    }

    @Override
    public FileReadyToDownload changeFileType(FileType fileType) {
        this.fileType = fileType;
        return this;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getOriginalFileName() {
        return originalFileName;
    }

    @Override
    public String getSavedFileName() {
        return savedFileName;
    }

    @Override
    public Integer getFileSize() {
        return fileSize;
    }

    @Override
    public String getFileExtensionType() {
        return fileExtensionType;
    }

    @Override
    public FileType getFileType() {
        return fileType;
    }

    @Override
    public FileStatus getFileStatus() {
        return fileStatus;
    }

    @Override
    public User getAddingUser() {
        return addingUser;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileToDownloadDescriptionEntity file = (FileToDownloadDescriptionEntity) o;

        if (id != null ? !id.equals(file.id) : file.id != null) return false;
        if (originalFileName != null ? !originalFileName.equals(file.originalFileName) : file.originalFileName != null)
            return false;
        if (savedFileName != null ? !savedFileName.equals(file.savedFileName) : file.savedFileName != null)
            return false;
        if (fileSize != null ? !fileSize.equals(file.fileSize) : file.fileSize != null) return false;
        if (fileExtensionType != null ? !fileExtensionType.equals(file.fileExtensionType) : file.fileExtensionType != null)
            return false;
        if (fileType != file.fileType) return false;
        if (fileStatus != file.fileStatus) return false;
        if (addingUser != null ? !addingUser.equals(file.addingUser) : file.addingUser != null) return false;
        return date != null ? date.equals(file.date) : file.date == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (originalFileName != null ? originalFileName.hashCode() : 0);
        result = 31 * result + (savedFileName != null ? savedFileName.hashCode() : 0);
        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
        result = 31 * result + (fileExtensionType != null ? fileExtensionType.hashCode() : 0);
        result = 31 * result + (fileType != null ? fileType.hashCode() : 0);
        result = 31 * result + (fileStatus != null ? fileStatus.hashCode() : 0);
        result = 31 * result + (addingUser != null ? addingUser.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FileToDownloadDescriptionEntity{" +
                "id=" + id +
                ", originalFileName='" + originalFileName + '\'' +
                ", savedFileName='" + savedFileName + '\'' +
                ", fileSize=" + fileSize +
                ", fileExtensionType='" + fileExtensionType + '\'' +
                ", fileType=" + fileType +
                ", fileStatus=" + fileStatus +
                ", addingUser=" + addingUser +
                ", date=" + date +
                '}';
    }
}
