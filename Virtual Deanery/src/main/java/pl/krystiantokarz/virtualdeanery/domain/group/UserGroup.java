package pl.krystiantokarz.virtualdeanery.domain.group;

import pl.krystiantokarz.virtualdeanery.domain.user.User;

import javax.persistence.*;

@Entity
@Table(name = "USER_GROUP")
public class UserGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "id")
    private Group group;

    @Column(name = "MEMBER_TYPE")
    @Enumerated(EnumType.STRING)
    private MemberType memberType;

    public UserGroup() {
    }

    private UserGroup(User user, Group group, MemberType memberType) {
        this.memberType = memberType;
        this.user = user;
        this.group = group;
    }

    static UserGroup from(User user, Group group, MemberType memberType){
        validate(user,group, memberType);
        return new UserGroup(user, group, memberType);
    }

    private static void validate(User user, Group group, MemberType memberType){
        if(user == null){
            throw new IllegalArgumentException("User cannot be null");
        }
        if(group == null){
            throw new IllegalArgumentException("Group cannot be null");
        }
        if(memberType == null){
            throw new IllegalArgumentException("MemberType cannot be null");
        }
    }

    public void changeMemberTypeToGroupAdministrator(){
        this.memberType = MemberType.ADMIN_MEMBER;

    }

    public void changeMemberTypeToNormalMember(){
        this.memberType = MemberType.NORMAL_MEMBER;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Group getGroup() {
        return group;
    }

    public MemberType getMemberType() {
        return memberType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserGroup userGroup = (UserGroup) o;

        if (id != null ? !id.equals(userGroup.id) : userGroup.id != null) return false;
        if (user != null ? !user.equals(userGroup.user) : userGroup.user != null) return false;
        if (group != null ? !group.equals(userGroup.group) : userGroup.group != null) return false;
        return memberType == userGroup.memberType;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (memberType != null ? memberType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserGroup{" +
                "id=" + id +
                ", user=" + user +
                ", group=" + group +
                ", memberType=" + memberType +
                '}';
    }
}
