package pl.krystiantokarz.virtualdeanery.domain.mail;

import pl.krystiantokarz.virtualdeanery.domain.user.User;

import java.time.LocalDateTime;
import java.util.List;

public class MailMessageFactory {

    public static MailMessageEntity createWithAttachments(String subject, String message, List<MailAttachmentFileDescription> attachments, User sender, User recipient){
        return MailMessageEntity.from(
                subject,
                message,
                MailMessageStatus.DELIVERED,
                attachments,
                sender,
                recipient);
    }

    public static MailMessageEntity createWithoutAttachments(String subject, String message, User sender, User recipient){
        return MailMessageEntity.from(
                subject,
                message,
                MailMessageStatus.DELIVERED,
                null,
                sender,
                recipient);
    }
}
