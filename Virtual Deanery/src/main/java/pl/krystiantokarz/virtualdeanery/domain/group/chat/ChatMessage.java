package pl.krystiantokarz.virtualdeanery.domain.group.chat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "CHAT_MESSAGE")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class ChatMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String message;

    @ManyToOne
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "id")
    private Group group;

    @ManyToOne
    @JoinColumn(name = "SENDER_ID", referencedColumnName = "id")
    private User sender;

    @CreatedDate
    private LocalDateTime date;


    public ChatMessage() {
    }

    private ChatMessage(String message, Group group, User sender) {
        this.message = message;
        this.group = group;
        this.sender = sender;
    }

    public static ChatMessage from(String message, Group group, User sender){
        validate(message,group,sender);
        return new ChatMessage(message,group, sender);
    }

    private static void validate(String message, Group group, User sender) {
        if(StringUtils.isBlank(message)){
            throw new IllegalArgumentException("Message cannot be empty or null");
        }
        if(group == null){
            throw new IllegalArgumentException("Group cannot be null");
        }
        if(sender == null){
            throw new IllegalArgumentException("User which send message cannot be null");
        }
    }

    public Long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Group getGroup() {
        return group;
    }

    public User getSender() {
        return sender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChatMessage that = (ChatMessage) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (group != null ? !group.equals(that.group) : that.group != null) return false;
        return sender != null ? sender.equals(that.sender) : that.sender == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (sender != null ? sender.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", date=" + date +
                ", group=" + group +
                ", sender=" + sender +
                '}';
    }
}
