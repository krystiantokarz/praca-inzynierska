package pl.krystiantokarz.virtualdeanery.domain.timetable;

import org.apache.commons.lang3.StringUtils;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "TIMETABLE_EVENT")
public class TimetableEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "EVENT_DATE")
    private LocalDate eventDate;

    @Column(name = "TIME_FROM")
    private LocalTime timeFrom;

    @Column(name = "TIME_TO")
    private LocalTime timeTo;

    private String topic;

    private String message;

    @ManyToOne
    @JoinColumn(name = "CREATING_USER_ID", referencedColumnName = "id")
    private User creatingUser;

    public TimetableEvent() {
    }

    private TimetableEvent(LocalDate eventDate, LocalTime timeFrom, LocalTime timeTo, String topic, String message, User creatingUser) {
        this.eventDate = eventDate;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.topic = topic;
        this.message = message;
        this.creatingUser = creatingUser;
    }

    public static TimetableEvent from(LocalDate eventDate, LocalTime timeFrom, LocalTime timeTo, String topic, String message, User creatingUser) {
        validate(eventDate, timeFrom, timeTo, topic, message, creatingUser);
        return new TimetableEvent(eventDate, timeFrom, timeTo, topic, message, creatingUser);
    }

    private static void validate(LocalDate eventDate, LocalTime timeFrom, LocalTime timeTo, String topic, String message, User creatingUser) {
        if(eventDate == null){
            throw new IllegalArgumentException("Event date cannot be null");
        }
        if(creatingUser == null){
            throw new IllegalArgumentException("User creating event cannot be null");
        }
        validate(timeFrom, timeTo, topic, message);


    }

    public void changeEventData(LocalTime timeFrom, LocalTime timeTo, String topic, String message){
        validate(timeFrom, timeTo, topic, message);
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.topic = topic;
        this.message = message;
    }

    private static void validate(LocalTime timeFrom, LocalTime timeTo, String topic, String message) {

        if(timeFrom == null){
            throw new IllegalArgumentException("Event time from cannot be null");
        }
        if(timeTo == null){
            throw new IllegalArgumentException("Event time to cannot be null");
        }
        if(StringUtils.isBlank(topic)){
            throw new IllegalArgumentException("Topic cannot be empty or null");
        }
        if(StringUtils.isBlank(message)){
            throw new IllegalArgumentException("Message cannot be empty or null");
        }
    }


    public Long getId() {
        return id;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }

    public LocalTime getTimeFrom() {
        return timeFrom;
    }

    public LocalTime getTimeTo() {
        return timeTo;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    public User getCreatingUser() {
        return creatingUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimetableEvent that = (TimetableEvent) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (eventDate != null ? !eventDate.equals(that.eventDate) : that.eventDate != null) return false;
        if (timeFrom != null ? !timeFrom.equals(that.timeFrom) : that.timeFrom != null) return false;
        if (timeTo != null ? !timeTo.equals(that.timeTo) : that.timeTo != null) return false;
        if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        return creatingUser != null ? creatingUser.equals(that.creatingUser) : that.creatingUser == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (eventDate != null ? eventDate.hashCode() : 0);
        result = 31 * result + (timeFrom != null ? timeFrom.hashCode() : 0);
        result = 31 * result + (timeTo != null ? timeTo.hashCode() : 0);
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (creatingUser != null ? creatingUser.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TimetableEvent{" +
                "id=" + id +
                ", eventDate=" + eventDate +
                ", timeFrom=" + timeFrom +
                ", timeTo=" + timeTo +
                ", topic='" + topic + '\'' +
                ", message='" + message + '\'' +
                ", creatingUser=" + creatingUser +
                '}';
    }
}
