package pl.krystiantokarz.virtualdeanery.domain.file;

public enum FileType {
    FORM,
    ARCHIVE
}
