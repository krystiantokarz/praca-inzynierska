package pl.krystiantokarz.virtualdeanery.domain.mail;

public enum MailMessageStatus {
    DELIVERED,
    READ,

}
