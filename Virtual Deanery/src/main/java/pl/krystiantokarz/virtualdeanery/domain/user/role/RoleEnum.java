package pl.krystiantokarz.virtualdeanery.domain.user.role;

public enum RoleEnum {
    ADMIN,
    EMPLOYEE
}
