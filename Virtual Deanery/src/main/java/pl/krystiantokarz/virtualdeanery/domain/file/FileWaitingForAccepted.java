package pl.krystiantokarz.virtualdeanery.domain.file;

public interface FileWaitingForAccepted extends FileToDownloadDescription {

    FileReadyToDownload acceptFile();

}
