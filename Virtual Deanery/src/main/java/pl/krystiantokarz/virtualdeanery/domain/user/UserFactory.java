package pl.krystiantokarz.virtualdeanery.domain.user;

import pl.krystiantokarz.virtualdeanery.domain.user.role.Role;

import java.util.List;

public class UserFactory {

    public static User build(String firstName, String lastName, String email, String login, String password, List<Role> roles) {

        LoginData loginData = LoginData.from(password, login);

        return User.from(
                firstName,
                lastName,
                email,
                loginData,
                true,
                roles
                );
    }


}
