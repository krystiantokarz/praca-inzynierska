package pl.krystiantokarz.virtualdeanery.domain.file;

import pl.krystiantokarz.virtualdeanery.domain.user.User;

public interface FileToDownloadDescription {

    Long getId();

    String getOriginalFileName();

    String getSavedFileName();

    Integer getFileSize();

    String getFileExtensionType();

    FileType getFileType();

    FileStatus getFileStatus();

    User getAddingUser();

}
