package pl.krystiantokarz.virtualdeanery.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.krystiantokarz.virtualdeanery.configuration.authetication.CustomLoginSuccessHandler;
import pl.krystiantokarz.virtualdeanery.configuration.authetication.CustomLogoutSuccessHandler;

import javax.sql.DataSource;


@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/", "/index").permitAll()
                .antMatchers("/js/**","/css/**").permitAll()
                .antMatchers("/images/**").permitAll()
                .antMatchers("/password/**").permitAll()
                .antMatchers("/home").hasRole("EMPLOYEE")
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated()
            .and()
                .formLogin()
                .successHandler(new CustomLoginSuccessHandler())
                .loginPage("/login")
                .usernameParameter("login").passwordParameter("password")
                .permitAll()
            .and()
                .logout()
                .logoutSuccessHandler(new CustomLogoutSuccessHandler())
                 .permitAll()
             .and()
                .csrf()
                .disable();

    }



    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
//                .passwordEncoder(passwordEncoder);
    }
}
