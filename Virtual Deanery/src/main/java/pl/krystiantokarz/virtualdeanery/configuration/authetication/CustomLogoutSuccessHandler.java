package pl.krystiantokarz.virtualdeanery.configuration.authetication;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {

    private static final String HEADER_REFERER_PAGE_URL = "Referer";
    private static final String HOST_URL = "Host";

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        String refererURL = httpServletRequest.getHeader(HEADER_REFERER_PAGE_URL);
        String hostURL = httpServletRequest.getHeader(HOST_URL);
        String[] split = refererURL.split("[/](?!/)");
        httpServletResponse.sendRedirect(split[0] + "/" + hostURL + "/" + split[2]);
    }

}