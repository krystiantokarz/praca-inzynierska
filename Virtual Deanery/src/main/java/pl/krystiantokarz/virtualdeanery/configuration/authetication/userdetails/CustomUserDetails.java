package pl.krystiantokarz.virtualdeanery.configuration.authetication.userdetails;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public class CustomUserDetails implements UserDetails {

    private User user;

    private static final String ROLE_PREFIX = "ROLE_";


    CustomUserDetails(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<String> rolesList = user.getRoles().stream()
                .map(e -> ROLE_PREFIX + e.getRole().toString())
                .collect(Collectors.toList());

        String roles = StringUtils.collectionToCommaDelimitedString(rolesList);
        return AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
    }

    @Override
    public String getPassword() {
        return user.getLoginData().getPassword();
    }

    @Override
    public String getUsername() {
        return user.getLoginData().getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    public User getUser() {
        return user;
    }
}
