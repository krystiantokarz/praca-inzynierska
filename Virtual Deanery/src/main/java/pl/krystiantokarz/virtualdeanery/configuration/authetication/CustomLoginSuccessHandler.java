package pl.krystiantokarz.virtualdeanery.configuration.authetication;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import pl.krystiantokarz.virtualdeanery.configuration.authetication.userdetails.CustomUserDetails;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class CustomLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        httpServletRequest.getSession().setAttribute("logged-user-id", userDetails.getUser().getId());
        httpServletRequest.getSession().setAttribute("logged-user-login", userDetails.getUser().getLoginData().getLogin());
        super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);

    }
}
