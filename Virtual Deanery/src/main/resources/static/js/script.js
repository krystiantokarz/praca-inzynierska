$(document).ready(function () {

    var applicationLocation = "http://localhost:8080";

    $(".homePageButton").click(function() {
        window.location.assign("/home")
    });

    $(".indexPageButton").click(function() {
        window.location.assign("/")
    });

    //function to serialize to json
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    //reset password
    
    $("#open-change-password-container-button").click(function (e) {
        e.preventDefault();
        $("#login-container").slideUp();
        $("#change-password-container").slideDown("slow");
        $("#login-input").val('');
        $("#password-input").val('');
        hideAlerts();
        hideIncorrectInputs();
    });

    $("#open-login-container-button").click(function () {
        $("#login-container").slideDown("slow");
        $("#change-password-container").slideUp();
        $("#email-input").val('');
        hideAlerts();
        hideIncorrectInputs();
    });

    $("#reset-password-button").click(function() {
        var email = $('#email-input').val();
        hideAlerts();
        hideIncorrectInputs();
        $.ajax({
            type: "PUT",
            url : applicationLocation + "/password/reset",
            data: {
                email : email
            },
            success: function() {
                $("#email-input").val('');
                $("#email-input").removeClass("incorrect-input");
                $("#change-password-success-alert").show();
            },
            error : function() {
                $("#email-input").addClass("incorrect-input");
                $("#change-password-error-alert").show();
            }
        });
    });


    var hideAlerts = function () {
        $("#change-password-error-alert").hide();
        $("#change-password-success-alert").hide();
        $("#error-login-alert").hide();
        $("#logout-alert").hide();
    };

    var hideIncorrectInputs = function () {
        $("#email-input").removeClass("incorrect-input");
        $("#login-input").removeClass("incorrect-input");
        $("#password-input").removeClass("incorrect-input");
    };


});

