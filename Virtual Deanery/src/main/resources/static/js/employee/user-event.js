$(document).ready(function () {

    var applicationEmployeeLocation = "http://localhost:8080/home";

    var clearPopups = function () {
        $("#error-load-calendar").addClass('hide');
        $("#error-load-timetable").addClass('hide');
        $("#error-load-event-information").addClass('hide');
        $("#error-search-user-popup").addClass('hide');
        $("#success-remove-event-popup").addClass('hide');
        $("#error-remove-event-popup").addClass('hide');

    };

    var events = [];
    $.ajax({
        type: "GET",
        url : applicationEmployeeLocation + "/timetable/events/user/all",
        success: function(data) {

            for(var i =0; i<data.length; i++){
                var eventDate = new Date(data[i].date);
                var day = eventDate.getUTCDate();
                var month = eventDate.getUTCMonth();
                var year = eventDate.getUTCFullYear();
                var eventId = data[i].eventId;
                events.push({'Date': new Date(year, month,day), 'Title': data[i].topic, 'Id': eventId})
            }
            var settings = {};
            var element = document.getElementById('caleandar');
            caleandar(element, events, settings);

        },
        error : function(xhr,data) {
            $("#error-load-calendar").removeClass('hide');
        }
    });


    var clickedDay = null;

    $("#caleandar").on('click',".eventday", function () {
        clearPopups();
        if(!$(event.target).is('.event')) {
            var day = $(this).attr('day-number');
            var data = $(".cld-days");
            var month = data.attr('month');
            var year = data.attr('year');

            var actuallyLoggedUserId;

            $.ajax({
                type: "GET",
                url: applicationEmployeeLocation + "/loggedUser",
                success: function (data) {
                    actuallyLoggedUserId = data;
                }
            });

            $.ajax({
                type: "GET",
                url: applicationEmployeeLocation + "/timetable/events/date/user/all",
                data: {
                    day: day,
                    month: month,
                    year: year
                },
                success: function (data) {
                    var timetable = new Timetable();
                    timetable.setScope(7, 21);
                    var users = [];
                    for (var i = 0; i < data.length; i++) {
                        var userName = data[i].userFirstName + " " + data[i].userLastName;
                        users.push({
                            userName: userName,
                            userId: data[i].userId
                        });
                    }
                    timetable.addLoggedUserId(actuallyLoggedUserId);
                    timetable.addLocations(users);

                    for (var i = 0; i < data.length; i++) {
                        var userName = data[i].userFirstName + " " + data[i].userLastName;
                        var eventDate = new Date(data[i].date);
                        var day = eventDate.getUTCDate();
                        var month = eventDate.getUTCMonth();
                        var year = eventDate.getUTCFullYear();
                        var hourSplitStartDate = data[i].timeFrom.split(":");
                        var hourStart = hourSplitStartDate[0];
                        var minuteStart = hourSplitStartDate[1];
                        var hourSplitEndDate =data[i].timeTo.split(":");

                        var hourEnd = hourSplitEndDate[0];
                        var minuteEnd = hourSplitEndDate[1];
                        var eventId = data[i].eventId;
                        var userId = data[i].userId;
                        timetable.addEvent(data[i].topic, userName, new Date(year, month, day, hourStart, minuteStart), new Date(year, month, day, hourEnd, minuteEnd),{ data:{eventId : eventId, userId : userId}});
                    }

                    var renderer = new Timetable.Renderer(timetable);
                    renderer.draw('#timetable');
                },
                error: function (xhr, data) {
                    $("#error-load-timetable").removeClass('hide');
                }
            });
        }
    });


    $("#caleandar").on('click',".currMonth", function (event) {
        if (!$(this).children("p").hasClass('eventday')) {
            $("#timetable").hide();
        }else{
            $("#timetable").show();
        }
        if (clickedDay !== null) {
            clickedDay.removeClass("clicked");
        }
        clickedDay = $(event.currentTarget);
        $(clickedDay).addClass("clicked");
    });






});