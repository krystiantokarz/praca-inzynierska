var stompClient = null;
var socket = null;
var applicationEmployeeLocation = "http://localhost:8080/home";

function connect(socket) {


    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        // setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/messages', function (message) {
            showSubscribeMessage(JSON.parse(message.body));
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    if (socket !== null) {
        socket.close();
    }

}

function sendMessage(button) {
    var groupId = $(button).data('id');
    var message = $("#chat-msg-input").val();
    stompClient.send("/destination/chat/"+groupId, {}, JSON.stringify({'groupId': groupId, 'message' : message}));
}

function showSubscribeMessage(message) {

    if (message.userId === loggedUserId) {
        createOwnerMessage(message);
    } else{
        createMemberMessage(message);
    }
}

function loadOldMessages(messages){
    messages.forEach(function (msg) {
        if(msg.userId === loggedUserId){
            createOwnerMessage(msg);
        }else{
            createMemberMessage(msg);
        }
    });
}

function addNextOldMessages(messages) {
    var tmpElement =  document.createElement('div');
    messages.forEach(function (msg) {
        if(msg.userId === loggedUserId){
            createOldMessage(tmpElement,msg,"right");
        }else{
            createOldMessage(tmpElement,msg,"left");
        }
    });
    $(".chat").prepend($(tmpElement).children());
}

function addErrorMessage(){
    var object = {};
    object.message = "BRAK POŁĄCZENIA! ";
    object.date = "";
    object.firstName = "";
    object.lastName = "";
    createMessage(object, "right");
}

function createOldMessage(tmpElement,messageObject, memberClass){
    var li = document.createElement('li');
    $(li).addClass(memberClass + " clearfix");
    var chatMessageBody = document.createElement('div');
    $(chatMessageBody).addClass("chat-body clearfix");
    var header = document.createElement('div');
    $(header).addClass("header");
    var userName = document.createElement('strong');
    $(userName).addClass("primary-font");
    $(userName).text(messageObject.firstName + " " + messageObject.lastName);
    $(header).append(userName);
    var message = document.createElement('p');
    $(message).text(messageObject.message);
    var info = document.createElement('p');
    $(info).addClass("text-info");
    var infoImage = document.createElement('span');
    $(infoImage).addClass("glyphicon glyphicon-time");
    var date = new Date(messageObject.date);
    $(infoImage).text(date.toLocaleDateString() + " " + date.toLocaleTimeString());
    $(info).append(infoImage);

    $(chatMessageBody).append(header);
    $(chatMessageBody).append(message);
    $(chatMessageBody).append(info);
    $(li).append(chatMessageBody);
    $(tmpElement).append(li);
}

function createOwnerMessage(message){
    createMessage(message, "right")
}

function createMemberMessage(message){
    createMessage(message, "left")
}


function createMessage(messageObject, memberClass){
    var li = document.createElement('li');
    $(li).addClass(memberClass + " clearfix");
    var chatMessageBody = document.createElement('div');
    $(chatMessageBody).addClass("chat-body clearfix");

    var header = document.createElement('div');
    $(header).addClass("header");
    var userName = document.createElement('strong');
    $(userName).addClass("primary-font");
    $(userName).text(messageObject.firstName + " " + messageObject.lastName);
    $(header).append(userName);
    var message = document.createElement('p');
    $(message).text(messageObject.message);
    var info = document.createElement('p');
    $(info).addClass("text-info");
    var infoImage = document.createElement('span');
    $(infoImage).addClass("glyphicon glyphicon-time");
    var date = new Date(messageObject.date);
    $(infoImage).text(date.toLocaleDateString() + " " + date.toLocaleTimeString());
    $(info).append(infoImage);

    $(chatMessageBody).append(header);
    $(chatMessageBody).append(message);
    $(chatMessageBody).append(info);
    $(li).append(chatMessageBody);
    $(".chat").append(li);

    $(".panel-body-chat").scrollTop($(".panel-body-chat")[0].scrollHeight);
}


var actuallyPage = 0;
var loggedUserId;
var groupId;
var notEnoughMessage = false;


$(document).ready(function () {
    socket = new SockJS('/chat-endpoint');
    connect(socket);
    $("#send-chat-msg-button").click(function() {
        sendMessage(this);
        $('#chat-msg-input').val('');
        $('#chat-msg-input').focus();
    });

    $('#chat-msg-input').keypress(function(event) {
        if (event.keyCode == 13) {
            sendMessage($("#send-chat-msg-button"));
            $('#chat-msg-input').val('');
        }
    });


    loggedUserId = parseInt($( "#send-chat-msg-button" ).attr("user-id"));
    groupId = $("#send-chat-msg-button").data('id');


    $.ajax({
        type: "GET",
        url : applicationEmployeeLocation + "/chat/message/"+groupId + "/?page=" + actuallyPage,
        contentType: "application/json",
        success: function(messages) {
            loadOldMessages(messages);
            actuallyPage++;
            $(".panel-body-chat").scrollTop($(".panel-body-chat")[0].scrollHeight);
        },
        error : function(xhr,data) {
            addErrorMessage();
            $('#chat-msg-input').attr('disabled','disabled');
            $('#send-chat-msg-button').attr('disabled','disabled');
        }
    });

    $('.panel-body-chat').on('scroll', function() {
        if($(this).scrollTop() <= 0 && notEnoughMessage === false) {
            $.ajax({
                type: "GET",
                url : applicationEmployeeLocation + "/chat/message/"+groupId + "/?page=" + actuallyPage,
                data:{
                    page : actuallyPage
                },
                contentType: "application/json",
                success: function(messages) {
                    if(messages.length == 0){
                        notEnoughMessage = true;
                    }else{
                        $(".panel-body-chat").scrollHeight
                        addNextOldMessages(messages);
                        actuallyPage++;
                        $(".panel-body-chat").scrollTop($(".panel-body-chat")[0].scrollHeight/ actuallyPage);
                    }
                },
                error : function(xhr,data) {
                    addErrorMessage();
                }
            });
        }
    });
});


$(window).on("beforeunload", function(e) {
    disconnect();
});