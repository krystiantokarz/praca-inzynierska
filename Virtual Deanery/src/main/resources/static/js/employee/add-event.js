$(document).ready(function () {


    var applicationEmployeeLocation = "http://localhost:8080/home";

    var hideAddEventPopups = function () {
        $("#success-add-event-popup").addClass('hide');
        $("#error-add-event-time-validation-popup").addClass('hide');
        $("#error-add-event-time-limit-popup").addClass('hide');
        $("#error-add-event-date-popup").addClass('hide');
        $("#error-load-calendar").addClass('hide');
        $("#error-load-timetable").addClass('hide');
        $("#error-add-event-popup").addClass("hide");
        $("#error-load-event-information").addClass("hide");
        $("#error-search-user-popup").addClass("hide");
    };

    var events = [];
    $.ajax({
        type: "GET",
        url : applicationEmployeeLocation + "/timetable/events/all",
        success: function(data) {

            for(var i =0; i<data.length; i++){

                var eventDate = new Date(data[i].date);
                var day = eventDate.getUTCDate();
                var month = eventDate.getUTCMonth();
                var year = eventDate.getUTCFullYear();
                var eventId = data[i].eventId;
                events.push({'Date': new Date(year, month,day), 'Title': data[i].topic, 'Id': eventId})
            }
            var settings = {};
            var element = document.getElementById('caleandar');
            caleandar(element, events, settings);
            events = [];

        },
        error : function(xhr,data) {
            $("#error-load-calendar").removeClass('hide');
            $("#popup-event-info").addClass('hide');
        }
    });

    $("#caleandar").on('click',".currMonth", function (event) {
        if (!$(this).children("p").hasClass('eventday')) {
            $("#timetable").hide();
        }else{
            $("#timetable").show();
        }
        hideAddEventPopups();
        if(!$(event.target).is('.event')) {
            if (clickedDay !== null) {
                clickedDay.removeClass("clicked");
            }
            clickedDay = $(event.currentTarget);
            $(clickedDay).addClass("clicked");
            $("#add-event-form").removeClass("hide");
            $("#datepicker-times-container").removeClass("hide");
            $("#start-event-time-input").val('');
            $("#stop-event-time-input").val('');
            $("#event-topic-container").addClass("hide");
            $("#event-message-container").addClass("hide");
            $("#message-textarea").val('');
            $("#topic-input").val('');
            $("#add-event-button").attr('disabled', 'disabled');
            $('.datetimepicker').data("DateTimePicker").clear();

        }
    });

    $("#caleandar").on('click',".eventday", function () {
        hideAddEventPopups();
        if(!$(event.target).is('.event')) {
            var day = $(this).attr('day-number');
            var data = $(".cld-days");
            var month = data.attr('month');
            var year = data.attr('year');

            var actuallyLoggedUserId;

            $.ajax({
                type: "GET",
                url: applicationEmployeeLocation + "/loggedUser",
                success: function (data) {
                    actuallyLoggedUserId = data;
                }
            });

            $.ajax({
                type: "GET",
                url: applicationEmployeeLocation + "/timetable/events/date/all",
                data: {
                    day: day,
                    month: month,
                    year: year
                },
                success: function (data) {
                    var timetable = new Timetable();
                    timetable.setScope(7, 21);
                    var users = [];
                    for (var i = 0; i < data.length; i++) {
                        var userName = data[i].userFirstName + " " + data[i].userLastName;
                        users.push({
                            userName: userName,
                            userId: data[i].userId
                        });
                    }
                    timetable.addLoggedUserId(actuallyLoggedUserId);
                    timetable.addLocations(users);

                    for (var i = 0; i < data.length; i++) {
                        var userName = data[i].userFirstName + " " + data[i].userLastName;
                        var eventDate = new Date(data[i].date);
                        var day = eventDate.getUTCDate();
                        var month = eventDate.getUTCMonth();
                        var year = eventDate.getUTCFullYear();
                        var hourSplitStartDate = data[i].timeFrom.split(":");
                        var hourStart = hourSplitStartDate[0];
                        var minuteStart = hourSplitStartDate[1];
                        var hourSplitEndDate =data[i].timeTo.split(":");

                        var hourEnd = hourSplitEndDate[0];
                        var minuteEnd = hourSplitEndDate[1];
                        var eventId = data[i].eventId;
                        var userId = data[i].userId;
                        timetable.addEvent(data[i].topic, userName, new Date(year, month, day, hourStart, minuteStart), new Date(year, month, day, hourEnd, minuteEnd),{ data:{eventId : eventId, userId : userId}});
                    }

                    var renderer = new Timetable.Renderer(timetable);
                    renderer.draw('#timetable');
                },
                error: function (xhr, data) {
                    $("#error-load-timetable").removeClass('hide');
                    $("#popup-event-info").addClass('hide');
                }
            });
        }
    });

    //add event



    $('.datetimepicker').datetimepicker({
        format: 'LT',
        locale: 'pl'
    });

    var clickedDay = null;




    $("#caleandar").bind("DOMSubtreeModified",".currMonth",function () {
        $("#start-event-time-input").val('');
        $("#stop-event-time-input").val('');
        $("#event-topic-container").addClass("hide");
        $("#event-message-container").addClass("hide");
        $("#add-event-form").addClass("hide");
        $("#message-textarea").val('');
        $("#topic-input").val('');
    });

    $("#start-event-time-input").on("change keyup paste click", function(){
        hideAddEventPopups();
        var startTimeValue = $("#start-event-time-input").val();
        var stopTimeValue = $("#stop-event-time-input").val();

        if(startTimeValue.length > 0 && stopTimeValue.length > 0){
            $("#event-topic-container").removeClass("hide");
            $("#event-message-container").removeClass("hide");
        }else{
            $("#event-topic-container").addClass("hide");
            $("#event-message-container").addClass("hide");
            $("#event-message-textarea").val('');
            $("#event-topic-input").val('');
        }
    });

    var checkCanAddEventDescription = function () {
        var startTimeValue = $("#start-event-time-input").val();
        var stopTimeValue = $("#stop-event-time-input").val();

        if(startTimeValue.length > 0 && stopTimeValue.length > 0){
            $("#event-topic-container").removeClass("hide");
            $("#event-message-container").removeClass("hide");
        }else{
            $("#event-topic-container").addClass("hide");
            $("#event-message-container").addClass("hide");
            $("#event-message-textarea").val('');
            $("#event-topic-input").val('');
        }
    };
    $("#stop-event-time-input").on("change keyup paste click", function(){
        hideAddEventPopups();
        checkCanAddEventDescription();
    });

    $('.datetimepicker').on('dp.show', function(e) {
        var currentDate = new Date();
        var currentMinutes = currentDate.getMinutes();
        var currentHours = currentDate.getHours();
        $(this).val(currentHours + ":" + currentMinutes);
        hideAddEventPopups();
        checkCanAddEventDescription();
    });


    $('.datetimepicker').on('dp.change', function(e) {
        hideAddEventPopups();
        checkCanAddEventDescription();
    });

    $("#event-topic-input").keyup(function(){
        var topic =$(this).val();
        var message = $("#event-message-textarea").val();

        if(message.length > 0 && topic.length > 0){
            $("#add-event-button").removeAttr("disabled");
        }else{
            $("#add-event-button").attr('disabled', 'disabled');
        }
    });

    $("#event-message-textarea").keyup(function(){
        var message =$(this).val();
        var topic = $("#event-topic-input").val();

        if(message.length > 0 && topic.length > 0){
            $("#add-event-button").removeAttr("disabled");
        }else{
            $("#add-event-button").attr('disabled', 'disabled');
        }
    });



    if(localStorage.getItem("show-success-add-event-popup") === 'true'){
        $("#success-add-event-popup").removeClass('hide');
        localStorage.removeItem("show-success-add-event-popup");
    }


    $("#add-event-form").submit(function (e) {
        e.preventDefault();
        hideAddEventPopups();
        var day = $(".clicked .cld-number").attr('day-number');
        var data = $(".cld-days");
        var month = data.attr('month');
        var year = data.attr('year');
        var form = $("#add-event-form").serializeObject();
        form.date = year+"-"+month+"-"+day;
        var json = JSON.stringify(form);

        $.ajax({
            type: "POST",
            url : applicationEmployeeLocation + "/timetable/event",
            data: json,
            contentType: "application/json",
            success: function(data) {
                localStorage.setItem("show-success-add-event-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                if (xhr.status === 406) {
                    if (xhr.responseJSON === "INVALIDATE_TIME") {
                        $("#error-add-event-time-validation-popup").removeClass('hide');
                    } else if (xhr.responseJSON === "TIME_LIMIT") {
                        $("#error-add-event-time-limit-popup").removeClass('hide');
                    } else if (xhr.responseJSON === "INVALIDATE_DATE") {
                        $("#error-add-event-date-popup").removeClass('hide');
                    }
                }else
                    $("#error-add-event-popup").removeClass("hide");
            }
        });
    });
});