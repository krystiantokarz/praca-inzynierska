$(document).ready(function () {

    var applicationEmployeeLocation = "http://localhost:8080/home";

    var clearPopups = function () {
        $("#success-edit-event-popup").addClass('hide');
        $("#error-edit-event-popup").addClass('hide');
    };

    $('.datetimepicker').datetimepicker({
        format: 'LT',
        locale: 'pl'
    });


    $('.datetimepicker').on('dp.change', function(e) {
        clearPopups();
        var startTimeValue = $("#start-event-time-input").val();
        var stopTimeValue = $("#stop-event-time-input").val();

        if(startTimeValue.length > 0 && stopTimeValue.length > 0){
            $("#event-topic-container").removeClass("hide");
            $("#event-message-container").removeClass("hide");
        }else{
            $("#event-topic-container").addClass("hide");
            $("#event-message-container").addClass("hide");
        }
    });


    $("#event-topic-input").keyup(function(){
        var topic =$(this).val();
        var message = $("#event-message-textarea").val();

        if(message.length > 0 && topic.length > 0){
            $("#edit-event-button").removeAttr("disabled");
        }else{
            $("#edit-event-button").attr('disabled', 'disabled');
        }
    });

    $("#event-message-textarea").keyup(function(){
        var message =$(this).val();
        var topic = $("#event-topic-input").val();

        if(message.length > 0 && topic.length > 0){
            $("#edit-event-button").removeAttr("disabled");
        }else{
            $("#edit-event-button").attr('disabled', 'disabled');
        }
    });



    if(localStorage.getItem("show-success-edit-event-popup") === 'true'){
        $("#success-edit-event-popup").removeClass('hide');
        localStorage.removeItem("show-success-edit-event-popup");
    }


    $("#edit-event-form").submit(function (e) {
        clearPopups();
        e.preventDefault();
        var eventId = $("#edit-event-button").data("id");
        $("#accept-edit-event-button").attr('data-id', eventId);
    });


    $("#accept-edit-event-button").click(function () {

        var eventId = $(this).data('id');
        var form = $("#edit-event-form").serializeObject();
        form.eventId = eventId;
        var json = JSON.stringify(form);
        $.ajax({
            type: "PUT",
            url : applicationEmployeeLocation + "/timetable/event/" + eventId + "/data",
            data: json,
            contentType: "application/json",
            success: function(data) {
                localStorage.setItem("show-success-edit-event-popup",'true');
                location.reload();
                $("#success-edit-event-popup").removeClass('hide');
            },
            error : function(xhr,data) {
                $("#error-edit-event-popup").removeClass('hide');
                $("#editEventModal").modal('hide');
            }
        });
    });
});