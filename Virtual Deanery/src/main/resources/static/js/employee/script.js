$(document).ready(function () {

    var applicationEmployeeLocation = "http://localhost:8080/home";


    //dropdown slide
    $(".dropdown").hover(
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');
        }
    );

    //function to serialize to json
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    //main page

    $("#users-widget-button").click(function () {
        window.location = applicationEmployeeLocation + "/users";
    });

    $("#groups-widget-button").click(function () {
        window.location = applicationEmployeeLocation + "/groups";
    });

    $("#timetable-widget-button").click(function () {
        window.location = applicationEmployeeLocation + "/timetable/events";
    });

    $("#form-file-widget-button").click(function () {
        window.location = applicationEmployeeLocation + "/files/form";
    });

    $("#archive-file-widget-button").click(function () {
        window.location = applicationEmployeeLocation + "/files/archive";
    });

    $("#mail-box-widget-button").click(function () {
        window.location = applicationEmployeeLocation + "/mail";
    });

    $("#user-account-widget-button").click(function () {
        window.location = applicationEmployeeLocation + "/account";
    });

    //USERS

    $(".pagination-users-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationEmployeeLocation + "/users?page=" + page;
    });

    //search
    $("#search-users-form").submit(function (e) {
        e.preventDefault();
        var searchUsersText = $('#search-text-input').val();
        $.ajax({
            type: "GET",
            url : applicationEmployeeLocation + "/users/search",
            data:{
                searchString : searchUsersText
            },
            success: function(response) {
                $("#search-users-container").html(response);
            }
        });
    });

    //GROUPS

    $(".pagination-groups-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationEmployeeLocation + "/groups?page=" + page;
    });


    $(".pagination-user-groups-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationEmployeeLocation + "/groups/user?page=" + page;
    });


    $(document).on("click", ".open-group-button", function(){
        var groupId = $(this).data("id");
        localStorage.setItem('back-group-page', window.location.href);
        window.location = applicationEmployeeLocation + "/group/" + groupId;
    });


    $(".back-groups-button").click(function () {
        var url = localStorage.getItem('back-group-page');
        localStorage.removeItem("back-group-page");
        window.location = url;
    });

    $("#search-groups-form").submit(function (e) {
        e.preventDefault()
        var searchText = $('#search-text-input').val();
        console.log(searchText);
        $.ajax({
            type: "GET",
            url : applicationEmployeeLocation + "/groups/search",
            data:{
                searchString : searchText
            },
            success: function(response) {
                $("#search-groups-container").html(response);
            }
        });
    });



    //group info

    var groupData;

    $(".open-info-group-button").click(function (event) {

        clearGroupsPopup();
        var selectedModalGroupId = $(event.currentTarget).data('id');

        $.ajax({
            type: "GET",
            url : applicationEmployeeLocation + "/group/" + selectedModalGroupId + "/description",
            success: function(data) {
                groupData = data;
                $("#groupInformationModal").modal();

            },
            error : function(xhr,data) {
                $("#error-search-group-popup").removeClass('hide');
            }
        });

    });


    $('#groupInformationModal').on('show.bs.modal', function (event) {
        var groupName = groupData.name;
        var groupDescription = groupData.description;

        var modal = $(this);
        modal.find('#group-name').text(groupName);
        modal.find('#group-desc').text(groupDescription);
        groupData = null;

    });


    //add groups;

    var clearAddGroupPopup = function () {
        $("#success-add-group-popup").addClass('hide');
        $("#error-name-add-group-popup").addClass('hide');
        $("#error-validate-add-group-popup").addClass('hide');
        $("#error-add-group-popup").addClass('hide');
    };

    $("#add-group-form").submit(function (e) {
        e.preventDefault();
        $("#addGroupModal").modal();

    });
    $("#accept-create-group-button").on('click', function() {
        clearAddGroupPopup();
        var form = $("#add-group-form").serializeObject();
        var json = JSON.stringify(form);

        $.ajax({
            type: "POST",
            url : applicationEmployeeLocation + "/group",
            data: json,
            contentType: "application/json",
            success: function(data) {
                $("#success-add-group-popup").removeClass('hide');
                $("#addGroupModal").modal('hide')
            },
            error : function(xhr,data) {
                if (xhr.status === 400) {
                    $("#error-validate-add-group-popup").removeClass('hide');
                } else if (xhr.status === 409) {
                    $("#error-name-add-group-popup").removeClass('hide');
                } else{
                    $("#error-add-group-popup").removeClass('hide');
                }
                $("#addGroupModal").modal('hide')
            }
        });
    });

    var clearSelectedGroupPopup = function () {
        $("#error-leave-group-popup").addClass("hide");
        $("#error-remove-group-popup").addClass("hide");
        $("#error-change-group-password-popup").addClass("hide");
        $("#success-change-group-password-popup").addClass("hide");
        $("#error-change-group-data-popup").addClass("hide");
        $("#success-change-group-data-popup").addClass("hide");
        $("#error-add-as-admin-group-popup").addClass("hide");
        $("#error-remove-from-admin-group-popup").addClass("hide");
        $("#error-delete-from-group-popup").addClass("hide");
        $("#success-remove-admin-group-popup").addClass("hide");
        $("#success-add-admin-group-popup").addClass("hide");
        $("#error-search-user-popup").addClass("hide");
    };

    //join
    if(localStorage.getItem('join-group-popup-show') === 'true'){
        $("#success-join-group-popup").removeClass("hide");
        localStorage.removeItem('join-group-popup-show');
    }


    var clearGroupsPopup = function () {
        $("#error-join-group-popup").addClass("hide");
        $("#success-join-group-popup").addClass("hide");
        $("#error-search-group-popup").addClass('hide');
        $("#success-remove-group-popup").addClass('hide');
        $("#success-leave-group-popup").addClass('hide');
    };

    $(document).on("click", ".join-group-button", function(){
        clearGroupsPopup();
        var groupId = $(this).data("id");
        $("#accept-join-group-button").attr('data-id', groupId);
        $("#group-password-input").val('');
    });


    $("#accept-join-group-button").on("click", function () {
        var groupId = $(this).data('id');
        var password = $("#group-password-input").val();
        $.ajax({
            type: "PUT",
            url : applicationEmployeeLocation +"/group/access/" + groupId,
            data:{
                pass : password
            },
            success: function(data) {
                localStorage.setItem('join-group-popup-show', 'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#joinGroupModal").modal('hide');
                $("#error-join-group-popup").removeClass("hide");

            }
        });
    });

    //leave

    if( localStorage.getItem("show-leave-group-popup") === 'true') {
        $("#success-leave-group-popup").removeClass('hide');
        localStorage.removeItem("show-leave-group-popup");
    }


    if( localStorage.getItem("show-remove-group-popup") === 'true'){
        $("#success-remove-group-popup").removeClass('hide');
        localStorage.removeItem("show-remove-group-popup")
    }

    $("#accept-leave-group-button").click(function () {
        clearSelectedGroupPopup();
        var groupId = $(this).data('id');
        $.ajax({
            type: "DELETE",
            url : applicationEmployeeLocation + "/group/access/" + groupId,
            success: function(data) {
                localStorage.setItem("show-leave-group-popup",'true');
                window.location = applicationEmployeeLocation + "/groups";
            },
            error : function(xhr,data) {
                $("#error-leave-group-popup").removeClass("hide");
                $("#leaveGroupModal").modal('hide');
            }
        });
    });

    //remove

    $("#accept-remove-group-button").click(function () {
        clearSelectedGroupPopup();
        var groupId = $(this).data('id');
        $.ajax({
            type: "DELETE",
            url : applicationEmployeeLocation + "/group/" + groupId,
            success: function(data) {
                localStorage.setItem("show-remove-group-popup",'true');
                window.location = applicationEmployeeLocation + "/groups";
            },
            error : function(xhr,data) {
                $("#error-leave-group-popup").removeClass("hide");
                $("#removeGroupModal").modal('hide');
            }
        });
    });


    //change group password


    $("#accept-change-group-password-button").click(function () {
        clearSelectedGroupPopup();
        $("#oldPassword").val("");
        $("#newPassword").val("");
        $("#repeatNewPassword").val("");
        var groupId = $(this).data('id');
        var form = $("#change-group-password-form").serializeObject();
        var json = JSON.stringify(form);
        $.ajax({
            type: "PUT",
            url : applicationEmployeeLocation +"/group/" + groupId +"/password",
            data: json,
            contentType: "application/json",
            success: function(data) {
                $("#success-change-group-password-popup").removeClass("hide");
                $("#changeGroupPasswordModal").modal('hide');
            },
            error : function(xhr,data) {
                $("#error-change-group-password-popup").removeClass("hide");
                $("#changeGroupPasswordModal").modal('hide');
            }
        });
    });

    //change group data

    if(localStorage.getItem('show-change-group-data-popup') === 'true'){
        $("#success-change-group-data-popup").removeClass("hide");
        localStorage.removeItem('show-change-group-data-popup')
    }


    $("#accept-change-group-info-button").click(function () {
        clearSelectedGroupPopup();
        var groupId = $(this).data('id');
        var form = $("#change-group-info-form").serializeObject();
        var json = JSON.stringify(form);
        $.ajax({
            type: "PUT",
            url : applicationEmployeeLocation +"/group/" + groupId,
            data: json,
            contentType: "application/json",
            success: function(data) {
                localStorage.setItem("show-change-group-data-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-change-group-data-popup").removeClass("hide");
                $("#changeGroupDataModal").modal('hide');
            }
        });
    });

    //group operations (for users)

    if(localStorage.getItem('show-add-admin-group-popup') === 'true'){
        $("#success-add-admin-group-popup").removeClass('hide');
        localStorage.removeItem('show-add-admin-group-popup')
    }

    if(localStorage.getItem('show-remove-admin-group-popup') === 'true'){
        $("#success-remove-admin-group-popup").removeClass('hide');
        localStorage.removeItem('show-remove-admin-group-popup')
    }

    if(localStorage.getItem('show-out-member-group-popup') === 'true'){
        $("#success-delete-from-group-popup").removeClass('hide');
        localStorage.removeItem('show-out-member-group-popup')
    }


    $(".add-as-admin").click(function () {
        var groupId = $("#group-data-panel").data('id');
        var userId = $(this).data('id');
        clearSelectedGroupPopup();
        $.ajax({
            type: "PUT",
            url : applicationEmployeeLocation +"/group/" + groupId + "/user/" + userId +"/access/admin",
            success: function(data) {
                localStorage.setItem("show-add-admin-group-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-add-as-admin-group-popup").removeClass("hide");
            }
        });
    });


    $(".remove-from-admin").click(function () {
        var groupId = $("#group-data-panel").data('id');
        var userId = $(this).data('id');
        clearSelectedGroupPopup();
        $.ajax({
            type: "PUT",
            url : applicationEmployeeLocation +"/group/" + groupId + "/user/" + userId +"/access/member",
            success: function(data) {
                localStorage.setItem('show-remove-admin-group-popup','true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-remove-from-admin-group-popup").removeClass("hide");
            }
        });
    });

    $(".out-from-group").click(function () {
        var groupId = $("#group-data-panel").data('id');
        var userId = $(this).data('id');
        clearSelectedGroupPopup();
        $.ajax({
            type: "DELETE",
            url : applicationEmployeeLocation +"/group/" + groupId +"/user/" + userId,
            success: function(data) {
                localStorage.setItem('show-out-member-group-popup','true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-delete-from-group-popup").removeClass("hide");
            }
        });
    });

    //user in groups

    var userData;
    $(".open-info-user-button").on('click',function (event) {
        clearSelectedGroupPopup();
        var selectedUserModalId = $(event.currentTarget).data('id');
        $.ajax({
            type: "GET",
            url : applicationEmployeeLocation + "/user/" + selectedUserModalId,
            success: function(data) {
                userData = data;
                $("#userInformationModal").modal();
            },
            error : function(xhr,data) {
                $("#error-search-user-popup").removeClass('hide');
            }
        });
    });

    $('#userInformationModal').on('show.bs.modal', function (event) {
        var firstName = userData.firstName;
        var lastName = userData.lastName;
        var email = userData.email;
        var id = userData.id;

        var modal = $(this);
        modal.find('#first-name-user').text(firstName);
        modal.find('#last-name-user').text(lastName);
        modal.find('#email-user').text(email);
        modal.find(".send-message-button").attr("user-id",id);
        userData = null;
    });


    //ACCOUNTS

    //back

    $(".back-account-button").click(function () {
        window.location = applicationEmployeeLocation + "/account";
    });

    //change password

    var hideEditAccountUserPopups = function () {
        $("#success-edit-password-popup").addClass("hide");
        $("#bad-password-matches-popup").addClass("hide");
        $("#bad-new-passwords-matches-popup").addClass("hide");
        $("#bad-validation-popup").addClass("hide");
    };

    var clearInput = function () {
        $("#new-password-input").removeClass("incorrect-input");
        $("#repeat-new-password-input").removeClass("incorrect-input");
        $("#old-password-input").removeClass("incorrect-input");
    };


    $("#change-employee-password-button").click(function () {
        window.location = applicationEmployeeLocation + "/account/password";
    });

    $("#change-password-form").submit(function (e) {
        e.preventDefault();
        hideEditAccountUserPopups();
        clearInput();
        var oldPassword = $(this).find('input[name="oldPassword"]').val();
        var newPassword = $(this).find('input[name="newPassword"]').val();
        var repeatNewPassword = $(this).find('input[name="repeatNewPassword"]').val();
        if(newPassword !== repeatNewPassword){
            $("#bad-new-passwords-matches-popup").removeClass("hide");
            $("#new-password-input").addClass("incorrect-input");
            $("#repeat-new-password-input").addClass("incorrect-input");
        }else {
            $.ajax({
                type: "PUT",
                url: applicationEmployeeLocation + "/account/password",
                data: {
                    oldPassword: oldPassword,
                    newPassword: newPassword
                },
                success: function (data) {
                    $("#success-edit-password-popup").removeClass("hide");
                },
                error: function (xhr, data) {

                    if (xhr.status === 406) {
                        $("#bad-password-matches-popup").removeClass("hide");
                    }else{
                        $("#bad-validation-popup").removeClass("hide");
                    }
                    $("#old-password-input").addClass("incorrect-input");
                }
            });
        }
    });

    //change account data

    var hideEditAccountPopupsAndInvalidOptions = function () {
        $("#success-edit-account-popup:visible").addClass("hide");
        $("#bad-email-edit-account-popup:visible").addClass("hide");
        $("#bad-login-edit-account-popup:visible").addClass("hide");
        $("#not-validate-account-data-popup:visible").addClass("hide");
        $("#error-account-data-popup").addClass('hide');
    };

    $("#change-employee-account-button").click(function () {
        window.location = applicationEmployeeLocation + "/account/data";
    });

    $("#edit-account-form").submit(function (e) {
        e.preventDefault();
        hideEditAccountPopupsAndInvalidOptions();
        var form = $("#edit-account-form").serializeObject();
        var json = JSON.stringify(form);
        $.ajax({
            type: "PUT",
            url: window.location.href,
            data: json,
            contentType: "application/json",
            success: function (data) {
                $("#success-edit-account-popup").removeClass('hide');
            },
            error: function (xhr, data) {
                if (xhr.status === 400) {
                    $("#not-validate-account-data-popup").removeClass('hide');
                } else if(xhr.status === 404) {
                    $("#not-validate-account-data-popup").removeClass('hide');
                }else  if (xhr.status === 409) {
                    if(xhr.responseJSON === "USER_EMAIL_EXIST" ){
                        $("#bad-email-edit-account-popup").removeClass('hide');
                    }else  if(xhr.responseJSON === "USER_LOGIN_EXIST" ){
                        $("#bad-login-edit-account-popup").removeClass('hide');
                    }
                }else{
                    $("#error-account-data-popup").removeClass('hide');
                }
            }
        });
    });

    //MESSAGE-BOX

    //back

    $(".back-mailbox-button").click(function () {
        window.location = applicationEmployeeLocation + "/mail";
    });

    //load

    $(".pagination-messagebox-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationEmployeeLocation + "/mail?page=" + page;
    });

    //select message

    $(".select-message-row").click(function () {
        var messageId = $(this).attr('message-id');
        window.location = applicationEmployeeLocation + "/mail/" + messageId;
    });

    //download attachment

    $(".download-attachment").click(function () {

        var attachmentId = {
            id: $(this).attr('data-id')
        };
        var query = $.param(attachmentId);
        window.location.href = applicationEmployeeLocation + "/mail/attachment?" + query;

    });



    //send message

    $("#send-message-form").submit(function (e) {
        e.preventDefault();

        var message = $("#message").val();
        var subject = $("#subject").val();
        var files= $("#message-attachment-files").prop('files');
        var formData = new FormData();
        for(var i=0; i < files.length; i++){
            formData.append('files', files[i]);
        }
        var userId = $("#send-email-button").attr('user-id');
        formData.append("idUserForSendMessage", userId);
        formData.append("message", message);
        formData.append("subject", subject);

        $.ajax({
            type: "POST",
            url : applicationEmployeeLocation + "/mail/message",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data) {
                $(".send-message-popup-container").slideDown("slow");
                $(".message-container").slideUp();
                $("#success-send-message-popup").removeClass("hide");
            },
            error : function(xhr,data) {
                $(".send-message-popup-container").slideDown("slow");
                $(".message-container").slideUp();
                $("#error-send-message-popup").removeClass("hide");
            }
        });
    });

    //send mail
    $(document).on("click", ".send-message-button", function(){
        var userId = $(this).attr('user-id');
        localStorage.setItem('back-user-message-page', window.location.href);
        window.location = applicationEmployeeLocation +"/mail/message/"+ userId ;
    });

    $(document).on("click", ".send-message-page-button", function(){
        var userId = $(this).attr('user-id');
        localStorage.setItem('back-user-message-page', window.location.href);
        window.location = applicationEmployeeLocation + "/mail/message/"+ userId ;
    });


    $(".back-users-button").click(function () {
        var url = localStorage.getItem('back-user-message-page');
        localStorage.removeItem("back-user-message-page");
        window.location = url;
    });

    //add attachment

    $("#message-attachment-files").change(function () {
        $("#message-attachment-files-list").html('');
        var inputFiles = $("#message-attachment-files")[0];
        if( inputFiles.files.length > 1){
            $(inputFiles.files).each(function () {
                $("#message-attachment-files-list").append("<li>" + this.name + "</li>");
            })
        }
    });




    //FILES

    //FORM FILES

    if(localStorage.getItem("show-success-upload-form-file-popup") === 'true'){
        $("#success-upload-form-file-popup").removeClass("hide");
        localStorage.removeItem("show-success-upload-form-file-popup");
    }



    var hideFormFilePopup = function () {
        $("#success-upload-form-file-popup").addClass("hide");
        $("#error-upload-form-file-popup").addClass("hide");
    };

    //find

    $(".pagination-form-files-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationEmployeeLocation + "/files/form?page=" + page;
    });

    //search file

    $("#search-form-files-form").submit(function (e) {
        e.preventDefault();
        hideFormFilePopup();
        var searchText = $('#search-text-input').val();
        $.ajax({
            type: "GET",
            url : applicationEmployeeLocation + "/files/form/search",
            data:{
                searchString : searchText
            },
            success: function(response) {
                $("#search-form-files-result").html(response);
            }
        });
    });

    //upload

    $("#upload-form-file").on("submit", function (e) {
        e.preventDefault();
        hideFormFilePopup();
        var form = $(this);
        var tmp = new FormData(form[0]);
        $.ajax({
            type: "POST",
            url : applicationEmployeeLocation + "/file/form",
            data: tmp,
            processData: false,
            contentType: false,
            success: function(data) {
                localStorage.setItem("show-success-upload-form-file-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                if (xhr.status === 409) {
                    $("#error-upload-name-form-file-popup").removeClass("hide");
                }else  if (xhr.status === 406) {
                    $("#error-upload-size-form-file-popup").removeClass("hide");
                }else{
                    $("#error-upload-form-file-popup").removeClass("hide");
                }
            }
        });
    });

    //download

    $(document).on("click", ".download-form-file-button", function(){
        var fileId = $(this).data("id");
        hideFormFilePopup();
        $("#accept-download-form-file-button").attr('data-id', fileId);
    });

    $("#accept-download-form-file-button").on("click", function () {
        var fileId = {
            fileId: $(this).attr('data-id')
        };
        var query = $.param(fileId);
        $("#downloadFileModal").modal('hide');
        window.location.href = applicationEmployeeLocation + "/file/form?" + query;
    });



    //ARCHIVE FILES

    $("#load-upload-component-button").click(function () {
        $(".upload-component").slideDown("slow");
        $(".upload-button-component").slideUp();
    });

    if(localStorage.getItem("show-success-upload-archive-file-popup") === 'true'){
        $("#success-upload-archive-file-popup").removeClass("hide");
        localStorage.removeItem("show-success-upload-archive-file-popup");
    }


    var hideArchiveFilePopup = function () {
        $("#success-upload-archive-file-popup").addClass("hide");
        $("#error-upload-archive-file-popup").addClass("hide");
        $("#error-upload-size-archive-file-popup").addClass("hide");
        $("#error-upload-name-archive-file-popup").addClass("hide");
    };

    //find

    $(".pagination-archive-files-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationEmployeeLocation + "/files/archive?page=" + page;
    });


    //search files

    $("#search-archive-files-form").submit(function (e) {
        e.preventDefault()
        var searchText = $('#search-text-input').val();
        $.ajax({
            type: "GET",
            url : applicationEmployeeLocation + "/files/archive/search",
            data:{
                searchString : searchText
            },
            success: function(response) {
                $("#search-archive-files-result").html(response);
            }
        });
    });

    //upload

    $("#upload-archive-file").on("submit", function (e) {
        e.preventDefault();
        hideArchiveFilePopup();
        var form = $(this);
        var tmp = new FormData(form[0]);
        $.ajax({
            type: "POST",
            url : applicationEmployeeLocation + "/file/archive",
            data: tmp,
            processData: false,
            contentType: false,
            success: function(data) {
                localStorage.setItem("show-success-upload-archive-file-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                if (xhr.status === 409) {
                    $("#error-upload-name-archive-file-popup").removeClass("hide");
                }else  if (xhr.status === 406) {
                    $("#error-upload-size-archive-file-popup").removeClass("hide");
                }else{
                    $("#error-upload-archive-file-popup").removeClass("hide");
                }
            }
        });
    });

    //download

    $(document).on("click", ".download-archive-file-button", function(){
        var fileId = $(this).data("id");
        hideArchiveFilePopup();
        $("#accept-download-archive-file-button").attr('data-id', fileId);
    });

    $("#accept-download-archive-file-button").on("click", function () {
        var fileId = {
            fileId: $(this).attr('data-id')
        };
        var query = $.param(fileId);
        $("#downloadFileModal").modal('hide');
        window.location.href = applicationEmployeeLocation + "/file/archive?" + query;
    });



    //EVENTS


    var clearEventsPopups = function () {
        $("#error-load-calendar").addClass('hide');
        $("#error-load-timetable").addClass('hide');
        $("#error-load-event-information").addClass('hide');
        $("#error-search-user-popup").addClass('hide');
        $("#success-remove-event-popup").addClass('hide');
        $("#error-remove-event-popup").addClass('hide');

    };

    //search selected event

    //1.1) from calendar

    var eventData = {};
    var actuallyLoggedUserId;

    $("#caleandar").on('click',".event", function (event) {
        clearEventsPopups();
        $.ajax({
            type: "GET",
            url: applicationEmployeeLocation + "/loggedUser",
            success: function (data) {
                actuallyLoggedUserId = data;
            },
            error: function (xhr, data) {
                $("#error-load-event-information").removeClass("hide");
            }
        });

        var selectedModalEventId = $(event.currentTarget).data('id');

        $.ajax({
            type: "GET",
            url :  applicationEmployeeLocation + "/timetable/event/user/" + selectedModalEventId,
            success: function(data) {
                eventData = data;
                $("#eventInformationModal").modal();
                eventData = {};

            },
            error : function(xhr,data) {
                $("#error-load-event-information").removeClass("hide");
            }
        });

    });

    // 1.2) from calendar

    $("#timetable").on('click',".event", function (event) {
        clearEventsPopups();
        $.ajax({
            type: "GET",
            url: applicationEmployeeLocation + "/loggedUser",
            success: function (data) {
                actuallyLoggedUserId = data;
            },
            error: function (xhr, data) {
                $("#error-load-event-information").removeClass("hide");
            }
        });

        var selectedModalEventId = $(event.currentTarget).data('id');

        $.ajax({
            type: "GET",
            url : applicationEmployeeLocation + "/timetable/event/user/" + selectedModalEventId,
            success: function(data) {
                eventData = data;
                $("#eventInformationModal").modal();
                eventData = {};

            },
            error : function(xhr,data) {
                $("#error-load-event-information").removeClass("hide");
            }
        });

    });

    //event infomation modal

    var editButton = null;
    var deleteButton = null;
    $('#eventInformationModal').on('show.bs.modal', function (event) {

        var userFirstName= eventData.userFirstName;
        var userLastName = eventData.userLastName;
        var userId = eventData.userId;
        var eventDate = eventData.date;
        var startTime = eventData.timeFrom;
        var endTime = eventData.timeTo;

        var topic = eventData.topic;
        var message = eventData.message;
        var eventId = eventData.eventId;

        var modal = $(this);
        modal.find('#topic-event-information').text(topic);
        modal.find('#user-event-information').text(userFirstName + " " + userLastName);
        modal.find('#date-event-information').text(eventDate);
        modal.find('#time-start-event-information').text(startTime);
        modal.find('#time-end-event-information').text(endTime);
        modal.find('#message-event-textarea-information').val(message);
        if(actuallyLoggedUserId !== userId){
            $("#edit-event-modal-button").removeAttr("data-id");
            $("#remove-event-modal-button").removeAttr("data-id");
            editButton = $("#edit-event-modal-button").detach();
            deleteButton =  $("#remove-event-modal-button").detach();
        }else{
            if(editButton !== null){
                $("#modal-buttons-footer").prepend(editButton);
                editButton = null;
            }
            if(deleteButton !== null) {
                $("#modal-buttons-footer").prepend(deleteButton);
                deleteButton = null;
            }
            $("#edit-event-modal-button").attr("data-id", eventId);
            $("#remove-event-modal-button").attr("data-id", eventId);
        }
        actuallyLoggedUserId = null;

    });



    $('#userInformationEventModal').on('show.bs.modal', function (event) {
        var firstName = userData.firstName;
        var lastName = userData.lastName;
        var email = userData.email;
        var id = userData.id;

        var modal = $(this);
        modal.find('#first-name-user').text(firstName);
        modal.find('#last-name-user').text(lastName);
        modal.find('#email-user').text(email);
        modal.find(".send-message-button").attr("user-id",id);
        userData = null;
    });

    //remove event

    if(localStorage.getItem("show-success-remove-event-popup") === 'true'){
        $("#success-remove-event-popup").removeClass("hide");
        localStorage.removeItem("show-success-remove-event-popup");
    }

    $(document).on("click", "#remove-event-modal-button", function(){
        clearEventsPopups();
        var eventId = $(this).data("id");
        $("#accept-remove-event-button").attr('data-id', eventId);
    });


    $("#accept-remove-event-button").click(function () {
        var eventId = $(this).data('id');
        $.ajax({
            type: "DELETE",
            url : applicationEmployeeLocation + "/timetable/event/" + eventId,
            success: function(data) {
                localStorage.setItem("show-success-remove-event-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-remove-event-popup").removeClass('hide');
                $("#removeEventModal").modal('hide');
            }
        });
    });

    //edit event

    var clearEditEventPopups = function () {
        $("#success-edit-event-popup").addClass('hide');
        $("#error-edit-event-popup").addClass('hide');
    };

    $(document).on("click", "#edit-event-modal-button", function(){
        clearEditEventPopups();
        var eventId = $(this).data('id');
        window.location = applicationEmployeeLocation + "/timetable/event/" +eventId + "/data";
    });

    //user information

    var userData;

    $("#timetable").on('click',".open-info-user-event-button", function (event) {
        clearEventsPopups();
        var selectedUserModalId = $(event.currentTarget).data('id');
        $.ajax({
            type: "GET",
            url : applicationEmployeeLocation + "/user/" + selectedUserModalId,
            success: function(data) {
                userData = data;
                $("#userInformationEventModal").modal();
            },
            error : function(xhr,data) {
                $("#error-search-user-popup").removeClass('hide');
                $("#popup-event-info").addClass('hide');
            }
        });
    });

});

