$(document).ready(function () {

    var applicationAdministratorLocation = "http://localhost:8080/admin";

    //dropdown slide
    $(".dropdown").hover(
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');
        }
    );





    //function to serialize to json
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };


    //USERS

    $(".pagination-users-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationAdministratorLocation + "/users?page=" + page;
    });

    //search

    var searchUsersText;

    $("#search-users-form").submit(function (e) {
        e.preventDefault();
        searchUsersText = $('#search-text-input').val();
        $.ajax({
            type: "GET",
            url : applicationAdministratorLocation + "/users/search",
            data:{
                searchString : searchUsersText
            },
            success: function(response) {
                $("#search-users-container").html(response);
            }
        });
    });



    $('#employee-checkbox').change(function() {
        $(".checkbox-inline").removeClass('invalid-string');
        $("#help-block-roles-error").addClass('hide');
    });

    $('#admin-checkbox').change(function() {
        $(".checkbox-inline").removeClass('invalid-string');
        $("#help-block-roles-error").addClass('hide');
    });



    //add user

    var clearAddUserInputs = function () {
        $("#email-input").val('');
        $("#firstName-input").val('');
        $("#lastName-input").val('');
        $("#employee-checkbox").prop('checked', true);
        $("#admin-checkbox").prop('checked', false);
    };

    var hideAddUserPopupsAndInvalidOptions = function () {
        $("#success-add-user-popup:visible").addClass("hide");
        $("#bad-email-add-user-popup:visible").addClass("hide");
        $("#not-validate-add-user-data-popup:visible").addClass("hide");
        $("#help-block-roles-error").addClass('hide');

    };



    $("#load-new-user-page-button").click(function() {
        window.location = applicationAdministratorLocation + "/user";
    });

    $("#add-user-form").submit(function (e) {
        e.preventDefault();
        hideAddUserPopupsAndInvalidOptions();
        var form = $("#add-user-form").serializeObject();
        var roleCheckboxVal = form.roles;
        if(roleCheckboxVal == undefined){
            $("#help-block-roles-error").removeClass('hide');
            $(".checkbox-inline").addClass('invalid-string');
        } else {
            if (!$.isArray(roleCheckboxVal)) {
                form.roles = [roleCheckboxVal];
            }
            var json = JSON.stringify(form);
            console.log(json);
            $.ajax({
                type: "POST",
                url: applicationAdministratorLocation + "/user",
                data: json,
                contentType: "application/json",
                success: function (data) {
                    $("#success-add-user-popup").removeClass('hide');
                    clearAddUserInputs();
                },
                error: function (xhr) {
                    if (xhr.status === 400) {
                        $("#not-validate-add-user-data-popup").removeClass('hide');
                    } else if (xhr.status === 409) {
                        $("#bad-email-add-user-popup").removeClass('hide');
                    }
                }
            });
        }
    });

    //edit user


    var hideEditUserPopupsAndInvalidOptions = function () {
        $("#success-edit-user-popup:visible").addClass("hide");
        $("#bad-email-edit-user-popup:visible").addClass("hide");
        $("#bad-login-edit-user-popup:visible").addClass("hide");
        $("#not-validate-edit-user-data-popup:visible").addClass("hide");
        $("#edit-user-not-found-popup:visible").addClass("hide");
        $("#help-block-roles-error").addClass('hide');
    };

    $(document).on("click", ".edit-user-button", function(){
        var userId = $(this).attr("user-id");
        window.location = applicationAdministratorLocation + "/user/" + userId;
    });

    $("#edit-user-form").submit(function (e) {
        e.preventDefault();
        hideEditUserPopupsAndInvalidOptions();
        var form = $("#edit-user-form").serializeObject();
        var roleCheckboxVal = form.roles;
        if(roleCheckboxVal == undefined){
            $("#help-block-roles-error").removeClass('hide');
            $(".checkbox-inline").addClass('invalid-string');
        } else {
            if (!$.isArray(roleCheckboxVal) && roleCheckboxVal != undefined) {
                form.roles = [roleCheckboxVal];
            }
            var json = JSON.stringify(form);
            $.ajax({
                type: "PUT",
                url: window.location.href ,
                data: json,
                contentType: "application/json",
                success: function () {
                    $("#success-edit-user-popup").removeClass('hide');
                },
                error: function (xhr) {
                    if (xhr.status === 400) {
                        $("#not-validate-edit-user-data-popup").removeClass('hide');
                    } else if(xhr.status === 404) {
                        $("#edit-user-not-found-popup").removeClass('hide');
                    }else  if (xhr.status === 409) {
                        if(xhr.responseJSON === "USER_EMAIL_EXIST" ){
                            $("#bad-email-edit-user-popup").removeClass('hide');
                        }else  if(xhr.responseJSON === "USER_LOGIN_EXIST" ){
                            $("#bad-login-edit-user-popup").removeClass('hide');
                        }
                    }
                }
            });
        }
    });


    //change user status

    var hideChangeUserStatusAlerts = function () {
        $("#success-enable-user-popup").hide();
        $("#success-disable-user-popup").hide();
        $("#error-enable-user-popup").hide();
        $("#error-disable-user-popup").hide();
    };

    var successDisable = localStorage.getItem("show-success-disable-user-popup");
    if (successDisable === 'true'){
        $("#success-disable-user-popup").removeClass('hide');
        localStorage.removeItem("show-success-disable-user-popup");
    }


    var successEnable = localStorage.getItem("show-success-enable-user-popup");
    if (successEnable === 'true'){
        $("#success-enable-user-popup").removeClass('hide');
        localStorage.removeItem("show-success-enable-user-popup");
    }

    if (localStorage.getItem("show-search-again-user") === 'true'){
        $.ajax({
            type: "GET",
            url : applicationAdministratorLocation + "/users/search",
            data:{
                searchString : searchUsersText
            },
            success: function(response) {
                $("#search-user-container").html(response);
            }
        });
        localStorage.removeItem("show-search-again-user");
    }

    //1.disable user



    $(document).on("click", ".disable-user-button", function(){
        var userId = $(this).data("id");
        $("#accept-disable-user-button").attr('data-id', userId);
        hideChangeUserStatusAlerts();
    });

    $("#accept-disable-user-button").click(function(){
        var userId = $(this).attr('data-id');
        $.ajax({
            type: "PUT",
            url : applicationAdministratorLocation + "/user/" + userId + "/disable",
            success: function(data) {
                localStorage.setItem('show-success-disable-user-popup', 'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-disable-user-popup").removeClass('hide');
            }
        });
    });


    $(document).on("click", ".disable-user-search-button", function(){
        var userId = $(this).data("id");
        $("#accept-disable-user-search-button").attr('data-id', userId);
        hideChangeUserStatusAlerts();
    });

    $(document).on("click", "#accept-disable-user-search-button", function(){
        var userId = $(this).attr('data-id');
        $.ajax({
            type: "PUT",
            url : applicationAdministratorLocation + "/user/" + userId + "/disable",
            success: function(data) {
                localStorage.setItem('show-success-disable-user-popup', 'true');
                localStorage.setItem('show-search-again-user', 'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-disable-user-popup").removeClass('hide');
            }
        });
    });


    //2.enable user

    $(document).on("click", ".enable-user-button", function(){
        var userId = $(this).data("id");
        $("#accept-enable-user-button").attr('data-id', userId);
        hideChangeUserStatusAlerts();
    });

    $("#accept-enable-user-button").click(function(){
        var userId = $(this).attr('data-id');
        $.ajax({
            type: "PUT",
            url : applicationAdministratorLocation + "/user/" + userId + "/enable",
            success: function(data) {
                localStorage.setItem('show-success-enable-user-popup', 'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-enable-user-popup").removeClass('hide');

            }
        });
    });

    $(document).on("click", ".enable-user-search-button", function(){
        var userId = $(this).data("id");
        $("#accept-enable-user-search-button").attr('data-id', userId);
        hideChangeUserStatusAlerts();
    });

    $(document).on("click", "#accept-enable-user-search-button", function(){
        var userId = $(this).attr('data-id');
        $.ajax({
            type: "PUT",
            url : applicationAdministratorLocation + "/user/" + userId + "/enable",
            success: function(data) {
                localStorage.setItem('show-success-enable-user-popup', 'true');
                localStorage.setItem('show-search-again-user', 'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-enable-user-popup").removeClass('hide');
            }
        });
    });



    //GROUP

    //search

    $(".pagination-groups-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationAdministratorLocation + "/groups?page=" + page;
    });

    var searchGroupsText;
    $("#search-groups-form").submit(function (e) {
        e.preventDefault();
        searchGroupsText = $('#search-text-input').val();
        $.ajax({
            type: "GET",
            url : applicationAdministratorLocation + "/groups/search",
            data:{
                searchString : searchGroupsText
            },
            success: function(response) {
                $("#search-groups-container").html(response);
            }
        });
    });

    //delete group

    var clearGroupsPopup = function () {
        $("#success-delete-group-popup").addClass("hide");
        $("#error-delete-group-popup").addClass("hide");
    };

    var successDelete = localStorage.getItem("show-success-delete-group-popup");
    if (successDelete === 'true'){
        $("#success-delete-group-popup").removeClass('hide');
        localStorage.removeItem("show-success-delete-group-popup");
    }

    if (localStorage.getItem("show-search-again-group") === 'true'){
        $.ajax({
            type: "GET",
            url : applicationAdministratorLocation + "/groups/search",
            data:{
                searchString : searchGroupsText
            },
            success: function(response) {
                $("#search-groups-container").html(response);
            }
        });
        localStorage.removeItem("show-search-again-group");
    }

    $(document).on("click", ".delete-group-button", function(){
        clearGroupsPopup();
        var groupId = $(this).data("id");
        $("#accept-delete-group-button").attr('group-id', groupId);
    });

    $("#accept-delete-group-button").click(function(){
        var groupId = $(this).attr('group-id');
        $.ajax({
            type: "DELETE",
            url : applicationAdministratorLocation + "/group/" + groupId,
            success: function(data) {
                localStorage.setItem('show-success-delete-group-popup', 'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-delete-group-popup").removeClass('hide');
            }
        });
    });

    $(document).on("click", ".delete-group-search-button", function(){
        clearGroupsPopup();
        var groupId = $(this).data("id");
        $("#accept-delete-group-search-button").attr('group-id', groupId);
    });


    $(document).on("click", "#accept-delete-group-search-button", function(){
        var groupId = $(this).attr('group-id');
        $.ajax({
            type: "DELETE",
            url : applicationAdministratorLocation + "/group/" + groupId,
            success: function(data) {
                localStorage.setItem('show-success-delete-group-popup', 'true');
                localStorage.setItem('show-search-again-group', 'true');
                location.reload();

            },
            error : function(xhr,data) {
                $("#error-delete-group-popup").removeClass('hide');
            }
        });
    });



    //FILE TO DOWNLOAD

    //TEMPORARY FILES

    $(".pagination-temporary-file-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationAdministratorLocation + "/files/?page=" + page;
    });

    var hideTemporaryFilePopup = function () {
        $("#error-accept-temporary-file-popup").addClass('hide');
        $("#success-accept-temporary-file-popup").addClass('hide');
        $("#success-reject-temporary-file-popup").addClass('hide');
        $("#error-reject-temporary-file-popup").addClass('hide');
        $("#success-download-temporary-file-popup").addClass("hide");
        $("#error-download-temporary-file-popup").addClass("hide");
        $("#error-accept-temporary-file-with-name-popup").addClass("hide");

    };
    //accept temporary file

    if(localStorage.getItem("show-success-accept-temp-file-popup") === 'true'){
        $("#success-accept-temporary-file-popup").removeClass('hide');
        localStorage.removeItem("show-success-accept-temp-file-popup");
    }

    $(document).on("click", ".accept-temporary-file-button", function(){
        var fileId = $(this).data("id");
        hideTemporaryFilePopup();
        $("#accept-accept-temporary-file-button").attr('data-id', fileId);
    });

    $("#accept-accept-temporary-file-button").on("click", function () {
        var fileId = $(this).attr('data-id');
        $.ajax({
            type: "PUT",
            url : applicationAdministratorLocation + "/file/" + fileId,
            success: function(data) {
                localStorage.setItem("show-success-accept-temp-file-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                if (xhr.status === 409) {
                    $("#error-accept-temporary-file-with-name-popup").removeClass('hide');
                }else{
                    $("#error-accept-temporary-file-popup").removeClass('hide');
                }
                $("#acceptTemporaryFileModal").modal('hide');
            }
        });
    });

    //reject temporary file
    if(localStorage.getItem("show-success-reject-temp-file-popup") === 'true'){
        $("#success-reject-temporary-file-popup").removeClass('hide');
        localStorage.removeItem("show-success-reject-temp-file-popup");
    }

    $(document).on("click", ".reject-temporary-file-button", function(){
        var fileId = $(this).data("id");
        hideTemporaryFilePopup();
        $("#accept-reject-temporary-file-button").attr('data-id', fileId);
    });

    $("#accept-reject-temporary-file-button").on("click", function () {
        var fileId = $(this).attr('data-id');
        $.ajax({
            type: "DELETE",
            url : applicationAdministratorLocation + "/file/" + fileId,
            success: function(data) {
                localStorage.setItem("show-success-reject-temp-file-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-reject-temporary-file-popup").removeClass('hide');
                $("#rejectTemporaryFileModal").modal('hide')
            }
        });
    });

    //download

    $(document).on("click", ".download-temporary-file-button", function(){
        hideTemporaryFilePopup();
        var fileId = $(this).data("id");
        $("#accept-download-temporary-file-button").attr('data-id', fileId);
    });

    $("#accept-download-temporary-file-button").on("click", function () {
        var fileId = {
            fileId: $(this).attr('data-id')
        };
        var query = $.param(fileId);
        window.location.href = applicationAdministratorLocation + "/file?" + query;
        $("#success-download-temporary-file-popup").removeClass("hide");
        $("#downloadTemporaryFileModal").modal("hide");
    });

    //FORM FILES

    if(localStorage.getItem("show-success-upload-form-file-popup") === 'true'){
        $("#success-upload-form-file-popup").removeClass("hide");
        localStorage.removeItem("show-success-upload-form-file-popup");
    }

    if(localStorage.getItem("show-success-change-status-form-file-popup") === 'true'){
        $("#success-change-status-form-file-popup").removeClass("hide");
        localStorage.removeItem("show-success-change-status-form-file-popup");
    }

    if(localStorage.getItem("show-success-delete-form-file-popup") === 'true'){
        $("#success-delete-form-file-popup").removeClass("hide");
        localStorage.removeItem("show-success-delete-form-file-popup");
    }


    var hideFormFilePopup = function () {
        $("#success-upload-form-file-popup").addClass("hide");
        $("#success-download-form-file-popup").addClass("hide");
        $("#error-upload-form-file-popup").addClass("hide");
        $("#success-delete-form-file-popup").addClass("hide");
        $("#error-delete-form-file-popup").addClass("hide");
        $("#success-change-status-form-file-popup").addClass("hide");
        $("#error-change-status-form-file-popup").addClass("hide");
        $("#error-change-status-form-file-name-popup").addClass("hide");
    };

    //find

    $(".pagination-form-files-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationAdministratorLocation + "/files/form?page=" + page;
    });


    //search file

    $("#search-form-files-form").submit(function (e) {
        e.preventDefault();
        hideFormFilePopup();
        var searchText = $('#search-text-input').val();
        $.ajax({
            type: "GET",
            url : applicationAdministratorLocation + "/files/form/search",
            data:{
                searchString : searchText
            },
            success: function(response) {
                $("#search-form-files-result").html(response);
            }
        });
    });

    //upload

    $("#upload-form-file").on("submit", function (e) {
        e.preventDefault();
        hideFormFilePopup();
        var form = $(this);
        var tmp = new FormData(form[0]);
        $.ajax({
            type: "POST",
            url : applicationAdministratorLocation + "/file/form",
            data: tmp,
            processData: false,
            contentType: false,
            success: function() {
                localStorage.setItem("show-success-upload-form-file-popup",'true');
                location.reload();
            },
            error : function(xhr) {
                if (xhr.status === 409) {
                    $("#error-upload-name-form-file-popup").removeClass("hide");
                }else  if (xhr.status === 406) {
                    $("#error-upload-size-form-file-popup").removeClass("hide");
                }else{
                    $("#error-upload-form-file-popup").removeClass("hide");
                }
            }
        });
    });

    //download

    $(document).on("click", ".download-form-file-button", function(){
        var fileId = $(this).data("id");
        hideFormFilePopup();
        $("#accept-download-form-file-button").attr('data-id', fileId);
    });

    $("#accept-download-form-file-button").on("click", function () {
        var fileId = {
            fileId: $(this).attr('data-id')
        };
        var query = $.param(fileId);
        window.location.href = applicationAdministratorLocation + "/file/form?" + query;
        $("#success-download-form-file-popup").removeClass("hide");
        $("#downloadFileModal").modal("hide");
    });

    //change status

    $(document).on("click", ".change-form-status-file-button", function(){
        var fileId = $(this).data("id");
        hideFormFilePopup();
        $("#accept-change-form-status-file-button").attr('data-id', fileId);
    });

    $("#accept-change-form-status-file-button").on("click", function () {
        var fileId = $(this).attr('data-id');
        $.ajax({
            type: "PUT",
            url : applicationAdministratorLocation + "/file/form/status/" + fileId,
            success: function(data) {
                localStorage.setItem("show-success-change-status-form-file-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                if (xhr.status === 409) {
                    $("#error-change-status-form-file-name-popup").removeClass("hide");
                }else {
                    $("#error-change-status-form-file-popup").removeClass("hide");
                }
                $("#changeStatusFileModal").modal('hide');
            }
        });
    });

    //delete

    $(document).on("click", ".delete-form-file-button", function(){
        var fileId = $(this).data("id");
        hideFormFilePopup();
        $("#accept-delete-form-file-button").attr('data-id', fileId);
    });

    $("#accept-delete-form-file-button").on("click", function () {
        var fileId = $(this).attr('data-id');
        $.ajax({
            type: "DELETE",
            url : applicationAdministratorLocation + "/file/form/" + fileId,
            success: function(data) {
                localStorage.setItem("show-success-delete-form-file-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-delete-form-file-popup").removeClass("hide");
            }
        });
    });



    //ARCHIVE FILES

    $("#load-upload-component-button").click(function () {
        $(".upload-component").slideDown("slow");
        $(".upload-button-component").slideUp();
    });

    if(localStorage.getItem("show-success-upload-archive-file-popup") === 'true'){
        $("#success-upload-archive-file-popup").removeClass("hide");
        localStorage.removeItem("show-success-upload-archive-file-popup");
    }

    if(localStorage.getItem("show-success-change-status-archive-file-popup") === 'true'){
        $("#success-change-status-archive-file-popup").removeClass("hide");
        localStorage.removeItem("show-success-change-status-archive-file-popup");
    }

    if(localStorage.getItem("show-success-delete-archive-file-popup") === 'true'){
        $("#success-delete-archive-file-popup").removeClass("hide");
        localStorage.removeItem("show-success-delete-archive-file-popup");
    }


    var hideArchiveFilePopup = function () {
        $("#success-download-archive-file-popup").addClass("hide");
        $("#success-upload-archive-file-popup").addClass("hide");
        $("#error-upload-archive-file-popup").addClass("hide");
        $("#success-delete-archive-file-popup").addClass("hide");
        $("#error-delete-archive-file-popup").addClass("hide");
        $("#success-change-status-archive-file-popup").addClass("hide");
        $("#error-change-status-archive-file-popup").addClass("hide");
        $("#error-upload-size-archive-file-popup").addClass("hide");
        $("#error-upload-name-archive-file-popup").addClass("hide");
        $("#error-change-status-archive-file-name-popup").addClass("hide");
    };

    //find

    $(".pagination-archive-files-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationAdministratorLocation + "/files/archive?page=" + page;
    });

    //search files

    $("#search-archive-files-form").submit(function (e) {
        e.preventDefault()
        var searchText = $('#search-text-input').val();
        $.ajax({
            type: "GET",
            url : applicationAdministratorLocation + "/files/archive/search",
            data:{
                searchString : searchText
            },
            success: function(response) {
                $("#search-archive-files-result").html(response);
            }
        });
    });


    //upload

    $("#upload-archive-file").on("submit", function (e) {
        e.preventDefault();
        hideArchiveFilePopup();
        var form = $(this);
        var tmp = new FormData(form[0]);
        $.ajax({
            type: "POST",
            url : applicationAdministratorLocation + "/file/archive",
            data: tmp,
            processData: false,
            contentType: false,
            success: function(data) {
                localStorage.setItem("show-success-upload-archive-file-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                if (xhr.status === 409) {
                    $("#error-upload-name-archive-file-popup").removeClass("hide");
                }else  if (xhr.status === 406) {
                    $("#error-upload-size-archive-file-popup").removeClass("hide");
                }else{
                    $("#error-upload-archive-file-popup").removeClass("hide");
                }
            }
        });
    });

    //download

    $(document).on("click", ".download-archive-file-button", function(){
        var fileId = $(this).data("id");
        hideArchiveFilePopup();
        $("#accept-download-archive-file-button").attr('data-id', fileId);
    });

    $("#accept-download-archive-file-button").on("click", function () {
        var fileId = {
            fileId: $(this).attr('data-id')
        };
        var query = $.param(fileId);
        window.location.href = applicationAdministratorLocation + "/file/archive?" + query;
        $("#success-download-archive-file-popup").removeClass("hide");
        $("#downloadFileModal").modal("hide");
    });


    //change status

    $(document).on("click", ".change-archive-status-file-button", function(){
        var fileId = $(this).data("id");
        hideArchiveFilePopup();
        $("#accept-change-archive-status-file-button").attr('data-id', fileId);
    });

    $("#accept-change-archive-status-file-button").on("click", function () {
        var fileId = $(this).attr('data-id');
        $.ajax({
            type: "PUT",
            url : applicationAdministratorLocation + "/file/archive/status/" + fileId,
            success: function(data) {
                localStorage.setItem("show-success-change-status-archive-file-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                if (xhr.status === 409) {
                    $("#error-change-status-archive-file-name-popup").removeClass("hide");
                }else {
                    $("#error-change-status-archive-file-popup").removeClass("hide");
                }
                $("#changeStatusFileModal").modal('hide');
            }
        });
    });


    //delete

    $(document).on("click", ".delete-archive-file-button", function(){
        var fileId = $(this).data("id");
        hideArchiveFilePopup();
        $("#accept-delete-archive-file-button").attr('data-id', fileId);
    });

    $("#accept-delete-archive-file-button").on("click", function () {
        var fileId = $(this).attr('data-id');
        $.ajax({
            type: "DELETE",
            url : applicationAdministratorLocation + "/file/archive/" + fileId,
            success: function(data) {
                localStorage.setItem("show-success-delete-archive-file-popup",'true');
                location.reload();
            },
            error : function(xhr,data) {
                $("#error-delete-archive-file-popup").removeClass("hide");
            }
        });
    });


    //ACCOUNT

    //back

    $(".back-account-button").click(function () {
        window.location = applicationAdministratorLocation + "/account";
    });



    var hideEditAccountUserPopups = function () {
        $("#success-edit-password-popup").addClass("hide");
        $("#bad-password-matches-popup").addClass("hide");
        $("#bad-new-passwords-matches-popup").addClass("hide");
        $("#bad-validation-popup").addClass("hide");
    };

    var clearInput = function () {
        $("#new-password-input").removeClass("incorrect-input");
        $("#repeat-new-password-input").removeClass("incorrect-input");
        $("#old-password-input").removeClass("incorrect-input");
    };


    $("#change-administrator-password-button").click(function () {
        window.location = applicationAdministratorLocation + "/account/password";
    });

    $("#change-password-form").submit(function (e) {
        e.preventDefault();
        hideEditAccountUserPopups();
        clearInput();
        var oldPassword = $(this).find('input[name="oldPassword"]').val();
        var newPassword = $(this).find('input[name="newPassword"]').val();
        var repeatNewPassword = $(this).find('input[name="repeatNewPassword"]').val();
        if(newPassword !== repeatNewPassword){
            $("#bad-new-passwords-matches-popup").removeClass("hide");

            $("#new-password-input").addClass("incorrect-input");
            $("#repeat-new-password-input").addClass("incorrect-input");
        }else {
            $.ajax({
                type: "PUT",
                url: applicationAdministratorLocation + "/account/password",
                data: {
                    oldPassword: oldPassword,
                    newPassword: newPassword
                },
                success: function (data) {
                    $("#success-edit-password-popup").removeClass("hide");
                },
                error: function (xhr, data){
                    if (xhr.status === 406) {
                        $("#bad-password-matches-popup").removeClass("hide");
                    }else{
                        $("#bad-validation-popup").removeClass("hide");
                    }
                    $("#old-password-input").addClass("incorrect-input");
                }
            });
        }
    });

    //change account data

    var hideEditAccountPopupsAndInvalidOptions = function () {
        $("#success-edit-account-popup:visible").addClass("hide");
        $("#bad-email-edit-account-popup:visible").addClass("hide");
        $("#bad-login-edit-account-popup:visible").addClass("hide");
        $("#not-validate-account-data-popup:visible").addClass("hide");
        $("#bad-edit-account:visible").addClass("hide");
        $("#help-block-roles-error").addClass('hide');
    };

    $("#change-administrator-account-data").click(function () {
        window.location = applicationAdministratorLocation + "/account/data";
    });

    $("#edit-account-form").submit(function (e) {
        e.preventDefault()
        hideEditAccountPopupsAndInvalidOptions();
        var form = $("#edit-account-form").serializeObject();
        var json = JSON.stringify(form);
        $.ajax({
            type: "PUT",
            url: window.location.href,
            data: json,
            contentType: "application/json",
            success: function (data) {
                $("#success-edit-account-popup").removeClass('hide');
            },
            error: function (xhr, data) {
                if (xhr.status === 400) {
                    $("#not-validate-account-data-popup").removeClass('hide');
                } else if(xhr.status === 404) {
                    $("#bad-edit-account").removeClass('hide');
                }else  if (xhr.status === 409) {
                    if(xhr.responseJSON === "USER_EMAIL_EXIST" ){
                        $("#bad-email-edit-account-popup").removeClass('hide');
                    }else  if(xhr.responseJSON === "USER_LOGIN_EXIST" ){
                        $("#bad-login-edit-account-popup").removeClass('hide');
                    }
                }
            }
        });
    });

    //MESSAGE-BOX

    //back

    $(".back-mailbox-button").click(function () {
        window.location = applicationAdministratorLocation + "/mail";
    });

    //load

    $(".pagination-messagebox-button").click(function(){
        var page = $(this).data("page");
        window.location = applicationAdministratorLocation + "/mail?page=" + page;
    });

    //select message

    $(".select-message-row").click(function () {
        var messageId = $(this).attr('message-id');
        window.location = applicationAdministratorLocation + "/mail/" + messageId;
    });

    //download attachment

    $(".download-attachment").click(function () {

        var attachmentId = {
            id: $(this).attr('data-id')
        };
        var query = $.param(attachmentId);
        window.location.href = applicationAdministratorLocation + "/mail/attachment?" + query;

    });



    //send message

    $("#send-message-form").submit(function (e) {
        e.preventDefault();

        var message = $("#message").val();
        var subject = $("#subject").val();
        var files= $("#message-attachment-files").prop('files');
        var formData = new FormData();
        for(var i=0; i < files.length; i++){
            formData.append('files', files[i]);
        }
        var userId = $("#send-email-button").attr('user-id');
        formData.append("idUserForSendMessage", userId);
        formData.append("message", message);
        formData.append("subject", subject);

            $.ajax({
            type: "POST",
            url : applicationAdministratorLocation + "/mail/message",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data) {
                $(".send-message-popup-container").slideDown("slow");
                $(".message-container").slideUp();
                $("#success-send-message-popup").removeClass("hide");
            },
            error : function(xhr,data) {
                $(".send-message-popup-container").slideDown("slow");
                $(".message-container").slideUp();
                $("#error-send-message-popup").removeClass("hide");
            }
        });
    });

    // send message

    $(".message-page-button").click(function () {
        var userId = $(this).attr('user-id');
        localStorage.setItem('back-user-page', window.location.href);
        window.location = applicationAdministratorLocation + "/mail/message/"+ userId ;
    });

    $(document).on("click", ".message-user-page-button", function(){
        var userId = $(this).attr('user-id');
        localStorage.setItem('back-user-page', window.location.href);
        window.location = applicationAdministratorLocation + "/mail/message/"+ userId ;
    });

    $(".back-users-button").click(function () {
        var url = localStorage.getItem('back-user-page');
        localStorage.removeItem("back-user-page");
        window.location = url;
    });

    //add attachment

    $("#message-attachment-files").change(function () {
        $("#message-attachment-files-list").html('');
        var inputFiles = $("#message-attachment-files")[0];
        if( inputFiles.files.length > 1){
            $(inputFiles.files).each(function () {
                $("#message-attachment-files-list").append("<li>" + this.name + "</li>");
            })
        }
    });

});

