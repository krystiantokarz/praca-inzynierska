package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

@DataJpaTest
@SpringBootTest
public class TimetableRepositoryTestJPA extends AbstractTransactionalTestNGSpringContextTests {


    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TimetableEventRepository timetableRepository;



    @Test
    public void shouldFindTimetableEventByCreatingUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent firstTimetableEvent = TimetableEventProvider.createTimetableEvent(user);
        TimetableEvent secondTimetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(firstTimetableEvent);
        entityManager.persist(secondTimetableEvent);

        //when
        List<TimetableEvent> resultList = timetableRepository.findByCreatingUser(user);

        //then
        assertThat(resultList).hasSize(2);
        assertThat(resultList).contains(firstTimetableEvent);
        assertThat(resultList).contains(secondTimetableEvent);

    }

    @Test
    public void shouldFindTimetableEventByDate() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        //when
        List<TimetableEvent> resultList = timetableRepository.findByEventDate(timetableEvent.getEventDate());

        //then
        assertThat(resultList).hasSize(1);
        assertThat(resultList).contains(timetableEvent);
    }

    @Test
    public void shouldFindTimetableEventByDateAndUserId() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        //when
        List<TimetableEvent> resultList = timetableRepository.findByEventDateAndCreatingUser(timetableEvent.getEventDate(),user);

        //then
        assertThat(resultList).hasSize(1);
        assertThat(resultList).contains(timetableEvent);

    }

    @Test
    public void shouldFindTimetableEventByIdIfExist() throws Exception {

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        //when
        TimetableEvent result = timetableRepository.findTimetableEventByIdIfExist(timetableEvent.getId());

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(timetableEvent);
    }

    @Test
    public void shouldFindAllEventsByEventDate() throws Exception {

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        //when
        List<TimetableEvent> result = timetableRepository.findAllByEventDate(
                timetableEvent.getEventDate().getDayOfMonth(),
                timetableEvent.getEventDate().getMonthValue(),
                timetableEvent.getEventDate().getYear()
        );


        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result).contains(timetableEvent);
    }

    @Test
    public void shouldFindAllEventsByEventDateForSelectedUser() throws Exception {

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        //when
        List<TimetableEvent> result = timetableRepository.findAllByEventDateForSelectedUser(
                timetableEvent.getEventDate().getDayOfMonth(),
                timetableEvent.getEventDate().getMonthValue(),
                timetableEvent.getEventDate().getYear(),
                user
        );


        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result).contains(timetableEvent);
    }

    @Test(expectedExceptions = TimetableEventNotExistException.class)
    public void shouldThrowExceptionWhenTimetableEventByIdNotExist() throws Exception {
        //given
        Long notExistId = 100L;
        //when
        timetableRepository.findTimetableEventByIdIfExist(notExistId);

    }

}