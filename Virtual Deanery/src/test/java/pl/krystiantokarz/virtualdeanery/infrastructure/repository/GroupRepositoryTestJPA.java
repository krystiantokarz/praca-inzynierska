package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DataJpaTest
@SpringBootTest
public class GroupRepositoryTestJPA extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private GroupRepository groupRepository;

    @Test
    public void shouldFindGroupByIdIfExist() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        //when
        Group result = groupRepository.findGroupByIdIfExist(group.getId());

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(group);
    }


    @Test
    public void shouldFindGroupsByNameContainingIgnoreCase() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        //when
        List<Group> result = groupRepository.findByNameContainingIgnoreCase(group.getName());

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result).contains(group);
    }


    @Test
    public void shouldReturnTrueWhenCheckGroupWithSelectedNameExistWithoutSelectedGroup() throws Exception {
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        Long notExistId = 100L;

        //when
        boolean result = groupRepository.checkGroupWithSelectedNameExistWithoutSelectedGroup(group.getName(), notExistId);

        //then
        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseWhenCheckGroupWithSelectedNameExistWithoutSelectedGroup() throws Exception {
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        //when
        boolean result = groupRepository.checkGroupWithSelectedNameExistWithoutSelectedGroup(group.getName(), group.getId());

        //then
        assertFalse(result);
    }


    @Test
    public void shouldCheckGroupWithSelectedNameExist() throws Exception {
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        //when
        boolean result = groupRepository.checkGroupWithSelectedNameExist(group.getName());

        //then
        assertTrue(result);
    }

    //user groups tests


    @Test
    public void shouldFindGroupsPageForSelectedUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        Page<Group> result = groupRepository.findGroupsForSelectedUser(user.getId(), new PageRequest(0, 10));

        //then
        assertThat(result.getContent()).isNotEmpty();
        assertThat(result.getContent()).contains(group);
    }

    @Test
    public void shouldFindGroupsListForSelectedUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        List<Group> result = groupRepository.findGroupsForSelectedUser(user.getId());

        //then
        assertThat(result).isNotEmpty();
        assertThat(result).contains(group);
    }

    @Test
    public void shouldFindUserGroupsByGroupId() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup firstUserGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(firstUserGroup);
        UserGroup secondUserGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(secondUserGroup);

        //when
        List<UserGroup> result = groupRepository.findUserGroupsByGroupId(group.getId());

        //then
        assertThat(result).isNotEmpty();
        assertThat(result).contains(firstUserGroup);
        assertThat(result).contains(secondUserGroup);
    }

    @Test
    public void shouldFindUserGroupByUserAndGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        UserGroup result = groupRepository.findUserGroupByUserAndGroup(user, group);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(userGroup);
    }

    @Test
    public void shouldFindUserGroupForAdminMember() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        UserGroup result = groupRepository.findUserGroupForAdminMember(user, group);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(userGroup);
    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldNotFindUserGroupForNotAdminGroupMember() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        groupRepository.findUserGroupForAdminMember(user, group);
    }

    @Test
    public void shouldDeleteUserGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        groupRepository.deleteUserGroup(userGroup);

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup.getId());
        assertThat(result).isNull();
    }

    @Test
    public void shouldDeleteAllUserGroupsForSelectedGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup firstUserGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(firstUserGroup);
        UserGroup secondUserGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(secondUserGroup);

        //when
        groupRepository.deleteAllUserGroupsForSelectedGroup(group);

        //then
        UserGroup firstDeleteUserGroups = entityManager.find(UserGroup.class, firstUserGroup.getId());
        assertThat(firstDeleteUserGroups).isNull();

        UserGroup secondDeleteUserGroups = entityManager.find(UserGroup.class, secondUserGroup.getId());
        assertThat(secondDeleteUserGroups).isNull();
    }

    @Test
    public void shouldReturnTrueWhenCheckLoggedUserIsAdminInSelectedGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        boolean result = groupRepository.checkLoggedUserIsAdminInSelectedGroup(user.getId(), group.getId());

        //then
        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseWhenCheckLoggedUserIsAdminInSelectedGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        boolean result = groupRepository.checkLoggedUserIsAdminInSelectedGroup(user.getId(), group.getId());

        //then
        assertFalse(result);
    }
}