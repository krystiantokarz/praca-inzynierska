package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class FileSizeConverterTest {


    @Test
    public void shouldConvertBytesToMegabytes_v0() throws Exception{
        Integer bytes = 10 * 1024 * 1024;

        int result = FileSizeConverter.convertBytesToMegabytes(bytes);

        assertThat(result).isEqualTo(10);
    }

    @Test
    public void shouldConvertBytesToMegabytes_v1() throws Exception{
        Integer bytes = 10 * 1024 * 1000;

        int result = FileSizeConverter.convertBytesToMegabytes(bytes);

        assertThat(result).isEqualTo(10);
    }

}