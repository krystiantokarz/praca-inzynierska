package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.file.out.FileToDownloadDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FileConverterTest {


    @Test
    public void shouldConvertFilesToDTOs() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);

        List<FileToDownloadDescriptionEntity> list = new ArrayList<>(Collections.singletonList(fileEntity));

        //when
        List<FileToDownloadDTO> result = FileConverter.convertFilesToDTOs(list);

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.get(0).getFileName()).isEqualTo(FileToDownloadDescriptionProvider.ORIGINAL_FILE_NAME);
        assertThat(result.get(0).getFileExtensionType()).isEqualTo("text");
        assertThat(result.get(0).getFileType()).isEqualTo(FileType.ARCHIVE.toString());
        assertThat(result.get(0).getFileSize()).isEqualTo(1);
    }

}