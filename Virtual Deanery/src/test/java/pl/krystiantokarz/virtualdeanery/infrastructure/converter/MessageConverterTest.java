package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageStatus;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out.MessageContentDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out.MessageDescriptionDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class MessageConverterTest {

    private static final MailMessageStatus deliveredMessageStatus = MailMessageStatus.DELIVERED;
    private static final MailMessageStatus readMessageStatus = MailMessageStatus.READ;

    @Test
    public void shouldConvertMessageToMessageContentDTO() throws Exception{
        //given
        User userFrom = UserProvider.createUserWithAllRoles();
        User userTo = UserProvider.createUserWithAllRoles();
        MailMessageEntity mailMessageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(userFrom, userTo);


        //when
        MessageContentDTO result = MessageConverter.convertMessageToMessageContentDTO(mailMessageEntity);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getAttachments()).isEmpty();
        assertThat(result.getSubject()).isEqualTo(mailMessageEntity.getSubject());
        assertThat(result.getMessage()).isEqualTo(mailMessageEntity.getMessage());
        assertThat(result.getFromFirstName()).isEqualTo(userFrom.getFirstName());
        assertThat(result.getFromLastName()).isEqualTo(userFrom.getLastName());
        assertThat(result.getAttachments()).isEmpty();
        assertThat(result.getStatus()).isEqualTo(deliveredMessageStatus.toString());

    }


    @Test
    public void shouldConvertMessagesToMessageDescriptionDTOs() throws Exception{
        //given
        User userFrom = UserProvider.createUserWithAllRoles();
        User userTo = UserProvider.createUserWithAllRoles();
        MailMessageEntity firstMailMessageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(userFrom, userTo);
        MailMessageEntity secondMailMessageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(userFrom, userTo);


        List<MailMessageEntity> mailMessageEntities = Arrays.asList(firstMailMessageEntity, secondMailMessageEntity);

        //when
        List<MessageDescriptionDTO> result = MessageConverter.convertMessagesToMessageDescriptionDTOs(mailMessageEntities);

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result).hasSize(2);

    }
}