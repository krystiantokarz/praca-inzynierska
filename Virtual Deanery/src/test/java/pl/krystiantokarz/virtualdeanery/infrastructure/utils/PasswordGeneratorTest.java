package pl.krystiantokarz.virtualdeanery.infrastructure.utils;

import org.testng.annotations.Test;

import java.util.regex.Pattern;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.testng.Assert.*;

public class PasswordGeneratorTest {

    @Test
    public void shouldGeneratePassword() throws Exception{
        //given
        int passwordLength = 10;

        //when
        String result = PasswordGenerator.generatePassword(passwordLength);

        //then
        assertThat(result).isNotNull();
        assertThat(result).hasSize(passwordLength);
    }

}