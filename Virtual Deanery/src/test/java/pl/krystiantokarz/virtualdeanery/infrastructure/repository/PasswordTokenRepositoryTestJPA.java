package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.passwordresettoken.PasswordResetToken;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.resetpassword.exception.PasswordResetTokenNotExistException;

import static org.assertj.core.api.Java6Assertions.assertThat;

@DataJpaTest
@SpringBootTest
public class PasswordTokenRepositoryTestJPA extends AbstractTransactionalTestNGSpringContextTests {


    private static final String TOKEN = "TOKEN";
    private static final String GENERATED_PASSWORD = "PASSWORD";

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PasswordTokenRepository passwordTokenRepository;


    @Test
    public void shouldFindTokenByTokenNumber() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        PasswordResetToken token = PasswordResetToken.from(TOKEN, GENERATED_PASSWORD, user);
        entityManager.persist(token);

        //when
        PasswordResetToken result = passwordTokenRepository.findByTokenIfExist(token.getToken());


        //then
        assertThat(result).isNotNull();
        assertThat(result.getPassword()).isEqualTo(GENERATED_PASSWORD);
        assertThat(result.getToken()).isEqualTo(TOKEN);
        assertThat(result.getUser()).isEqualTo(user);

    }

    @Test(expectedExceptions = PasswordResetTokenNotExistException.class)
    public void shouldThrowExceptionWhenTokenNotExist() throws Exception{

        PasswordResetToken result = passwordTokenRepository.findByTokenIfExist("NOT-EXIST");

    }

}