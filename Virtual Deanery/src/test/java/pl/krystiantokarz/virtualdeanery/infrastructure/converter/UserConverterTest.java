package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class UserConverterTest {


    @Test
    public void shouldConvertUserToDTO() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();

        //when
        FindUserDTO result = UserConverter.convertUserToDTO(user);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getEmail()).isEqualTo(user.getEmail());
        assertThat(result.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.getLastName()).isEqualTo(user.getLastName());
        assertThat(result.getLogin()).isEqualTo(user.getLoginData().getLogin());
    }

    @Test
    public void shouldConvertUsersToDTOs()throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();

        //when
        List<FindUserDTO> result = UserConverter.convertUsersToDTOs(Collections.singletonList(user));
        //then

        //then
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).hasSize(1);
        assertThat(result.get(0).getEmail()).isEqualTo(user.getEmail());
        assertThat(result.get(0).getFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.get(0).getLastName()).isEqualTo(user.getLastName());
        assertThat(result.get(0).getLogin()).isEqualTo(user.getLoginData().getLogin());


    }

}