package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FileTypeConverterTest {

    private static final String correctType = "application/json";

    @Test
    public void shouldConvertFileType() throws Exception{
        //when
        String result = FileTypeConverter.convertFileType(correctType);

        //then
        assertEquals(result,"json");
    }

    @Test
    public void shouldNotConvertFileType() throws Exception{

        //when
        String result = FileTypeConverter.convertFileType("NOT_FOUND");

        //then
        assertEquals(result,"not-found");
    }
}