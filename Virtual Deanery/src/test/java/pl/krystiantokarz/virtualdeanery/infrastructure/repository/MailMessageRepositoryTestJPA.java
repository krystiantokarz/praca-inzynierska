package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailAttachmentFileDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailAttachmentFileDescriptionNotExist;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailMessageNotExistException;

import java.util.Collections;

import static org.assertj.core.api.Java6Assertions.assertThat;

@DataJpaTest
@SpringBootTest
public class MailMessageRepositoryTestJPA extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MailMessageRepository messageRepository;

    @Test
    public void shouldFindMessagesForSelectedUserWhenMessagesNotExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        Page<MailMessageEntity> result = messageRepository.findMessagesForSelectedUser(user, new PageRequest(0, 10));

        //then
        assertThat(result).isNotNull();
        assertThat(result.getTotalElements()).isEqualTo(0);
        assertThat(result.getContent()).isEmpty();
    }

    @Test
    public void shouldFindMessagesForSelectedUser() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        MailMessageEntity mailMessageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(user, user);
        entityManager.persist(mailMessageEntity);

        //when
        Page<MailMessageEntity> result = messageRepository.findMessagesForSelectedUser(user, new PageRequest(0, 10));

        //then
        assertThat(result).isNotNull();
        assertThat(result.getTotalElements()).isEqualTo(1);
        assertThat(result.getContent()).contains(mailMessageEntity);
    }

    @Test(expectedExceptions = MailMessageNotExistException.class)
    public void shouldThrowExceptionWhenFindMessageByIdForSelectedUserWhenMessageAndUserNotExist() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Long notExistMessageId = 100L;

        //when
        MailMessageEntity result = messageRepository.findMailMessageByIdForSelectedRecipientIfExist(notExistMessageId,user);


    }

    @Test
    public void shouldFindMailMessageByIdForSelectedRecipientIfExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        MailMessageEntity mailMessageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(user, user);
        entityManager.persist(mailMessageEntity);

        //when
        MailMessageEntity result = messageRepository.findMailMessageByIdForSelectedRecipientIfExist(mailMessageEntity.getId(),user);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(mailMessageEntity);
    }


    @Test(expectedExceptions = MailAttachmentFileDescriptionNotExist.class)
    public void shouldThrowExceptionWhenFindMailAttachmentFileDescriptionByIdForSelectedRecipientNotExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        MailMessageEntity mailMessageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(user, user);
        entityManager.persist(mailMessageEntity);

        //when
        messageRepository.findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(mailMessageEntity.getId(),user);

    }


    @Test
    public void shouldFindMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        MailAttachmentFileDescription mailAttachmentFileDescription = MailAttachmentFileDescriptionProvider.createMailAttachmentFileDescription();
        entityManager.persist(mailAttachmentFileDescription);
        MailMessageEntity mailMessageEntity = MailMessageProvider.createMailMessageEntityWithAttachments(Collections.singletonList(mailAttachmentFileDescription),user, user);
        entityManager.persist(mailMessageEntity);

        //when
        MailAttachmentFileDescription result = messageRepository.findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(mailMessageEntity.getId(), user);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(mailAttachmentFileDescription);

    }

}