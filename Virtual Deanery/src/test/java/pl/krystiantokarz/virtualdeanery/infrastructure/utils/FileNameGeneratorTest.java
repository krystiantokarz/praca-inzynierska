package pl.krystiantokarz.virtualdeanery.infrastructure.utils;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.testng.Assert.*;

public class FileNameGeneratorTest {

    @Test
    public void shouldGenerateUniqueName(){

        //given
        Set<String> fileNameSet = new HashSet<>(100);
        //when

        for(int i=0; i <100; i++ ){
            fileNameSet.add(FileNameGenerator.generateFileIdentity());
        }

        //then
        assertThat(fileNameSet).isNotEmpty();
        assertThat(fileNameSet).hasSize(100);

    }
}