package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventCalendarDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventDescriptionDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventTimetableDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class TimetableEventConverterTest {

    @Test
    public void shouldConvertEventsToCalendarEventDTOs() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);

        //when
        List<EventCalendarDTO> result = TimetableEventConverter.convertEventsToCalendarEventDTOs(Collections.singletonList(timetableEvent));

        //then

        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result).hasSize(1);
        assertThat(result.get(0).getDate()).isEqualTo(timetableEvent.getEventDate());
        assertThat(result.get(0).getTopic()).isEqualTo(timetableEvent.getTopic());

    }

    @Test
    public void shouldEventToTimetableEventDescriptionDTO() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);

        //when
        EventDescriptionDTO result = TimetableEventConverter.convertEventToTimetableEventDescriptionDTO(timetableEvent);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getDate()).isEqualTo(timetableEvent.getEventDate());
        assertThat(result.getTimeFrom()).isEqualTo(timetableEvent.getTimeFrom());
        assertThat(result.getTimeTo()).isEqualTo(timetableEvent.getTimeTo());
        assertThat(result.getMessage()).isEqualTo(timetableEvent.getMessage());
        assertThat(result.getTopic()).isEqualTo(timetableEvent.getTopic());
        assertThat(result.getUserFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.getUserLastName()).isEqualTo(user.getLastName());

    }

    @Test
    public void shouldConvertEventsToTimetableEventDTOs() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);

        //when
        List<EventTimetableDTO> result = TimetableEventConverter.convertEventsToTimetableEventDTOs(Collections.singletonList(timetableEvent));

        //then

        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result).hasSize(1);
        assertThat(result.get(0).getDate()).isEqualTo(timetableEvent.getEventDate());
        assertThat(result.get(0).getUserFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.get(0).getUserLastName()).isEqualTo(user.getLastName());
        assertThat(result.get(0).getTimeFrom()).isEqualTo(timetableEvent.getTimeFrom());
        assertThat(result.get(0).getTimeTo()).isEqualTo(timetableEvent.getTimeTo());
        assertThat(result.get(0).getTopic()).isEqualTo(timetableEvent.getTopic());


    }
}