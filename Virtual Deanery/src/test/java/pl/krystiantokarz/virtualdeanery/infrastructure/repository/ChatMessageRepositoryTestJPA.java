package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.chat.ChatMessage;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.ChatMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import static org.assertj.core.api.Java6Assertions.assertThat;

@DataJpaTest
@SpringBootTest
public class ChatMessageRepositoryTestJPA extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Test
    public void shouldFindChatMessagesForGroupOrderByDateDesc() throws Exception{

        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);


        ChatMessage firstChatMessage = ChatMessageProvider.createChatMessage(user, group);
        ChatMessage secondChatMessage = ChatMessageProvider.createChatMessage(user, group);
        entityManager.persist(firstChatMessage);
        Thread.sleep(1000);
        entityManager.persist(secondChatMessage);
        entityManager.flush();

        Page<ChatMessage> result = chatMessageRepository.findByGroupOrderByDateDesc(group, new PageRequest(0, 10));

        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.getTotalElements()).isEqualTo(2);
        assertThat(result.getContent().get(0)).isEqualTo(secondChatMessage);
        assertThat(result.getContent().get(1)).isEqualTo(firstChatMessage);
    }

}