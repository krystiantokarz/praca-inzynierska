package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@SpringBootTest
public class UserRepositoryTestJPA extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;


    @Test
    public void shouldFindUserByLogin() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        User result = userRepository.findUserByLoginIfExist(user.getLoginData().getLogin());

        //then
        assertThat(result).isNotNull();
        assertThat(result.getLoginData().getLogin()).isEqualTo(user.getLoginData().getLogin());
        assertThat(result.getLoginData().getPassword()).isEqualTo(user.getLoginData().getPassword());
        assertThat(result.getEmail()).isEqualTo(user.getEmail());
        assertThat(result.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.getLastName()).isEqualTo(user.getLastName());
        assertThat(result.getRoles()).isNotEmpty();
        assertThat(result.getId()).isNotNull();
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowExceptionWhenUserWithSelectedLoginNotExist() throws Exception{

        //given
        String notExistLogin = "NOT-EXIST";

        //when
        User result = userRepository.findUserByLoginIfExist(notExistLogin);

    }


    @Test
    public void shouldFindUserByEmail() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        User result = userRepository.findUserByEmailIfExist(user.getEmail());

        //then
        assertThat(result).isNotNull();
        assertThat(result.getLoginData().getLogin()).isEqualTo(user.getLoginData().getLogin());
        assertThat(result.getLoginData().getPassword()).isEqualTo(user.getLoginData().getPassword());
        assertThat(result.getEmail()).isEqualTo(user.getEmail());
        assertThat(result.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.getLastName()).isEqualTo(user.getLastName());
        assertThat(result.getRoles()).isNotEmpty();
        assertThat(result.getId()).isNotNull();
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowExceptionWhenUserWithSelectedEmailNotExist() throws Exception{

        //given
        String notExistEmail = "NOT-EXIST";

        //when
        User result = userRepository.findUserByEmailIfExist(notExistEmail);

    }

    @Test
    public void shouldReturnTrueWhenUserWithSelectedEmailExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        boolean result = userRepository.checkUserWithSelectedEmailExist(user.getEmail());

        //then
        assertThat(result).isTrue();

    }

    @Test
    public void shouldReturnFalseWhenUserWithSelectedEmailNotExist() throws Exception{

        //when
        String notExistEmail = "NOT-EXIST";

        //when
        boolean result = userRepository.checkUserWithSelectedEmailExist(notExistEmail);

        //then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnTrueWhenUserWithSelectedEmailExistWithoutActuallyLoggedUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Long loggedUserId = -100L;

        //when
        boolean result = userRepository.checkUserWithSelectedEmailExistWithoutSelectedUser(user.getEmail(), loggedUserId);

        //then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenUserWithSelectedEmailNotExistWithoutActuallyLoggedUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        boolean result = userRepository.checkUserWithSelectedEmailExistWithoutSelectedUser(user.getEmail(), user.getId());

        //then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnTrueWhenUserWithSelectedLoginExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        boolean result = userRepository.checkUserWithSelectedLoginExist(user.getLoginData().getLogin());

        //then
        assertThat(result).isTrue();

    }

    @Test
    public void shouldReturnFalseWhenUserWithSelectedLoginNotExist() throws Exception{

        //given
        String notExistLogin = "NOT-EXIST";

        //when
        boolean result = userRepository.checkUserWithSelectedLoginExist(notExistLogin);

        //then
        assertThat(result).isFalse();
    }


    @Test
    public void shouldReturnTrueWhenUserWithSelectedLoginExistWithoutActuallyLoggedUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Long loggedUserId = -100L;

        //when
        boolean result = userRepository.checkUserWithSelectedLoginExistWithoutSelectedUser(user.getLoginData().getLogin(), loggedUserId);

        //then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenUserWithSelectedLoginNotExistWithoutActuallyLoggedUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        boolean result = userRepository.checkUserWithSelectedLoginExistWithoutSelectedUser(user.getLoginData().getLogin(), user.getId());

        //then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldFindUserByFirstNameIgnoreCaseAndLastNameIgnoreCase() throws Exception{
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        List<User> result = userRepository.findByFirstNameIgnoreCaseAndLastNameIgnoreCase(user.getFirstName(), user.getLastName());

        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).contains(user);

    }

    @Test
    public void shouldFindUserByFirstNameContainingIgnoreCase() throws Exception{
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        List<User> result = userRepository.findByFirstNameContainingIgnoreCase(user.getFirstName().toUpperCase());

        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).contains(user);

    }

    @Test
    public void shouldFindUserByLastNameContainingIgnoreCase() throws Exception{
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        List<User> result = userRepository.findByLastNameContainingIgnoreCase(user.getLastName().toLowerCase());

        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).contains(user);

    }

    @Test
    public void shouldNotFindUserSearchedString() throws Exception{
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        List<User> result = userRepository.findByLastNameContainingIgnoreCase("not-exist-string");

        assertThat(result).isNotNull();
        assertThat(result).isEmpty();
    }


    @Test
    public void shouldFindUserByIdIfExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        User result = userRepository.findUserByIdIfExist(user.getId());

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(user);
    }

    @Test
    public void findAllForSelectedPageAndPageSize() throws Exception{
        //given
        User firstUser = UserProvider.createUserWithAllRoles();
        User secondUser = UserProvider.createUserWithAllRoles();
        entityManager.persist(firstUser);
        entityManager.persist(secondUser);

        //when
        Page<User> result = userRepository.findAllForSelectedPageAndPageSize(0, 10);


        assertThat(result).isNotNull();
        assertThat(result.getTotalElements()).isEqualTo(2);
        assertThat(result.getContent()).contains(firstUser);
        assertThat(result.getContent()).contains(secondUser);

    }


}