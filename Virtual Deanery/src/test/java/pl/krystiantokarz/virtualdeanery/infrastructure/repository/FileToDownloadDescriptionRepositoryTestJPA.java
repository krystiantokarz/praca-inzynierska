package pl.krystiantokarz.virtualdeanery.infrastructure.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DataJpaTest
@SpringBootTest
public class FileToDownloadDescriptionRepositoryTestJPA extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;


    @Test
    public void shouldFindAllFileToDownloadDescriptionByFileStatusAndFileTypePage() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity);

        //when
        Page<FileToDownloadDescription> result = fileToDownloadDescriptionRepository.findAllFileToDownloadDescriptionByFileStatusAndFileTypePage(FileStatus.READY_TO_DOWNLOAD, FileType.ARCHIVE, new PageRequest(0, 10));

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.getTotalElements()).isEqualTo(1);
        assertThat(result.getContent()).contains(fileToDownloadDescriptionEntity);
    }

    @Test
    public void shouldFindAllReadyToDownloadFormFileDescription() throws Exception{
         //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity1 = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity1);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity2 = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity2);

        //when
        Page<FileToDownloadDescription> result = fileToDownloadDescriptionRepository.findAllFileToDownloadDescriptionByFileStatusPage(FileStatus.READY_TO_DOWNLOAD, new PageRequest(0, 10));

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.getTotalElements()).isEqualTo(1);
        assertThat(result.getContent()).contains(fileToDownloadDescriptionEntity1);
    }

    @Test
    public void shouldFindByOriginalFileNameContainingIgnoreCaseAndFileTypeAndFileStatus() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity1 = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity1);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity2 = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity2);

        //when
        List<FileReadyToDownload> result = fileToDownloadDescriptionRepository.findByOriginalFileNameContainingIgnoreCaseAndFileTypeAndFileStatus(fileToDownloadDescriptionEntity1.getOriginalFileName(), FileType.ARCHIVE, FileStatus.READY_TO_DOWNLOAD);

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result).contains(fileToDownloadDescriptionEntity1);
    }


    @Test
    public void shouldReturnTrueWhenCheckFileToDownloadWithSelectedOriginalNameAndTypeAndStatusExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity);

        //when
        boolean result = fileToDownloadDescriptionRepository.checkFileToDownloadWithSelectedOriginalNameAndTypeAndStatusExist(fileToDownloadDescriptionEntity.getOriginalFileName(), fileToDownloadDescriptionEntity.getFileType(), fileToDownloadDescriptionEntity.getFileStatus());

        //then
        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseWhenCheckFileToDownloadWithSelectedOriginalNameAndTypeAndStatusNotExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity);

        //when
        boolean result = fileToDownloadDescriptionRepository.checkFileToDownloadWithSelectedOriginalNameAndTypeAndStatusExist("NOT-EXIST-NAME", fileToDownloadDescriptionEntity.getFileType(), fileToDownloadDescriptionEntity.getFileStatus());

        //then
        assertFalse(result);
    }

    @Test
    public void shouldFindFileWaitingForAcceptedByFileId() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableFormFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity);

        //when
        FileWaitingForAccepted result = fileToDownloadDescriptionRepository.findFileWaitingForAcceptedByFileId(fileToDownloadDescriptionEntity.getId());

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(fileToDownloadDescriptionEntity);
    }

    @Test(expectedExceptions = FileNotExistException.class)
    public void shouldThrowExceptionWhenFindFileWaitingForAcceptedByFileIdNotExist() throws Exception{
        //given
        Long notExistId = 100L;

        //when
        fileToDownloadDescriptionRepository.findFileWaitingForAcceptedByFileId(notExistId);

    }

    @Test
    public void shouldFindAllFileReadyToDownloadPageByFileType() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity1 = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity1);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity2 = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity2);

        //when
        Page<FileReadyToDownload> result = fileToDownloadDescriptionRepository.findAllFileReadyToDownloadPageByFileType(FileType.ARCHIVE, new PageRequest(0, 10));

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.getContent()).hasSize(1);
        assertThat(result.getContent()).contains(fileToDownloadDescriptionEntity1);
    }

    @Test
    public void shouldFindAllFileWaitingForAcceptedPage() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity1 = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity1);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity2 = FileToDownloadDescriptionProvider.createWaitingForAcceptableFormFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity2);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity3 = FileToDownloadDescriptionProvider.createReadyToDownloadFormFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity3);

        //when
        Page<FileWaitingForAccepted> result = fileToDownloadDescriptionRepository.findAllFileWaitingForAcceptedPage(new PageRequest(0, 10));

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.getContent()).hasSize(2);
        assertThat(result.getContent()).contains(fileToDownloadDescriptionEntity1);
        assertThat(result.getContent()).contains(fileToDownloadDescriptionEntity2);
    }

    @Test
    public void findFileDescriptionByIdIfExist() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileToDownloadDescriptionEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileToDownloadDescriptionEntity);


        //then
        FileToDownloadDescription result = fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileToDownloadDescriptionEntity.getId());

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(fileToDownloadDescriptionEntity);
    }

    @Test(expectedExceptions = FileNotExistException.class)
    public void shouldThrowExceptionWhenFindFileDescriptionByIdNotExist() throws Exception {

        //given
        Long notExistId = 100L;

        //then
        fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(notExistId);

    }



}