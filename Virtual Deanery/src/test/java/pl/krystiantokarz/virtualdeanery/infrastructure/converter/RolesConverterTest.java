package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.role.Role;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.*;

public class RolesConverterTest {

    @Test
    public void shouldConvertUserRolesToStringList() throws Exception{
        //given
        List<Role> roles = new ArrayList<>();
        roles.add(Role.from(RoleEnum.EMPLOYEE));

        //when
        List<String> result = RolesConverter.convertUserRolesToStringList(roles);

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.get(0)).isEqualTo(RoleEnum.EMPLOYEE.toString());

    }
    @Test
    public void shouldConvertRolesStringToObjects() throws Exception{
        //given
        List<String> roles = new ArrayList<>();
        roles.add(RoleEnum.EMPLOYEE.toString());

        //when
        List<Role> result = RolesConverter.convertRolesStringToObjects(roles);

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.get(0).getRole()).isEqualTo(RoleEnum.EMPLOYEE);

    }
}