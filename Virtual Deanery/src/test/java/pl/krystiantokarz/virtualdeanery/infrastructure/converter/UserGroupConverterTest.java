package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.UserInGroupDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class UserGroupConverterTest {


    @Test
    public void shouldConvertUserGroupToUserInGroupDTO() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);

        //when
        List<UserInGroupDTO> result = UserGroupConverter.convertUserGroupToUserInGroupDTO(Collections.singletonList(userGroup));

        //then

        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).hasSize(1);
        assertThat(result.get(0).getFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.get(0).getLastName()).isEqualTo(user.getLastName());
    }

}