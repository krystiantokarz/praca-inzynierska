package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.chat.ChatMessage;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.chat.out.OutputChatMessageDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.ChatMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class ChatMessageConverterTest {

    @Test
    public void shouldConvertChatMessageToOutputDTO() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        ChatMessage chatMessage = ChatMessageProvider.createChatMessage(user, group);

        //when
        OutputChatMessageDTO result = ChatMessageConverter.convertChatMessageToOutputDTO(chatMessage);

        //then

        assertThat(result).isNotNull();
        assertThat(result.getDate()).isEqualTo(chatMessage.getDate());
        assertThat(result.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.getLastName()).isEqualTo(user.getLastName());
        assertThat(result.getMessage()).isEqualTo(chatMessage.getMessage());
    }


    @Test
    public void shouldConvertChatMessageListToOutputDTOList() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        ChatMessage chatMessage = ChatMessageProvider.createChatMessage(user, group);

        //when
        List<OutputChatMessageDTO> result = ChatMessageConverter.convertChatMessageListToOutputDTOList(Collections.singletonList(chatMessage));

        //then

        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.get(0).getDate()).isEqualTo(chatMessage.getDate());
        assertThat(result.get(0).getFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.get(0).getLastName()).isEqualTo(user.getLastName());
        assertThat(result.get(0).getMessage()).isEqualTo(chatMessage.getMessage());
    }


}