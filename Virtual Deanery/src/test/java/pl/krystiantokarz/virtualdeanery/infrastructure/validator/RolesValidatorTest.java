package pl.krystiantokarz.virtualdeanery.infrastructure.validator;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.utils.validator.RolesValidator;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

public class RolesValidatorTest {


    @Test
    public void shouldValidateRoles() throws Exception{
        //given
        List<String> roles = new ArrayList<>();
        roles.add(RoleEnum.EMPLOYEE.toString());
        roles.add(RoleEnum.ADMIN.toString());

        RolesValidator rolesValidator = new RolesValidator();
        //when
        boolean result = rolesValidator.isValid(roles, null);
        //then
        assertTrue(result);
    }

    @Test
    public void shouldNotValidateRoles() throws Exception{
        //given
        List<String> roles = new ArrayList<>();
        roles.add("ADMINNNNN");
        roles.add("EMPLOYEEEEEEE");

        RolesValidator rolesValidator = new RolesValidator();
        //when
        boolean result = rolesValidator.isValid(roles, null);
        //then
        assertFalse(result);
    }

}