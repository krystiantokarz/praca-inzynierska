package pl.krystiantokarz.virtualdeanery.infrastructure.converter;

import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.GroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.GroupWithAccessDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GroupConverterTest {



    @Test
    public void shouldConvertSingleGroupToDTOO() throws Exception{
        //given
        Group group = GroupProvider.createGroup();

        //when
        GroupDTO result = GroupConverter.convertSingleGroupToDTO(group);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getName()).isEqualTo(GroupProvider.GROUP_NAME);
        assertThat(result.getDescription()).isEqualTo(GroupProvider.GROUP_DESCRIPTION);
    }

    @Test
    public void shouldConvertGroupsToGroupsDTOs() throws Exception{
        //given
        Group firstGroup = GroupProvider.createGroup();
        Group secondGroup = GroupProvider.createGroup();
        List<Group> groups = Arrays.asList(firstGroup, secondGroup);

        //when
        List<GroupDTO> result = GroupConverter.convertGroupsToGroupsDTOs(groups);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).hasSize(2);
    }

    @Test
    public void shouldConvertGroupsToGroupsWithAccessDTOs() throws Exception{
        //given
        Group firstGroup = GroupProvider.createGroup();
        Field fieldFirstGroup = firstGroup.getClass().getDeclaredField("id");
        fieldFirstGroup.setAccessible(true);
        fieldFirstGroup.set(firstGroup,1L);
        Group secondGroup = GroupProvider.createGroup();
        Field fieldSecondGroup = secondGroup.getClass().getDeclaredField("id");
        fieldSecondGroup.setAccessible(true);
        fieldSecondGroup.set(secondGroup,1L);

        List<Group> firstGroupList = Collections.singletonList(firstGroup);
        List<Group> secondGroupList = Collections.singletonList(secondGroup);

        //when
        List<GroupWithAccessDTO> result = GroupConverter.convertGroupsToGroupsWithAccessDTOs(firstGroupList, secondGroupList);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).hasSize(1);
        assertThat(result.get(0).isAccess()).isTrue();
    }

}