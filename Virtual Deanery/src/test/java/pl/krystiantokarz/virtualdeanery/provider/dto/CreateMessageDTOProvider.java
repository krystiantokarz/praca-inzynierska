package pl.krystiantokarz.virtualdeanery.provider.dto;

import org.springframework.web.multipart.MultipartFile;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.in.CreateMessageDTO;

import java.util.List;

public class CreateMessageDTOProvider {

    public static final String MESSAGE = "TEST-MESSAGE";
    public static final String SUBJECT = "TEST-SUBJECT";

    public static CreateMessageDTO prepareCreateMessageDTOWithoutAttachments(){
        CreateMessageDTO createMessageDTO = new CreateMessageDTO();
        createMessageDTO.setFiles(null);
        createMessageDTO.setIdUserForSendMessage(1L);
        createMessageDTO.setMessage(MESSAGE);
        createMessageDTO.setSubject(SUBJECT);
        return createMessageDTO;
    }


    public static CreateMessageDTO prepareCreateMessageDTOWithAttachments(List<MultipartFile> files){
        CreateMessageDTO createMessageDTO = new CreateMessageDTO();
        createMessageDTO.setFiles(files);
        createMessageDTO.setIdUserForSendMessage(1L);
        createMessageDTO.setMessage(MESSAGE);
        createMessageDTO.setSubject(SUBJECT);
        return createMessageDTO;
    }
}
