package pl.krystiantokarz.virtualdeanery.provider.entity;

import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.domain.user.UserFactory;
import pl.krystiantokarz.virtualdeanery.domain.user.role.Role;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class UserProvider {

    public static String FIRST_NAME = "firstName-test";
    public static String LAST_NAME = "lastName-test";
    public static String EMAIL = "email-test@email.pl";
    public static String LOGIN = "login-test";
    public static String PASSWORD = "password-test";


    public static User createUserWithAllRoles(){
        List<Role> roles = new ArrayList<>();
        roles.add(Role.from(RoleEnum.EMPLOYEE));
        roles.add(Role.from(RoleEnum.ADMIN));
        return UserFactory.build(
                FIRST_NAME,
                LAST_NAME,
                EMAIL,
                LOGIN,
                PASSWORD,
                roles
        );
    }

    public static User createUserWithEmployeeRole(){
        List<Role> roles = new ArrayList<>();
        roles.add(Role.from(RoleEnum.EMPLOYEE));
        return UserFactory.build(
                FIRST_NAME,
                LAST_NAME,
                EMAIL,
                LOGIN,
                PASSWORD,
                roles
        );
    }

    public static User createUserWithAdminRole(){
        List<Role> roles = new ArrayList<>();
        roles.add(Role.from(RoleEnum.ADMIN));
        return UserFactory.build(
                FIRST_NAME,
                LAST_NAME,
                EMAIL,
                LOGIN,
                PASSWORD,
                roles
        );
    }

    public static User createAppUser(){
        List<Role> roles = new ArrayList<>();
        roles.add(Role.from(RoleEnum.EMPLOYEE));
        roles.add(Role.from(RoleEnum.ADMIN));
        return UserFactory.build(
                FIRST_NAME,
                LAST_NAME,
                "virtual.deanery@gmail.com",
                LOGIN,
                PASSWORD,
                roles
        );
    }
}
