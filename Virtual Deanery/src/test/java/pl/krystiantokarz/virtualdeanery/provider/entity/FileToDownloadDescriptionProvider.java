package pl.krystiantokarz.virtualdeanery.provider.entity;

import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

public abstract class FileToDownloadDescriptionProvider {

    public static final String ORIGINAL_FILE_NAME = "TEST-ORIGINAL_FILE-NAME";
    public static final String SAVED_FILE_NAME = "TEST-SAVED-FILE-NAME";
    public static final Integer FILE_SIZE = 10;
    public static final String FILE_EXTENSION_TYPE = "text/plain";


    public static FileToDownloadDescriptionEntity createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(User user){
        return FileToDownloadDescriptionEntity.from(
                ORIGINAL_FILE_NAME,
                SAVED_FILE_NAME,
                FILE_SIZE,
                FILE_EXTENSION_TYPE,
                FileType.ARCHIVE,
                FileStatus.WAITING_FOR_ACCEPTED,
                user
        );
    }

    public static FileToDownloadDescriptionEntity createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(User user,String savedFileName){
        return FileToDownloadDescriptionEntity.from(
                ORIGINAL_FILE_NAME,
                savedFileName,
                FILE_SIZE,
                FILE_EXTENSION_TYPE,
                FileType.ARCHIVE,
                FileStatus.WAITING_FOR_ACCEPTED,
                user
        );
    }

    public static FileToDownloadDescriptionEntity createWaitingForAcceptableFormFileToDownloadDescriptionEntity(User user){
        return FileToDownloadDescriptionEntity.from(
                ORIGINAL_FILE_NAME,
                SAVED_FILE_NAME,
                FILE_SIZE,
                FILE_EXTENSION_TYPE,
                FileType.FORM,
                FileStatus.WAITING_FOR_ACCEPTED,
                user
        );
    }

    public static FileToDownloadDescriptionEntity createWaitingForAcceptableFormFileToDownloadDescriptionEntity(User user,String savedFileName){
        return FileToDownloadDescriptionEntity.from(
                ORIGINAL_FILE_NAME,
                savedFileName,
                FILE_SIZE,
                FILE_EXTENSION_TYPE,
                FileType.FORM,
                FileStatus.WAITING_FOR_ACCEPTED,
                user
        );
    }


    public static FileToDownloadDescriptionEntity createReadyToDownloadArchiveFileToDownloadDescriptionEntity(User user){
        return FileToDownloadDescriptionEntity.from(
                ORIGINAL_FILE_NAME,
                SAVED_FILE_NAME,
                FILE_SIZE,
                FILE_EXTENSION_TYPE,
                FileType.ARCHIVE,
                FileStatus.READY_TO_DOWNLOAD,
                user
        );
    }

    public static FileToDownloadDescriptionEntity createReadyToDownloadArchiveFileToDownloadDescriptionEntity(User user,String savedFileName){
        return FileToDownloadDescriptionEntity.from(
                ORIGINAL_FILE_NAME,
                savedFileName,
                FILE_SIZE,
                FILE_EXTENSION_TYPE,
                FileType.ARCHIVE,
                FileStatus.READY_TO_DOWNLOAD,
                user
        );
    }


    public static FileToDownloadDescriptionEntity createReadyToDownloadFormFileToDownloadDescriptionEntity(User user){
        return FileToDownloadDescriptionEntity.from(
                ORIGINAL_FILE_NAME,
                SAVED_FILE_NAME,
                FILE_SIZE,
                FILE_EXTENSION_TYPE,
                FileType.FORM,
                FileStatus.READY_TO_DOWNLOAD,
                user
        );
    }

    public static FileToDownloadDescriptionEntity createReadyToDownloadFormFileToDownloadDescriptionEntity(User user, String savedFileName){
        return FileToDownloadDescriptionEntity.from(
                ORIGINAL_FILE_NAME,
                savedFileName,
                FILE_SIZE,
                FILE_EXTENSION_TYPE,
                FileType.FORM,
                FileStatus.READY_TO_DOWNLOAD,
                user
        );
    }


}
