package pl.krystiantokarz.virtualdeanery.provider.entity;

import pl.krystiantokarz.virtualdeanery.domain.group.Group;

public abstract class GroupProvider {

    public static String GROUP_NAME = "groupName-test";
    public static String GROUP_DESCRIPTION ="groupDescription-test";
    public static String GROUP_PASSWORD = "groupPassword-test";

    public static Group createGroup(){
        return Group.from(
                GROUP_NAME,
                GROUP_PASSWORD,
                GROUP_DESCRIPTION);
    }

}
