package pl.krystiantokarz.virtualdeanery.provider.entity;

import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.chat.ChatMessage;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

public abstract class ChatMessageProvider {

    public static String CHAT_MESSAGE = "chatMessage-test";

    public static ChatMessage createChatMessage(User user, Group group){
        return ChatMessage.from(
                CHAT_MESSAGE,
                group,
                user
        );
    }
}
