package pl.krystiantokarz.virtualdeanery.provider.entity;

import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;

public abstract class MailAttachmentFileDescriptionProvider {

    public static String ORIGINAL_NAME = "ORIGINAL-NAME-TEST";
    public static String SAVED_NAME = "SAVED-NAME-TEST";
    public static Integer fileSize = 10;
    public static String extensionType = "type";

    public static MailAttachmentFileDescription createMailAttachmentFileDescription(){
        return MailAttachmentFileDescription.from(
                ORIGINAL_NAME,
                SAVED_NAME,
                fileSize,
                extensionType
        );
    }

    public static MailAttachmentFileDescription createMailAttachmentFileDescription(String fileName){
        return MailAttachmentFileDescription.from(
                ORIGINAL_NAME,
                fileName,
                fileSize,
                extensionType
        );
    }
}
