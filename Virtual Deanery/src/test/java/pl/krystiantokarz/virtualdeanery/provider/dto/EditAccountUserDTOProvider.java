package pl.krystiantokarz.virtualdeanery.provider.dto;

import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditAccountUserDTO;

public abstract class EditAccountUserDTOProvider {

    public static String USER_FIRST_NAME_TEST = "TEST_FIRST_NAME";
    public static String USER_LAST_NAME_TEST = "TEST_LAST_NAME";
    public static String USER_EMAIL_TEST = "TEST_EMAIL@EMAIL.EMAIL";
    public static String USER_LOGIN_TEST = "USER_LOGIN_TEST";

    public static EditAccountUserDTO prepareEditAccountUserDTO(){
        EditAccountUserDTO editAccountUserDTO = new EditAccountUserDTO();
        editAccountUserDTO.setFirstName(USER_FIRST_NAME_TEST);
        editAccountUserDTO.setLastName(USER_LAST_NAME_TEST);
        editAccountUserDTO.setEmail(USER_EMAIL_TEST);
        editAccountUserDTO.setLogin(USER_LOGIN_TEST);
        return editAccountUserDTO;
    }
}
