package pl.krystiantokarz.virtualdeanery.provider.entity;

import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroupFactory;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

public abstract class UserGroupProvider {

    public static UserGroup createUserGroupForNormalGroupMember(User user, Group group){
        return UserGroupFactory.createUserGroupForNormalMember(user,group);
    }

    public static UserGroup createUserGroupForAdministratorGroupMember(User user, Group group){
        return UserGroupFactory.createUserGroupForAdministratorMember(user,group);
    }
}
