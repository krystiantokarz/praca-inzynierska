package pl.krystiantokarz.virtualdeanery.provider.entity;

import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageFactory;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageStatus;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import java.util.List;

public abstract class MailMessageProvider {

    public static final String SUBJECT = "TEST-SUBJECT";
    public static final String MESSAGE = "TEST-MESSAGE";


    public static MailMessageEntity createMailMessageEntityWithoutAttachments(User userFrom, User userTo){
        return MailMessageFactory.createWithoutAttachments(
                SUBJECT,
                MESSAGE,
                userFrom,
                userTo
        );
    }

    public static MailMessageEntity createMailMessageEntityWithAttachments(List<MailAttachmentFileDescription> attachments,User userFrom, User userTo){
        return MailMessageFactory.createWithAttachments(
                SUBJECT,
                MESSAGE,
                attachments,
                userFrom,
                userTo
        );
    }
}
