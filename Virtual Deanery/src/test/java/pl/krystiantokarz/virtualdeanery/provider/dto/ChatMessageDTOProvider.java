package pl.krystiantokarz.virtualdeanery.provider.dto;

import pl.krystiantokarz.virtualdeanery.infrastructure.dto.chat.in.ChatMessageDTO;

public abstract class ChatMessageDTOProvider {

    public static final String TEST_CHAT_MESSAGE= "TEST-CHAT-MESSAGE";

    public static ChatMessageDTO prepareChatMessageDTO(Long groupId){
        ChatMessageDTO chatMessageDTO = new ChatMessageDTO();
        chatMessageDTO.setMessage(TEST_CHAT_MESSAGE);
        chatMessageDTO.setGroupId(groupId);
        return chatMessageDTO;
    }
}
