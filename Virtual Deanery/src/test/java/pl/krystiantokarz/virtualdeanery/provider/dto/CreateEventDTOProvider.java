package pl.krystiantokarz.virtualdeanery.provider.dto;

import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.CreateEventDTO;

import java.time.LocalDate;
import java.time.LocalTime;

public abstract class CreateEventDTOProvider {

    public static String TEST_TOPIC = "TEST-TOPIC";
    public static String TEST_MESSAGE = "TEST-MESSAGE";

    public static CreateEventDTO prepareCreateEventDTO(){
        LocalTime timeFrom = LocalTime.of(10, 0);
        LocalTime timeTo = LocalTime.of(11, 0);
        LocalDate date = LocalDate.now();
        CreateEventDTO createEventDTO = new CreateEventDTO();
        createEventDTO.setDate(date);
        createEventDTO.setEventTimeTo(timeTo);
        createEventDTO.setEventTimeFrom(timeFrom);
        createEventDTO.setTopic(TEST_TOPIC);
        createEventDTO.setMessage(TEST_MESSAGE);
        return createEventDTO;
    }
}
