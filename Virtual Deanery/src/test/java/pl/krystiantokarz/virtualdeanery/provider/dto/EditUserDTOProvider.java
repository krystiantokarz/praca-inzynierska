package pl.krystiantokarz.virtualdeanery.provider.dto;

import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditUserDTO;

import java.util.Collections;
import java.util.List;

public abstract class EditUserDTOProvider {

    public static final String TEST_FIRST_NAME = "TEST-FIRST-NAME";
    public static final String TEST_LAST_NAME = "TEST-LAST-NAME";
    public static final String TEST_LOGIN = "TEST-LOGIN";
    public static final String TEST_EMAIL= "TEST_EMAIL@EMAIL.EMAIL";
    public static final RoleEnum testRole= RoleEnum.ADMIN;

    public static EditUserDTO prepareEditUserDTO(){
        List<String> newRoles = Collections.singletonList(testRole.toString());
        EditUserDTO editUserDTO = new EditUserDTO();
        editUserDTO.setFirstName(TEST_FIRST_NAME);
        editUserDTO.setLastName(TEST_LAST_NAME);
        editUserDTO.setLogin(TEST_LOGIN);
        editUserDTO.setEmail(TEST_EMAIL);
        editUserDTO.setRoles(newRoles);
        return editUserDTO;
    }
}
