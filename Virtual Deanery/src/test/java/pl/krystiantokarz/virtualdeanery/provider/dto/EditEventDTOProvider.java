package pl.krystiantokarz.virtualdeanery.provider.dto;

import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.EditEventDTO;

import java.time.LocalTime;

public abstract class EditEventDTOProvider {

    public static String TEST_TOPIC = "TEST-TOPIC";
    public static String TEST_MESSAGE = "TEST-MESSAGE";

    public static EditEventDTO prepareEditEventDTO(Long eventId){
        LocalTime newTimeFrom = LocalTime.of(10, 0);
        LocalTime newTimeTo = LocalTime.of(11, 0);
        EditEventDTO editEventDTO = new EditEventDTO();
        editEventDTO.setEventId(eventId);
        editEventDTO.setMessage(TEST_MESSAGE);
        editEventDTO.setTopic(TEST_TOPIC);
        editEventDTO.setTimeFrom(newTimeFrom);
        editEventDTO.setTimeTo(newTimeTo);
        return editEventDTO;
    }
}
