package pl.krystiantokarz.virtualdeanery.provider.dto;

import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.CreateUserDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class CreateUserDTOProvider {

    public static String USER_FIRST_NAME_TEST = "TEST_FIRST_NAME";
    public static String USER_LAST_NAME_TEST = "TEST_LAST_NAME";
    public static String USER_EMAIL_TEST = "TEST_EMAIL@EMAIL.EMAIL";
    public static String USER_ROLE_TEST = RoleEnum.EMPLOYEE.toString();

    public static CreateUserDTO prepareCreateUserDTO(){
        List<String> userRoles = new ArrayList<>(Collections.singletonList(USER_ROLE_TEST));
        CreateUserDTO createUserDTO = new CreateUserDTO();
        createUserDTO.setFirstName(USER_FIRST_NAME_TEST);
        createUserDTO.setLastName(USER_LAST_NAME_TEST);
        createUserDTO.setEmail(USER_EMAIL_TEST);
        createUserDTO.setRoles(userRoles);
        return createUserDTO;
    }
}
