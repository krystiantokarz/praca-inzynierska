package pl.krystiantokarz.virtualdeanery.provider.dto;

import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupPasswordDTO;

public abstract class EditGroupPasswordDTOProvider {

    public static String NEW_PASSWORD = "NEW-PASSWORD";

    public static EditGroupPasswordDTO prepareEditGroupPasswordDTO(String oldPassword){
        EditGroupPasswordDTO editGroupPasswordDTO = new EditGroupPasswordDTO();
        editGroupPasswordDTO.setNewPassword(NEW_PASSWORD);
        editGroupPasswordDTO.setOldPassword(oldPassword);
        return editGroupPasswordDTO;
    }
}
