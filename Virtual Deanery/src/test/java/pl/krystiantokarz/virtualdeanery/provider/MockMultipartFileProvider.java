package pl.krystiantokarz.virtualdeanery.provider;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MockMultipartFileProvider {

    public static String MULTIPART_FILE_NAME = "TEST-MULTIPART-FILE-NAME";
    public static String MULTIPART_ORIGINAL_FILE_NAME = "TEST-MULTIPART-ORIGINAL-FILE-NAME";
    public static String CONTENT_TYPE = "text/plain";

    public static MultipartFile prepareMultipartFile(){
        MultipartFile mockMultipartFile = new MockMultipartFile(MULTIPART_FILE_NAME,MULTIPART_ORIGINAL_FILE_NAME,CONTENT_TYPE, new byte[]{1});
        return mockMultipartFile;
    }

    public static List<MultipartFile> prepareMultipartFileList(int listSize){
        List<MultipartFile> multipartFileList = new ArrayList<>();
        for(int i=0; i<listSize; i++){
            MultipartFile mockMultipartFile = new MockMultipartFile(MULTIPART_FILE_NAME,MULTIPART_ORIGINAL_FILE_NAME,CONTENT_TYPE, new byte[]{1});
            multipartFileList.add(mockMultipartFile);
        }
        return multipartFileList;
    }
}
