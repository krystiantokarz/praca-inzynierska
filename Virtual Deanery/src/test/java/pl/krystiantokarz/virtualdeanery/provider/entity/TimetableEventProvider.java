package pl.krystiantokarz.virtualdeanery.provider.entity;

import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import java.time.LocalDate;
import java.time.LocalTime;

public abstract class TimetableEventProvider {

    public static LocalDate eventDate = LocalDate.now();
    public static LocalTime timeFrom = LocalTime.now();
    public static LocalTime timeTo = LocalTime.now();
    public static String TOPIC = "TOPIC-TEST";
    public static String MESSAGE = "MESSAGE-TEST";

    public static TimetableEvent createTimetableEvent(User user){
        return TimetableEvent.from(
                eventDate,
                timeFrom,
                timeTo,
                TOPIC,
                MESSAGE,
                user
        );
    }
}
