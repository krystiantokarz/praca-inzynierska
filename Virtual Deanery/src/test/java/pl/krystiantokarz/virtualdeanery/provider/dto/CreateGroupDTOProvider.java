package pl.krystiantokarz.virtualdeanery.provider.dto;

import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.CreateGroupDTO;

public abstract class CreateGroupDTOProvider {

    public static String GROUP_DESCRIPTION = "GROUP-DESCRIPTION";
    public static String GROUP_NAME = "GROUP-NAME";
    public static String GROUP_PASSWORD = "GROUP-PASSWORD";

    public static CreateGroupDTO prepareCreateGroupDTO(){
        CreateGroupDTO createGroupDTO = new CreateGroupDTO();
        createGroupDTO.setDescription(GROUP_DESCRIPTION);
        createGroupDTO.setName(GROUP_NAME);
        createGroupDTO.setPassword(GROUP_PASSWORD);
        return createGroupDTO;
    }
}
