package pl.krystiantokarz.virtualdeanery.provider.dto;

import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupDTO;

public abstract class EditGroupDTOProvider {

    public static String NEW_GROUP_DESCRIPTION = "NEW_GROUP_DESCRIPTION";
    public static String NEW_GROUP_NAME = "NEW_GROUP_NAME";

    public static EditGroupDTO prepareEditGroupDTO(){
        EditGroupDTO editGroupDTO = new EditGroupDTO();
        editGroupDTO.setDescription(NEW_GROUP_DESCRIPTION);
        editGroupDTO.setName(NEW_GROUP_NAME);
        return editGroupDTO;
    }
}
