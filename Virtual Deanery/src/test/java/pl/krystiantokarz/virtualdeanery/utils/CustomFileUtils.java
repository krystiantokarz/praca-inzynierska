package pl.krystiantokarz.virtualdeanery.utils;

import org.apache.commons.text.RandomStringGenerator;
import org.apache.tomcat.util.http.fileupload.FileUtils;

import java.io.File;
import java.io.IOException;

public class CustomFileUtils {

    public static final String FILE_PREFIX = "FILE_";

    public static void createFolderIfNotExists(String directory){
        File theDir = new File(directory);
        if (!theDir.exists()) {
            boolean result = theDir.mkdirs();
            if(!result){
                throw new IllegalArgumentException(String.format("Cannot create folder in directory = %s", directory));
            }
        }
    }

    public static String generateTestFileName(){

        RandomStringGenerator randomStringGenerator =
                new RandomStringGenerator.Builder()
                        .withinRange('0', '9')
                        .build();
        String number = randomStringGenerator.generate(10);

        return FILE_PREFIX + number;
    }

    public static void deleteAllFilesInFolder(String directory) {
        File theDir = new File(directory);
        File[] files = theDir.listFiles();
        if(files!=null) { //some JVMs return null for empty dirs
            for(File f: files) {
                boolean result = f.delete();
                if(!result){
                    throw new IllegalArgumentException(String.format("Cannot delete files in folder in directory = %s", directory));
                }
            }
        }
    }
    public static void deleteFolderWithAllFiles(String directory) {
        try {
            FileUtils.deleteDirectory(new File(directory));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Cannot delete folder with files in directory = %s", directory));
        }
    }
}
