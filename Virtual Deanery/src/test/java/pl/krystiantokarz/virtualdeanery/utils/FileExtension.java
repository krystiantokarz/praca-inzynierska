package pl.krystiantokarz.virtualdeanery.utils;

public enum FileExtension {

    ZIP("zip"), TXT("txt"), PDF("pdf");

    private String type;

    FileExtension(String type) {
        this.type = type;
    }

    public String getExtension(){
        return type;
    }



}
