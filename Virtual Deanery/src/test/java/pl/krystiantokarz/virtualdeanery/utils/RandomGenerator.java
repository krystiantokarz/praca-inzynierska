package pl.krystiantokarz.virtualdeanery.utils;

import org.apache.commons.text.RandomStringGenerator;

public class RandomGenerator {


    public static String generateNumberStringWithLength(int length){
        RandomStringGenerator randomStringGenerator =
                new RandomStringGenerator.Builder()
                        .withinRange('0', '9')
                        .build();
        return randomStringGenerator.generate(length);
    }
}
