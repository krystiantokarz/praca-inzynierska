package pl.krystiantokarz.virtualdeanery;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;


@SpringBootTest
@ComponentScan
@EnableAutoConfiguration
public class VirtualdeaneryApplicationTests {



}
