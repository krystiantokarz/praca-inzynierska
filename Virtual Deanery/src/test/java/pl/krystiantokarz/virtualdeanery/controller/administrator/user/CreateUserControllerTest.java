package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.*;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.CreateUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.service.user.CreateUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
public class CreateUserControllerTest {

    @Mock
    private CreateUserService createUserService;

    @InjectMocks
    private CreateUserController createUserController;

    @Captor
    private ArgumentCaptor<CreateUserDTO> createUserDTOCaptor;

    private MockMvc mockMvc;

    private ObjectMapper mapper;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
    }

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(createUserController).build();
    }


    @Test
    public void shouldCreateNewUser() throws Exception{
        //given
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        String content = mapper.writeValueAsString(createUserDTO);

        //when
        mockMvc.perform(post(AdministratorPath.userPath)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isOk());

        //then
        verify(createUserService, times(1)).createNewUser(createUserDTOCaptor.capture(), any(Locale.class));
        assertThat(createUserDTOCaptor.getValue().getEmail()).isEqualTo(createUserDTO.getEmail());
        assertThat(createUserDTOCaptor.getValue().getFirstName()).isEqualTo(createUserDTO.getFirstName());
        assertThat(createUserDTOCaptor.getValue().getLastName()).isEqualTo(createUserDTO.getLastName());
        assertThat(createUserDTOCaptor.getValue().getRoles()).hasSize(1);
        assertThat(createUserDTOCaptor.getValue().getRoles()).contains(RoleEnum.EMPLOYEE.toString());
    }


    @Test
    public void shouldNotCreateNewUser_whenDTOObjectIsNotValid() throws Exception{
        //given
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        createUserDTO.setEmail("INVALID");
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(createUserDTO);

        //when
        mockMvc.perform(post(AdministratorPath.userPath)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isBadRequest());

        //then
        verifyZeroInteractions(createUserService);
    }

    @Test
    public void shouldNotCreateNewUser_whenRolesNameIsIncorrect() throws Exception{
        //given
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        createUserDTO.getRoles().add("INVALID");
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(createUserDTO);

        //when
        mockMvc.perform(post(AdministratorPath.userPath)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isBadRequest());

        //then
        verifyZeroInteractions(createUserService);
    }

    @Test
    public void shouldNotCreateNewUser_whenUserWithSelectedEmailExist() throws Exception{

        //given
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();

        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(createUserDTO);

        //when
        doThrow(UserWithSelectedEmailExistException.class).when(createUserService).createNewUser(any(CreateUserDTO.class), any(Locale.class));

        //when
        MvcResult mvcResult = mockMvc.perform(post(AdministratorPath.userPath)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isConflict())
                .andReturn();


        //then
        assertThat(mvcResult.getResponse().getContentAsString()).isEqualTo("\"USER_EMAIL_EXIST\"");
        verify(createUserService, times(1)).createNewUser(any(CreateUserDTO.class),any(Locale.class));
    }

}