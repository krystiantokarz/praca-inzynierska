package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupPasswordDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditGroupDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditGroupPasswordDTOProvider;
import pl.krystiantokarz.virtualdeanery.service.group.EditGroupService;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class ManagedGroupControllerTest {

    @Mock
    private EditGroupService editGroupService;

    @InjectMocks
    private ManagedGroupController managedGroupController;

    private MockMvc mockMvc;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(managedGroupController).build();
    }


    @Test
    public void shouldEditGroup() throws Exception{

        //given
        Long groupId = 100L;
        Long userId = 100L;

        EditGroupDTO editGroupDTO = EditGroupDTOProvider.prepareEditGroupDTO();

        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(editGroupDTO);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" + groupId)
                .sessionAttr("logged-user-id", userId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isOk());

        //then
        verify(editGroupService, times(1)).editGroupData(groupId, userId, editGroupDTO);
    }



    @Test
    public void shouldEditGroupPassword() throws Exception{
        //given
        Long groupId = 100L;
        Long userId = 100L;

        String oldPassword = "OLD-PASSWORD";
        EditGroupPasswordDTO editGroupPasswordDTO = EditGroupPasswordDTOProvider.prepareEditGroupPasswordDTO(oldPassword);
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(editGroupPasswordDTO);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" +  groupId+ "/password")
                .sessionAttr("logged-user-id", userId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isOk());
        //then
        verify(editGroupService, times(1)).editGroupPassword(groupId, userId, editGroupPasswordDTO);

    }

}