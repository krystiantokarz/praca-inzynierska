package pl.krystiantokarz.virtualdeanery.controller.administrator.group;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.GroupDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.service.group.FindGroupService;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@SpringBootTest
public class FindGroupControllerTest {


    @Mock
    private FindGroupService findGroupService;

    @InjectMocks
    private FindGroupController findGroupController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(findGroupController).build();
    }

    @Test
    public void shouldGetGroupsBySelectedPage() throws Exception{
        //given
        Integer page = 0;
        Integer size = 10;
        Group group = GroupProvider.createGroup();

        GroupDTO groupDTO = new GroupDTO(
                group.getId(),
                group.getName(),
                group.getDescription());


        //when
        when(findGroupService.getGroupsByPageAndSize(page, size)).thenReturn(new PageImpl<>(new ArrayList<>(Collections.singletonList(group))));

        mockMvc.perform(get(AdministratorPath.groupsPath)
                .param("page",page.toString()))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/group/groups"))
                .andExpect(model().attribute("groups",new ArrayList<>(Collections.singletonList(groupDTO))))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));


    }


}