package pl.krystiantokarz.virtualdeanery.controller.employee.messagebox;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailAttachmentFileDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class DownloadMessageAttachmentFileControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    private static final Long loggedUserId = 11111L;

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldDownloadMessageAttachmentFile() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        String savedFileName = "example.txt";

        MailAttachmentFileDescription messageAttachmentFileDescription = MailAttachmentFileDescriptionProvider.createMailAttachmentFileDescription(savedFileName);
        MailMessageEntity messageEntity = MailMessageProvider.createMailMessageEntityWithAttachments(new ArrayList<>(Collections.singletonList(messageAttachmentFileDescription)),user,user);

        entityManager.persist(user);
        entityManager.persist(messageEntity);


        //when
        MvcResult result = mockMvc.perform(get(EmployeePath.mailPath + "/attachment")
                .sessionAttr("logged-user-id", user.getId())
                .param("id", messageEntity.getAttachments().get(0).getId().toString())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andReturn();

        //then
        assertThat(result.getResponse().getHeader("Content-Disposition")).isEqualTo("attachment; filename=\"" + messageAttachmentFileDescription.getOriginalFileName() +"\"");
        assertThat(result.getResponse().getContentAsByteArray().length).isGreaterThan(0);
    }

    @Test
    public void shouldNotDownloadMessageAttachmentFile_whenMessageAttachmentDescriptionNotExist() throws Exception {
        //given
        Long notExistAttachmentId = 100L;

        //when
        mockMvc.perform(get(EmployeePath.mailPath + "/box/download")
                .sessionAttr("logged-user-id", loggedUserId)
                .param("id", notExistAttachmentId.toString())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isNotFound());

    }
}