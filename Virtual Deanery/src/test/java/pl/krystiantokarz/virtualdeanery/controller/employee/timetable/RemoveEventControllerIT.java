package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-test-controller.properties"
)
@AutoConfigureMockMvc
public class RemoveEventControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldRemoveSelectedEventByUser() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        //when
        mockMvc.perform(delete(EmployeePath.timetable + "/event/" +timetableEvent.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", user.getId()))
                .andExpect(status().isOk());

        //then
        List<TimetableEvent> resultlist = entityManager.createQuery("SELECT t FROM TimetableEvent t WHERE t.id = :id", TimetableEvent.class)
                .setParameter("id", timetableEvent.getId())
                .getResultList();


        assertThat(resultlist).isEmpty();
    }

    @Test
    public void shouldNotRemoveBecauseUserNotHavePrivilegeToDoIt() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        User userWithoutPrivilege = UserProvider.createUserWithAllRoles();
        entityManager.persist(userWithoutPrivilege);

        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        //when
        mockMvc.perform(delete(EmployeePath.timetable + "/event/" +timetableEvent.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", userWithoutPrivilege.getId()))
                .andExpect(status().isForbidden());

        //then

        List<TimetableEvent> resultlist = entityManager.createQuery("SELECT t FROM TimetableEvent t WHERE t.id = :id", TimetableEvent.class)
                .setParameter("id", timetableEvent.getId())
                .getResultList();


        assertThat(resultlist).isNotEmpty();
        assertThat(resultlist).contains(timetableEvent);
    }
}