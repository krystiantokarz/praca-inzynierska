package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
public class FindUserControllerTest {

    @Mock
    private FindUserService findUserService;

    @InjectMocks
    private FindUserController findUserController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(findUserController).build();
    }

    @Test
    public void shouldGetUsersBySelectedPage() throws Exception{
        //given
        Integer page = 0;
        Integer size = 10;

        Long loggedUserId = 1L;

        User user = UserProvider.createUserWithAdminRole();

        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );



        //when
        when(findUserService.getUsersByPageAndPageSize(page, size)).thenReturn(new PageImpl<>(new ArrayList<>(Collections.singletonList(user))));

        mockMvc.perform(get(AdministratorPath.usersPath)
                .param("page","0")
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/user/users"))
                .andExpect(model().attribute("users",new ArrayList<>(Collections.singletonList(findUserDTO))))
                .andExpect(model().attribute("loggedUserId",loggedUserId))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));
    }

    @Test
    public void shouldSearchUsersBySearchString() throws Exception{
        //given
        Long loggedUserId = 1L;
        String searchedString = "searched-string";
        User user = UserProvider.createUserWithAdminRole();

        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );

        //when
        when(findUserService.searchUsers(searchedString)).thenReturn(Collections.singletonList(user));
        mockMvc.perform(get(AdministratorPath.usersPath + "/search")
                .sessionAttr("logged-user-id", loggedUserId)
                .param("searchString", searchedString))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/user/fragment/search-users-fragment :: correct-result"))
                .andExpect(model().attribute("users", new ArrayList<>(Collections.singletonList(findUserDTO))));
    }

    @Test
    public void shouldSearchUsersBySearchStringAndNotFound() throws Exception{
        //given
        Long loggedUserId = 1L;

        String notExistString = "not-exist-string";

        //when
        when(findUserService.searchUsers(notExistString)).thenReturn(Collections.emptyList());
        mockMvc.perform(get(AdministratorPath.usersPath + "/search")
                .sessionAttr("logged-user-id", loggedUserId)
                .param("searchString",notExistString))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/user/fragment/search-users-fragment :: not-found-result"));
    }



}