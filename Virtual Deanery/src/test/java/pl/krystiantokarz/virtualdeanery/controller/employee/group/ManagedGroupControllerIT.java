package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupPasswordDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditGroupDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditGroupPasswordDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class ManagedGroupControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private ObjectMapper mapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
    }

    @Test
    public void shouldEditGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(userGroup);

        EditGroupDTO editGroupDTO = EditGroupDTOProvider.prepareEditGroupDTO();


        String content = mapper.writeValueAsString(editGroupDTO);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" +  group.getId())
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isOk());

        Group result = entityManager.find(Group.class, group.getId());

        //then
        assertThat(result.getDescription()).isEqualTo(editGroupDTO.getDescription());
        assertThat(result.getName()).isEqualTo(editGroupDTO.getName());

    }

    @Test
    public void shouldNotEditGroupWhenUserNotHavePrivilegeToEditGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);
        EditGroupDTO editGroupDTO = EditGroupDTOProvider.prepareEditGroupDTO();

        String content = mapper.writeValueAsString(editGroupDTO);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" +  group.getId())
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldNotEditGroupWhenNewGroupNameIsNotUnique() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        Group exampleGroup = GroupProvider.createGroup();
        entityManager.persist(exampleGroup);
        Group groupToEdit = GroupProvider.createGroup();
        entityManager.persist(groupToEdit);
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, groupToEdit);
        entityManager.persist(userGroup);

        EditGroupDTO editGroupDTO = EditGroupDTOProvider.prepareEditGroupDTO();
        editGroupDTO.setName(exampleGroup.getName());


        String content = mapper.writeValueAsString(editGroupDTO);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" +  groupToEdit.getId())
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isConflict());

        Group result = entityManager.find(Group.class, groupToEdit.getId());

        //then
        assertThat(result.getDescription()).isEqualTo(groupToEdit.getDescription());
        assertThat(result.getName()).isEqualTo(groupToEdit.getName());
    }


    @Test
    public void shouldEditGroupPassword() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        String enteredPassword = group.getPassword();
        group.changeGroupPassword(passwordEncoder.encode(enteredPassword));
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(userGroup);

        EditGroupPasswordDTO editGroupPasswordDTO = EditGroupPasswordDTOProvider.prepareEditGroupPasswordDTO(enteredPassword);


        String content = mapper.writeValueAsString(editGroupPasswordDTO);
        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" +  group.getId() + "/password")
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isOk());


        //then
        Group result = entityManager.find(Group.class, group.getId());

        assertThat(passwordEncoder.matches(editGroupPasswordDTO.getNewPassword(), result.getPassword())).isTrue();
    }

}