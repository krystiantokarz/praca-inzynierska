package pl.krystiantokarz.virtualdeanery.controller.employee.messagebox;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageStatus;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.in.CreateMessageDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateMessageDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class SendMessageControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldGetPageForSendMessageToSelectedUser() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        //when
        mockMvc.perform(get(EmployeePath.mailPath + "/message/" + user.getId())
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/messagebox/send-message"))
                .andExpect(model().attribute("firstName",user.getFirstName()))
                .andExpect(model().attribute("lastName",user.getLastName()))
                .andExpect(model().attribute("email",user.getEmail()))
                .andExpect(model().attribute("id",user.getId()));
    }

    @Test
    public void shouldNotGetPageForSendMessageToSelectedUserWhenUserNotExist() throws Exception{
        Long testUserId = -100L;

        mockMvc.perform(get(EmployeePath.mailPath + "/message/" + testUserId)
                .sessionAttr("logged-user-id", testUserId)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/error/error-page"));

    }

    @Test
    public void shouldSendMessageForSelectedUser() throws Exception{

        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        CreateMessageDTO createMessageDTO = CreateMessageDTOProvider.prepareCreateMessageDTOWithoutAttachments();

        mockMvc.perform(post(EmployeePath.mailPath + "/message")
                .sessionAttr("logged-user-id", user.getId().toString())
                .param("idUserForSendMessage",user.getId().toString())
                .param("subject",createMessageDTO.getSubject())
                .param("message",createMessageDTO.getMessage())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk());

        MailMessageEntity singleResult = entityManager.createQuery("SELECT msg From MailMessageEntity msg WHERE msg.sender = :user", MailMessageEntity.class)
                .setParameter("user", user)
                .getSingleResult();

        assertThat(singleResult).isNotNull();
        assertThat(singleResult.getAttachments()).isNull();
        assertThat(singleResult.getMessageStatus()).isEqualTo(MailMessageStatus.DELIVERED);
    }


    @Test
    public void shouldSendMessageWithAttachmentForSelectedUser() throws Exception{

        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        CreateMessageDTO createMessageDTO = CreateMessageDTOProvider.prepareCreateMessageDTOWithoutAttachments();
        String filename = "files";
        String originalFileName = "test";
        MockMultipartFile mockMultipartFile = new MockMultipartFile(filename, originalFileName , "text/plain", "This is a Test".getBytes());


        //when
        mockMvc.perform(MockMvcRequestBuilders.fileUpload(EmployeePath.mailPath + "/message")
                .file(mockMultipartFile)
                .sessionAttr("logged-user-id", user.getId().toString())
                .param("idUserForSendMessage",user.getId().toString())
                .param("subject",createMessageDTO.getSubject())
                .param("message",createMessageDTO.getMessage())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk());

        MailMessageEntity singleResult = entityManager.createQuery("SELECT msg From MailMessageEntity msg WHERE msg.sender = :user", MailMessageEntity.class)
                .setParameter("user", user)
                .getSingleResult();

        assertThat(singleResult).isNotNull();
        assertThat(singleResult.getAttachments()).isNotNull();
        assertThat(singleResult.getAttachments()).isNotEmpty();
        assertThat(singleResult.getMessageStatus()).isEqualTo(MailMessageStatus.DELIVERED);
        assertThat(singleResult.getAttachments()).hasSize(1);
        assertThat(singleResult.getAttachments().get(0).getOriginalFileName()).isEqualTo(originalFileName);

        String savedFileName = singleResult.getAttachments().get(0).getSavedFileName();

        File savedFile = new File(getClass().getResource("/repository/fileattachment/").toURI().getPath(), savedFileName);

        assertThat(savedFile.exists()).isTrue();

        savedFile.delete();
    }


    @Test
    public void shouldNotSendMessageWhenUserNotExist() throws Exception{
        Long testUserId = -100L;
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        mockMvc.perform(post(EmployeePath.mailPath + "/message")
                .sessionAttr("logged-user-id", user.getId().toString())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .param("idUserForSendMessage",testUserId.toString())
                .param("subject","SUBJECT")
                .param("message","MESSAGE"))
                .andExpect(status().isNotFound());
    }
}