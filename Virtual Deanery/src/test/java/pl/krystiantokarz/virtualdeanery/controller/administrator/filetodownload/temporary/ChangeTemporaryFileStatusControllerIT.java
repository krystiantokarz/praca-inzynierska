package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.temporary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class ChangeTemporaryFileStatusControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String ARCHIVE_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/archive/";
    private static final String FORM_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/form/";
    private static final String TMP_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/tmp/";
    private static final String TMP_FILE_NAME = "example-tmp-to-remove.txt";


    @BeforeClass
    public void init(){
        CustomFileUtils.createFolderIfNotExists(ARCHIVE_REPOSITORY_PATH);
        CustomFileUtils.createFolderIfNotExists(FORM_REPOSITORY_PATH);
        CustomFileUtils.createFolderIfNotExists(TMP_REPOSITORY_PATH);
    }

    @Test
    public void shouldAcceptTemporaryArchiveFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        User appUser = UserProvider.createAppUser();
        entityManager.persist(appUser);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user,TMP_FILE_NAME);

        entityManager.persist(fileEntity);

        //when
        mockMvc.perform(put(AdministratorPath.filePath + "/" + fileEntity.getId())
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        File removedFile = new File(TMP_REPOSITORY_PATH + TMP_FILE_NAME);
        assertThat(removedFile.exists()).isFalse();
        removedFile.createNewFile();

        FileToDownloadDescriptionEntity changedFileDescription = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(changedFileDescription).isNotNull();
        assertThat(changedFileDescription.getFileType()).isEqualTo(FileType.ARCHIVE);
        assertThat(changedFileDescription.getFileStatus()).isEqualTo(FileStatus.READY_TO_DOWNLOAD);

        File changedFile = new File(ARCHIVE_REPOSITORY_PATH + TMP_FILE_NAME);
        assertThat(changedFile.exists()).isTrue();
        changedFile.delete();
    }

    @Test
    public void shouldAcceptTemporaryFormFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        User appUser = UserProvider.createAppUser();
        entityManager.persist(appUser);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableFormFileToDownloadDescriptionEntity(user,TMP_FILE_NAME);

        entityManager.persist(fileEntity);

        //when
        mockMvc.perform(put(AdministratorPath.filePath + "/" + fileEntity.getId())
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        File removedFile = new File(TMP_REPOSITORY_PATH + TMP_FILE_NAME);
        assertThat(removedFile.exists()).isFalse();
        removedFile.createNewFile();

        FileToDownloadDescriptionEntity changedFileDescription = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(changedFileDescription).isNotNull();
        assertThat(changedFileDescription.getFileType()).isEqualTo(FileType.FORM);
        assertThat(changedFileDescription.getFileStatus()).isEqualTo(FileStatus.READY_TO_DOWNLOAD);

        File changedFile = new File(FORM_REPOSITORY_PATH + TMP_FILE_NAME);
        assertThat(changedFile.exists()).isTrue();
        changedFile.delete();
    }

    @Test
    public void shouldRejectTemporaryFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        User appUser = UserProvider.createAppUser();
        entityManager.persist(appUser);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableFormFileToDownloadDescriptionEntity(user,TMP_FILE_NAME);

        entityManager.persist(fileEntity);

        //when
        mockMvc.perform(delete(AdministratorPath.filePath + "/" + fileEntity.getId())
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        File removedFile = new File(TMP_REPOSITORY_PATH + TMP_FILE_NAME);
        assertThat(removedFile.exists()).isFalse();
        removedFile.createNewFile();

        FileToDownloadDescriptionEntity changedFileDescription = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(changedFileDescription).isNull();
    }
}