package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.service.timetable.RemoveEventService;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class RemoveEventControllerTest {

    @Mock
    private RemoveEventService removeEventService;

    @InjectMocks
    private RemoveEventController removeEventController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(removeEventController).build();
    }


    @Test
    public void shouldRemoveSelectedEvent() throws Exception{
        //given
        Long eventId = 100L;
        Long userId = 100L;

        //when
        mockMvc.perform(delete(EmployeePath.timetable + "/event/" +eventId)
                .sessionAttr("logged-user-id", userId))
                .andExpect(status().isOk());

        //then
        verify(removeEventService, times(1)).removeSelectedEventByUser(eventId, userId);
    }
}