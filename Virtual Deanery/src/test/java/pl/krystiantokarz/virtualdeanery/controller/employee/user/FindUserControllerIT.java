package pl.krystiantokarz.virtualdeanery.controller.employee.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-test-controller.properties"
)
@AutoConfigureMockMvc
public class FindUserControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;


    @Test
    public void shouldGetUsersBySelectedPage() throws Exception{
        //given
        Integer page = 0;
        Integer size = 10;

        Long loggedUserId = 1L;

        User user = UserProvider.createUserWithAllRoles();

        entityManager.persist(user);

        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );



        mockMvc.perform(get(EmployeePath.usersPath)
                .param("page","0")
                .sessionAttr("logged-user-id", loggedUserId)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/employee/user/users"))
                .andExpect(model().attribute("users",new ArrayList<>(Collections.singletonList(findUserDTO))))
                .andExpect(model().attribute("loggedUserId",loggedUserId))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));
    }

    @Test
    public void shouldSearchUsersBySearchString() throws Exception{

        Long loggedUserId = 1L;

        User user =  UserProvider.createUserWithAllRoles();

        entityManager.persist(user);

        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );


        mockMvc.perform(get(EmployeePath.usersPath + "/search")
                .sessionAttr("logged-user-id", loggedUserId)
                .param("searchString", user.getFirstName())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/employee/user/fragment/search-users-fragment :: correct-result"))
                .andExpect(model().attribute("users", new ArrayList<>(Collections.singletonList(findUserDTO))));
    }

    @Test
    public void shouldSearchUsersBySearchStringAndNotFound() throws Exception{

        Long loggedUserId = 1L;

        String notExistString = "not-exist-string";

        mockMvc.perform(get(EmployeePath.usersPath + "/search")
                .sessionAttr("logged-user-id", loggedUserId)
                .param("searchString",notExistString)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/employee/user/fragment/search-users-fragment :: not-found-result"));
    }



}