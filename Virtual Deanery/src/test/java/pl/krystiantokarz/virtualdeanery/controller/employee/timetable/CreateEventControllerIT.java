package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.CreateEventDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateEventDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-test-controller.properties"
)
@AutoConfigureMockMvc
public class CreateEventControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private ObjectMapper mapper;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    @Test
    public void shouldAddMessage() throws Exception{

        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();

        String content = mapper.findAndRegisterModules().writeValueAsString(createEventDTO);

        //when
        mockMvc.perform(post(EmployeePath.timetable + "/event")
                .sessionAttr("logged-user-id", user.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .content(content))
                .andExpect(status().isOk());

        //then
        TimetableEvent timetableEvent = entityManager.createQuery("SELECT t FROM TimetableEvent t WHERE t.message = :msg", TimetableEvent.class)
                .setParameter("msg",createEventDTO.getMessage())
                .getSingleResult();

        assertThat(timetableEvent).isNotNull();
        assertThat(timetableEvent.getMessage()).isEqualTo(createEventDTO.getMessage());
        assertThat(timetableEvent.getTopic()).isEqualTo(createEventDTO.getTopic());
        assertThat(timetableEvent.getCreatingUser()).isEqualTo(user);
        assertThat(timetableEvent.getTimeTo()).isEqualTo(createEventDTO.getEventTimeTo());
        assertThat(timetableEvent.getTimeFrom()).isEqualTo(createEventDTO.getEventTimeFrom());
        assertThat(timetableEvent.getEventDate()).isEqualTo(createEventDTO.getDate());

    }

    @Test
    public void shouldNotAddMessageWhenDateIsInPast() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setDate(LocalDate.now().minusDays(10));

        String content = mapper.findAndRegisterModules().writeValueAsString(createEventDTO);

        //when
        mockMvc.perform(post(EmployeePath.timetable + "/event")
                .sessionAttr("logged-user-id", user.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .content(content))
                .andExpect(status().isNotAcceptable());

    }

    @Test
    public void shouldNotAddMessageWhenTimeIsInvalid() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setEventTimeFrom(LocalTime.of(12,10));
        createEventDTO.setEventTimeTo(LocalTime.of(10,10));

        String content = mapper.findAndRegisterModules().writeValueAsString(createEventDTO);

        //when
        mockMvc.perform(post(EmployeePath.timetable + "/event")
                .sessionAttr("logged-user-id", user.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .content(content))
                .andExpect(status().isNotAcceptable());

    }

}