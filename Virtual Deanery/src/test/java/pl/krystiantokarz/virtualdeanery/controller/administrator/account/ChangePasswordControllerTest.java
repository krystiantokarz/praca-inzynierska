package pl.krystiantokarz.virtualdeanery.controller.administrator.account;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.service.account.ChangePasswordService;
import pl.krystiantokarz.virtualdeanery.service.account.exceptions.NotMatchesPasswordsException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.testng.Assert.*;

public class ChangePasswordControllerTest {

    private static final String OLD_PASSWORD = "OLD-PASSWORD-TEST";
    private static final String NEW_PASSWORD = "NEW-PASSWORD-TEST";
    private static final Long loggedUserId = 1L;

    @Mock
    private ChangePasswordService changePasswordService;

    @InjectMocks
    private ChangePasswordController changePasswordController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(changePasswordController).build();
    }


    @Test
    public void shouldGetChangePasswordPage() throws Exception{
        mockMvc.perform(get(AdministratorPath.accountPath + "/password"))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/account/change-password"));
    }

    @Test
    public void shouldChangePassword() throws Exception{
        doNothing().when(changePasswordService).changePassword(loggedUserId,OLD_PASSWORD, NEW_PASSWORD);
        mockMvc.perform(put(AdministratorPath.accountPath + "/password")
                .param("oldPassword",OLD_PASSWORD)
                .param("newPassword",NEW_PASSWORD)
                .sessionAttr("logged-user-id",loggedUserId))
                .andExpect(status().isOk());
    }


    @Test
    public void shouldNotChangePassword_whenNoMatchesOldPassword() throws Exception{
        doThrow(NotMatchesPasswordsException.class).when(changePasswordService).changePassword(loggedUserId,OLD_PASSWORD, NEW_PASSWORD);
        mockMvc.perform(put(AdministratorPath.accountPath + "/password")
                .param("oldPassword",OLD_PASSWORD)
                .param("newPassword",NEW_PASSWORD)
                .sessionAttr("logged-user-id",loggedUserId))
                .andExpect(status().isNotAcceptable());
    }


    @Test
    public void shouldNotChangePassword_whenUserNotExist() throws Exception{
        doThrow(UserNotExistException.class).when(changePasswordService).changePassword(loggedUserId,OLD_PASSWORD, NEW_PASSWORD);
        mockMvc.perform(put(AdministratorPath.accountPath + "/password")
                .param("oldPassword",OLD_PASSWORD)
                .param("newPassword",NEW_PASSWORD)
                .sessionAttr("logged-user-id",loggedUserId))
                .andExpect(status().isNotFound());
    }



}