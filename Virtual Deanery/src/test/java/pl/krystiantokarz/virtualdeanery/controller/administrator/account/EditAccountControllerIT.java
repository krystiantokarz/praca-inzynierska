package pl.krystiantokarz.virtualdeanery.controller.administrator.account;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditAccountUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditAccountUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class EditAccountControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private ObjectMapper mapper;

    @BeforeClass
    public void init() {
        mapper = new ObjectMapper();
    }

    @Test
    public void shouldGetEditAccountPage() throws Exception{

        //given
        User user = UserProvider.createUserWithAdminRole();

        entityManager.persist(user);

        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                true
        );

        //when
        mockMvc.perform(get(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/account/account-edit"))
                .andExpect(model().attribute("roles", new ArrayList<>(Collections.singletonList(RoleEnum.ADMIN.toString()))))
                .andExpect(model().attribute("administrator", findUserDTO));
    }

    @Test
    public void shouldNotGetEditAccountPage_whenUserNotExist() throws Exception {
        //given
        Long loggedUserId = -100L;

        //when
        mockMvc.perform(get(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", loggedUserId)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(view().name("/administrator/error/error-page"));
    }

    @Test
    public void shouldEditUserAccount() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();

        entityManager.persist(user);

        EditAccountUserDTO editAccountUserDTO = EditAccountUserDTOProvider.prepareEditAccountUserDTO();

        String content = mapper.writeValueAsString(editAccountUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", user.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        User result = entityManager.find(User.class, user.getId());

        assertThat(result.getEmail()).isEqualTo(editAccountUserDTO.getEmail());
        assertThat(result.getFirstName()).isEqualTo(editAccountUserDTO.getFirstName());
        assertThat(result.getLastName()).isEqualTo(editAccountUserDTO.getLastName());
        assertThat(result.getLoginData().getLogin()).isEqualTo(editAccountUserDTO.getLogin());
    }

    @Test
    public void shouldNotEditUserAccountWhenSelectedEmailIsNotUnique() throws Exception{
        //given
        User userToEdit = UserProvider.createUserWithAdminRole();
        entityManager.persist(userToEdit);
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        EditAccountUserDTO editAccountUserDTO = EditAccountUserDTOProvider.prepareEditAccountUserDTO();
        editAccountUserDTO.setEmail(user.getEmail());

        String content = mapper.writeValueAsString(editAccountUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", userToEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isConflict());
    }

    @Test
    public void shouldNotEditUserAccountWhenSelectedLoginIsNotUnique() throws Exception{
        //given
        User userToEdit = UserProvider.createUserWithAdminRole();
        entityManager.persist(userToEdit);
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        EditAccountUserDTO editAccountUserDTO = EditAccountUserDTOProvider.prepareEditAccountUserDTO();
        editAccountUserDTO.setLogin(user.getLoginData().getLogin());

        String content = mapper.writeValueAsString(editAccountUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", userToEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isConflict());
    }



    @Test
    public void shouldNotEditUserAccountWhenDTOIsNotValidate() throws Exception{
        //given
        EditAccountUserDTO editAccountUserDTO = EditAccountUserDTOProvider.prepareEditAccountUserDTO();
        editAccountUserDTO.setEmail("EMAIL-ERROR");

        String content = mapper.writeValueAsString(editAccountUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isBadRequest());
    }
}