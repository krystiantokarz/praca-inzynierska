package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.service.group.ManagedUsersInGroupService;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class ManagedUsersInGroupControllerTest {

    @Mock
    private ManagedUsersInGroupService managedUsersInGroupService;

    @InjectMocks
    private ManagedUsersInGroupController managedUsersInGroupController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(managedUsersInGroupController).build();
    }


    @Test
    public void shouldAddAdminAccess() throws Exception{
        Long groupId = 100L;
        Long userId = 100L;

        mockMvc.perform(put(EmployeePath.groupPath + "/" +groupId + "/user/" + userId + "/access/admin")
                .sessionAttr("logged-user-id", userId))
                .andExpect(status().isOk());

        verify(managedUsersInGroupService, times(1)).addAdminAccess(userId, groupId, userId);
    }

    @Test
    public void shouldRemoveAdminAccess() throws Exception{
        Long groupId = 100L;
        Long userId = 100L;

        mockMvc.perform(put(EmployeePath.groupPath + "/" +groupId + "/user/" + userId + "/access/member")
                .sessionAttr("logged-user-id", userId))
                .andExpect(status().isOk());

        verify(managedUsersInGroupService, times(1)).removeAdminAccess(userId, groupId, userId);
    }

    @Test
    public void shouldRemoveUserFromGroup() throws Exception{
        Long groupId = 100L;
        Long userId = 100L;

        mockMvc.perform(delete(EmployeePath.groupPath + "/" +groupId + "/user/" + userId )
                .sessionAttr("logged-user-id", userId))
                .andExpect(status().isOk());

        verify(managedUsersInGroupService, times(1)).removeUserFromGroup(userId, groupId, userId);
    }
}