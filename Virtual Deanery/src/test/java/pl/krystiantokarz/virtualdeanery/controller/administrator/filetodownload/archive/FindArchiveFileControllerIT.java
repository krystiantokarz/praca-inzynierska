package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.archive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.FileSizeConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.file.out.FileToDownloadDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class FindArchiveFileControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldGetArchiveFilesReadyToDownloadBySelectedPage() throws Exception{

        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);
        Integer page = 0;

        String savedFileName = "example-archive.txt";
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user,savedFileName);
        entityManager.persist(fileEntity);

        FileToDownloadDTO fileToDownloadDTO = new FileToDownloadDTO(
                fileEntity.getId(),
                fileEntity.getOriginalFileName(),
                FileSizeConverter.convertBytesToMegabytes(fileEntity.getFileSize()),
                "text",
                FileType.ARCHIVE.toString()
        );

        //when
        mockMvc.perform(get(AdministratorPath.archiveFilesPath)
                .param("page",page.toString())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/file/archive/files"))
                .andExpect(model().attribute("files",new ArrayList<>(Collections.singletonList(fileToDownloadDTO))))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));
    }

    @Test
    public void shouldSearchArchiveFilesBySearchString() throws Exception{

        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);
        Integer page = 0;

        String savedFileName = "example-archive.txt";
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user,savedFileName);
        entityManager.persist(fileEntity);

        FileToDownloadDTO fileToDownloadDTO = new FileToDownloadDTO(
                fileEntity.getId(),
                fileEntity.getOriginalFileName(),
                FileSizeConverter.convertBytesToMegabytes(fileEntity.getFileSize()),
                "text",
                FileType.ARCHIVE.toString()
        );


        mockMvc.perform(get(AdministratorPath.archiveFilesPath + "/search")
                .sessionAttr("logged-user-id", user.getId())
                .param("searchString", fileEntity.getOriginalFileName())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/file/archive/fragment/search-files-fragment :: correct-result"))
                .andExpect(model().attribute("files", new ArrayList<>(Collections.singletonList(fileToDownloadDTO))));
    }
}