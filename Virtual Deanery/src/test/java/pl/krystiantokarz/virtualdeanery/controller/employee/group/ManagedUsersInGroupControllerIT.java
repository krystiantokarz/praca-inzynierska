package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;

import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class ManagedUsersInGroupControllerIT extends AbstractTransactionalTestNGSpringContextTests {


    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldAddAdminAccess() throws Exception {
        //given
        User adminUser = UserProvider.createUserWithEmployeeRole();

        User userToChangeAccess =UserProvider.createUserWithEmployeeRole();
        entityManager.persist(adminUser);
        entityManager.persist(userToChangeAccess);

        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        UserGroup userGroup1 = UserGroupProvider.createUserGroupForAdministratorGroupMember(adminUser, group);
        entityManager.persist(userGroup1);
        UserGroup userGroup2 = UserGroupProvider.createUserGroupForNormalGroupMember(userToChangeAccess, group);
        entityManager.persist(userGroup2);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" + group.getId() + "/user/" + userToChangeAccess.getId() + "/access/admin")
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", adminUser.getId()))
                .andExpect(status().isOk());

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup2.getId());

        assertThat(result.getMemberType()).isEqualTo(MemberType.ADMIN_MEMBER);
        assertThat(result.getGroup()).isEqualTo(group);
        assertThat(result.getUser()).isEqualTo(userToChangeAccess);

    }

    @Test
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToAddAdminAccess() throws Exception {
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" + group.getId() + "/user/" + user.getId() + "/access/admin")
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", user.getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldRemoveAdminAccess() throws Exception {
        //given
        User adminUser = UserProvider.createUserWithEmployeeRole();

        User userToChangeAccess =UserProvider.createUserWithEmployeeRole();
        entityManager.persist(adminUser);
        entityManager.persist(userToChangeAccess);

        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        UserGroup userGroup1 = UserGroupProvider.createUserGroupForAdministratorGroupMember(adminUser, group);
        entityManager.persist(userGroup1);
        UserGroup userGroup2 = UserGroupProvider.createUserGroupForAdministratorGroupMember(userToChangeAccess, group);
        entityManager.persist(userGroup2);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" + group.getId() + "/user/" + userToChangeAccess.getId() + "/access/member")
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", adminUser.getId()))
                .andExpect(status().isOk());

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup2.getId());

        assertThat(result.getMemberType()).isEqualTo(MemberType.NORMAL_MEMBER);
        assertThat(result.getGroup()).isEqualTo(group);
        assertThat(result.getUser()).isEqualTo(userToChangeAccess);
    }

    @Test
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToRemoveAdminAccess() throws Exception {
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user,group);
        entityManager.persist(userGroup);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/" + group.getId() + "/user/" + user.getId() + "/access/member")
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", user.getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldRemoveUserFromGroup() throws Exception {
        //given
        User adminUser = UserProvider.createUserWithEmployeeRole();

        User userToRemove =UserProvider.createUserWithEmployeeRole();
        entityManager.persist(adminUser);
        entityManager.persist(userToRemove);

        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        UserGroup userGroup1 = UserGroupProvider.createUserGroupForAdministratorGroupMember(adminUser, group);
        entityManager.persist(userGroup1);
        UserGroup userGroup2 = UserGroupProvider.createUserGroupForNormalGroupMember(userToRemove, group);
        entityManager.persist(userGroup2);

        //when
        mockMvc.perform(delete(EmployeePath.groupPath + "/" + group.getId() + "/user/" + userToRemove.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", adminUser.getId()))
                .andExpect(status().isOk());

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup2.getId());
        assertThat(result).isNull();
    }

    @Test
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToRemoveUserFromGroup() throws Exception {
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user,group);
        entityManager.persist(userGroup);

        //when
        mockMvc.perform(delete(EmployeePath.groupPath + "/" + group.getId() + "/user/" + user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", user.getId()))
                .andExpect(status().isForbidden());
    }


}