package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class UploadFormFileControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    private static final String MULTIPART_FILE_NAME = "TEST-MULTIPART-FILE-NAME";

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldUploadFormFile() throws Exception{

        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", MULTIPART_FILE_NAME, "text/plain",new byte[1]);


        //when
        mockMvc.perform(MockMvcRequestBuilders.fileUpload(AdministratorPath.formFilePath)
                .file(mockMultipartFile)
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        FileToDownloadDescriptionEntity result = entityManager.createQuery("SELECT file FROM FileToDownloadDescriptionEntity file WHERE file.originalFileName = :name", FileToDownloadDescriptionEntity.class)
                .setParameter("name", mockMultipartFile.getOriginalFilename())
                .getSingleResult();

        assertThat(result).isNotNull();
        assertThat(result.getAddingUser()).isEqualTo(user);
        assertThat(result.getFileType()).isEqualTo(FileType.FORM);
        assertThat(result.getFileStatus()).isEqualTo(FileStatus.READY_TO_DOWNLOAD);
        assertThat(result.getSavedFileName()).isNotEmpty();

        File uploadFile = new File(getClass().getResource("/repository/filetodownload/form").toURI().getPath(), result.getSavedFileName());
        assertThat(uploadFile.exists()).isTrue();

        uploadFile.delete();
    }


    @Test
    public void shouldNotUploadFormFileWhenUserNotExist() throws Exception{

        //given
        Long notExistUserId = -100L;

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", MULTIPART_FILE_NAME, "text/plain",new byte[0]);

        //when
        mockMvc.perform(MockMvcRequestBuilders.fileUpload(AdministratorPath.formFilePath)
                .file(mockMultipartFile)
                .sessionAttr("logged-user-id", notExistUserId)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isNotFound());
    }


    @Test
    public void shouldNotUploadFormFileWhenUploadFileIsEmpty() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", MULTIPART_FILE_NAME, "txt",new byte[0]);

        //when
        mockMvc.perform(MockMvcRequestBuilders.fileUpload(AdministratorPath.formFilePath)
                .file(mockMultipartFile)
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void shouldNotUploadFormFileWhenUploadFileNameAlreadyExist() throws Exception{
        //given
        shouldUploadFormFile();
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", MULTIPART_FILE_NAME, "txt",new byte[1]);

        //when
        mockMvc.perform(MockMvcRequestBuilders.fileUpload(AdministratorPath.formFilePath)
                .file(mockMultipartFile)
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isConflict());
    }

}