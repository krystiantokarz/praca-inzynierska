package pl.krystiantokarz.virtualdeanery.controller.administrator.messagebox;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.in.CreateMessageDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateMessageDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.messagebox.SendMessageService;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class SendMessageControllerTest {

    @Mock
    private FindUserService findUserService;

    @Mock
    private SendMessageService sendMessageService;

    @InjectMocks
    private SendMessageController sendMessageController;

    private MockMvc mockMvc;

    private ObjectMapper mapper;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
    }


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(sendMessageController).build();
    }

    @Test
    public void shouldGetPageForSendMessageToSelectedUser() throws Exception{

        Long testUserId = 100L;
        User user = UserProvider.createUserWithAdminRole();

        when(findUserService.findUserById(testUserId)).thenReturn(user);

        mockMvc.perform(get(AdministratorPath.mailPath + "/message/" + testUserId)
                .sessionAttr("logged-user-id", testUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("administrator/messagebox/send-message"))
                .andExpect(model().attribute("firstName",user.getFirstName()))
                .andExpect(model().attribute("lastName",user.getLastName()))
                .andExpect(model().attribute("email",user.getEmail()));
    }

    @Test
    public void shouldNotGetPageForSendMessageToSelectedUserWhenUserNotExist() throws Exception{
        Long testUserId = 100L;

        doThrow(UserNotExistException.class).when(findUserService).findUserById(testUserId);
        mockMvc.perform(get(AdministratorPath.mailPath + "/message/" + testUserId)
                .sessionAttr("logged-user-id", testUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("administrator/error/error-page"));

    }

    @Test
    public void shouldSendMessageForSelectedUser() throws Exception{

        Long testUserId = 100L;

        CreateMessageDTO createMessageDTO = CreateMessageDTOProvider.prepareCreateMessageDTOWithoutAttachments();

        doNothing().when(sendMessageService).sendMessage(createMessageDTO, testUserId);


        mockMvc.perform(post(AdministratorPath.mailPath + "/message")
                .sessionAttr("logged-user-id", testUserId)
                .param("idUserForSendMessage",testUserId.toString())
                .param("subject",createMessageDTO.getSubject())
                .param("message",createMessageDTO.getMessage())).
                andExpect(status().isOk());
    }

}