package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-test-controller.properties"
)
@AutoConfigureMockMvc
public class FindTimetableEventControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldFindTimetableDescriptionEventById() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);


        //when
        mockMvc.perform(get(EmployeePath.timetable + "/event/user/" + timetableEvent.getId())
                .param("eventId", timetableEvent.getId().toString())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id",user.getId()))
                .andExpect(jsonPath("$.userId", Matchers.is(user.getId().intValue())))
                .andExpect(jsonPath("$.message", Matchers.is(timetableEvent.getMessage())))
                .andExpect(jsonPath("$.topic", Matchers.is(timetableEvent.getTopic())))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldFindAllEventCalendarDescriptions() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        TimetableEvent firstTimetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(firstTimetableEvent);
        TimetableEvent secondTimetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(secondTimetableEvent);


        //when
        mockMvc.perform(get(EmployeePath.timetable + "/events/all")
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id",user.getId()))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(status().isOk());
    }

}