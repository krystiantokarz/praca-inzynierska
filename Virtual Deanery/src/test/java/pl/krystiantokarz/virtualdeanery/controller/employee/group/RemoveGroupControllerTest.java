package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.service.group.DeleteGroupService;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class RemoveGroupControllerTest {

    private MockMvc mockMvc;

    @Mock
    private DeleteGroupService deleteGroupService;

    @InjectMocks
    private RemoveGroupController removeGroupController;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(removeGroupController).build();
    }

    @Test
    public void shouldRemoveGroup() throws Exception {
        Long userId = 100L;
        Long groupId = 100L;

        mockMvc.perform(delete(EmployeePath.groupPath + "/" + groupId)
                .sessionAttr("logged-user-id", userId))
                .andExpect(status().isOk());

        verify(deleteGroupService, times(1)).deleteGroupByGroupAdministrator(userId, groupId);
    }
}