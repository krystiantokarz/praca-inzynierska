package pl.krystiantokarz.virtualdeanery.controller.path;

public class AdministratorPath {

    public static final String usersPath = "/admin/users";
    public static final String userPath = "/admin/user";
    public static final String groupPath = "/admin/group";
    public static final String groupsPath = "/admin/groups";
    public static final String filePath = "/admin/file";
    public static final String filesPath = "/admin/files";
    public static final String formFilePath = "/admin/file/form";
    public static final String formFilesPath = "/admin/files/form";
    public static final String archiveFilePath = "/admin/file/archive";
    public static final String archiveFilesPath = "/admin/files/archive";
    public static final String accountPath = "/admin/account";
    public static final String mailPath = "/admin/mail";
}
