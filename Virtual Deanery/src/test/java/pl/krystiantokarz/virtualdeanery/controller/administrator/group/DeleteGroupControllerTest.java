package pl.krystiantokarz.virtualdeanery.controller.administrator.group;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.service.group.DeleteGroupService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
public class DeleteGroupControllerTest {

    @Mock
    private DeleteGroupService deleteGroupService;

    @InjectMocks
    private DeleteGroupController deleteGroupController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(deleteGroupController).build();
    }

    @Test
    public void shouldDeleteGroupBySelectedGroupId() throws Exception{
        //given
        Long groupId = 1L;

        //when
        mockMvc.perform(delete(AdministratorPath.groupPath + "/" + groupId))
                .andExpect(status().isOk());

        //then
        verify(deleteGroupService, times(1)).deleteGroupBySystemAdministrator(groupId);
        verifyNoMoreInteractions(deleteGroupService);
    }

    @Test
    public void shouldNotDeleteGroupsBySelectedGroupId_whenGroupNotExist() throws Exception{
        //given
        Long groupId = 1L;

        //when
        doThrow(GroupNotExistException.class).when(deleteGroupService).deleteGroupBySystemAdministrator(groupId);

        mockMvc.perform(delete(AdministratorPath.groupPath + "/" + groupId))
                .andExpect(status().isNotFound());

        //then
        verify(deleteGroupService, times(1)).deleteGroupBySystemAdministrator(groupId);
        verifyNoMoreInteractions(deleteGroupService);
    }


}