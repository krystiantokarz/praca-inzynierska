package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class DeleteFormFileControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String FORM_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/form/";
    private static final String FORM_FILE_NAME = "example-form-to-remove.txt";

    @BeforeClass
    public void init(){
        CustomFileUtils.createFolderIfNotExists(FORM_REPOSITORY_PATH);
    }

    @Test
    public void shouldDeleteSelectedFormFileToDownload() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadFormFileToDownloadDescriptionEntity(user,FORM_FILE_NAME);

        entityManager.persist(fileEntity);

        //when
        mockMvc.perform(delete(AdministratorPath.formFilePath + "/" + fileEntity.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        File removedFile = new File(FORM_REPOSITORY_PATH + FORM_FILE_NAME);
        assertThat(removedFile.exists()).isFalse();

        FileToDownloadDescriptionEntity rejectFile = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(rejectFile).isNull();

        removedFile.createNewFile();
    }
}