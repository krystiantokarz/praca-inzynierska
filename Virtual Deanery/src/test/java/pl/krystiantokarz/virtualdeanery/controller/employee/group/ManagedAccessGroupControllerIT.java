package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.*;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class ManagedAccessGroupControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void shouldJoinIntoSelectedGroup() throws Exception{
        //given
        Group group = GroupProvider.createGroup();

        String enteredPassword = group.getPassword();
        group.changeGroupPassword(passwordEncoder.encode(enteredPassword));
        entityManager.persist(group);
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);


        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/access/" +group.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .param("pass", enteredPassword)
                .sessionAttr("logged-user-id", user.getId()))
                .andExpect(status().isOk());

        //then
        List<UserGroup> resultList = entityManager.createQuery("SELECT us FROM UserGroup us WHERE us.group = :gr AND us.user = :us", UserGroup.class)
                .setParameter("gr", group)
                .setParameter("us", user)
                .getResultList();

        assertThat(resultList).isNotEmpty();
        assertThat(resultList.get(0).getMemberType()).isEqualTo(MemberType.NORMAL_MEMBER);
    }

    @Test
    public void shouldThrowExceptionWhenGroupAndEnteredPasswordIsNotMatched() throws Exception{

        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        //when
        mockMvc.perform(put(EmployeePath.groupPath + "/access/" + group.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .param("pass", group.getPassword())
                .sessionAttr("logged-user-id", user.getId()))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void shouldLeaveFromSelectedGroup() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        User firstUser = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(firstUser);
        User secondUser = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(secondUser);
        UserGroup userGroup1 = UserGroupProvider.createUserGroupForNormalGroupMember(firstUser,group);
        UserGroup userGroup2 = UserGroupProvider.createUserGroupForNormalGroupMember(secondUser,group);

        entityManager.persist(userGroup1);
        entityManager.persist(userGroup2);

        //when
        mockMvc.perform(delete(EmployeePath.groupPath + "/access/" +group.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", firstUser.getId()))
                .andExpect(status().isOk());

        UserGroup result = entityManager.find(UserGroup.class, userGroup1.getId());
        assertNull(result);

        //then
        Group resultGroup = entityManager.find(Group.class, group.getId());
        assertNotNull(resultGroup);


    }

    @Test
    public void shouldLeaveFromSelectedGroupAndDeleteId() throws Exception {
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        mockMvc.perform(delete(EmployeePath.groupPath + "/access/" +group.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", user.getId()))
                .andExpect(status().isOk());

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup.getId());
        assertNull(result);

        Group resultGroup = entityManager.find(Group.class, group.getId());
        assertNull(resultGroup);
    }

}