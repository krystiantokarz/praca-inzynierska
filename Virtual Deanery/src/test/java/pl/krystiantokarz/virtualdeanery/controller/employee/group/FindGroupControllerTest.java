package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.GroupWithAccessDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.service.group.FindGroupService;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
public class FindGroupControllerTest {

    @Mock
    private FindGroupService findGroupService;

    @InjectMocks
    private FindGroupController findGroupController;

    @Mock
    private Group group;

    @Mock
    private UserGroup userGroup;

    @Mock
    private User user;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(findGroupController).build();
    }

    @Test
    public void shouldGetGroupsBySelectedPage() throws Exception{
        //given
        Integer page = 0;
        Integer size = 10;

        Long loggedUserId = 10L;
        Long groupId = 10L;

        GroupWithAccessDTO groupDTO = new GroupWithAccessDTO(groupId, group.getName(),group.getDescription(),true);

        when(group.getId()).thenReturn(groupId);
        when(findGroupService.getGroupsByPageAndSize(page, size)).thenReturn(new PageImpl<>(new ArrayList<>(Collections.singletonList(group))));
        when(findGroupService.getAllGroupsForUser(loggedUserId)).thenReturn(new ArrayList<>(Collections.singletonList(group)));


        mockMvc.perform(get(EmployeePath.groupsPath)
                .param("page",page.toString())
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("/employee/group/groups"))
                .andExpect(model().attribute("groups",new ArrayList<>(Collections.singletonList(groupDTO))))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));
    }

    @Test
    public void shouldGetGroupsForUserBySelectedPage() throws Exception{
        //given
        Integer size = 10;
        Integer page = 0;
        Long loggedUserId = 100L;

        when(findGroupService.getAllGroupsForUserWithPageAndSize(loggedUserId, page, size)).thenReturn(new PageImpl<Group>(Collections.emptyList()));

        mockMvc.perform(get(EmployeePath.groupsPath + "/user")
                .param("page",page.toString())
                .sessionAttr("logged-user-id",loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("/employee/group/groups-for-user"))
                .andExpect(model().attribute("groups",new ArrayList<>(Collections.emptyList())))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));
    }

    @Test
    public void shouldGetSelectedGroup() throws Exception{

        Long loggedUserId = 100L;
        Long groupId = 100L;

        when(group.getId()).thenReturn(groupId);
        when(userGroup.getGroup()).thenReturn(group);
        when(userGroup.getUser()).thenReturn(user);
        when(user.getId()).thenReturn(loggedUserId);
        when(findGroupService.findUserGroup(loggedUserId, groupId)).thenReturn(userGroup);
        when(findGroupService.findUserGroupsForSelectedGroup(groupId)).thenReturn(Collections.singletonList(userGroup));


        mockMvc.perform(get(EmployeePath.groupPath + "/" + group.getId())
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/group/selected-group"))
                .andExpect(model().attribute("name", group.getName()))
                .andExpect(model().attribute("groupId", group.getId()))
                .andExpect(model().attribute("description", group.getDescription()))
                .andExpect(model().attribute("loggedUserId", loggedUserId));
    }
}