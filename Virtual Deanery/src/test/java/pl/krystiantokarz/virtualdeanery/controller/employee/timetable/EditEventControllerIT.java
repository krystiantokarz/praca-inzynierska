package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.EditEventDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditEventDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-test-controller.properties"
)
@AutoConfigureMockMvc
public class EditEventControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private ObjectMapper mapper;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    @Test
    public void shouldEditSelectedEventByUser() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);


        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(timetableEvent.getId());

        String content = mapper.findAndRegisterModules().writeValueAsString(editEventDTO);

        //when
        mockMvc.perform(put(EmployeePath.timetable + "/event/" + timetableEvent.getId() + "/data")
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isOk());


        TimetableEvent updateTimetableEvent = entityManager.createQuery("SELECT t FROM TimetableEvent t WHERE t.id = :id", TimetableEvent.class)
                .setParameter("id",timetableEvent.getId())
                .getSingleResult();

        assertThat(updateTimetableEvent).isNotNull();
        assertThat(updateTimetableEvent.getTimeFrom()).isEqualTo(editEventDTO.getTimeFrom());
        assertThat(updateTimetableEvent.getTimeTo()).isEqualTo(editEventDTO.getTimeTo());
        assertThat(updateTimetableEvent.getTopic()).isEqualTo(editEventDTO.getTopic());
        assertThat(updateTimetableEvent.getMessage()).isEqualTo(editEventDTO.getMessage());
    }


    @Test
    public void shouldNotEditEventIfSelectedEventIsNotEventForUser() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        User notEventUser = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(notEventUser);

        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);


        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(timetableEvent.getId());

        String content = mapper.findAndRegisterModules().writeValueAsString(editEventDTO);

        //when
        mockMvc.perform(put(EmployeePath.timetable + "/event/" + timetableEvent.getId() + "/data")
                .sessionAttr("logged-user-id", notEventUser.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isForbidden());
    }
}