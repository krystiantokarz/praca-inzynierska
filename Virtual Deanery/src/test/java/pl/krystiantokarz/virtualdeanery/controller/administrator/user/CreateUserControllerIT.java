package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.CreateUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-test-controller.properties"
)
@AutoConfigureMockMvc
public class CreateUserControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private ObjectMapper mapper;

    @BeforeClass
    public void init() {
        mapper = new ObjectMapper();
    }

    @Test
    public void shouldCreateNewUser() throws Exception{
        //given
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();

        String content = mapper.writeValueAsString(createUserDTO);

        //when
        mockMvc.perform(post(AdministratorPath.userPath)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        User result = entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email", User.class)
                .setParameter("email", createUserDTO.getEmail())
                .getSingleResult();

        assertThat(result).isNotNull();
        assertThat(result.getEmail()).isEqualTo(createUserDTO.getEmail());
        assertThat(result.getLoginData().getLogin()).isEqualTo(createUserDTO.getEmail());
        assertThat(result.getFirstName()).isEqualTo(createUserDTO.getFirstName());
        assertThat(result.getLastName()).isEqualTo(createUserDTO.getLastName());
        assertThat(result.getRoles()).isNotEmpty();
    }


    @Test
    public void shouldNotCreateNewUser_whenDTOObjectIsNotValid() throws Exception{
        //given
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        createUserDTO.setEmail("NOT-VALID");
        String content = mapper.writeValueAsString(createUserDTO);

        //when
        mockMvc.perform(post(AdministratorPath.userPath)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isBadRequest());

        //then
        List<User> resultList = entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email", User.class)
                .setParameter("email", createUserDTO.getEmail())
                .getResultList();

        assertThat(resultList).isEmpty();

    }

    @Test
    public void shouldNotCreateNewUser_whenRolesNameIsIncorrect() throws Exception{
        //given
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        createUserDTO.getRoles().add("NOT_VALID");
        String content = mapper.writeValueAsString(createUserDTO);

        //when
        mockMvc.perform(post(AdministratorPath.userPath)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isBadRequest());

        //then
        List<User> resultList = entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email", User.class)
                .setParameter("email", createUserDTO.getEmail())
                .getResultList();


        assertThat(resultList).isEmpty();

    }

    @Test
    public void shouldNotCreateNewUser_whenUserWithSelectedEmailExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        createUserDTO.setEmail(user.getEmail());
        String content = mapper.writeValueAsString(createUserDTO);


        //when
        mockMvc.perform(post(AdministratorPath.userPath)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isConflict())
                .andReturn();

        //then
        List<User> resultList = entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email", User.class)
                .setParameter("email", createUserDTO.getEmail())
                .getResultList();

        assertThat(resultList).isNotNull().isNotEmpty();
        assertThat(resultList.size()).isEqualTo(1);
    }
}