package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-test-controller.properties"
)
@AutoConfigureMockMvc
public class ChangeUserStatusControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;


    @Test
    public void shouldDisableSelectedUser() throws Exception {
        //given
        User user = UserProvider.createUserWithAdminRole();

        entityManager.persist(user);

        //then
        mockMvc.perform(put(AdministratorPath.userPath + "/" + user.getId() + "/disable")
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        User savedUser = entityManager.find(User.class, user.getId());

        assertThat(savedUser).isNotNull();
        assertThat(savedUser.isEnabled()).isFalse();
    }


    @Test
    public void shouldNotDisableSelectedUser_whenUserNotExist() throws Exception {
        //given
        Long userId = 1L;

        //when
        mockMvc.perform(put(AdministratorPath.userPath + "/" + userId + "/disable")
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isNotFound());
    }


    @Test
    public void shouldEnableSelectedUser() throws Exception {

        //given
        User user = UserProvider.createUserWithAdminRole();

        entityManager.persist(user);


        //when
        mockMvc.perform(put(AdministratorPath.userPath + "/" + user.getId() + "/enable")
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        User savedUser = entityManager.find(User.class, user.getId());

        assertThat(savedUser).isNotNull();
        assertThat(savedUser.isEnabled()).isTrue();
    }

    @Test
    public void shouldNotEnableSelectedUser_whenUserNotExist() throws Exception {
        //given
        Long userId = 1L;

        //when
        mockMvc.perform(put(AdministratorPath.userPath + "/" + userId + "/enable")
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isNotFound());

    }




}