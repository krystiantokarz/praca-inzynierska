package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.CreateEventDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateEventDTOProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.CreateEventService;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class CreateEventControllerTest {

    @Mock
    private CreateEventService createEventService;

    @InjectMocks
    private CreateEventController createEventController;

    private MockMvc mockMvc;

    private ObjectMapper mapper;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(createEventController).build();
    }

    @Test
    public void shouldCreateNewEvent() throws Exception{

        //given
        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();

        String content = mapper.findAndRegisterModules().writeValueAsString(createEventDTO);

        Long userId = 100L;

        //when
        mockMvc.perform(post(EmployeePath.timetable + "/event")
                .sessionAttr("logged-user-id", userId.toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isOk());

        //then
        verify(createEventService, times(1)).createEvent(createEventDTO,userId);
    }
}