package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class DownloadFormFileControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldDownloadFormFile() throws Exception{

        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        String savedFileName = "example-form.txt";
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadFormFileToDownloadDescriptionEntity(user,savedFileName);

        entityManager.persist(fileEntity);

        //when
        MvcResult result = mockMvc.perform(get(AdministratorPath.formFilePath)
                .param("fileId", fileEntity.getId().toString())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andReturn();

        //then
        assertThat(result.getResponse().getHeader("Content-Disposition")).isEqualTo("attachment; filename=\"" +fileEntity.getOriginalFileName() +"\"");
        assertThat(result.getResponse().getContentType()).isEqualTo("text/plain");
        assertThat(result.getResponse().getContentAsByteArray().length).isGreaterThan(0);
        assertThat(result.getResponse().getContentLength()).isEqualTo(FileToDownloadDescriptionProvider.FILE_SIZE);
    }


    @Test
    public void shouldNotDownloadFormFile_whenMessageAttachmentDescriptionNotExist() throws Exception {

        //when
        mockMvc.perform(get(AdministratorPath.formFilePath)
                .param("fileId", "-1000")
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldNotDownloadFormFile_whenDownloadFileIsImpossible() throws Exception {

        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        String savedNotExistFileName = "example-blabala.txt";
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadFormFileToDownloadDescriptionEntity(user,savedNotExistFileName);

        entityManager.persist(fileEntity);

        //when
        mockMvc.perform(get(AdministratorPath.formFilePath)
                .param("fileId", fileEntity.getId().toString())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isBadRequest());
    }

}