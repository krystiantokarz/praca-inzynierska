package pl.krystiantokarz.virtualdeanery.controller;

public class SecurityConfig {

    public static final String LOGIN = "TEST-LOGIN";
    public static final String PASSWORD = "TEST-PASSWORD";
    public static final String EMPLOYEE_ROLE = "EMPLOYEE";
    public static final String ADMIN_ROLE = "ADMIN";
}
