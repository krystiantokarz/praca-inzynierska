package pl.krystiantokarz.virtualdeanery.controller.employee.messagebox;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageStatus;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out.MessageContentDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out.MessageDescriptionDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.messagebox.FindMessageService;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.MailMessageNotExistException;


import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class FindMessageControllerTest {

    @Mock
    private FindMessageService findMessageService;

    @InjectMocks
    private FindMessageController findMessageController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(findMessageController).build();
    }


    @Test
    public void shouldGetPageForMessageBoxForSelectedUser() throws Exception{
        //given
        Integer page = 0;
        Integer size = 20;
        User user = UserProvider.createUserWithEmployeeRole();
        Long loggedUserId = 10L;

        MailMessageEntity messageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(user, user);


        MessageDescriptionDTO messageDescriptionDTO = new MessageDescriptionDTO(
                messageEntity.getId(),
                user.getFirstName(),
                user.getLastName(),
                messageEntity.getSubject(),
                messageEntity.getDate(),
                MailMessageStatus.DELIVERED.toString()
        );

        //when
        when(findMessageService.findMessagesForUserByPageAndPageSize(loggedUserId,page, size)).thenReturn(new PageImpl<>(new ArrayList<>(Collections.singletonList(messageEntity))));
        mockMvc.perform(get(EmployeePath.mailPath)
                .param("page","0")
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/messagebox/message-box"))
                .andExpect(model().attribute("messages",new ArrayList<>(Collections.singletonList(messageDescriptionDTO))))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));
    }

    @Test
    public void shouldGetMessage() throws Exception{
        //given
        Long messageId = 1L;
        User user = UserProvider.createUserWithEmployeeRole();
        Long loggedUserId = 10L;

        MailMessageEntity messageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(user, user);

        MessageContentDTO messageContentDTO = new MessageContentDTO(
                user.getFirstName(),
                user.getLastName(),
                messageEntity.getSubject(),
                messageEntity.getMessage(),
                messageEntity.getDate(),
                MailMessageStatus.DELIVERED.toString(),
                Collections.emptyList()
        );

        //when
        when(findMessageService.findMessageByIdForSelectedUserAndChangeMessageStatusIfNotReadYet(messageId, loggedUserId)).thenReturn(messageEntity);

        mockMvc.perform(get(EmployeePath.mailPath + "/" + messageId)
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/messagebox/selected-message"))
                .andExpect(model().attribute("messageContent",messageContentDTO));
    }

    @Test
    public void shouldNotGetMessageWhenMessageNotExist() throws Exception{
        //given
        Long messageId = 1L;
        Long loggedUserId = 10L;

        //when
        doThrow(MailMessageNotExistException.class).when(findMessageService).findMessageByIdForSelectedUserAndChangeMessageStatusIfNotReadYet(messageId, loggedUserId);
        mockMvc.perform(get(EmployeePath.mailPath + "/" + messageId)
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("/employee/error/error-page"));
    }
}