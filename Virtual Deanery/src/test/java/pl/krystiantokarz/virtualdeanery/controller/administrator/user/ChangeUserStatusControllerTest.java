package pl.krystiantokarz.virtualdeanery.controller.administrator.user;


import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.service.user.ChangeUserStatusService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebAppConfiguration
public class ChangeUserStatusControllerTest extends AbstractTestNGSpringContextTests {

    @Mock
    private ChangeUserStatusService changeUserStatusService;

    @InjectMocks
    private ChangeUserStatusController changeUserStatusController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(changeUserStatusController).build();
    }


    @Test
    public void shouldDisableSelectedUser() throws Exception {
        //given
        Long userId = 1L;
        //when
        doNothing().when(changeUserStatusService).disableUser(userId, Locale.getDefault());

        //then
        mockMvc.perform(put(AdministratorPath.userPath + "/" + userId + "/disable"))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldNotDisableSelectedUser_whenUserNotExist() throws Exception {
        //given
        Long userId = 1L;
        //when
        doThrow(UserNotExistException.class).when(changeUserStatusService).disableUser(eq(userId),any(Locale.class));
        //then
        mockMvc.perform(put(AdministratorPath.userPath + "/" + userId + "/disable"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldEnableSelectedUser() throws Exception {
        //given
        Long userId = 1L;
        //when
        doNothing().when(changeUserStatusService).enableUser(userId,Locale.getDefault());

        //then
        mockMvc.perform(put(AdministratorPath.userPath + "/" + userId + "/enable"))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldNotEnableSelectedUser_whenUserNotExist() throws Exception {
        //given
        Long userId = 1L;
        //when
        doThrow(UserNotExistException.class).when(changeUserStatusService).enableUser(eq(userId),any(Locale.class));
        //then
        mockMvc.perform(put(AdministratorPath.userPath + "/" + userId + "/enable"))
                .andExpect(status().isNotFound());
    }



}