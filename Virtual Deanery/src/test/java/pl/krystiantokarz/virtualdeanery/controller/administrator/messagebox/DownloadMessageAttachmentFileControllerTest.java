package pl.krystiantokarz.virtualdeanery.controller.administrator.messagebox;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.controller.shared.PrepareDownloadFileComponent;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailAttachmentFileDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.service.messagebox.DownloadMessageAttachmentFileService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DownloadMessageAttachmentFileControllerTest {

    private static final Long loggedUserId = 1L;
    private static final Long attachmentId = 1L;

    @Mock
    private DownloadMessageAttachmentFileService downloadMessageAttachmentService;

    @Mock
    private PrepareDownloadFileComponent prepareDownloadFileComponent;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private ServletOutputStream outputStream;

    @InjectMocks
    private DownloadMessageAttachmentFileController downloadMessageAttachmentFileController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(downloadMessageAttachmentFileController).build();
    }

    @Test
    public void shouldDownloadMessageAttachmentFile() throws Exception{



        //when
        when(prepareDownloadFileComponent.prepareResponseForMessageAttachment(any(HttpServletResponse.class), eq(attachmentId), eq(loggedUserId))).thenReturn(httpServletResponse);
        when(httpServletResponse.getOutputStream()).thenReturn(outputStream);

        MvcResult result = mockMvc.perform(get(AdministratorPath.mailPath + "/attachment")
                .sessionAttr("logged-user-id", loggedUserId)
                .param("id", attachmentId.toString()))
                .andExpect(status().isOk())
                .andReturn();

    }

}