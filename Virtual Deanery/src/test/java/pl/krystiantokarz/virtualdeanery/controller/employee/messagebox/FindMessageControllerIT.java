package pl.krystiantokarz.virtualdeanery.controller.employee.messagebox;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageStatus;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out.MessageContentDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.out.MessageDescriptionDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class FindMessageControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldGetPageForMessageBoxForSelectedUser() throws Exception{
        //when
        Integer page = 0;
        User user = UserProvider.createUserWithEmployeeRole();

        MailMessageEntity messageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(user, user);

        entityManager.persist(user);
        entityManager.persist(messageEntity);


        MessageDescriptionDTO messageDescriptionDTO = new MessageDescriptionDTO(
                messageEntity.getId(),
                user.getFirstName(),
                user.getLastName(),
                messageEntity.getSubject(),
                messageEntity.getDate(),
                MailMessageStatus.DELIVERED.toString()
        );

        //when
        mockMvc.perform(get(EmployeePath.mailPath)
                .param("page","0")
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/messagebox/message-box"))
                .andExpect(model().attribute("messages",new ArrayList<>(Collections.singletonList(messageDescriptionDTO))))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));
    }

    @Test
    public void shouldGetMessage() throws Exception{
        //given

        User user = UserProvider.createUserWithEmployeeRole();

        MailMessageEntity messageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(user, user);

        entityManager.persist(user);
        entityManager.persist(messageEntity);


        MessageContentDTO messageContentDTO = new MessageContentDTO(
                user.getFirstName(),
                user.getLastName(),
                messageEntity.getSubject(),
                messageEntity.getMessage(),
                messageEntity.getDate(),
                MailMessageStatus.READ.toString(),
                Collections.emptyList()
        );

        //when
        mockMvc.perform(get(EmployeePath.mailPath  + "/" + messageEntity.getId())
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/messagebox/selected-message"))
                .andExpect(model().attribute("messageContent",messageContentDTO));
    }

    @Test
    public void shouldNotGetMessageWhenMessageNotExist() throws Exception{
        Long messageId = -1001L;
        Long loggedUserId = 1L;

        mockMvc.perform(get(EmployeePath.mailPath + "/" + messageId)
                .sessionAttr("logged-user-id", loggedUserId)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/employee/error/error-page"));



    }
}