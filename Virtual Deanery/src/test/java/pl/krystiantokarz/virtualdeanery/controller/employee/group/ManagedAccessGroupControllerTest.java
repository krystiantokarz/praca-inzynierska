package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.service.group.ManagedAccessGroupService;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class ManagedAccessGroupControllerTest {

    @Mock
    private ManagedAccessGroupService managedAccessGroupService;

    @InjectMocks
    private ManagedAccessGroupController managedAccessGroupController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(managedAccessGroupController).build();
    }

    @Test
    public void shouldJoinIntoGroup() throws Exception{
        Long userId= 100L;
        Long groupId = 100L;
        String groupPass = "PASSWORD";

        mockMvc.perform(put(EmployeePath.groupPath + "/access/" +groupId)
                .param("pass", groupPass)
                .sessionAttr("logged-user-id", userId))
                .andExpect(status().isOk());

        verify(managedAccessGroupService, times(1)).joinIntoSelectedGroup(groupId, userId,groupPass);
    }

    @Test
    public void shouldLeaveFromGroup() throws Exception{

        Long userId= 100L;
        Long groupId = 100L;
        mockMvc.perform(delete(EmployeePath.groupPath + "/access/" +groupId)
                .sessionAttr("logged-user-id", userId))
                .andExpect(status().isOk());

        verify(managedAccessGroupService, times(1)).leaveFromSelectedGroup(userId, groupId);
    }

}