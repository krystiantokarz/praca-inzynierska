package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-test-controller.properties"
)
@AutoConfigureMockMvc
public class EditUserControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private ObjectMapper mapper;

    @BeforeClass
    public void init() {
        mapper = new ObjectMapper();
    }

    @Test
    public void shouldGetPageAndUserDataToEdit() throws Exception {

        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);
        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );

        //when
        mockMvc.perform(get(AdministratorPath.userPath + "/" + user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/user/user-edit"))
                .andExpect(model().attribute("user",findUserDTO))
                .andExpect(model().attribute("roles",Collections.singletonList(RoleEnum.ADMIN.toString())));
    }

    @Test
    public void shouldGetErrorPage_whenUserNotExist() throws Exception {

        //given
        Long userId = 1L;

        //when
        mockMvc.perform(get(AdministratorPath.userPath + "/" + userId)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/error/error-page"));
    }

    @Test
    public void shouldEditSelectedUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();

        String content = mapper.writeValueAsString(editUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.userPath + "/" + user.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        assertThat(user).isNotNull();
        assertThat(user.getLastName()).isEqualTo(editUserDTO.getLastName());
        assertThat(user.getFirstName()).isEqualTo(editUserDTO.getFirstName());
        assertThat(user.getEmail()).isEqualTo(editUserDTO.getEmail());
        assertThat(user.getLoginData().getLogin()).isEqualTo(editUserDTO.getLogin());
        assertThat(user.getRoles()).isNotEmpty();
    }

    @Test
    public void shouldNotEditSelectedUser_whenUserNotExist() throws Exception{
        //given
        Long notExistId  = -100L;
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        String content = mapper.writeValueAsString(editUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.userPath + "/" + notExistId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isNotFound());

        //then
        List<User> resultList = entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email", User.class)
                .setParameter("email", editUserDTO.getEmail())
                .getResultList();

        assertThat(resultList).isEmpty();

    }

    @Test
    public void shouldNotEditSelectedUser_whenUserWithSelectedLoginExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);
        User editUser = UserProvider.createUserWithAdminRole();
        entityManager.persist(editUser);
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        editUserDTO.setLogin(user.getLoginData().getLogin());

        String content = mapper.writeValueAsString(editUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.userPath + "/" + editUser.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isConflict())
                .andReturn();

    }

    @Test
    public void shouldNotEditSelectedUser_whenUserWithSelectedEmailExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);
        User editUser = UserProvider.createUserWithAdminRole();
        entityManager.persist(editUser);
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        editUserDTO.setEmail(user.getEmail());

        String content = mapper.writeValueAsString(editUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.userPath + "/" + editUser.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isConflict())
                .andReturn();
    }
}