package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.CreateGroupDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateGroupDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class CreateGroupControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private ObjectMapper mapper;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
    }

    @Test
    public void shouldGetPageForCreateNewGroup() throws Exception{
        //given
        Long loggedUserId = 100L;
        //when
        mockMvc.perform(get(EmployeePath.groupPath)
                .sessionAttr("logged-user-id", loggedUserId)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldCreateNewGroup() throws Exception{

        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        CreateGroupDTO createGroupDTO = CreateGroupDTOProvider.prepareCreateGroupDTO();

        String content = mapper.writeValueAsString(createGroupDTO);

        //when
        mockMvc.perform(post(EmployeePath.groupPath)
                .sessionAttr("logged-user-id", user.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk());
        //then
        Group savedGroup = entityManager.createQuery("SELECT g From Group g WHERE g.name = :name ", Group.class)
                .setParameter("name", createGroupDTO.getName())
                .getSingleResult();

        assertThat(savedGroup).isNotNull();
        assertThat(savedGroup.getPassword()).isNotEmpty();
        assertThat(savedGroup.getName()).isEqualTo(createGroupDTO.getName());
        assertThat(savedGroup.getDescription()).isEqualTo(createGroupDTO.getDescription());

        List<UserGroup> userGroupResultList = entityManager.createNativeQuery("SELECT * FROM user_group ug WHERE ug.group_id = :groupId", UserGroup.class)
                .setParameter("groupId", savedGroup.getId())
                .getResultList();

        assertThat(userGroupResultList).isNotEmpty();
        assertThat(userGroupResultList.get(0).getUser()).isEqualTo(user);
        assertThat(userGroupResultList.get(0).getGroup()).isEqualTo(savedGroup);
        assertThat(userGroupResultList.get(0).getMemberType()).isEqualTo(MemberType.ADMIN_MEMBER);
    }

    @Test
    public void shouldNotCreateNewGroupWhenGroupNameIsNotUnique() throws Exception{

        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        CreateGroupDTO createGroupDTO = CreateGroupDTOProvider.prepareCreateGroupDTO();
        createGroupDTO.setName(group.getName());

        String content = mapper.writeValueAsString(createGroupDTO);

        //when
        mockMvc.perform(post(EmployeePath.groupPath)
                .sessionAttr("logged-user-id", user.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isConflict());

        //then
        List<Group> groups = entityManager.createQuery("SELECT g From Group g WHERE g.name = :name ", Group.class)
                .setParameter("name", createGroupDTO.getName())
                .getResultList();

        assertThat(groups).isNotNull();
        assertThat(groups).hasSize(1);

    }


}
