package pl.krystiantokarz.virtualdeanery.controller.administrator.account;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;


import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class FindAccountControllerTest {

    @Mock
    private FindUserService findUserService;

    @InjectMocks
    private FindAccountController findAccountController;

    private MockMvc mockMvc;

    private static final Long loggedUserId = 1L;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(findAccountController).build();
    }

    @Test
    public void shouldGetAccountPage() throws Exception{
        User user = UserProvider.createUserWithAdminRole();

        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );

        when(findUserService.findUserById(loggedUserId)).thenReturn(user);

        mockMvc.perform(get(AdministratorPath.accountPath)
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/account/account"))
                .andExpect(model().attribute("user", findUserDTO));
    }

    @Test
    public void shouldNotGetAccountPageWhenLoggedUserNotExist() throws Exception{
        doThrow(UserNotExistException.class).when(findUserService).findUserById(loggedUserId);

        mockMvc.perform(get(AdministratorPath.accountPath)
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/error/error-page"));
    }


}