package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.CreateGroupDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateGroupDTOProvider;
import pl.krystiantokarz.virtualdeanery.service.group.CreateGroupService;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupWithSelectedNameExistException;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest
public class CreateGroupControllerTest {

    @Mock
    private CreateGroupService createGroupService;

    @InjectMocks
    private CreateGroupController createGroupController;

    private MockMvc mockMvc;

    private ObjectMapper mapper;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
    }

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(createGroupController).build();
    }


    @Test
    public void shouldCreateNewGroup() throws Exception {

        //given
        CreateGroupDTO createGroupDTO = CreateGroupDTOProvider.prepareCreateGroupDTO();

        Long userId= 100L;

        String content = mapper.writeValueAsString(createGroupDTO);

        //when
        mockMvc.perform(post(EmployeePath.groupPath)
                .sessionAttr("logged-user-id", userId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isOk());

        verify(createGroupService, times(1)).createGroup(createGroupDTO, userId);
    }

    @Test
    public void shouldNotCreateNewGroupWhenGroupNameIsNotUnique() throws Exception{

        //given
        Long userId = 100L;

        CreateGroupDTO createGroupDTO = CreateGroupDTOProvider.prepareCreateGroupDTO();

        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(createGroupDTO);

        //when
        doThrow(GroupWithSelectedNameExistException.class).when(createGroupService).createGroup(createGroupDTO, userId);
        mockMvc.perform(post(EmployeePath.groupPath)
                .sessionAttr("logged-user-id", userId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content)
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isConflict());

    }
}
