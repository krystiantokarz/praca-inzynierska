package pl.krystiantokarz.virtualdeanery.controller.administrator.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-test-controller.properties"
)
@AutoConfigureMockMvc
@Rollback
public class ChangePasswordControllerIT extends AbstractTransactionalTestNGSpringContextTests {


    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Test
    public void shouldGetChangePasswordPage() throws Exception{
        mockMvc.perform(get(AdministratorPath.accountPath + "/password")
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/account/change-password"));
    }

    @Test
    public void shouldChangePassword() throws Exception{

        //given
        String newPassword = "NEW-PASSWORD-TEST";
        User user = UserProvider.createUserWithAdminRole();
        String oldPassword = user.getLoginData().getPassword();
        String encodeOldPassword = passwordEncoder.encode(oldPassword);
        user.changeUserPassword(encodeOldPassword);
        entityManager.persist(user);

        //when
        mockMvc.perform(put(AdministratorPath.accountPath + "/password")
                .param("oldPassword",oldPassword)
                .param("newPassword",newPassword)
                .sessionAttr("logged-user-id",user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        User result = entityManager.find(User.class, user.getId());

        assertThat(result.getLoginData().getPassword()).isNotEqualTo(encodeOldPassword);

        boolean matches = passwordEncoder.matches(newPassword, user.getLoginData().getPassword());
        assertThat(matches).isTrue();
    }


    @Test
    public void shouldNotChangePassword_whenNoMatchesOldPassword() throws Exception{
        //given
        String notMatchesOldPassword = "NOT-MATCHES-PASSWORD";
        String newPassword = "NEW-PASSWORD-TEST";
        User user = UserProvider.createUserWithAdminRole();

        entityManager.persist(user);

        //when
        mockMvc.perform(put(AdministratorPath.accountPath + "/password")
                .param("oldPassword",notMatchesOldPassword)
                .param("newPassword",newPassword)
                .sessionAttr("logged-user-id",user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isNotAcceptable());
    }


    @Test
    public void shouldNotChangePassword_whenUserNotExist() throws Exception{
        //given
        Long notExistUserId = -100L;

        //when
        mockMvc.perform(put(AdministratorPath.accountPath + "/password")
                .param("oldPassword","OLD-PASSWORD")
                .param("newPassword","NEW-PASSWORD")
                .sessionAttr("logged-user-id",notExistUserId)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isNotFound());
    }
}