package pl.krystiantokarz.virtualdeanery.controller.path;

public class EmployeePath {

    public static final String usersPath = "/home/users";
    public static final String groupsPath = "/home/groups";
    public static final String groupPath = "/home/group";
    public static final String formFilePath = "/home/file/form";
    public static final String formFilesPath = "/home/files/form";
    public static final String archiveFilePath = "/home/file/archive";
    public static final String archiveFilesPath = "/home/files/archive";
    public static final String accountPath = "/home/account";
    public static final String mailPath = "/home/mail";
    public static final String chat = "/home/chat";
    public static final String timetable = "/home/timetable";
}
