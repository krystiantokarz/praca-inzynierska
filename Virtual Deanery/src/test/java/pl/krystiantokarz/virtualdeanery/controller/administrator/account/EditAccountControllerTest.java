package pl.krystiantokarz.virtualdeanery.controller.administrator.account;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditAccountUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditAccountUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.account.EditAccountService;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedLoginExistException;

import java.util.ArrayList;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class EditAccountControllerTest {

    @Mock
    private FindUserService findUserService;

    @Mock
    private EditAccountService editAccountService;

    @InjectMocks
    private EditAccountController editAccountController;

    private MockMvc mockMvc;

    private ObjectMapper mapper;

    private static final Long loggedUserId = 1L;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
    }

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(editAccountController).build();
    }

    @Test
    public void shouldGetEditAccountPage() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();

        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );

        //when
        when(findUserService.findUserById(loggedUserId)).thenReturn(user);

        mockMvc.perform(get(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/account/account-edit"))
                .andExpect(model().attribute("roles", new ArrayList<>(Collections.singletonList(RoleEnum.ADMIN.toString()))))
                .andExpect(model().attribute("administrator", findUserDTO));
    }


    @Test
    public void shouldNotGetEditAccountPage_whenUserNotExist() throws Exception {
        //given
        doThrow(UserNotExistException.class).when(findUserService).findUserById(loggedUserId);

        //when
        mockMvc.perform(get(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", loggedUserId))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/error/error-page"));
    }

    @Test
    public void shouldEditUserAccount() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        EditAccountUserDTO editAccountUserDTO = EditAccountUserDTOProvider.prepareEditAccountUserDTO();

        String content = mapper.writeValueAsString(editAccountUserDTO);

        //when
        when(editAccountService.editSelectedUser(loggedUserId, editAccountUserDTO)).thenReturn(user);


        mockMvc.perform(put(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", loggedUserId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isOk());

        //then
        verify(editAccountService, times(1)).editSelectedUser(loggedUserId, editAccountUserDTO);

    }

    @Test
    public void shouldNotEditUserAccountWhenDTOIsNotValidate() throws Exception{
        //given
        EditAccountUserDTO editAccountUserDTO = EditAccountUserDTOProvider.prepareEditAccountUserDTO();
        editAccountUserDTO.setEmail("NOT-VALID");

        String content = mapper.writeValueAsString(editAccountUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", loggedUserId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void shouldNotEditUserAccountWhenUserNotExist() throws Exception{
        //given
        EditAccountUserDTO editAccountUserDTO = EditAccountUserDTOProvider.prepareEditAccountUserDTO();

        String content = mapper.writeValueAsString(editAccountUserDTO);

        //when
        doThrow(UserNotExistException.class).when(editAccountService).editSelectedUser(loggedUserId, editAccountUserDTO);

        mockMvc.perform(put(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", loggedUserId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isNotFound());

        //then
        verify(editAccountService, times(1)).editSelectedUser(loggedUserId, editAccountUserDTO);

    }

    @Test
    public void shouldNotEditUserAccountWhenUserWithSelectedLoginExist() throws Exception{
        //given
        EditAccountUserDTO editAccountUserDTO = EditAccountUserDTOProvider.prepareEditAccountUserDTO();

        String content = mapper.writeValueAsString(editAccountUserDTO);


        //when
        doThrow(UserWithSelectedLoginExistException.class).when(editAccountService).editSelectedUser(loggedUserId, editAccountUserDTO);
        MvcResult mvcResult = mockMvc.perform(put(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", loggedUserId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isConflict())
                .andReturn();

        //then
        assertThat(mvcResult.getResponse().getContentAsString()).isEqualTo("\"USER_LOGIN_EXIST\"");
        verify(editAccountService, times(1)).editSelectedUser(loggedUserId, editAccountUserDTO);
    }

    @Test
    public void shouldNotEditUserAccountWhenUserWithSelectedEmailExist() throws Exception{
        //given
        EditAccountUserDTO editAccountUserDTO = EditAccountUserDTOProvider.prepareEditAccountUserDTO();

        String content = mapper.writeValueAsString(editAccountUserDTO);


        //when
        doThrow(UserWithSelectedEmailExistException.class).when(editAccountService).editSelectedUser(loggedUserId, editAccountUserDTO);
        MvcResult mvcResult = mockMvc.perform(put(AdministratorPath.accountPath + "/data")
                .sessionAttr("logged-user-id", loggedUserId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isConflict())
                .andReturn();

        //then
        assertThat(mvcResult.getResponse().getContentAsString()).isEqualTo("\"USER_EMAIL_EXIST\"");
        verify(editAccountService, times(1)).editSelectedUser(loggedUserId, editAccountUserDTO);
    }
}