package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.converter.TimetableEventConverter;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.EditEventDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.out.EventDescriptionDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditEventDTOProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.EditEventService;
import pl.krystiantokarz.virtualdeanery.service.timetable.FindEventService;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@SpringBootTest
public class EditEventControllerTest {

    @Mock
    private EditEventService editEventService;

    @Mock
    private FindEventService findEventService;

    @InjectMocks
    private EditEventController editEventController;

    private MockMvc mockMvc;

    @Mock
    private User user;

    private ObjectMapper mapper;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    private static final String TOPIC = "TOPIC";
    private static final String MESSAGE = "MESSAGE";


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(editEventController).build();
    }

    @Test
    public void shoutGetPageToEditEvent() throws Exception{

        //given
        Long eventId = 100L;
        Long userId = 100L;
        TimetableEvent timetableEvent = prepareTimetableEvent();

        EventDescriptionDTO dto = TimetableEventConverter.convertEventToTimetableEventDescriptionDTO(timetableEvent);

        //when
        when(findEventService.getTimetableEventToEdit(eventId, userId)).thenReturn(timetableEvent);

        mockMvc.perform(get(EmployeePath.timetable + "/event/" + eventId + "/data")
                .sessionAttr("logged-user-id", userId))
                .andExpect(status().isOk())
                .andExpect(model().attribute("event", dto))
                .andExpect(view().name("/employee/timetable/edit-event"));
    }

    @Test
    public void shoutEditSelectedEvent() throws Exception{
        //given
        Long eventId = 100L;
        Long userId = 100L;

        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(eventId);

        String content = mapper.findAndRegisterModules().writeValueAsString(editEventDTO);


        //when
        mockMvc.perform(put(EmployeePath.timetable + "/event/" + eventId + "/data")
                .sessionAttr("logged-user-id", userId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(content))
                .andExpect(status().isOk());

        //then
        verify(editEventService, times(1)).editSelectedEventByUser(editEventDTO,userId);
    }

    private TimetableEvent prepareTimetableEvent(){
        return TimetableEvent.from(
                LocalDate.now(),
                LocalTime.of(10,10),
                LocalTime.of(11,11),
                TOPIC,
                MESSAGE,
                user
        );
    }
}