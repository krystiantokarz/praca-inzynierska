package pl.krystiantokarz.virtualdeanery.controller.administrator.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.EditUserService;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedLoginExistException;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@WebAppConfiguration
public class EditUserControllerTest {

    @Mock
    private EditUserService editUserService;

    @Mock
    private FindUserService findUserService;

    @InjectMocks
    private EditUserController editUserController;

    private MockMvc mockMvc;

    private ObjectMapper mapper;

    @BeforeClass
    public void init(){
        mapper = new ObjectMapper();
    }

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(editUserController).build();
    }

    @Test
    public void shouldGetPageAndUserDataToEdit() throws Exception {

        //given
        Long userId = 1L;
        User user = UserProvider.createUserWithAdminRole();

        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );


        //when
        when(findUserService.findUserById(userId)).thenReturn(user);

        mockMvc.perform(get(AdministratorPath.userPath + "/" + userId)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(model().attribute("user",findUserDTO))
                .andExpect(model().attribute("roles",Collections.singletonList(RoleEnum.ADMIN.toString())));

        //then
        verify(findUserService, times(1)).findUserById(userId);
        verifyNoMoreInteractions(findUserService);
    }

    @Test
    public void shouldGetErrorPage_whenUserNotExist() throws Exception {

        //given
        Long userId = 1L;

        //when
        doThrow(UserNotExistException.class).when(findUserService).findUserById(userId);

        mockMvc.perform(get(AdministratorPath.userPath + "/" + userId)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/error/error-page"));

        //then
        verify(findUserService, times(1)).findUserById(userId);
    }

    @Test
    public void shouldEditSelectedUser() throws Exception{
        //given
        Long userId = 1L;

        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        String content = mapper.writeValueAsString(editUserDTO);

        //when
        mockMvc.perform(put(AdministratorPath.userPath + "/" + userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isOk());

        //then

        verify(editUserService, times(1)).editSelectedUser(userId, editUserDTO);
        verifyNoMoreInteractions(editUserService);
    }

    @Test
    public void shouldNotEditSelectedUser_whenUserNotExist() throws Exception{
        //given
        Long userId = 1L;

        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();

        String content = mapper.writeValueAsString(editUserDTO);
        //when

        doThrow(UserNotExistException.class).when(editUserService).editSelectedUser(userId, editUserDTO);

        mockMvc.perform(put(AdministratorPath.userPath + "/" + userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isNotFound());

        //then

        verify(editUserService, times(1)).editSelectedUser(userId, editUserDTO);
        verifyNoMoreInteractions(editUserService);
    }

    @Test
    public void shouldNotEditSelectedUser_whenUserWithSelectedLoginExist() throws Exception{
        //given
        Long userId = 1L;

        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();

        String content = mapper.writeValueAsString(editUserDTO);
        //when

        doThrow(UserWithSelectedLoginExistException.class).when(editUserService).editSelectedUser(userId, editUserDTO);

        MvcResult mvcResult = mockMvc.perform(put(AdministratorPath.userPath + "/" + userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isConflict())
                .andReturn();

        //then
        assertThat(mvcResult.getResponse().getContentAsString()).isEqualTo("\"USER_LOGIN_EXIST\"");
        verify(editUserService, times(1)).editSelectedUser(userId, editUserDTO);
        verifyNoMoreInteractions(editUserService);
    }

    @Test
    public void shouldNotEditSelectedUser_whenUserWithSelectedEmailExist() throws Exception{
        //given
        Long userId = 1L;
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();

        String content = mapper.writeValueAsString(editUserDTO);
        //when

        doThrow(UserWithSelectedEmailExistException.class).when(editUserService).editSelectedUser(userId, editUserDTO);

        MvcResult mvcResult = mockMvc.perform(put(AdministratorPath.userPath + "/" + userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isConflict())
                .andReturn();

        //then
        assertThat(mvcResult.getResponse().getContentAsString()).isEqualTo("\"USER_EMAIL_EXIST\"");
        verify(editUserService, times(1)).editSelectedUser(userId, editUserDTO);
        verifyNoMoreInteractions(editUserService);
    }
}