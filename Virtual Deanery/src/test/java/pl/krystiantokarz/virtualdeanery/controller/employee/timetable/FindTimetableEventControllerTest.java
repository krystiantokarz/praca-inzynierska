package pl.krystiantokarz.virtualdeanery.controller.employee.timetable;

import org.hamcrest.Matchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.employee.user.FindUserController;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.FindEventService;
import pl.krystiantokarz.virtualdeanery.service.user.FindUserService;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.*;

@WebAppConfiguration
public class FindTimetableEventControllerTest {

    private static final String TOPIC = "TOPIC";
    private static final String MESSAGE = "MESSAGE";

    @Mock
    private User user;

    @Mock
    private FindEventService findEventService;

    @InjectMocks
    private FindTimetableEventController findTimetableEventController;

    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(findTimetableEventController).build();
    }

    @Test
    public void shouldFindTimetableEventById() throws Exception{
        //given
        Long userId = 100L;
        Long eventId = 100L;
        TimetableEvent timetableEvent = prepareTimetableEvent();

        //when
        when(findEventService.findTimetableEventById(eventId)).thenReturn(timetableEvent);
        when(user.getId()).thenReturn(userId);
        mockMvc.perform(get(EmployeePath.timetable + "/event/user/" + eventId)
                .param("eventId", eventId.toString())
                .sessionAttr("logged-user-id", userId))
                .andExpect(jsonPath("$.userId", Matchers.equalTo(100)))
                .andExpect(jsonPath("$.message", Matchers.is(MESSAGE)))
                .andExpect(jsonPath("$.topic", Matchers.is(TOPIC)))
                .andExpect(status().isOk());

        //then
        TimetableEvent result = findEventService.findTimetableEventById(eventId);
        assertThat(result).isEqualTo(timetableEvent);
    }

    private TimetableEvent prepareTimetableEvent(){
        return TimetableEvent.from(
                LocalDate.now(),
                LocalTime.now(),
                LocalTime.now(),
                TOPIC,
                MESSAGE,
                user
        );
    }

}