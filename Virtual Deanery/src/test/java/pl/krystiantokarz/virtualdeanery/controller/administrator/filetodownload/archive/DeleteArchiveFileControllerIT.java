package pl.krystiantokarz.virtualdeanery.controller.administrator.filetodownload.archive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class DeleteArchiveFileControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String ARCHIVE_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/archive/";
    private static final String ARCHIVE_FILE_NAME = "example-archive-to-remove.txt";

    @BeforeClass
    public void init(){
        CustomFileUtils.createFolderIfNotExists(ARCHIVE_REPOSITORY_PATH);
    }

    @Test
    public void shouldDeleteSelectedArchiveFileToDownload() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user,ARCHIVE_FILE_NAME);

        entityManager.persist(fileEntity);

        //when
        mockMvc.perform(delete(AdministratorPath.archiveFilePath + "/" + fileEntity.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk());

        //then
        File removedFile = new File(ARCHIVE_REPOSITORY_PATH + ARCHIVE_FILE_NAME);
        assertThat(removedFile.exists()).isFalse();

        FileToDownloadDescriptionEntity rejectFile = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(rejectFile).isNull();

        removedFile.createNewFile();
    }
}