package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.assertNull;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class RemoveGroupControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldRemoveGroup() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        User user = UserProvider.createUserWithEmployeeRole();
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(user);
        entityManager.persist(group);
        entityManager.persist(userGroup);

        //when
        mockMvc.perform(delete(EmployeePath.groupPath + "/" +group.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", user.getId()))
                .andExpect(status().isOk());


        //then
        Group groupResult = entityManager.find(Group.class, group.getId());
        List<UserGroup> userGroupResultList = entityManager.createNativeQuery("SELECT * FROM user_group ug WHERE ug.group_id = :groupId", UserGroup.class)
                .setParameter("groupId", group.getId())
                .getResultList();

        assertNull(groupResult);
        assertThat(userGroupResultList).isEmpty();
    }

    @Test
    public void shouldNotRemoveUserGroupWhenUserNotHavePrivilege() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        User user = UserProvider.createUserWithEmployeeRole();
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(user);
        entityManager.persist(group);
        entityManager.persist(userGroup);

        //when
        mockMvc.perform(delete(EmployeePath.groupPath + "/" +group.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE))
                .sessionAttr("logged-user-id", user.getId()))
                .andExpect(status().isForbidden());
    }



}