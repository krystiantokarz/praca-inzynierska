package pl.krystiantokarz.virtualdeanery.controller.administrator.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.AdministratorPath;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.out.FindUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.ADMIN_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class FindAccountControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;


    @Test
    public void shouldGetAccountPage() throws Exception{
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        FindUserDTO findUserDTO = new FindUserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getLoginData().getLogin(),
                user.getEmail(),
                user.isEnabled()
        );

        //when
        mockMvc.perform(get(AdministratorPath.accountPath)
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/account/account"))
                .andExpect(model().attribute("user", findUserDTO));
    }

    @Test
    public void shouldNotGetAccountPageWhenLoggedUserNotExist() throws Exception{
        //given
        Long notExistUser = -100L;
        //when
        mockMvc.perform(get(AdministratorPath.accountPath)
                .sessionAttr("logged-user-id", notExistUser)
                .with(user(LOGIN).password(PASSWORD).roles(ADMIN_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/administrator/error/error-page"));
    }

}