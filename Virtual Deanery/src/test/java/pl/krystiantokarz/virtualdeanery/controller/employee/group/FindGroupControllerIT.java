package pl.krystiantokarz.virtualdeanery.controller.employee.group;

import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.controller.path.EmployeePath;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.GroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.out.GroupWithAccessDTO;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.EMPLOYEE_ROLE;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.LOGIN;
import static pl.krystiantokarz.virtualdeanery.controller.SecurityConfig.PASSWORD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-controller.properties")
@AutoConfigureMockMvc
public class FindGroupControllerIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldGetGroupsBySelectedPage() throws Exception{
        //given
        Integer page = 0;

        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        GroupWithAccessDTO groupDTO = new GroupWithAccessDTO(group.getId(), group.getName(),group.getDescription(),false);

        //when
        mockMvc.perform(get(EmployeePath.groupsPath)
                .param("page",page.toString())
                .sessionAttr("logged-user-id",user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/employee/group/groups"))
                .andExpect(model().attribute("groups",new ArrayList<>(Collections.singletonList(groupDTO))))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));
    }

    @Test
    public void shouldGetGroupsForUserBySelectedPage() throws Exception{
        //given
        Integer page = 0;

        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        GroupDTO groupDTO = new GroupDTO(group.getId(), group.getName(), group.getDescription());

        //when
        mockMvc.perform(get(EmployeePath.groupsPath + "/user")
                .param("page",page.toString())
                .sessionAttr("logged-user-id",user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("/employee/group/groups-for-user"))
                .andExpect(model().attribute("groups",new ArrayList<>(Collections.singletonList(groupDTO))))
                .andExpect(model().attribute("nextPage",page+1))
                .andExpect(model().attribute("previousPage",page-1))
                .andExpect(model().attribute("blockPreviousPage",true))
                .andExpect(model().attribute("blockNextPage",true));
    }


    @Test
    public void shouldFindGroupById() throws Exception{

        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        //when
        mockMvc.perform(get(EmployeePath.groupPath + "/" + group.getId() + "/description")
                .param("groupId",group.getId().toString())
                .sessionAttr("logged-user-id",user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(group.getId().intValue())))
                .andExpect(jsonPath("$.description", Matchers.is(group.getDescription())))
                .andExpect(jsonPath("$.name", Matchers.is(group.getName())));
    }

    @Test
    public void shouldGetSelectedGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when

        mockMvc.perform(get(EmployeePath.groupPath + "/" + group.getId())
                .sessionAttr("logged-user-id", user.getId())
                .with(user(LOGIN).password(PASSWORD).roles(EMPLOYEE_ROLE)))
                .andExpect(status().isOk())
                .andExpect(view().name("employee/group/selected-group"))
                .andExpect(model().attribute("name", group.getName()))
                .andExpect(model().attribute("groupId", group.getId()))
                .andExpect(model().attribute("description", group.getDescription()))
                .andExpect(model().attribute("loggedUserId", user.getId()));
    }

}