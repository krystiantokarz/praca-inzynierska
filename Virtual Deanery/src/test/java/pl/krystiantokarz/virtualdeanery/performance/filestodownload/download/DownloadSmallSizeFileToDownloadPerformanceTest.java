package pl.krystiantokarz.virtualdeanery.performance.filestodownload.download;


public class DownloadSmallSizeFileToDownloadPerformanceTest extends BaseDownloadFileToDownloadPerformanceTest {

    private static final String fileName = "small-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
