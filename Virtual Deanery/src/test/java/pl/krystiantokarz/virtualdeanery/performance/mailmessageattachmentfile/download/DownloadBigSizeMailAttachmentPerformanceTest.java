package pl.krystiantokarz.virtualdeanery.performance.mailmessageattachmentfile.download;


public class DownloadBigSizeMailAttachmentPerformanceTest extends BaseDownloadMailPerformanceTest {

    private static final String fileName = "big-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
