package pl.krystiantokarz.virtualdeanery.performance.mailmessageattachmentfile.download;


public class DownloadSmallSizeMailAttachmentPerformanceTest extends BaseDownloadMailPerformanceTest {

    private static final String fileName = "small-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
