package pl.krystiantokarz.virtualdeanery.performance.filestodownload.upload;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;


public class UploadBigFileToDownloadPerformanceTest extends BaseUploadFileToDownloadPerformanceTest {

    private static final String fileName = "big-file";
    private static final String ORIGINAL_FILE_NAME = "big-file";
    private static final String CONTENT_TYPE = "NONE";

    @Override
    protected MultipartFile prepareMultipartFile() throws Exception{
        File file = new File(FILE_DIR + fileName);
        try (InputStream inputStream = new FileInputStream(file)) {
            return new MockMultipartFile(fileName,ORIGINAL_FILE_NAME,CONTENT_TYPE, inputStream);
        }
    }

    @Override
    protected String getFileName() throws Exception {
        return fileName;
    }
}
