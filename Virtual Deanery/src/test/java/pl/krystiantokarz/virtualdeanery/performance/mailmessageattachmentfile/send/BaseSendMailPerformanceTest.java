package pl.krystiantokarz.virtualdeanery.performance.mailmessageattachmentfile.send;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.openjdk.jmh.annotations.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.multipart.MultipartFile;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.in.CreateMessageDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateMessageDTOProvider;
import pl.krystiantokarz.virtualdeanery.service.file.SaveFileService;
import pl.krystiantokarz.virtualdeanery.service.messagebox.SendMessageService;
import pl.krystiantokarz.virtualdeanery.performance.BasePerformanceTest;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import java.util.List;

import static org.mockito.Mockito.*;

@State(Scope.Thread)
public abstract class BaseSendMailPerformanceTest extends BasePerformanceTest {

    private static final String TARGET_DIR = "target/test-classes/performance/messagebox/";

    @Mock
    private UserRepository userRepository;

    @Mock
    private MailMessageRepository mailMessageRepository;

    @Spy
    private SaveFileService saveFileService;

    @Mock
    private User user;

    @InjectMocks
    private SendMessageService sendMessageService;

    private CreateMessageDTO createMessageDTO;

    private Long testUserId = 100L;


    @Setup(Level.Invocation)
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(sendMessageService,"repositoryPath",TARGET_DIR);

        CustomFileUtils.createFolderIfNotExists(TARGET_DIR);

        createMessageDTO = CreateMessageDTOProvider.prepareCreateMessageDTOWithAttachments(prepareMultipartFile());
        createMessageDTO.setIdUserForSendMessage(testUserId);
        when(userRepository.findUserByIdIfExist(testUserId)).thenReturn(user);
    }


    @Benchmark
    public void test() throws Exception {
        sendMessageService.sendMessage(createMessageDTO,testUserId);
    }


    @TearDown
    public void tearDown() {
        verify(mailMessageRepository, times(1)).save(any(MailMessageEntity.class));
        CustomFileUtils.deleteAllFilesInFolder(TARGET_DIR);
    }


    protected abstract List<MultipartFile> prepareMultipartFile();

}
