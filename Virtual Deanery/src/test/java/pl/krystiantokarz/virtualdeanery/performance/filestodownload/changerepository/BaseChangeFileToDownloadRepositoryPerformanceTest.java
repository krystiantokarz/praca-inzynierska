package pl.krystiantokarz.virtualdeanery.performance.filestodownload.changerepository;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.openjdk.jmh.annotations.*;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.performance.BasePerformanceTest;
import pl.krystiantokarz.virtualdeanery.service.file.ChangeFileRepositoryService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.ChangeReadyToDownloadFileTypeService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.FileRepositoryPathComponent;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import java.io.File;
import java.nio.file.Files;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;


@State(Scope.Thread)
public abstract class BaseChangeFileToDownloadRepositoryPerformanceTest extends BasePerformanceTest {

    protected static final String FILE_DIR = "target/test-classes/performance/examplefile/";
    protected static final String RENAME_FILE_DIR = "target/test-classes/performance/filetodownload/rename/";

    @Spy
    private ChangeFileRepositoryService changeFileRepositoryService;

    @Mock
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Mock
    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Mock
    private FileToDownloadDescriptionEntity fileToDownloadDescription;

    @InjectMocks
    private ChangeReadyToDownloadFileTypeService changeReadyToDownloadFileTypeService;

    private Long fileId = 100L;

    private FileStatus fileStatus = FileStatus.READY_TO_DOWNLOAD;

    private FileType oldFileType = FileType.ARCHIVE;
    private FileType newFileType = FileType.FORM;



    @Setup(Level.Invocation)
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        CustomFileUtils.createFolderIfNotExists(RENAME_FILE_DIR);
        CustomFileUtils.deleteAllFilesInFolder(RENAME_FILE_DIR);

        File originalFile = new File(FILE_DIR + getSavedFileName());
        File file = new File(RENAME_FILE_DIR + getSavedFileName());
        Files.copy(originalFile.toPath(), file.toPath());


        when(fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId)).thenReturn(fileToDownloadDescription);
        when(fileToDownloadDescription.getOriginalFileName()).thenReturn(CustomFileUtils.generateTestFileName());
        when(fileToDownloadDescription.getSavedFileName()).thenReturn(getSavedFileName());
        when(fileToDownloadDescription.getFileStatus()).thenReturn(fileStatus);
        when(fileRepositoryPathComponent.getRepositoryPath(fileStatus, oldFileType)).thenReturn(RENAME_FILE_DIR);
        when(fileRepositoryPathComponent.getRepositoryPath(fileStatus, newFileType)).thenReturn(RENAME_FILE_DIR);
    }


    @Benchmark
    public void test() throws Exception {
        changeReadyToDownloadFileTypeService.changeFileFromArchiveToFormFileType(fileId);
    }


    @TearDown
    public void tearDown() throws Exception {
        verify(fileToDownloadDescriptionRepository, times(1)).save(any(FileReadyToDownload.class));
        CustomFileUtils.deleteAllFilesInFolder(RENAME_FILE_DIR);
    }


    protected abstract String getSavedFileName();

}
