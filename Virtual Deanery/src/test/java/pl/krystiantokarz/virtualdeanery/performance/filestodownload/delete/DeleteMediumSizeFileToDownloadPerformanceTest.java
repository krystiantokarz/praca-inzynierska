package pl.krystiantokarz.virtualdeanery.performance.filestodownload.delete;


public class DeleteMediumSizeFileToDownloadPerformanceTest extends BaseDeleteFileToDownloadPerformanceTest {

    private static final String fileName = "medium-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
