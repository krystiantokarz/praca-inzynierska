package pl.krystiantokarz.virtualdeanery.performance.filestodownload.delete;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.openjdk.jmh.annotations.*;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescription;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.service.file.RemoveFileService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.DeleteFileToDownloadService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.FileRepositoryPathComponent;
import pl.krystiantokarz.virtualdeanery.performance.BasePerformanceTest;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import java.io.File;
import java.nio.file.Files;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@State(Scope.Thread)
public abstract class BaseDeleteFileToDownloadPerformanceTest extends BasePerformanceTest {

    protected static final String FILE_DIR = "target/test-classes/performance/examplefile/";
    protected static final String REMOVE_FILE_DIR = "target/test-classes/performance/filetodownload/remove/";

    @Spy
    private RemoveFileService removeFileService;

    @Mock
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Mock
    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Mock
    private FileToDownloadDescription fileToDownloadDescription;

    @InjectMocks
    private DeleteFileToDownloadService deleteFileToDownloadService;

    private Long fileId = 100L;

    private File file;

    @Setup(Level.Invocation)
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        CustomFileUtils.createFolderIfNotExists(REMOVE_FILE_DIR);
        CustomFileUtils.deleteAllFilesInFolder(REMOVE_FILE_DIR);

        File originalFile = new File(FILE_DIR + getSavedFileName());
        file = new File(REMOVE_FILE_DIR + getSavedFileName());
        Files.copy(originalFile.toPath(), file.toPath());

        when(fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId)).thenReturn(fileToDownloadDescription);
        when(fileRepositoryPathComponent.getRepositoryPath(any(FileStatus.class),any(FileType.class))).thenReturn(REMOVE_FILE_DIR);
        when(fileToDownloadDescription.getSavedFileName()).thenReturn(getSavedFileName());
        when(fileToDownloadDescription.getFileStatus()).thenReturn(FileStatus.READY_TO_DOWNLOAD);
    }


    @Benchmark
    public void test() throws Exception {
        deleteFileToDownloadService.deleteFile(fileId);
    }


    @TearDown
    public void tearDown() throws Exception {
        verify(fileToDownloadDescriptionRepository, times(1)).delete(fileId);

    }


    protected abstract String getSavedFileName();

}
