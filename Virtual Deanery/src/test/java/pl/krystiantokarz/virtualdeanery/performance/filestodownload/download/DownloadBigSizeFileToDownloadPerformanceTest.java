package pl.krystiantokarz.virtualdeanery.performance.filestodownload.download;


public class DownloadBigSizeFileToDownloadPerformanceTest extends BaseDownloadFileToDownloadPerformanceTest {

    private static final String fileName = "big-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
