package pl.krystiantokarz.virtualdeanery.performance.filestodownload.download;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.openjdk.jmh.annotations.*;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescription;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.service.file.DownloadFileService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.DownloadFileToDownloadService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.FileRepositoryPathComponent;
import pl.krystiantokarz.virtualdeanery.performance.BasePerformanceTest;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static org.mockito.Mockito.*;

@State(Scope.Thread)
public abstract class BaseDownloadFileToDownloadPerformanceTest extends BasePerformanceTest {

    protected static final String TARGET_DIR = "target/test-classes/performance/filetodownload/";
    protected static final String FILE_DIR = "target/test-classes/performance/examplefile/";

    @Spy
    private DownloadFileService downloadFileService;

    @Mock
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Mock
    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Mock
    private FileToDownloadDescription fileToDownloadDescription;

    @InjectMocks
    private DownloadFileToDownloadService downloadFileToDownloadService;

    private Long fileId = 100L;

    private File file;


    @Setup(Level.Invocation)
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);

        CustomFileUtils.createFolderIfNotExists(TARGET_DIR);
        file = new File(TARGET_DIR + CustomFileUtils.generateTestFileName());
        file.createNewFile();
        file.deleteOnExit();

        when(fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId)).thenReturn(fileToDownloadDescription);
        when(fileToDownloadDescription.getSavedFileName()).thenReturn(getSavedFileName());
        when(fileRepositoryPathComponent.getRepositoryPath(any(FileStatus.class),any(FileType.class))).thenReturn(FILE_DIR);
    }


    @Benchmark
    public void test() throws Exception {
        try (OutputStream outputStream = new FileOutputStream(file)) {
            downloadFileToDownloadService.downloadFile(fileId,outputStream);
        }
    }


    @TearDown
    public void tearDown() throws Exception {
        verify(downloadFileService, times(1)).downloadFile(any(String.class),any(String.class),any(OutputStream.class));
    }


    protected abstract String getSavedFileName();

}
