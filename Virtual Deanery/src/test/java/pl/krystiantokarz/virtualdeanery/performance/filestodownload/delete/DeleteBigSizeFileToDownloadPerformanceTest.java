package pl.krystiantokarz.virtualdeanery.performance.filestodownload.delete;


public class DeleteBigSizeFileToDownloadPerformanceTest extends BaseDeleteFileToDownloadPerformanceTest {

    private static final String fileName = "big-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
