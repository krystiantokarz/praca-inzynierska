package pl.krystiantokarz.virtualdeanery.performance.mailmessageattachmentfile.download;


public class DownloadMediumSizeMailAttachmentPerformanceTest extends BaseDownloadMailPerformanceTest {

    private static final String fileName = "medium-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
