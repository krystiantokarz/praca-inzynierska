package pl.krystiantokarz.virtualdeanery.performance.filestodownload.download;


public class DownloadMediumSizeFileToDownloadPerformanceTest extends BaseDownloadFileToDownloadPerformanceTest {

    private static final String fileName = "medium-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
