package pl.krystiantokarz.virtualdeanery.performance.filestodownload.changerepository;


public class ChangeBigFileToDownloadRepositoryPerformanceTest extends BaseChangeFileToDownloadRepositoryPerformanceTest {

    private static final String fileName = "big-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
