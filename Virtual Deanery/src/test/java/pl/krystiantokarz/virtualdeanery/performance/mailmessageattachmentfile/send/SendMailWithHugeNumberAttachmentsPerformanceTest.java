package pl.krystiantokarz.virtualdeanery.performance.mailmessageattachmentfile.send;

import org.springframework.web.multipart.MultipartFile;
import pl.krystiantokarz.virtualdeanery.provider.MockMultipartFileProvider;

import java.util.List;


public class SendMailWithHugeNumberAttachmentsPerformanceTest extends BaseSendMailPerformanceTest {

    public static Integer FILE_SIZE = 100;

    @Override
    protected List<MultipartFile> prepareMultipartFile() {
        return MockMultipartFileProvider.prepareMultipartFileList(FILE_SIZE);
    }
}
