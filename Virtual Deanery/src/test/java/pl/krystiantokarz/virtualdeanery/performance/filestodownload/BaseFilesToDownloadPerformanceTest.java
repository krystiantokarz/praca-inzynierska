package pl.krystiantokarz.virtualdeanery.performance.filestodownload;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import pl.krystiantokarz.virtualdeanery.performance.BasePerformanceTest;

@State(Scope.Thread)
public abstract class BaseFilesToDownloadPerformanceTest extends BasePerformanceTest {



    @Benchmark
    public void benchmarkTest(){

    }


    public abstract String getFilePath();

    public abstract String getFileName();
}
