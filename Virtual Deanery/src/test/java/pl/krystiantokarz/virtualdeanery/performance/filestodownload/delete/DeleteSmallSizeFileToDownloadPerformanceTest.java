package pl.krystiantokarz.virtualdeanery.performance.filestodownload.delete;


public class DeleteSmallSizeFileToDownloadPerformanceTest extends BaseDeleteFileToDownloadPerformanceTest {

    private static final String fileName = "small-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
