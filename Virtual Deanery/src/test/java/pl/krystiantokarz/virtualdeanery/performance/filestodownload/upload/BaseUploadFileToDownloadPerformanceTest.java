package pl.krystiantokarz.virtualdeanery.performance.filestodownload.upload;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.openjdk.jmh.annotations.*;
import org.springframework.web.multipart.MultipartFile;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.service.file.SaveFileService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.FileRepositoryPathComponent;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.UploadFileToDownloadService;
import pl.krystiantokarz.virtualdeanery.performance.BasePerformanceTest;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

@State(Scope.Thread)
public abstract class BaseUploadFileToDownloadPerformanceTest extends BasePerformanceTest {

    protected static final String TARGET_DIR = "target/test-classes/performance/filetodownload/";
    protected static final String FILE_DIR = "target/test-classes/performance/examplefile/";

    @Mock
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Mock
    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Spy
    private SaveFileService saveFileService;

    @Mock
    private User user;

    @InjectMocks
    private UploadFileToDownloadService uploadFileToDownloadService;

    private MultipartFile file;

    @Setup(Level.Invocation)
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);

        CustomFileUtils.createFolderIfNotExists(TARGET_DIR);

        file = prepareMultipartFile();
        when(fileRepositoryPathComponent.getRepositoryPath(any(FileStatus.class),any(FileType.class))).thenReturn(TARGET_DIR);
    }


    @Benchmark
    public void test() throws Exception {
        uploadFileToDownloadService.uploadFileToDownload(file,user,FileStatus.READY_TO_DOWNLOAD,FileType.ARCHIVE);
    }


    @TearDown
    public void tearDown() {
        verify(fileToDownloadDescriptionRepository, times(1)).save(any(FileToDownloadDescriptionEntity.class));
        CustomFileUtils.deleteAllFilesInFolder(TARGET_DIR);
    }


    protected abstract MultipartFile prepareMultipartFile() throws Exception;

    protected abstract String getFileName() throws Exception;

}
