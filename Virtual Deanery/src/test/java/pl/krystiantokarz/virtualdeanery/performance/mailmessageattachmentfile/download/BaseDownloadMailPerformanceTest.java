package pl.krystiantokarz.virtualdeanery.performance.mailmessageattachmentfile.download;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.openjdk.jmh.annotations.*;
import org.springframework.test.util.ReflectionTestUtils;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.file.DownloadFileService;
import pl.krystiantokarz.virtualdeanery.service.messagebox.DownloadMessageAttachmentFileService;
import pl.krystiantokarz.virtualdeanery.performance.BasePerformanceTest;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static org.mockito.Mockito.*;

@State(Scope.Thread)
public abstract class BaseDownloadMailPerformanceTest extends BasePerformanceTest {

    private static final String TARGET_DIR = "target/test-classes/performance/messagebox/";
    private static final String FILE_DIR = "target/test-classes/performance/examplefile/";

    @Spy
    private DownloadFileService downloadFileService;

    @Mock
    private MailMessageRepository mailMessageRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private User user;

    @Mock
    private MailAttachmentFileDescription mailAttachmentFileDescription;

    @InjectMocks
    private DownloadMessageAttachmentFileService downloadMessageAttachmentFileService;


    private Long testUserId = 100L;

    private Long testAttachmentFileId = 100L;

    private File file;


    @Setup(Level.Invocation)
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(downloadMessageAttachmentFileService,"repositoryPath",FILE_DIR);

        CustomFileUtils.createFolderIfNotExists(TARGET_DIR);
        file = new File(TARGET_DIR + CustomFileUtils.generateTestFileName());
        file.createNewFile();
        file.deleteOnExit();

        when(userRepository.findUserByIdIfExist(testUserId)).thenReturn(user);
        when(mailMessageRepository.findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(testAttachmentFileId,user)).thenReturn(mailAttachmentFileDescription);
        when(mailAttachmentFileDescription.getSavedFileName()).thenReturn(getSavedFileName());
    }


    @Benchmark
    public void test() throws Exception {
        try (OutputStream outputStream = new FileOutputStream(file)) {
            downloadMessageAttachmentFileService.downloadFile(testAttachmentFileId, testUserId, outputStream);
        }
    }


    @TearDown
    public void tearDown() throws Exception {
        verify(downloadFileService, times(1)).downloadFile(any(String.class),any(String.class),any(OutputStream.class));
    }


    protected abstract String getSavedFileName();

}
