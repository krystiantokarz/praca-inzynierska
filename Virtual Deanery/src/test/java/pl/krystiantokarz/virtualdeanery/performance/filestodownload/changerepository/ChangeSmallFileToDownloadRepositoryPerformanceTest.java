package pl.krystiantokarz.virtualdeanery.performance.filestodownload.changerepository;


public class ChangeSmallFileToDownloadRepositoryPerformanceTest extends BaseChangeFileToDownloadRepositoryPerformanceTest {

    private static final String fileName = "small-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
