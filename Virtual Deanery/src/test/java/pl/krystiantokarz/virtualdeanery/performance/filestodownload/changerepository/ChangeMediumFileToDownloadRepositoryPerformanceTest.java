package pl.krystiantokarz.virtualdeanery.performance.filestodownload.changerepository;


public class ChangeMediumFileToDownloadRepositoryPerformanceTest extends BaseChangeFileToDownloadRepositoryPerformanceTest {

    private static final String fileName = "medium-file";

    @Override
    protected String getSavedFileName() {
        return fileName;
    }
}
