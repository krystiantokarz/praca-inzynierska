package pl.krystiantokarz.virtualdeanery.service.user;

import org.mockito.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedLoginExistException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class EditUserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Captor
    private ArgumentCaptor<User> userCaptor;

    @InjectMocks
    private EditUserService editUserService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldEditSelectedUser() throws Exception{
        //given
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        Long userId = 100L;
        User user = UserProvider.createUserWithAllRoles();

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(userRepository.checkUserWithSelectedEmailExistWithoutSelectedUser(editUserDTO.getEmail(),userId)).thenReturn(false);
        when(userRepository.checkUserWithSelectedLoginExistWithoutSelectedUser(editUserDTO.getLogin(),userId)).thenReturn(false);

        editUserService.editSelectedUser(userId, editUserDTO);

        //then
        verify(userRepository, times(1)).save(userCaptor.capture());

        assertThat(userCaptor.getValue()).isNotNull();
        assertThat(userCaptor.getValue().getFirstName()).isEqualTo(editUserDTO.getFirstName());
        assertThat(userCaptor.getValue().getLastName()).isEqualTo(editUserDTO.getLastName());
        assertThat(userCaptor.getValue().getEmail()).isEqualTo(editUserDTO.getEmail());
        assertThat(userCaptor.getValue().getLoginData().getLogin()).isEqualTo(editUserDTO.getLogin());

    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowException_whenUserNotExist() throws Exception{
        //given
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        Long userId = 1L;

        //when
        doThrow(UserNotExistException.class).when(userRepository).findUserByIdIfExist(userId);

        editUserService.editSelectedUser(userId, editUserDTO);
    }

    @Test(expectedExceptions = UserWithSelectedEmailExistException.class)
    public void shouldThrowException_whenUserWithSelectedEmailExist() throws Exception{
        //given
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        Long userId = 100L;
        User user = UserProvider.createUserWithAllRoles();

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(userRepository.checkUserWithSelectedEmailExistWithoutSelectedUser(editUserDTO.getEmail(),userId)).thenReturn(true);

        editUserService.editSelectedUser(userId, editUserDTO);
    }

    @Test(expectedExceptions = UserWithSelectedLoginExistException.class)
    public void shouldThrowException_whenUserWithSelectedLoginExist() throws Exception{
        //given
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        Long userId = 100L;
        User user = UserProvider.createUserWithAllRoles();

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(userRepository.checkUserWithSelectedLoginExistWithoutSelectedUser(editUserDTO.getLogin(),userId)).thenReturn(true);

        editUserService.editSelectedUser(userId, editUserDTO);
    }
}