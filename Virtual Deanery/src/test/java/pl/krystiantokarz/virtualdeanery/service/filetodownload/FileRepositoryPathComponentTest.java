package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.testng.Assert.*;

public class FileRepositoryPathComponentTest {

    private static final String ARCHIVE_REPOSITORY_PATH = "ARCHIVE-REPOSITORY-PATH";
    private static final String FORM_REPOSITORY_PATH = "FORM-REPOSITORY-PATH";
    private static final String TEMPORARY_REPOSITORY_PATH = "TEMPORARY-REPOSITORY-PATH";

    @InjectMocks
    private FileRepositoryPathComponent fileRepositoryPathComponent;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(fileRepositoryPathComponent, "archiveRepositoryPath", ARCHIVE_REPOSITORY_PATH);
        ReflectionTestUtils.setField(fileRepositoryPathComponent, "formRepositoryPath", FORM_REPOSITORY_PATH);
        ReflectionTestUtils.setField(fileRepositoryPathComponent, "tmpRepositoryPath", TEMPORARY_REPOSITORY_PATH);
    }

    @Test
    public void shouldGetArchiveRepositoryPath() throws Exception{

        //given
        FileStatus fileStatus = FileStatus.READY_TO_DOWNLOAD;
        FileType fileType = FileType.ARCHIVE;

        //when
        String repositoryPath = fileRepositoryPathComponent.getRepositoryPath(fileStatus, fileType);

        //then
        assertNotNull(repositoryPath);
        assertThat(repositoryPath).isEqualTo(ARCHIVE_REPOSITORY_PATH);
    }

    @Test
    public void shouldGetFormRepositoryPath() throws Exception{

        //given
        FileStatus fileStatus = FileStatus.READY_TO_DOWNLOAD;
        FileType fileType = FileType.FORM;

        //when
        String repositoryPath = fileRepositoryPathComponent.getRepositoryPath(fileStatus, fileType);

        //then
        assertNotNull(repositoryPath);
        assertThat(repositoryPath).isEqualTo(FORM_REPOSITORY_PATH);
    }

    @Test
    public void shouldGetTemporaryRepositoryPath() throws Exception{

        //given
        FileStatus fileStatus = FileStatus.WAITING_FOR_ACCEPTED;
        FileType fileType = FileType.FORM;

        //when
        String repositoryPath = fileRepositoryPathComponent.getRepositoryPath(fileStatus, fileType);

        //then
        assertNotNull(repositoryPath);
        assertThat(repositoryPath).isEqualTo(TEMPORARY_REPOSITORY_PATH);
    }


}