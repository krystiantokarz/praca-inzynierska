package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.mockito.*;
import org.springframework.mock.web.MockMultipartFile;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.file.SaveFileService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileIsEmptyException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

public class UploadFileToDownloadServiceTest {

    private static final String REPOSITORY_PATH = "TEST_REPOSITORY_PATH";
    private static final String ARCHIVE_FILE_PREFIX = "archive_";
    private static final String SAVED_FILE_NAME = "TEST_SAVED_FILE_NAME";

    private static final String MULTIPART_FILE_NAME = "TEST-MULTIPART-FILE-NAME";
    private static final String MULTIPART_ORIGINAL_FILE_NAME = "TEST-MULTIPART-FILE-NAME";

    @Mock
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Mock
    private SaveFileService saveFileService;

    @Mock
    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Captor
    private ArgumentCaptor<FileToDownloadDescriptionEntity> fileToDownloadDescriptionEntityCaptor;

    @InjectMocks
    private UploadFileToDownloadService uploadFileToDownloadService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void shouldUploadFileToDownload() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        MockMultipartFile mockMultipartFile = new MockMultipartFile(MULTIPART_FILE_NAME, MULTIPART_ORIGINAL_FILE_NAME, "txt",new byte[]{1});


        FileType fileType = FileType.ARCHIVE;
        FileStatus fileStatus = FileStatus.READY_TO_DOWNLOAD;

        //when
        when(fileRepositoryPathComponent.getRepositoryPath(fileStatus, fileType)).thenReturn(REPOSITORY_PATH);
        when(saveFileService.saveFile(REPOSITORY_PATH,ARCHIVE_FILE_PREFIX, mockMultipartFile)).thenReturn(SAVED_FILE_NAME);
        when(fileToDownloadDescriptionRepository.save(fileToDownloadDescriptionEntityCaptor.capture())).thenReturn(null);

        uploadFileToDownloadService.uploadFileToDownload(mockMultipartFile, user, fileStatus, fileType);

        //then
        assertThat(fileToDownloadDescriptionEntityCaptor.getValue()).isNotNull();
        assertThat(fileToDownloadDescriptionEntityCaptor.getValue().getSavedFileName()).isEqualTo(SAVED_FILE_NAME);
        assertThat(fileToDownloadDescriptionEntityCaptor.getValue().getAddingUser()).isEqualTo(user);
        assertThat(fileToDownloadDescriptionEntityCaptor.getValue().getOriginalFileName()).isEqualTo(MULTIPART_ORIGINAL_FILE_NAME);
        assertThat(fileToDownloadDescriptionEntityCaptor.getValue().getFileStatus()).isEqualTo(fileStatus);
        assertThat(fileToDownloadDescriptionEntityCaptor.getValue().getFileType()).isEqualTo(fileType);

    }

    @Test(expectedExceptions = FileIsEmptyException.class)
    public void shouldThrowException_whenFileIsEmpty() throws Exception{
        //given

        MockMultipartFile mockMultipartFile = new MockMultipartFile(MULTIPART_FILE_NAME, MULTIPART_ORIGINAL_FILE_NAME, "txt",new byte[]{});
        User user = UserProvider.createUserWithAllRoles();

        FileType fileType = FileType.ARCHIVE;
        FileStatus fileStatus = FileStatus.READY_TO_DOWNLOAD;

        //when
        uploadFileToDownloadService.uploadFileToDownload(mockMultipartFile, user, fileStatus, fileType);
    }

    @Test(expectedExceptions = FileWithSelectedNameExistException.class)
    public void shouldThrowException_whenFileNameIsNotUnique() throws Exception{
        //given

        MockMultipartFile mockMultipartFile = new MockMultipartFile(MULTIPART_FILE_NAME, MULTIPART_ORIGINAL_FILE_NAME, "txt",new byte[]{1});

        User user = UserProvider.createUserWithAllRoles();

        FileType fileType = FileType.ARCHIVE;
        FileStatus fileStatus = FileStatus.READY_TO_DOWNLOAD;

        //when
        when(fileToDownloadDescriptionRepository.checkFileToDownloadWithSelectedOriginalNameAndTypeAndStatusExist(MULTIPART_FILE_NAME, fileType,fileStatus)).thenReturn(true);
        uploadFileToDownloadService.uploadFileToDownload(mockMultipartFile, user, fileStatus, fileType);
    }
}