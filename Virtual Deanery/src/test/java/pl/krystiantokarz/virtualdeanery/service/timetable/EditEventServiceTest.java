package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.mockito.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.EditEventDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.TimetableEventRepository;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditEventDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.EventTimeLimitException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventTimeException;

import java.time.LocalTime;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;

public class EditEventServiceTest {

    @Mock
    private TimetableEventRepository timetableEventRepository;

    @Mock
    private PrivilegeService privilegeService;

    @Mock
    private User user;

    @Captor
    private ArgumentCaptor<TimetableEvent> timetableEventArgumentCaptor;

    @InjectMocks
    private EditEventService editEventService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldEditSelectedEventByUser() throws Exception{

        //given
        Long eventId = 100L;
        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(eventId);

        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);

        Long userId = 100L;

        //when
        when(user.getId()).thenReturn(userId);
        when(timetableEventRepository.findTimetableEventByIdIfExist(eventId)).thenReturn(timetableEvent);
        editEventService.editSelectedEventByUser(editEventDTO,userId);

        //then
        verify(timetableEventRepository,times(1)).save(timetableEventArgumentCaptor.capture());
        assertThat(timetableEventArgumentCaptor.getValue()).isNotNull();
        assertThat(timetableEventArgumentCaptor.getValue().getMessage()).isEqualTo(editEventDTO.getMessage());
        assertThat(timetableEventArgumentCaptor.getValue().getTopic()).isEqualTo(editEventDTO.getTopic());
        assertThat(timetableEventArgumentCaptor.getValue().getTimeFrom()).isEqualTo(editEventDTO.getTimeFrom());
        assertThat(timetableEventArgumentCaptor.getValue().getTimeTo()).isEqualTo(editEventDTO.getTimeTo());
    }


    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowPrivilegeExceptionWhenUserDoesNotHaveAccessToEditSelectedEvent() throws Exception{
        //given
        Long eventId = 100L;
        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(eventId);

        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);

        Long userId = 100L;

        //when
        when(user.getId()).thenReturn(userId);
        when(timetableEventRepository.findTimetableEventByIdIfExist(eventId)).thenReturn(timetableEvent);
        doThrow(PrivilegeException.class).when(privilegeService).checkUserHasAccessToEditEvent(timetableEvent, userId);
        editEventService.editSelectedEventByUser(editEventDTO,userId);
    }

    @Test(expectedExceptions = EventTimeLimitException.class)
    public void shouldThrowExceptionWhenEventStartTimeIsBeforeCorrectTimes() throws Exception{
        //given
        Long eventId = 100L;
        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(eventId);
        editEventDTO.setTimeFrom(LocalTime.of(1,0,0));

        Long userId = 100L;

        //when
        editEventService.editSelectedEventByUser(editEventDTO,userId);
    }

    @Test(expectedExceptions = EventTimeLimitException.class)
    public void shouldThrowExceptionWhenEventEndTimeIsAfterCorrectTimes() throws Exception{
        //given
        Long eventId = 100L;
        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(eventId);
        editEventDTO.setTimeTo(LocalTime.of(23,0,0));

        Long userId = 100L;

        //when
        editEventService.editSelectedEventByUser(editEventDTO,userId);
    }

    @Test(expectedExceptions = ValidEventTimeException.class)
    public void shouldThrowExceptionWhenStartEventTimeIsAfterEndEventTime() throws Exception{
        //given
        Long eventId = 100L;
        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(eventId);
        editEventDTO.setTimeFrom(LocalTime.of(11,0,0));
        editEventDTO.setTimeTo(LocalTime.of(10,0,0));

        Long userId = 100L;

        //when
        editEventService.editSelectedEventByUser(editEventDTO,userId);
    }
}