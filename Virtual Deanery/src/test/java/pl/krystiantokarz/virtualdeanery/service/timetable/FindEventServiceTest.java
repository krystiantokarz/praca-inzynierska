package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.TimetableEventRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

public class FindEventServiceTest {


    @Mock
    private TimetableEventRepository timetableRepository;

    @Mock
    private User user;

    @InjectMocks
    private FindEventService findEventService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldFindTimetableEventById() throws Exception{
        Long eventId = 100L;
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        when(timetableRepository.findTimetableEventByIdIfExist(eventId)).thenReturn(timetableEvent);

        TimetableEvent result = findEventService.findTimetableEventById(eventId);

        assertThat(result).isEqualTo(timetableEvent);
    }




}