package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.TimetableEventNotExistException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class FindEventServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private FindEventService findEventService;


    @Test
    public void shouldFindTimetableEventById() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        //given
        TimetableEvent result = findEventService.findTimetableEventById(timetableEvent.getId());

        //then
        assertThat(result).isEqualTo(timetableEvent);
    }

    @Test
    public void shouldFindAllTimetableEvents() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);
        //when
        List<TimetableEvent> result = findEventService.findAllTimetableEvents();
        //then
        assertThat(result).contains(timetableEvent);
    }

    @Test
    public void shouldFAllTimetableEventsForSelectedUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);
        //when
        List<TimetableEvent> result = findEventService.findAllTimetableEventsForSelectedUser(user.getId());
        //then
        assertThat(result).contains(timetableEvent);
    }
    @Test
    public void shouldFindAllTimetableEventsByEventDate() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);
        //when
        List<TimetableEvent> result = findEventService.findAllTimetableEventsByEventDate(
                timetableEvent.getEventDate().getDayOfMonth(),
                timetableEvent.getEventDate().getMonthValue(),
                timetableEvent.getEventDate().getYear()
                );
        //then
        assertThat(result).contains(timetableEvent);
    }


    @Test(expectedExceptions = TimetableEventNotExistException.class)
    public void shouldThrowTimetableEventNotExistExceptionWhenTimetableNotExist() throws Exception{
        //given
        Long notExistEventId = -100L;
        //when
        findEventService.findTimetableEventById(notExistEventId);

    }


}