package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.web.multipart.MultipartFile;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.FileStatus;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescription;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.MockMultipartFileProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileIsEmptyException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class UploadFileToDownloadServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    public static String MULTIPART_FILE_NAME = "TEST-MULTIPART-FILE-NAME";
    public static String MULTIPART_ORIGINAL_FILE_NAME = "TEST-MULTIPART-ORIGINAL-FILE-NAME";
    public static String CONTENT_TYPE = "TEST-CONTENT-TYPE";

    @Autowired
    private UploadFileToDownloadService uploadFileToDownloadService;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldUploadFileToDownload() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        MultipartFile multipartFile = MockMultipartFileProvider.prepareMultipartFile();

        //when
        uploadFileToDownloadService.uploadFileToDownload(multipartFile, user, FileStatus.WAITING_FOR_ACCEPTED, FileType.ARCHIVE);

        //then
        FileToDownloadDescription result = entityManager.createQuery("SELECT f FROM FileToDownloadDescriptionEntity f WHERE f.originalFileName = :fileName", FileToDownloadDescription.class)
                .setParameter("fileName", multipartFile.getOriginalFilename())
                .getSingleResult();

        assertThat(result).isNotNull();
        assertThat(result.getOriginalFileName()).isEqualTo(multipartFile.getOriginalFilename());

    }

    @Test(expectedExceptions = FileIsEmptyException.class)
    public void shouldThrowException_whenFileIsEmpty() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        MultipartFile mockMultipartFile = new MockMultipartFile(MULTIPART_FILE_NAME,MULTIPART_ORIGINAL_FILE_NAME,CONTENT_TYPE, new byte[]{});

        //when
        uploadFileToDownloadService.uploadFileToDownload(mockMultipartFile, user, FileStatus.WAITING_FOR_ACCEPTED, FileType.ARCHIVE);
    }

    @Test(expectedExceptions = FileWithSelectedNameExistException.class)
    public void shouldThrowException_whenFileNameIsNotUnique() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        MultipartFile mockMultipartFile = new MockMultipartFile(MULTIPART_FILE_NAME,FileToDownloadDescriptionProvider.ORIGINAL_FILE_NAME,CONTENT_TYPE, new byte[]{1});

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileEntity);

        //when
        uploadFileToDownloadService.uploadFileToDownload(mockMultipartFile, user,FileStatus.READY_TO_DOWNLOAD, FileType.ARCHIVE);
    }

}