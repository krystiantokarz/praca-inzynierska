package pl.krystiantokarz.virtualdeanery.service.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.account.exceptions.NotMatchesPasswordsException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static org.assertj.core.api.Java6Assertions.assertThat;


@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class ChangePasswordServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ChangePasswordService changePasswordService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void shouldChangePassword() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        String oldNotEncodedPassword = user.getLoginData().getPassword();
        user.changeUserPassword(passwordEncoder.encode(oldNotEncodedPassword));
        entityManager.persist(user);
        String NEW_PASSWORD = "NEW-TEST-PASSWORD";

        //when
        changePasswordService.changePassword(user.getId(), oldNotEncodedPassword, NEW_PASSWORD);

        //then
        User userInDb = entityManager.find(User.class, user.getId());
        boolean result = passwordEncoder.matches(NEW_PASSWORD, userInDb.getLoginData().getPassword());
        assertThat(result).isTrue();
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowUserNotExistException() throws Exception{
        //given
        String PASSWORD = "NEW-TEST-PASSWORD";
        Long notExistUserId = 100L;

        //when
        changePasswordService.changePassword(notExistUserId, PASSWORD, PASSWORD);
    }


    @Test(expectedExceptions = NotMatchesPasswordsException.class)
    public void shouldThrowNotMatchesPasswordsException() throws Exception{
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        String NEW_PASSWORD = "NEW-TEST-PASSWORD";
        String BAD_OLD_PASSWORD = "BAD-OLD-PASSWORD";

        changePasswordService.changePassword(user.getId(), BAD_OLD_PASSWORD, NEW_PASSWORD);
    }

}