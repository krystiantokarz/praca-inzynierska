package pl.krystiantokarz.virtualdeanery.service.user;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

public class FindUserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private FindUserService findUserService;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldFindUser() throws Exception{
        //given
        Long userId = 1L;
        User user = UserProvider.createUserWithAllRoles();

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        User result = findUserService.findUserById(userId);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getEmail()).isEqualTo(user.getEmail());
        assertThat(result.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(result.getLastName()).isEqualTo(user.getLastName());
        assertThat(result.getLoginData().getLogin()).isEqualTo(user.getLoginData().getLogin());
        assertThat(result.getLoginData().getPassword()).isEqualTo(user.getLoginData().getPassword());
    }

}