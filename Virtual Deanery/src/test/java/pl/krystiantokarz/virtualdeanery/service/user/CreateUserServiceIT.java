package pl.krystiantokarz.virtualdeanery.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.domain.user.role.RoleEnum;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.CreateUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;
import java.util.Locale;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class CreateUserServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    private String USER_FIRST_NAME_TEST = "TEST_FIRST_NAME";
    private String USER_LAST_NAME_TEST = "TEST_LAST_NAME";
    private String USER_EMAIL_TEST = "TEST_EMAIL";
    private String USER_ROLE_TEST = RoleEnum.EMPLOYEE.toString();



    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CreateUserService createUserService;


    @Test
    public void shouldCreateNewUser() throws Exception{

        //given
        List<User> resultList = entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
        int oldSizeOfUsers = resultList.size();

        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        Locale locale = Locale.getDefault();

        //when
        createUserService.createNewUser(createUserDTO,locale);

        //then
        List<User> actuallyResultList = entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
        int newSizeOfUsers = actuallyResultList.size();
        assertThat(newSizeOfUsers).isEqualTo(oldSizeOfUsers + 1);
    }


    @Test(expectedExceptions = UserWithSelectedEmailExistException.class)
    public void shouldNotCreateNewUser_whenThrowUserWithSelectedEmailExistException() throws Exception {

        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        createUserDTO.setEmail(user.getEmail());
        Locale locale = Locale.getDefault();
        //when
        createUserService.createNewUser(createUserDTO, locale);
    }

}