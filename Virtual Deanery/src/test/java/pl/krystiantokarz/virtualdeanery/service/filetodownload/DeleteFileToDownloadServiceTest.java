package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.file.RemoveFileService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.RemoveFileException;

import static org.mockito.Mockito.*;

public class DeleteFileToDownloadServiceTest {

    private static final String REPOSITORY_PATH = "TEST-REPOSITORY-PATH";

    @Mock
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Mock
    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Mock
    private RemoveFileService removeFileService;

    @InjectMocks
    private DeleteFileToDownloadService deleteFileToDownloadService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void shouldDeleteFileReadyToDownload() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        Long fileId = 1L;

        //when
        when(fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId)).thenReturn(fileEntity);
        when(fileRepositoryPathComponent.getRepositoryPath(fileEntity.getFileStatus(), fileEntity.getFileType())).thenReturn(REPOSITORY_PATH);
        deleteFileToDownloadService.deleteFile(fileId);

        //then
        verify(removeFileService, times(1)).removeFileFromRepository(REPOSITORY_PATH, fileEntity.getSavedFileName());
        verifyNoMoreInteractions(removeFileService);
        verify(fileToDownloadDescriptionRepository, times(1)).delete(fileId);
    }


    @Test(expectedExceptions = RemoveFileException.class)
    public void shouldThrowException_whenRemoveFileWaitingForAcceptableIsImpossible() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        Long fileId = 1L;

        //when
        when(fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId)).thenReturn(fileEntity);
        when(fileRepositoryPathComponent.getRepositoryPath(fileEntity.getFileStatus(), fileEntity.getFileType())).thenReturn(REPOSITORY_PATH);
        doThrow(RemoveFileException.class).when(removeFileService).removeFileFromRepository(REPOSITORY_PATH,fileEntity.getSavedFileName());
        deleteFileToDownloadService.deleteFile(fileId);
    }

}