package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.file.FileType;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;

import static org.assertj.core.api.Java6Assertions.assertThat;


@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class ChangeReadyToDownloadFileTypeServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private ChangeReadyToDownloadFileTypeService changeReadyToDownloadFileTypeService;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String ARCHIVE_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/archive/";
    private static final String ARCHIVE_FILE_NAME = "example-archive-to-remove.txt";
    private static final String FORM_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/form/";



    @Test
    public void shouldChangeArchiveFileToFormFileStatus() throws Exception {
        //given
        User user = UserProvider.createUserWithAdminRole();
        entityManager.persist(user);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user, ARCHIVE_FILE_NAME);

        entityManager.persist(fileEntity);

        //when
        changeReadyToDownloadFileTypeService.changeFileFromArchiveToFormFileType(fileEntity.getId());

        //then
        File removedFile = new File(ARCHIVE_REPOSITORY_PATH + ARCHIVE_FILE_NAME);
        assertThat(removedFile.exists()).isFalse();
        removedFile.createNewFile();

        FileToDownloadDescriptionEntity changedFileDescription = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(changedFileDescription).isNotNull();
        assertThat(changedFileDescription.getFileType()).isEqualTo(FileType.FORM);

        File changedFile = new File(FORM_REPOSITORY_PATH + ARCHIVE_FILE_NAME);
        assertThat(changedFile.exists()).isTrue();
        changedFile.delete();



    }

}