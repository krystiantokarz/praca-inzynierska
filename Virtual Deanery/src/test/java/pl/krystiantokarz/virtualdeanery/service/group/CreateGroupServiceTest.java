package pl.krystiantokarz.virtualdeanery.service.group;

import org.mockito.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.CreateGroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateGroupDTOProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupWithSelectedNameExistException;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CreateGroupServiceTest {

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private User user;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Captor
    private ArgumentCaptor<Group> groupsArgumentCaptor;

    @Captor
    private ArgumentCaptor<UserGroup> userGroupArgumentCaptor;

    @InjectMocks
    private CreateGroupService createGroupService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldCreateGroup() throws Exception{
        //given
        Long userId = 10L;
        CreateGroupDTO createGroupDTO = CreateGroupDTOProvider.prepareCreateGroupDTO();

        //when
        when(groupRepository.checkGroupWithSelectedNameExist(createGroupDTO.getName())).thenReturn(false);
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(passwordEncoder.encode(createGroupDTO.getPassword())).thenReturn(createGroupDTO.getPassword());

        createGroupService.createGroup(createGroupDTO, userId);

        //then
        verify(groupRepository, times(1)).save(groupsArgumentCaptor.capture());
        verify(userRepository, times(1)).findUserByIdIfExist(userId);
        verify(groupRepository, times(1)).saveUserGroup(userGroupArgumentCaptor.capture());
        verify(passwordEncoder, times(1)).encode(createGroupDTO.getPassword());

        assertThat(groupsArgumentCaptor.getValue().getName()).isEqualTo(createGroupDTO.getName());
        assertThat(groupsArgumentCaptor.getValue().getDescription()).isEqualTo(createGroupDTO.getDescription());
        assertThat(groupsArgumentCaptor.getValue().getPassword()).isEqualTo(createGroupDTO.getPassword());
        assertThat(userGroupArgumentCaptor.getValue().getGroup()).isEqualTo(groupsArgumentCaptor.getValue());
        assertThat(userGroupArgumentCaptor.getValue().getUser()).isEqualTo(user);
        assertThat(userGroupArgumentCaptor.getValue().getMemberType()).isEqualTo(MemberType.ADMIN_MEMBER);

    }


    @Test(expectedExceptions = GroupWithSelectedNameExistException.class)
    public void throwExceptionWhenGroupWithSelectedNameExistInSystem() throws Exception{

        Long userId = 10L;
        CreateGroupDTO createGroupDTO = CreateGroupDTOProvider.prepareCreateGroupDTO();

        when(groupRepository.checkGroupWithSelectedNameExist(createGroupDTO.getName())).thenReturn(true);

        createGroupService.createGroup(createGroupDTO,userId);
    }
}