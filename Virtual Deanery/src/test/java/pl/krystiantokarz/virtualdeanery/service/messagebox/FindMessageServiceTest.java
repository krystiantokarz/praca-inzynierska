package pl.krystiantokarz.virtualdeanery.service.messagebox;

import org.mockito.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageStatus;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class FindMessageServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private MailMessageRepository mailMessageRepository;

    @InjectMocks
    private FindMessageService findMessageService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldFindMessageByIdForSelectedUserAndChangeMessageStatusIfNotReadYet() throws Exception{

        //given
        User userFrom = UserProvider.createUserWithAllRoles();
        User userTo = UserProvider.createUserWithAllRoles();
        MailMessageEntity messageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(userFrom, userTo);

        Long userId = 100L;
        Long mailMessageId = 100L;

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(userTo);
        when(mailMessageRepository.findMailMessageByIdForSelectedRecipientIfExist(mailMessageId,userTo)).thenReturn(messageEntity);
        MailMessageEntity result = findMessageService.findMessageByIdForSelectedUserAndChangeMessageStatusIfNotReadYet(mailMessageId, userId);

        //then
        verify(mailMessageRepository, times(1)).save(any(MailMessageEntity.class));
        assertThat(result).isNotNull();
        assertThat(result.getMessageStatus()).isEqualTo(MailMessageStatus.READ);
    }
}