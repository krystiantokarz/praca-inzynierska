package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.mockito.*;
import org.springframework.context.MessageSource;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.bundle.BundleInterceptor;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.file.ChangeFileRepositoryService;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileWithSelectedNameExistException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.SendMessageService;

import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class ChangeFileStatusServiceTest {

    @Mock
    private DeleteFileToDownloadService deleteFileToDownloadService;

    @Mock
    private ChangeFileRepositoryService changeFileRepositoryService;

    @Mock
    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Mock
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Mock
    private SendMessageService sendMessageService;

    @Mock
    private BundleInterceptor bundleInterceptor;

    @Mock
    private MessageSource messageSource;

    @InjectMocks
    private ChangeFileStatusService changeFileStatusService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldRejectFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user);

        Long fileId = 1L;

        //when
        when(fileToDownloadDescriptionRepository.findFileWaitingForAcceptedByFileId(fileId)).thenReturn(fileEntity);
        changeFileStatusService.rejectFile(fileId, Locale.getDefault());

        //then
        verify(deleteFileToDownloadService, times(1)).deleteFile(fileId);
        verify(sendMessageService, times(1)).sendMessageFromSystemUser(any(String.class), any(String.class), any(Long.class));

    }

    @Test
    public void shouldAcceptFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user);

        Long fileId = 1L;

        //when
        when(fileToDownloadDescriptionRepository.findFileWaitingForAcceptedByFileId(fileId)).thenReturn(fileEntity);

        changeFileStatusService.acceptFile(fileId, Locale.getDefault());

        //then
        verify(changeFileRepositoryService, times(1)).changeFileRepository(any(String.class),any(String.class),any(String.class));
        verify(fileToDownloadDescriptionRepository, times(1)).save(any(FileReadyToDownload.class));

    }

    @Test(expectedExceptions = FileWithSelectedNameExistException.class)
    public void shouldThrowException_whenFileWithSelectedNameExist() throws Exception {
        //given
        User user = UserProvider.createUserWithAllRoles();
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);

        Long fileId = 1L;

        //when
        when(fileToDownloadDescriptionRepository.findFileWaitingForAcceptedByFileId(fileId)).thenReturn(fileEntity);
        when(fileToDownloadDescriptionRepository.checkFileToDownloadWithSelectedOriginalNameAndTypeAndStatusExist(fileEntity.getOriginalFileName(),fileEntity.getFileType(),fileEntity.getFileStatus())).thenReturn(true);
        changeFileStatusService.acceptFile(fileId,Locale.getDefault());
    }
}