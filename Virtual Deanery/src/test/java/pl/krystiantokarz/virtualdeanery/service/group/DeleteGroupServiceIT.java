package pl.krystiantokarz.virtualdeanery.service.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.testng.Assert.*;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
@Transactional
public class DeleteGroupServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private DeleteGroupService deleteGroupService;

    @Test
    public void shouldDeleteGroupWithUserGroupByGroupAdministrator() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        User user = UserProvider.createUserWithEmployeeRole();
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(user);
        entityManager.persist(group);
        entityManager.persist(userGroup);

        //when
        deleteGroupService.deleteGroupByGroupAdministrator(user.getId(),group.getId());

        //then
        Group groupResult = entityManager.find(Group.class, group.getId());
        UserGroup userGroupResult = entityManager.find(UserGroup.class, userGroup.getId());
        assertNull(groupResult);
        assertNull(userGroupResult);

        List<UserGroup> userGroupResultList = entityManager.createNativeQuery("SELECT * FROM user_group ug WHERE ug.group_id = :groupId", UserGroup.class)
                .setParameter("groupId", group.getId())
                .getResultList();
        assertThat(userGroupResultList).isEmpty();
    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowExceptionAndNotDeleteGroupWithUserGroupsBecauseUserIsNotAdmin() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        User user = UserProvider.createUserWithEmployeeRole();
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(user);
        entityManager.persist(group);
        entityManager.persist(userGroup);

        //when
        deleteGroupService.deleteGroupByGroupAdministrator(user.getId(),group.getId());

    }


    @Test
    public void shouldDeleteGroupWithUserGroupBySystemAdministrator() throws Exception {
        Group group = GroupProvider.createGroup();
        User user = UserProvider.createUserWithAdminRole();
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user, group);
        entityManager.persist(user);
        entityManager.persist(group);
        entityManager.persist(userGroup);

        deleteGroupService.deleteGroupBySystemAdministrator(group.getId());

        UserGroup resultUserGroups = entityManager.find(UserGroup.class, userGroup.getId());
        Group resultGroups = entityManager.find(Group.class, group.getId());
        User resultUser = entityManager.find(User.class, user.getId());
        assertNull(resultUserGroups);
        assertNull(resultGroups);
        assertNotNull(resultUser);

        List<UserGroup> userGroupResultList = entityManager.createNativeQuery("SELECT * FROM user_group ug WHERE ug.group_id = :groupId", UserGroup.class)
                .setParameter("groupId", group.getId())
                .getResultList();
        assertThat(userGroupResultList).isEmpty();
    }

    @Test(expectedExceptions = GroupNotExistException.class)
    public void shouldThrowExceptionWhenGroupNotExist() throws Exception{
        Long notExistGroupId = -100L;
        deleteGroupService.deleteGroupBySystemAdministrator(notExistGroupId);
    }


}