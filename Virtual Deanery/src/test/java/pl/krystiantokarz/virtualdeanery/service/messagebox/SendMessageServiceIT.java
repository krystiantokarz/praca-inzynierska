package pl.krystiantokarz.virtualdeanery.service.messagebox;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.web.multipart.MultipartFile;
import org.testng.annotations.*;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.in.CreateMessageDTO;
import pl.krystiantokarz.virtualdeanery.provider.MockMultipartFileProvider;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateMessageDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class SendMessageServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private SendMessageService sendMessageService;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String FILE_PATH = "target/test-classes/repository/fileattachment/";


    @Test()
    public void shouldSendMessage() throws Exception {
        //given
        User userFrom = UserProvider.createUserWithAllRoles();
        User userTo = UserProvider.createUserWithAllRoles();

        entityManager.persist(userFrom);
        entityManager.persist(userTo);

        List<MultipartFile> multipartFiles = MockMultipartFileProvider.prepareMultipartFileList(1);
        CreateMessageDTO createMessageDTO = CreateMessageDTOProvider.prepareCreateMessageDTOWithoutAttachments();
        createMessageDTO.setFiles(multipartFiles);
        createMessageDTO.setIdUserForSendMessage(userTo.getId());

        //when
        int oldMessageSize = entityManager.createQuery("SELECT msg FROM MailMessageEntity msg", MailMessageEntity.class).getResultList().size();
        sendMessageService.sendMessage(createMessageDTO, userFrom.getId());

        //then
        int newMessageSize = entityManager.createQuery("SELECT msg FROM MailMessageEntity msg", MailMessageEntity.class).getResultList().size();
        assertThat(newMessageSize).isEqualTo(oldMessageSize + 1);

        MailMessageEntity result = entityManager.createQuery("SELECT msg FROM MailMessageEntity msg WHERE msg.sender = :userFrom", MailMessageEntity.class)
                .setParameter("userFrom", userFrom)
                .getSingleResult();

        assertThat(result).isNotNull();
        assertThat(result.getAttachments()).isNotEmpty();
        assertThat(result.getAttachments().size()).isEqualTo(1);
        assertThat(result.getAttachments().get(0).getSavedFileName()).isNotEmpty();

        String savedFileName = result.getAttachments().get(0).getSavedFileName();

        File file = new File(FILE_PATH + savedFileName);
        file.deleteOnExit();
        assertThat(file.exists()).isTrue();
    }
}