package pl.krystiantokarz.virtualdeanery.service.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.DownloadFileException;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class DownloadFileServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private DownloadFileService downloadFileService;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String PATH_TO_SAVE_DOWNLOADED_FILE = "target/test-classes/download/file/";
    private static final String EXAMPLE_FILENAME = "example.txt";
    private static final String PATH_TO_DOWNLOAD_FILE = "target/test-classes/repository/file/";
    private static final String DOWNLOAD_FILE_NAME = "file-to-download.txt";


    @BeforeClass
    public void init(){
        CustomFileUtils.createFolderIfNotExists(PATH_TO_DOWNLOAD_FILE);
        CustomFileUtils.createFolderIfNotExists(PATH_TO_SAVE_DOWNLOADED_FILE);
    }

    @Test
    public void shouldDownloadFile() throws Exception{

        //given
        File file = new File(PATH_TO_SAVE_DOWNLOADED_FILE + EXAMPLE_FILENAME);
        file.createNewFile();
        file.deleteOnExit();
        OutputStream outputStream = new FileOutputStream(file);

        //when
        downloadFileService.downloadFile(PATH_TO_DOWNLOAD_FILE, DOWNLOAD_FILE_NAME, outputStream);

        //then
        assertThat(file).isNotNull();
        assertThat(file.length()).isGreaterThan(0);


    }

    @Test(expectedExceptions = DownloadFileException.class)
    public void shouldThrowDownloadFileException_whenRepositoryPathNotExist() throws Exception{
        //given
        File file = new File(PATH_TO_SAVE_DOWNLOADED_FILE + EXAMPLE_FILENAME);
        file.createNewFile();
        file.deleteOnExit();
        OutputStream outputStream = new FileOutputStream(file);
        String notExistRepositoryPath = "BAD_PATH";

        //when
        downloadFileService.downloadFile(notExistRepositoryPath, DOWNLOAD_FILE_NAME, outputStream);
    }


    @Test(expectedExceptions = DownloadFileException.class)
    public void shouldThrowDownloadFileException_whenFileNameNotExist() throws Exception{
        //given
        File file = new File(PATH_TO_SAVE_DOWNLOADED_FILE + EXAMPLE_FILENAME);
        file.createNewFile();
        file.deleteOnExit();
        OutputStream outputStream = new FileOutputStream(file);

        String notExistFileName = "BAD_NAME";

        //when
        downloadFileService.downloadFile(PATH_TO_DOWNLOAD_FILE, notExistFileName, outputStream);
    }

    @Test(expectedExceptions = DownloadFileException.class)
    public void shouldThrowDownloadFileException_whenOutputStreamIsNull() throws Exception{
        //when
        downloadFileService.downloadFile(PATH_TO_DOWNLOAD_FILE, DOWNLOAD_FILE_NAME, null);
    }


}