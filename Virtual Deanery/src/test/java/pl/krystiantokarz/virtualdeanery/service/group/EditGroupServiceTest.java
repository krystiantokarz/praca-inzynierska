package pl.krystiantokarz.virtualdeanery.service.group;

import org.mockito.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupPasswordDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditGroupDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditGroupPasswordDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupWithSelectedNameExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.NotMatchesPasswordsException;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;

public class EditGroupServiceTest {

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserGroup userGroup;

    @Mock
    private User user;

    @Mock
    private Group group;

    @Captor
    private ArgumentCaptor<Group> groupsArgumentCaptor;

    @InjectMocks
    private EditGroupService editGroupService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldEditGroup() throws Exception{
        //given
        EditGroupDTO editGroupDTO = EditGroupDTOProvider.prepareEditGroupDTO();
        Long userId = 100L;
        Long groupId = 100L;
        Group group = GroupProvider.createGroup();

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(groupRepository.findUserGroupForAdminMember(user, group)).thenReturn(userGroup);
        when(groupRepository.checkGroupWithSelectedNameExistWithoutSelectedGroup(editGroupDTO.getName(),groupId)).thenReturn(false);

        editGroupService.editGroupData(groupId, userId, editGroupDTO);

        //then
        verify(groupRepository, times(1)).save(groupsArgumentCaptor.capture());
        assertThat(groupsArgumentCaptor.getValue().getDescription()).isEqualTo(editGroupDTO.getDescription());
        assertThat(groupsArgumentCaptor.getValue().getName()).isEqualTo(editGroupDTO.getName());
    }

    @Test(expectedExceptions = GroupWithSelectedNameExistException.class)
    public void shouldThrowExceptionWhenNewGroupNameIsNotUnique() throws Exception{
        //given
        EditGroupDTO editGroupDTO = EditGroupDTOProvider.prepareEditGroupDTO();
        Long userId = 100L;
        Long groupId = 100L;

        //when
        when(groupRepository.checkGroupWithSelectedNameExistWithoutSelectedGroup(editGroupDTO.getName(),groupId)).thenReturn(true);
        editGroupService.editGroupData(groupId, userId, editGroupDTO);
    }

    @Test
    public void shouldEditGroupPassword() throws Exception{
        //given
        String OLD_PASSWORD = "OLD-PASSWORD";
        String NEW_PASSWORD = "NEW-PASSWORD";
        EditGroupPasswordDTO editGroupPasswordDTO = EditGroupPasswordDTOProvider.prepareEditGroupPasswordDTO(OLD_PASSWORD);
        Long userId = 10L;
        Long groupId = 10L;
        Group group = GroupProvider.createGroup();
        group.changeGroupPassword(OLD_PASSWORD);

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(passwordEncoder.matches(OLD_PASSWORD,OLD_PASSWORD)).thenReturn(true);
        when(passwordEncoder.encode(editGroupPasswordDTO.getNewPassword())).thenReturn(NEW_PASSWORD);
        editGroupService.editGroupPassword(groupId,userId,editGroupPasswordDTO);
        //then
        verify(groupRepository, times(1)).save(groupsArgumentCaptor.capture());
        assertThat(groupsArgumentCaptor.getValue().getPassword()).isEqualTo(NEW_PASSWORD);

    }

    @Test(expectedExceptions = NotMatchesPasswordsException.class)
    public void shouldThrowExceptionWhenOldPasswordIsNotMatches() throws Exception{
        //given
        String OLD_PASSWORD = "OLD-PASSWORD";
        EditGroupPasswordDTO editGroupPasswordDTO = EditGroupPasswordDTOProvider.prepareEditGroupPasswordDTO(OLD_PASSWORD);
        Long userId = 10L;
        Long groupId = 10L;

        //when
        when(group.getPassword()).thenReturn(OLD_PASSWORD);
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(passwordEncoder.matches(OLD_PASSWORD,OLD_PASSWORD)).thenReturn(false);
        editGroupService.editGroupPassword(groupId,userId,editGroupPasswordDTO);

    }
}