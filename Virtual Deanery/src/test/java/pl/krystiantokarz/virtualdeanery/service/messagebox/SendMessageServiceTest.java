package pl.krystiantokarz.virtualdeanery.service.messagebox;

import org.mockito.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageStatus;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.message.in.CreateMessageDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.MockMultipartFileProvider;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateMessageDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.file.SaveFileService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.SaveFileException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.SendMessageException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class SendMessageServiceTest {

    private static final String TEST_REPOSITORY_PATH = "TEST-REPOSITORY-PATH";
    private static final String TEST_APPLICATION_EMAIL = "TEST-APP-EMAIL";
    private static final String FILE_PREFIX = "attachment_";

    @Mock
    private UserRepository userRepository;

    @Mock
    private MailMessageRepository mailMessageRepository;

    @Mock
    private SaveFileService saveFileService;

    @Captor
    private ArgumentCaptor<MailMessageEntity> messageEntityCaptor;

    @InjectMocks
    private SendMessageService sendMessageService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(sendMessageService,"repositoryPath",TEST_REPOSITORY_PATH);
        ReflectionTestUtils.setField(sendMessageService,"applicationEmail",TEST_APPLICATION_EMAIL);
    }


    @Test
    public void shouldSendMessageWithoutAttachments() throws Exception {
        //given
        CreateMessageDTO createMessageDTO = CreateMessageDTOProvider.prepareCreateMessageDTOWithoutAttachments();

        User userFrom = UserProvider.createUserWithAllRoles();
        User userTo = UserProvider.createUserWithAllRoles();

        Long userFromId = 2L;

        //when
        when(userRepository.findUserByIdIfExist(createMessageDTO.getIdUserForSendMessage())).thenReturn(userTo);
        when(userRepository.findUserByIdIfExist(userFromId)).thenReturn(userFrom);

        sendMessageService.sendMessage(createMessageDTO,userFromId);

        //then
        verify(mailMessageRepository, times(1)).save(messageEntityCaptor.capture());
        verifyZeroInteractions(saveFileService);
        verify(mailMessageRepository, times(1)).save(any(MailMessageEntity.class));
        assertThat(messageEntityCaptor.getValue()).isNotNull();
        assertThat(messageEntityCaptor.getValue().getMessageStatus()).isEqualTo(MailMessageStatus.DELIVERED);
        assertThat(messageEntityCaptor.getValue().getMessage()).isEqualTo(createMessageDTO.getMessage());
        assertThat(messageEntityCaptor.getValue().getSubject()).isEqualTo(createMessageDTO.getSubject());
        assertThat(messageEntityCaptor.getValue().getRecipient()).isEqualTo(userTo);
        assertThat(messageEntityCaptor.getValue().getSender()).isEqualTo(userFrom);
        assertThat(messageEntityCaptor.getValue().getAttachments()).isNull();

    }
    @Test
    public void shouldSendMessageWithAttachments() throws Exception {
        //given
        List<MultipartFile> multipartFiles = MockMultipartFileProvider.prepareMultipartFileList(1);
        CreateMessageDTO createMessageDTO = CreateMessageDTOProvider.prepareCreateMessageDTOWithoutAttachments();
        createMessageDTO.setFiles(multipartFiles);

        User userFrom = UserProvider.createUserWithAllRoles();
        User userTo = UserProvider.createUserWithAllRoles();

        Long userFromId = 2L;

        //when
        when(userRepository.findUserByIdIfExist(createMessageDTO.getIdUserForSendMessage())).thenReturn(userTo);
        when(userRepository.findUserByIdIfExist(userFromId)).thenReturn(userFrom);
        when(saveFileService.saveFile(TEST_REPOSITORY_PATH,FILE_PREFIX, multipartFiles.get(0))).thenReturn("SAVED-NAME");

        sendMessageService.sendMessage(createMessageDTO,userFromId);

        //then
        verify(mailMessageRepository, times(1)).save(messageEntityCaptor.capture());
        verify(saveFileService, times(1)).saveFile(TEST_REPOSITORY_PATH, FILE_PREFIX, multipartFiles.get(0));
        verify(mailMessageRepository, times(1)).save(any(MailMessageEntity.class));
        assertThat(messageEntityCaptor.getValue()).isNotNull();
        assertThat(messageEntityCaptor.getValue().getMessageStatus()).isEqualTo(MailMessageStatus.DELIVERED);
        assertThat(messageEntityCaptor.getValue().getMessage()).isEqualTo(createMessageDTO.getMessage());
        assertThat(messageEntityCaptor.getValue().getSubject()).isEqualTo(createMessageDTO.getSubject());
        assertThat(messageEntityCaptor.getValue().getRecipient()).isEqualTo(userTo);
        assertThat(messageEntityCaptor.getValue().getSender()).isEqualTo(userFrom);
        assertThat(messageEntityCaptor.getValue().getAttachments()).isNotNull();

    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowException_whenUserIsNotFound() throws Exception{
        //given
        CreateMessageDTO createMessageDTO = CreateMessageDTOProvider.prepareCreateMessageDTOWithoutAttachments();

        //when
        doThrow(UserNotExistException.class).when(userRepository).findUserByIdIfExist(createMessageDTO.getIdUserForSendMessage());
        sendMessageService.sendMessage(createMessageDTO,1L);

    }

    @Test(expectedExceptions = SendMessageException.class)
    public void shouldThrowException_whenSendMessageIsImpossible() throws Exception {
        //given
        List<MultipartFile> multipartFiles = MockMultipartFileProvider.prepareMultipartFileList(1);
        CreateMessageDTO createMessageDTO = CreateMessageDTOProvider.prepareCreateMessageDTOWithoutAttachments();
        createMessageDTO.setFiles(multipartFiles);

        User userFrom = UserProvider.createUserWithAllRoles();
        User userTo = UserProvider.createUserWithAllRoles();

        Long userFromId = 2L;

        //when
        when(userRepository.findUserByIdIfExist(createMessageDTO.getIdUserForSendMessage())).thenReturn(userTo);
        when(userRepository.findUserByIdIfExist(userFromId)).thenReturn(userFrom);
        doThrow(SaveFileException.class).when(saveFileService).saveFile(eq(TEST_REPOSITORY_PATH), any(String.class), eq(multipartFiles.get(0)));

        sendMessageService.sendMessage(createMessageDTO,userFromId);

    }




}