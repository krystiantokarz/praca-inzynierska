package pl.krystiantokarz.virtualdeanery.service.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.RemoveFileException;

import java.io.File;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class RemoveFileServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private RemoveFileService removeFileService;

    private static final String PATH_TO_REMOVE_FILE = "target/test-classes/repository/file/";
    private static final String REMOVE_FILE_NAME = "file-to-remove.txt";


    @Test
    public void shouldRemoveFile() throws Exception{

        //when

        removeFileService.removeFileFromRepository(PATH_TO_REMOVE_FILE, REMOVE_FILE_NAME);

        //then
        File file = new File(PATH_TO_REMOVE_FILE + REMOVE_FILE_NAME);
        assertThat(file.exists()).isFalse();

        file.createNewFile();
    }

    @Test(expectedExceptions = RemoveFileException.class)
    public void shouldThrowRemoveFileException_whenRepositoryPathNotExist() throws Exception{
        //given
        String notExistRepositoryPath = "NOT_EXIST";

        //when
        removeFileService.removeFileFromRepository(notExistRepositoryPath, REMOVE_FILE_NAME);
    }

}