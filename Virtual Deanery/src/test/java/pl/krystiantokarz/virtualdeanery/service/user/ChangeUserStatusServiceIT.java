package pl.krystiantokarz.virtualdeanery.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Locale;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.testng.Assert.*;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class ChangeUserStatusServiceIT extends AbstractTransactionalTestNGSpringContextTests {


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ChangeUserStatusService changeUserStatusService;


    @Test
    public void shouldDisableUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //then
        changeUserStatusService.disableUser(user.getId(), Locale.getDefault());

        //when
        User savedUser = entityManager.find(User.class, user.getId());

        assertNotNull(savedUser);
        assertThat(savedUser.getId()).isEqualTo(user.getId());
        assertThat(savedUser.getLastName()).isEqualTo(user.getLastName());
        assertThat(savedUser.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(savedUser.getEmail()).isEqualTo(user.getEmail());
        assertThat(savedUser.isEnabled()).isFalse();
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldNotDisableUser_whenThrowUserNotExistException() throws Exception{
        //given
        Long notExistUserId = -10L;
        //when
        changeUserStatusService.disableUser(notExistUserId,Locale.getDefault());
    }

    @Test
    public void shouldEnabledUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        changeUserStatusService.enableUser(user.getId(), Locale.getDefault());
        User savedUser = entityManager.find(User.class, user.getId());

        //then
        assertNotNull(savedUser);
        assertThat(savedUser.getId()).isEqualTo(user.getId());
        assertThat(savedUser.getLastName()).isEqualTo(user.getLastName());
        assertThat(savedUser.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(savedUser.getEmail()).isEqualTo(user.getEmail());
        assertThat(savedUser.isEnabled()).isTrue();
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldNotEnabledUser_whenThrowUserNotExistException() throws Exception{
        //given
        Long notExistUserId = -10L;
        //when
        changeUserStatusService.enableUser(notExistUserId,Locale.getDefault());
    }


}