package pl.krystiantokarz.virtualdeanery.service.filetodownload;


import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import java.util.ArrayList;
import java.util.Collections;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

public class FindFileToDownloadServiceTest {

    @Mock
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @InjectMocks
    private FindFileToDownloadService findFileToDownloadService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldGetFileReadyToDownloadDescriptionsByTypeAndPageAndSize() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);

        //when
        when(fileToDownloadDescriptionRepository.findAllFileReadyToDownloadPageByFileType(FileType.ARCHIVE, new PageRequest(0, 10))).thenReturn(new PageImpl<>(new ArrayList<>(Collections.singletonList(fileEntity))));
        Page<FileReadyToDownload> result = findFileToDownloadService.getFileReadyToDownloadDescriptionsByFileTypeAndPageAndPageSize(FileType.ARCHIVE, 0, 10);

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.getContent()).isNotEmpty();
        assertThat(result.getContent()).contains(fileEntity);
    }

    @Test
    public void shouldGetFileWaitingForAcceptedDescriptionsByPageAndSize() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user);

        //when
        when(fileToDownloadDescriptionRepository.findAllFileWaitingForAcceptedPage(new PageRequest(0, 10))).thenReturn(new PageImpl<>(new ArrayList<>(Collections.singletonList(fileEntity))));
        Page<FileWaitingForAccepted> result = findFileToDownloadService.getFileWaitingForAcceptedDescriptionsByPageAndPageSize(0, 10);

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.getContent()).isNotEmpty();
        assertThat(result.getContent()).contains(fileEntity);
    }

}
