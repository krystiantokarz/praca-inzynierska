package pl.krystiantokarz.virtualdeanery.service.chat;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.chat.ChatMessage;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.chat.in.ChatMessageDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.ChatMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.dto.ChatMessageDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.ChatMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.Collections;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class ChatServiceTest {

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ChatMessageRepository chatMessageRepository;

    @Mock
    private UserGroup userGroup;

    @Mock
    private User user;

    @Mock
    private Group group;

    @InjectMocks
    private ChatService chatService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldAddMessage() throws Exception{

        //given
        Long exampleUserId = 10L;
        Long exampleGroupId = 10L;;
        ChatMessageDTO chatMessageDTO = ChatMessageDTOProvider.prepareChatMessageDTO(exampleGroupId);

        //when
        when(group.getId()).thenReturn(exampleGroupId);
        when(user.getId()).thenReturn(exampleUserId);
        when(userGroup.getGroup()).thenReturn(group);
        when(userGroup.getUser()).thenReturn(user);

        when(userRepository.findUserByIdIfExist(exampleUserId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(exampleUserId)).thenReturn(group);
        when(groupRepository.findUserGroupByUserAndGroup(user,group)).thenReturn(userGroup);

        ChatMessage result = chatService.addMessage(exampleUserId, exampleGroupId, chatMessageDTO);
        //then
        assertThat(result).isNotNull();
        assertThat(result.getMessage()).isEqualTo(chatMessageDTO.getMessage());
        assertThat(result.getSender().getId()).isEqualTo(exampleUserId);
        assertThat(result.getGroup().getId()).isEqualTo(exampleGroupId);
        verify(chatMessageRepository, times(1)).save(any(ChatMessage.class));
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowException_whenUserNotExist() throws Exception{
        //given
        Long exampleUserId = 10L;
        Long exampleGroupId = 10L;
        ChatMessageDTO chatMessageDTO = ChatMessageDTOProvider.prepareChatMessageDTO(exampleGroupId);

        //when
        doThrow(UserNotExistException.class).when(userRepository).findUserByIdIfExist(exampleUserId);
        chatService.addMessage(exampleUserId, exampleGroupId, chatMessageDTO);
    }

    @Test(expectedExceptions = GroupNotExistException.class)
    public void shouldThrowException_whenGroupNotExist() throws Exception{
        //given
        Long exampleUserId = 10L;
        Long exampleGroupId = 10L;
        ChatMessageDTO chatMessageDTO = ChatMessageDTOProvider.prepareChatMessageDTO(exampleGroupId);

        //when
        doThrow(GroupNotExistException.class).when(groupRepository).findGroupByIdIfExist(exampleGroupId);
        chatService.addMessage(exampleUserId, exampleGroupId, chatMessageDTO);
    }
    @Test(expectedExceptions = UserGroupNotExistException.class)
    public void shouldThrowException_whenUserGroupNotExist() throws Exception{
        //given
        Long exampleUserId = 10L;
        Long exampleGroupId = 10L;
        ChatMessageDTO chatMessageDTO = ChatMessageDTOProvider.prepareChatMessageDTO(exampleGroupId);

        //when
        when(userRepository.findUserByIdIfExist(exampleUserId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(exampleUserId)).thenReturn(group);
        doThrow(UserGroupNotExistException.class).when(groupRepository).findUserGroupByUserAndGroup(user,group);
        chatService.addMessage(exampleUserId, exampleGroupId, chatMessageDTO);
    }

    @Test
    public void shouldFindChatMessageForGroup() throws Exception{

        Long exampleGroupId = 100L;
        Group group = GroupProvider.createGroup();
        User user = UserProvider.createUserWithAllRoles();
        Integer page = 0;
        ChatMessage chatMessage = ChatMessageProvider.createChatMessage(user, group);

        when(groupRepository.findGroupByIdIfExist(exampleGroupId)).thenReturn(group);
        when(chatMessageRepository.findByGroupOrderByDateDesc(eq(group), any(PageRequest.class))).thenReturn(new PageImpl(Collections.singletonList(chatMessage)));

        Page<ChatMessage> result = chatService.findChatMessagesForGroupByPage(exampleGroupId, page);

        assertThat(result).isNotNull();
        verify(chatMessageRepository, times(1)).findByGroupOrderByDateDesc(any(Group.class), any(PageRequest.class));
    }

}