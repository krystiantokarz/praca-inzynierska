package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.mockito.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.CreateEventDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.TimetableEventRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateEventDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.EventTimeLimitException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventDateException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CreateEventServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private TimetableEventRepository timetableRepository;

    @Captor
    private ArgumentCaptor<TimetableEvent> timetableEventArgumentCaptor;

    @InjectMocks
    private CreateEventService createEventService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldCreateEvent() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Long userId = 100L;

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        createEventService.createEvent(createEventDTO,userId);

        //then
        verify(timetableRepository, times(1)).save(timetableEventArgumentCaptor.capture());

        assertThat(timetableEventArgumentCaptor.getValue()).isNotNull();
        assertThat(timetableEventArgumentCaptor.getValue().getEventDate()).isEqualTo(createEventDTO.getDate());
        assertThat(timetableEventArgumentCaptor.getValue().getTimeFrom()).isEqualTo(createEventDTO.getEventTimeFrom());
        assertThat(timetableEventArgumentCaptor.getValue().getTimeTo()).isEqualTo(createEventDTO.getEventTimeTo());
        assertThat(timetableEventArgumentCaptor.getValue().getCreatingUser()).isEqualTo(user);
        assertThat(timetableEventArgumentCaptor.getValue().getTopic()).isEqualTo(createEventDTO.getTopic());
        assertThat(timetableEventArgumentCaptor.getValue().getMessage()).isEqualTo(createEventDTO.getMessage());

    }

    @Test(expectedExceptions = EventTimeLimitException.class)
    public void shouldThrowExceptionWhenEventStartTimeIsBeforeCorrectTimes() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Long userId = 100L;

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setEventTimeFrom(LocalTime.of(1,0,0));

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        createEventService.createEvent(createEventDTO, user.getId());
    }

    @Test(expectedExceptions = EventTimeLimitException.class)
    public void shouldThrowExceptionWhenEventEndTimeIsAfterCorrectTimes() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Long userId = 100L;

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setEventTimeTo(LocalTime.of(23,0,0));

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        createEventService.createEvent(createEventDTO, user.getId());
    }

    @Test(expectedExceptions = ValidEventTimeException.class)
    public void shouldThrowExceptionWhenStartEventTimeIsAfterEndEventTime() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Long userId = 100L;

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setEventTimeFrom(LocalTime.of(11,0,0));
        createEventDTO.setEventTimeTo(LocalTime.of(10,0,0));

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        createEventService.createEvent(createEventDTO, user.getId());
    }

    @Test(expectedExceptions = ValidEventDateException.class)
    public void shouldThrowExceptionWhenEventDateIsBeforeNow() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Long userId = 100L;

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setDate(LocalDate.now().minusDays(10));

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        createEventService.createEvent(createEventDTO, user.getId());
    }


}