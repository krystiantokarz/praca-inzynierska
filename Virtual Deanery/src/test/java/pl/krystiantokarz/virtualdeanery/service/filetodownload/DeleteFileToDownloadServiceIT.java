package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.RemoveFileException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class DeleteFileToDownloadServiceIT extends AbstractTransactionalTestNGSpringContextTests {
    @Autowired
    private DeleteFileToDownloadService deleteFileToDownloadService;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String ARCHIVE_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/archive/";
    private static final String FORM_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/form/";
    private static final String ARCHIVE_FILE_NAME = "example-archive-to-remove.txt";
    private static final String FORM_FILE_NAME = "example-form-to-remove.txt";

    @Test
    public void shouldDeleteArchiveFileReadyToDownload() throws Exception{

        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user, ARCHIVE_FILE_NAME);
        entityManager.persist(fileEntity);

        //when
        deleteFileToDownloadService.deleteFile(fileEntity.getId());

        //then
        File removedFile = new File(ARCHIVE_REPOSITORY_PATH + ARCHIVE_FILE_NAME);
        assertThat(removedFile.exists()).isFalse();

        FileToDownloadDescriptionEntity rejectFile = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(rejectFile).isNull();

        removedFile.createNewFile();
    }

    @Test
    public void shouldDeleteFormFileReadyToDownload() throws Exception{

        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadFormFileToDownloadDescriptionEntity(user, FORM_FILE_NAME);
        entityManager.persist(fileEntity);

        //when
        deleteFileToDownloadService.deleteFile(fileEntity.getId());

        //then
        File removedFile = new File(FORM_REPOSITORY_PATH + FORM_FILE_NAME);
        assertThat(removedFile.exists()).isFalse();

        FileToDownloadDescriptionEntity rejectFile = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(rejectFile).isNull();

        removedFile.createNewFile();
    }


    @Test(expectedExceptions = RemoveFileException.class)
    public void shouldRemoveFileException_whenReadyToDownloadFileNotExist() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadFormFileToDownloadDescriptionEntity(user, "NOT-EXIST");
        entityManager.persist(fileEntity);

        //when
        deleteFileToDownloadService.deleteFile(fileEntity.getId());

    }

}