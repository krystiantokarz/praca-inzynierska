package pl.krystiantokarz.virtualdeanery.service.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Java6Assertions.assertThat;


@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class ChangeFileRepositoryServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private ChangeFileRepositoryService changeFileRepositoryService;

    private static final String OLD_PATH_TO_RENAME_FILE = "target/test-classes/repository/file/";
    private static final String NEW_PATH_TO_RENAME_FILE = "target/test-classes/repository/file/rename/";
    private static final String RENAME_FILE_NAME = "file-to-rename.txt";


    @BeforeClass
    public void init(){
        CustomFileUtils.createFolderIfNotExists(OLD_PATH_TO_RENAME_FILE);
        CustomFileUtils.createFolderIfNotExists(NEW_PATH_TO_RENAME_FILE);
    }

    @Test
    public void shouldRenameFile() throws Exception{

        //when
        changeFileRepositoryService.changeFileRepository(OLD_PATH_TO_RENAME_FILE, NEW_PATH_TO_RENAME_FILE, RENAME_FILE_NAME);

        //then
        File fileInOldPath = new File(OLD_PATH_TO_RENAME_FILE + RENAME_FILE_NAME);
        File fileInNewPath  = new File(NEW_PATH_TO_RENAME_FILE + RENAME_FILE_NAME);
        assertThat(fileInOldPath.exists()).isFalse();
        assertThat(fileInNewPath.exists()).isTrue();
    }

    @AfterMethod
    public void before() throws IOException {
        File oldFile = new File(OLD_PATH_TO_RENAME_FILE + RENAME_FILE_NAME);
        oldFile.createNewFile();

        File newFile = new File(NEW_PATH_TO_RENAME_FILE + RENAME_FILE_NAME);
        newFile.delete();
    }




}