package pl.krystiantokarz.virtualdeanery.service.messagebox;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageStatus;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class FindMessageServiceIT extends AbstractTransactionalTestNGSpringContextTests {


    @Autowired
    private FindMessageService findMessageService;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldFindMessageByIdForSelectedUserAndChangeMessageStatusIfNotReadYet() throws Exception{

        //given
        User userFrom = UserProvider.createUserWithAllRoles();
        User userTo = UserProvider.createUserWithAllRoles();
        MailMessageEntity messageEntity = MailMessageProvider.createMailMessageEntityWithoutAttachments(userFrom, userTo);

        entityManager.persist(userFrom);
        entityManager.persist(userTo);
        entityManager.persist(messageEntity);

        //when
        MailMessageEntity result = findMessageService.findMessageByIdForSelectedUserAndChangeMessageStatusIfNotReadYet(messageEntity.getId(), userTo.getId());

        //then
        assertThat(result).isNotNull();
        assertThat(result.getMessageStatus()).isEqualTo(MailMessageStatus.READ);
    }

}