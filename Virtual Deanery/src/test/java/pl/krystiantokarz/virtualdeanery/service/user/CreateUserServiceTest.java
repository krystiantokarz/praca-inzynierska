package pl.krystiantokarz.virtualdeanery.service.user;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.CreateUserDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.EmailSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.createaccount.CreateNewAccountSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class CreateUserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private EmailSender emailSender;

    @InjectMocks
    private CreateUserService createUserService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(createUserService,"mailEnabled",true);
    }

    @Test
    public void shouldCreateNewUser() throws Exception{
        //given
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        Locale locale = Locale.getDefault();

        String ENCODED_PASSWORD = "ENCODED_PASSWORD";

        //when
        when(passwordEncoder.encode(any(String.class))).thenReturn(ENCODED_PASSWORD);
        createUserService.createNewUser(createUserDTO, locale);

        //then
        verify(passwordEncoder, times(1)).encode(any(String.class));
        verify(userRepository, times(1)).save(any(User.class));
        verify(emailSender, times(1)).sendEmail(any(CreateNewAccountSender.class),any(Locale.class));


    }

    @Test(expectedExceptions = UserWithSelectedEmailExistException.class)
    public void shouldThrowException_whenSelectedEmailExist() throws Exception{
        //given
        CreateUserDTO createUserDTO = CreateUserDTOProvider.prepareCreateUserDTO();
        Locale locale = Locale.getDefault();

        //when
        doThrow(UserWithSelectedEmailExistException.class).when(userRepository).checkUserWithSelectedEmailExist(createUserDTO.getEmail());
        createUserService.createNewUser(createUserDTO,locale);
    }


}