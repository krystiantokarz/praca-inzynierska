package pl.krystiantokarz.virtualdeanery.service.user;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.EmailSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.mail.changeuserstatus.ChangeUserStatusSender;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class ChangeUserStatusServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private EmailSender emailSender;

    @InjectMocks
    private ChangeUserStatusService changeUserStatusService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(changeUserStatusService, "mailEnabled", true);
    }

    @Test
    public void shouldDisableUserAndSendEmail() throws Exception {
        //given
        Long userId = 1L;
        User user = UserProvider.createUserWithAllRoles();

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        changeUserStatusService.disableUser(userId,Locale.getDefault());

        //then
        assertThat(user.isEnabled()).isFalse();
        verify(userRepository, times(1)).save(any(User.class));
        verify(emailSender, times(1)).sendEmail(any(ChangeUserStatusSender.class),any(Locale.class));
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowException_whenUserNotExistForDisable() throws Exception{
        //given
        Long userId = 1L;

        //when
        doThrow(UserNotExistException.class).when(userRepository).findUserByIdIfExist(userId);
        changeUserStatusService.disableUser(userId,Locale.getDefault());
    }

    @Test
    public void shouldEnableUserAndSendEmail() throws Exception {
        //given
        Long userId = 1L;
        User user = UserProvider.createUserWithAllRoles();

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        changeUserStatusService.enableUser(userId,Locale.getDefault());

        //then
        assertThat(user.isEnabled()).isTrue();
        verify(userRepository, times(1)).save(any(User.class));
        verify(emailSender, times(1)).sendEmail(any(ChangeUserStatusSender.class),any(Locale.class));
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowException_whenUserNotExistForEnable() throws Exception{
        //given
        Long userId = 1L;

        //when
        doThrow(UserNotExistException.class).when(userRepository).findUserByIdIfExist(userId);
        changeUserStatusService.enableUser(userId,Locale.getDefault());
    }
}