package pl.krystiantokarz.virtualdeanery.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class FindUserServiceIT extends AbstractTransactionalTestNGSpringContextTests {


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private FindUserService findUserService;

    @Test
    public void shouldFindUserById() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        User result = findUserService.findUserById(user.getId());

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(user);
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowExceptionWhenUserNotExist() throws Exception{
        //given
        Long notExistUserId = 100L;

        //when
         findUserService.findUserById(notExistUserId);
    }


    @Test
    public void shouldGetUsersByPageAndPageSize() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        //when
        Page<User> result = findUserService.getUsersByPageAndPageSize(0, 10);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result.getTotalPages()).isEqualTo(1);
        assertThat(result.getContent()).contains(user);
    }

     @Test
     public void shouldGetUserByPageAndSizeWhenUsersNotExist() throws Exception{

         //when
         Page<User> result = findUserService.getUsersByPageAndPageSize(0, 10);

         //then
         assertThat(result).isNotNull();
         assertThat(result).isEmpty();
     }


     @Test
     public void shouldSearchUsersBySearchedFirstNameString() throws Exception{
         //given
         User user = UserProvider.createUserWithAllRoles();
         entityManager.persist(user);
         String searchString = user.getFirstName();

         //when
         List<User> result = findUserService.searchUsers(searchString);

         //then
         assertThat(result).isNotNull().isNotEmpty();
         assertThat(result).contains(user);
     }

    @Test
    public void shouldSearchUsersBySearchedLastNameUpperCaseString() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        String searchString = user.getLastName();

        //when
        List<User> result = findUserService.searchUsers(searchString.toUpperCase());

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result).contains(user);
    }

}