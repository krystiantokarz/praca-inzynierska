package pl.krystiantokarz.virtualdeanery.service.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupDTO;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.EditGroupPasswordDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditGroupDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditGroupPasswordDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupWithSelectedNameExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.NotMatchesPasswordsException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class EditGroupServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private EditGroupService editGroupService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void shouldEditGroup() throws Exception{

        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user,group);
        entityManager.persist(userGroup);
        EditGroupDTO editGroupDTO = EditGroupDTOProvider.prepareEditGroupDTO();

        //when
        editGroupService.editGroupData(group.getId(), user.getId(), editGroupDTO);

        //then
        Group result = entityManager.find(Group.class, group.getId());
        assertThat(result.getDescription()).isEqualTo(editGroupDTO.getDescription());
        assertThat(result.getName()).isEqualTo(editGroupDTO.getName());
    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToEditGroup() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user,group);
        entityManager.persist(userGroup);
        EditGroupDTO editGroupDTO = EditGroupDTOProvider.prepareEditGroupDTO();

        //when
        editGroupService.editGroupData(group.getId(), user.getId(), editGroupDTO);
    }


    @Test(expectedExceptions = GroupWithSelectedNameExistException.class)
    public void shouldThrowExceptionWhenNewGroupNameExist() throws Exception {
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        Group groupToEdit = GroupProvider.createGroup();
        groupToEdit.changeGroupData("name","password");
        entityManager.persist(groupToEdit);
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user,group);
        entityManager.persist(userGroup);

        EditGroupDTO editGroupDTO = EditGroupDTOProvider.prepareEditGroupDTO();
        editGroupDTO.setName(group.getName());

        //when
        editGroupService.editGroupData(groupToEdit.getId(), user.getId(), editGroupDTO);
    }

    @Test
    public void shouldEditGroupPassword() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        String notEncodingPassword = group.getPassword();
        group.changeGroupPassword(passwordEncoder.encode(notEncodingPassword));
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user,group);
        entityManager.persist(userGroup);

        EditGroupPasswordDTO editGroupPasswordDTO = EditGroupPasswordDTOProvider.prepareEditGroupPasswordDTO(notEncodingPassword);

        //when
        editGroupService.editGroupPassword(group.getId(),user.getId(),editGroupPasswordDTO);

        //then
        assertThat(group.getPassword()).isNotNull();
        assertThat(passwordEncoder.matches(editGroupPasswordDTO.getNewPassword(),group.getPassword())).isTrue();

    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToEditPasswordGroup() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        String notEncodingPassword = group.getPassword();
        group.changeGroupPassword(passwordEncoder.encode(notEncodingPassword));
        entityManager.persist(group);
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user,group);
        entityManager.persist(userGroup);

        EditGroupPasswordDTO editGroupPasswordDTO = EditGroupPasswordDTOProvider.prepareEditGroupPasswordDTO(notEncodingPassword);

        //when
        editGroupService.editGroupPassword(group.getId(),user.getId(),editGroupPasswordDTO);
    }

    @Test(expectedExceptions = NotMatchesPasswordsException.class)
    public void shouldThrowExceptionWhenGroupIsNotMatchedPassword() throws Exception {
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        String notEncodingPassword = group.getPassword();
        group.changeGroupPassword(passwordEncoder.encode(notEncodingPassword));
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user,group);
        entityManager.persist(userGroup);

        EditGroupPasswordDTO editGroupPasswordDTO = EditGroupPasswordDTOProvider.prepareEditGroupPasswordDTO("BAD-PASSWORD");

        //when
        editGroupService.editGroupPassword(group.getId(),user.getId(),editGroupPasswordDTO);
    }

}