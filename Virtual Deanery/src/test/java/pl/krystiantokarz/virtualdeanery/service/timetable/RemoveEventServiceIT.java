package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.PrivilegeException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;


@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class RemoveEventServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private RemoveEventService removeEventService;

    @Test
    public void shouldRemoveSelectedEventByUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        //when
        removeEventService.removeSelectedEventByUser(timetableEvent.getId(), user.getId());

        //then
        TimetableEvent result = entityManager.find(TimetableEvent.class, timetableEvent.getId());
        assertThat(result).isNull();

    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowPrivilegeExceptionWhenUserNotHaveAccessToRemoveSelectedGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);
        Long userWithoutPrivilege = 100L;

        //when
        removeEventService.removeSelectedEventByUser(timetableEvent.getId(), userWithoutPrivilege);
    }
}