package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class FindFileToDownloadServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private FindFileToDownloadService findFileToDownloadService;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void shouldGetFileReadyToDownloadDescriptionsByTypeAndPageAndSize() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileEntity);

        //when
        Page<FileReadyToDownload> result = findFileToDownloadService.getFileReadyToDownloadDescriptionsByFileTypeAndPageAndPageSize(FileType.ARCHIVE, 0, 10);

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.getContent()).isNotEmpty();
        assertThat(result.getContent()).contains(fileEntity);
    }

    @Test
    public void shouldGetFileWaitingForAcceptedDescriptionsByPageAndSize() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileEntity);

        //when
        Page<FileWaitingForAccepted> result = findFileToDownloadService.getFileWaitingForAcceptedDescriptionsByPageAndPageSize(0, 10);

        //then
        assertThat(result).isNotNull().isNotEmpty();
        assertThat(result.getContent()).isNotEmpty();
        assertThat(result.getContent()).contains(fileEntity);
    }

    @Test
    public void shouldSearchFilesReadyToDownloadBySearchedStringOriginalNameContainingIgnoreCaseAndFileType() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);
        entityManager.persist(fileEntity);

        //when
        List<FileReadyToDownload> result = findFileToDownloadService.searchFilesReadyToDownloadBySearchedStringOriginalNameContainingIgnoreCaseAndFileType(fileEntity.getOriginalFileName(), FileType.ARCHIVE);

        //then
        assertThat(result).isNotNull().isNotNull();
        assertThat(result).contains(fileEntity);
    }
}
