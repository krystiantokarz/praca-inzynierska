package pl.krystiantokarz.virtualdeanery.service.messagebox;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailMessageEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailAttachmentFileDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Collections;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class DownloadMessageAttachmentFileServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private DownloadMessageAttachmentFileService downloadMessageAttachmentFileService;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String PATH_TO_SAVE_DOWNLOADED_FILE = "target/test-classes/download/fileattachment/";
    private static final String FILE_NAME = "example.txt";


    @BeforeClass
    public void init(){
        CustomFileUtils.createFolderIfNotExists(PATH_TO_SAVE_DOWNLOADED_FILE);
    }


    @Test
    public void shouldDownloadMessageAttachment() throws Exception{

        //given
        MailAttachmentFileDescription mailAttachmentFileDescription = MailAttachmentFileDescriptionProvider.createMailAttachmentFileDescription("example.txt");
        User userFrom = UserProvider.createUserWithAllRoles();
        User userTo = UserProvider.createUserWithAllRoles();
        MailMessageEntity mailMessageEntity = MailMessageProvider.createMailMessageEntityWithAttachments(Collections.singletonList(mailAttachmentFileDescription),userFrom, userTo);

        entityManager.persist(userFrom);
        entityManager.persist(userTo);
        entityManager.persist(mailMessageEntity);

        File file = new File(PATH_TO_SAVE_DOWNLOADED_FILE + FILE_NAME);
        file.createNewFile();
        file.deleteOnExit();
        OutputStream outputStream = new FileOutputStream(file);
        //when
        downloadMessageAttachmentFileService.downloadFile(mailAttachmentFileDescription.getId(),userTo.getId(),outputStream);

        //then
        assertThat(file).isNotNull();
        assertThat(file.length()).isGreaterThan(0);
    }

}