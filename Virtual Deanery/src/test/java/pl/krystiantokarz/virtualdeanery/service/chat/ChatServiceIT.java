package pl.krystiantokarz.virtualdeanery.service.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.chat.ChatMessage;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.chat.in.ChatMessageDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.ChatMessageDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.ChatMessageProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.UserGroupNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;


@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class ChatServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ChatService chatService;

    @Test
    public void shouldAddMessage() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        entityManager.persist(user);
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user,group);
        entityManager.persist(userGroup);
        ChatMessageDTO chatMessageDTO = ChatMessageDTOProvider.prepareChatMessageDTO(group.getId());

        //when
        ChatMessage chatMessage = chatService.addMessage(user.getId(), group.getId(), chatMessageDTO);

        assertThat(chatMessage).isNotNull();
        assertThat(chatMessage.getGroup()).isEqualTo(group);
        assertThat(chatMessage.getSender()).isEqualTo(user);
        assertThat(chatMessage.getMessage()).isEqualTo(chatMessageDTO.getMessage());
        assertThat(chatMessage.getDate()).isNotNull();

        ChatMessage savedChatMessage = entityManager.find(ChatMessage.class, chatMessage.getId());
        assertThat(savedChatMessage).isEqualTo(chatMessage);
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowException_whenUserNotExist() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        ChatMessageDTO chatMessageDTO = ChatMessageDTOProvider.prepareChatMessageDTO(group.getId());
        Long notExistUserId = -100L;
        //when
        chatService.addMessage(notExistUserId, group.getId(), chatMessageDTO);
    }

    @Test(expectedExceptions = GroupNotExistException.class)
    public void shouldThrowException_whenGroupNotExist() throws Exception{

        //given
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        entityManager.persist(user);
        ChatMessageDTO chatMessageDTO = ChatMessageDTOProvider.prepareChatMessageDTO(group.getId());
        Long notExistGroupId = -100L;


        //when
        chatService.addMessage(user.getId(), notExistGroupId, chatMessageDTO);
    }
    @Test(expectedExceptions = UserGroupNotExistException.class)
    public void shouldThrowException_whenUserGroupNotExist() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        entityManager.persist(user);
        entityManager.persist(group);
        ChatMessageDTO chatMessageDTO = ChatMessageDTOProvider.prepareChatMessageDTO(group.getId());

        //when
        chatService.addMessage(user.getId(), group.getId(), chatMessageDTO);
    }

    @Test
    public void shouldFindChatMessagesForGroupByPage() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        entityManager.persist(user);
        entityManager.persist(group);

        ChatMessage chatMessage = ChatMessageProvider.createChatMessage(user, group);
        entityManager.persist(chatMessage);

        //when
        Page<ChatMessage> result = chatService.findChatMessagesForGroupByPage(group.getId(), 0);

        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result.getContent()).hasSize(1);
        assertThat(result.getContent()).contains(chatMessage);

    }

}