package pl.krystiantokarz.virtualdeanery.service.account;


import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.account.exceptions.NotMatchesPasswordsException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;

import static org.mockito.Mockito.*;

public class ChangePasswordServiceTest {

    private static final String USER_OLD_PASSWORD_TEST = "USER_OLD_PASSWORD_TEST";
    private static final String USER_NEW_PASSWORD_TEST = "USER_OLD_PASSWORD_TEST";
    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private ChangePasswordService changePasswordService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldChangePassword() throws Exception{
        //given
        Long userId = 1L;
        String ENCODE_PASSWORD = "ENCODE_PASSWORD";
        User user = UserProvider.createUserWithAllRoles();
        user.changeUserPassword(USER_OLD_PASSWORD_TEST);

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(passwordEncoder.matches(USER_OLD_PASSWORD_TEST,USER_OLD_PASSWORD_TEST)).thenReturn(true);
        when(passwordEncoder.encode(USER_NEW_PASSWORD_TEST)).thenReturn(ENCODE_PASSWORD);

        changePasswordService.changePassword(userId, USER_OLD_PASSWORD_TEST,USER_NEW_PASSWORD_TEST);

        //then
        verify(passwordEncoder, times(1)).matches(USER_OLD_PASSWORD_TEST,USER_OLD_PASSWORD_TEST);
        verify(userRepository, times(1)).save(any(User.class));
        verify(passwordEncoder, times(1)).encode(USER_NEW_PASSWORD_TEST);
    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldThrowException_whenUserIsNotFound() throws Exception{
        //given
        Long userId = 1L;

        //when
        doThrow(UserNotExistException.class).when(userRepository).findUserByIdIfExist(userId);

        changePasswordService.changePassword(userId, USER_OLD_PASSWORD_TEST,USER_NEW_PASSWORD_TEST);
    }

    @Test(expectedExceptions = NotMatchesPasswordsException.class)
    public void shouldThrowException_whenOldPasswordIsNotMatches() throws Exception {
        //given
        Long userId = 1L;
        User user = UserProvider.createUserWithAllRoles();
        String BAD_PASSWORD = "BAD_PASSWORD";

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(passwordEncoder.matches(BAD_PASSWORD,USER_OLD_PASSWORD_TEST)).thenReturn(false);

        changePasswordService.changePassword(userId, BAD_PASSWORD,USER_NEW_PASSWORD_TEST);


    }

}