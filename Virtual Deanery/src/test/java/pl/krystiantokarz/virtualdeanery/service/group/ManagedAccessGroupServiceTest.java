package pl.krystiantokarz.virtualdeanery.service.group;

import org.mockito.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.NotMatchesGroupPasswordException;

import java.util.Collections;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;

public class ManagedAccessGroupServiceTest {

    private static final String GROUP_PASSWORD = "GROUP-PASSWORD";

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserRepository userRepository;

    @Mock
    private GroupRepository groupRepository;

    @InjectMocks
    private ManagedAccessGroupService managedAccessGroupService;

    @Mock
    private User user;

    @Mock
    private Group group;

    @Mock
    private UserGroup userGroup;

    @Captor
    private ArgumentCaptor<UserGroup> userGroupArgumentCaptor;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldJoinIntoSelectedGroup() throws Exception{

        //given
        Long groupId = 100L;
        Long userId = 100L;

        //when
        when(group.getPassword()).thenReturn(GROUP_PASSWORD);
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(passwordEncoder.matches(GROUP_PASSWORD, GROUP_PASSWORD)).thenReturn(true);

        managedAccessGroupService.joinIntoSelectedGroup(groupId, userId, GROUP_PASSWORD);

        //then
        verify(groupRepository, times(1)).saveUserGroup(userGroupArgumentCaptor.capture());

        assertThat(userGroupArgumentCaptor.getValue().getUser()).isEqualTo(user);
        assertThat(userGroupArgumentCaptor.getValue().getGroup()).isEqualTo(group);
        assertThat(userGroupArgumentCaptor.getValue().getMemberType()).isEqualTo(MemberType.NORMAL_MEMBER);
    }

    @Test(expectedExceptions = NotMatchesGroupPasswordException.class)
    public void shouldThrowExceptionWhenGroupAndEnteredPasswordIsNotMatched() throws Exception{
        //given
        Long groupId = 100L;
        Long userId = 100L;

        //when
        when(group.getPassword()).thenReturn(GROUP_PASSWORD);
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(passwordEncoder.matches(GROUP_PASSWORD, GROUP_PASSWORD)).thenReturn(false);

        //when
        managedAccessGroupService.joinIntoSelectedGroup(groupId, userId, GROUP_PASSWORD);
    }

    @Test
    public void shouldLeaveFromSelectedGroup() throws Exception{
        //given
        Long groupId = 100L;
        Long userId = 100L;

        //when
        when(group.getId()).thenReturn(groupId);
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(groupRepository.findUserGroupByUserAndGroup(user,group)).thenReturn(userGroup);
        when(groupRepository.findUserGroupsByGroupId(groupId)).thenReturn(Collections.singletonList(userGroup));

        managedAccessGroupService.leaveFromSelectedGroup(groupId, userId);

        //then
        verify(groupRepository, times(1)).deleteUserGroup(userGroup);
        verify(groupRepository, never()).delete(group);
    }

    @Test
    public void shouldLeaveFromSelectedGroupAndDeleteId() throws Exception {
        //given
        Long groupId = 100L;
        Long userId = 100L;

        //when
        when(group.getId()).thenReturn(groupId);
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(groupRepository.findUserGroupByUserAndGroup(user,group)).thenReturn(userGroup);
        when(groupRepository.findUserGroupsByGroupId(groupId)).thenReturn(Collections.emptyList());

        managedAccessGroupService.leaveFromSelectedGroup(groupId, userId);

        //then
        verify(groupRepository, times(1)).deleteUserGroup(userGroup);
        verify(groupRepository, times(1)).delete(group);
    }

}