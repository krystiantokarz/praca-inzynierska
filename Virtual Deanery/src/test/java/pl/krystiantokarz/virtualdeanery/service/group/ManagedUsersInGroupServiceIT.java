package pl.krystiantokarz.virtualdeanery.service.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class ManagedUsersInGroupServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ManagedUsersInGroupService managedUsersInGroupService;

    @Test
    public void shouldAddAdminAccess() throws Exception{
        //given
        User adminGroupUser = UserProvider.createUserWithAllRoles();
        User userToChangeAccess = UserProvider.createUserWithAllRoles();
        entityManager.persist(adminGroupUser);
        entityManager.persist(userToChangeAccess);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup1 = UserGroupProvider.createUserGroupForNormalGroupMember(userToChangeAccess, group);
        entityManager.persist(userGroup1);
        UserGroup userGroup2 = UserGroupProvider.createUserGroupForAdministratorGroupMember(adminGroupUser,group);
        entityManager.persist(userGroup2);

        //when
        managedUsersInGroupService.addAdminAccess(userToChangeAccess.getId(),group.getId(),adminGroupUser.getId());

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup1.getId());
        assertThat(result.getMemberType()).isEqualTo(MemberType.ADMIN_MEMBER);
        assertThat(result.getGroup()).isEqualTo(group);
        assertThat(result.getUser()).isEqualTo(userToChangeAccess);

    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToAddAdminAccess() throws Exception{
        //given
        User user =  UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        managedUsersInGroupService.addAdminAccess(user.getId(),group.getId(),user.getId());
    }

    @Test
    public void shouldRemoveAdminAccess() throws Exception{
        //given
        User adminGroupUser = UserProvider.createUserWithAllRoles();
        User userToChangeAccess = UserProvider.createUserWithAllRoles();
        entityManager.persist(adminGroupUser);
        entityManager.persist(userToChangeAccess);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup1 = UserGroupProvider.createUserGroupForAdministratorGroupMember(userToChangeAccess, group);
        entityManager.persist(userGroup1);
        UserGroup userGroup2 = UserGroupProvider.createUserGroupForAdministratorGroupMember(adminGroupUser,group);
        entityManager.persist(userGroup2);

        //when
        managedUsersInGroupService.removeAdminAccess(userToChangeAccess.getId(),group.getId(),adminGroupUser.getId());

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup1.getId());
        assertThat(result.getMemberType()).isEqualTo(MemberType.NORMAL_MEMBER);
        assertThat(result.getGroup()).isEqualTo(group);
        assertThat(result.getUser()).isEqualTo(userToChangeAccess);
    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToRemoveAdminAccess() throws Exception{
        //given
        User user =  UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        managedUsersInGroupService.removeAdminAccess(user.getId(),group.getId(),user.getId());
    }

    @Test
    public void shouldRemoveUserFromGroup() throws Exception{
        //given
        User adminUser = UserProvider.createUserWithAllRoles();
        User userToRemove = UserProvider.createUserWithAllRoles();
        entityManager.persist(adminUser);
        entityManager.persist(userToRemove);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup1 = UserGroupProvider.createUserGroupForAdministratorGroupMember(adminUser, group);
        entityManager.persist(userGroup1);
        UserGroup userGroup2 = UserGroupProvider.createUserGroupForAdministratorGroupMember(userToRemove,group);
        entityManager.persist(userGroup2);

        //when
        managedUsersInGroupService.removeUserFromGroup(userToRemove.getId(),group.getId(),adminUser.getId());

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup2.getId());
        assertThat(result).isNull();
    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToRemoveUserFromGroup() throws Exception{
        //given
        User notAdminGroup = UserProvider.createUserWithAllRoles();
        User userToRemove = UserProvider.createUserWithAllRoles();
        entityManager.persist(notAdminGroup);
        entityManager.persist(userToRemove);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        UserGroup userGroup1 = UserGroupProvider.createUserGroupForNormalGroupMember(notAdminGroup, group);
        entityManager.persist(userGroup1);
        UserGroup userGroup2 = UserGroupProvider.createUserGroupForAdministratorGroupMember(userToRemove,group);
        entityManager.persist(userGroup2);

        //when
        managedUsersInGroupService.removeUserFromGroup(userToRemove.getId(),group.getId(),notAdminGroup.getId());
    }

}