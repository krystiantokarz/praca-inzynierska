package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.EditEventDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditEventDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.EventTimeLimitException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.PrivilegeException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventTimeException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalTime;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class EditEventServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private EditEventService editEventService;

    @Test
    public void shouldEditSelectedEventByUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(timetableEvent.getId());

        //when
        editEventService.editSelectedEventByUser(editEventDTO, user.getId());

        //then
        TimetableEvent updateTimetableEvent = entityManager.createQuery("SELECT t FROM TimetableEvent t WHERE t.id = :id", TimetableEvent.class)
                .setParameter("id",timetableEvent.getId())
                .getSingleResult();

        assertThat(updateTimetableEvent).isNotNull();
        assertThat(updateTimetableEvent.getTimeFrom()).isEqualTo(editEventDTO.getTimeFrom());
        assertThat(updateTimetableEvent.getTimeTo()).isEqualTo(editEventDTO.getTimeTo());
        assertThat(updateTimetableEvent.getTopic()).isEqualTo(editEventDTO.getTopic());
        assertThat(updateTimetableEvent.getMessage()).isEqualTo(editEventDTO.getMessage());
    }


    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowPrivilegeExceptionWhenUserDoesNotHaveAccessToEditSelectedEvent() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);
        Long userWithoutPrivilegeId = 100L;

        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(timetableEvent.getId());

        //when
        editEventService.editSelectedEventByUser(editEventDTO, userWithoutPrivilegeId);

    }
    @Test(expectedExceptions = EventTimeLimitException.class)
    public void shouldThrowExceptionWhenEventStartTimeIsBeforeCorrectTimes() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(timetableEvent.getId());
        editEventDTO.setTimeFrom(LocalTime.of(1,0,0));

        //when
        editEventService.editSelectedEventByUser(editEventDTO, user.getId());
    }

    @Test(expectedExceptions = EventTimeLimitException.class)
    public void shouldThrowExceptionWhenEventEndTimeIsAfterCorrectTimes() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(timetableEvent.getId());
        editEventDTO.setTimeTo(LocalTime.of(23,0,0));

        //when
        editEventService.editSelectedEventByUser(editEventDTO, user.getId());
    }

    @Test(expectedExceptions = ValidEventTimeException.class)
    public void shouldThrowExceptionWhenStartEventTimeIsAfterEndEventTime() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        entityManager.persist(timetableEvent);

        EditEventDTO editEventDTO = EditEventDTOProvider.prepareEditEventDTO(timetableEvent.getId());
        editEventDTO.setTimeFrom(LocalTime.of(11,0,0));
        editEventDTO.setTimeTo(LocalTime.of(10,0,0));

        //when
        editEventService.editSelectedEventByUser(editEventDTO, user.getId());
    }
}