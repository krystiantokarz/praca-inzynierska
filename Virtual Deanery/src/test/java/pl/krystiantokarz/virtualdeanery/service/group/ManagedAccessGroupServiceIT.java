package pl.krystiantokarz.virtualdeanery.service.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.NotMatchesGroupPasswordException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.testng.Assert.*;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class ManagedAccessGroupServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ManagedAccessGroupService managedAccessGroupService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void shouldJoinIntoSelectedGroup() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        String notEncodedGroupPassword = group.getPassword();
        group.changeGroupPassword(passwordEncoder.encode(notEncodedGroupPassword));
        entityManager.persist(user);
        entityManager.persist(group);

        //when
        managedAccessGroupService.joinIntoSelectedGroup(group.getId(), user.getId(), notEncodedGroupPassword);

        //then
        List<UserGroup> resultList = entityManager.createQuery("SELECT us FROM UserGroup us WHERE us.group = :gr AND us.user = :us", UserGroup.class)
                .setParameter("gr", group)
                .setParameter("us", user)
                .getResultList();

        assertThat(resultList).isNotEmpty();
        assertThat(resultList.get(0).getMemberType()).isEqualTo(MemberType.NORMAL_MEMBER);


    }

    @Test(expectedExceptions = NotMatchesGroupPasswordException.class)
    public void shouldThrowExceptionWhenGroupAndEnteredPasswordIsNotMatched() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        String notEncodedGroupPassword = group.getPassword();
        group.changeGroupPassword(passwordEncoder.encode(notEncodedGroupPassword));
        entityManager.persist(user);
        entityManager.persist(group);

        //when
        managedAccessGroupService.joinIntoSelectedGroup(group.getId(), user.getId(), "BAD-PASSWORD");

    }

    @Test
    public void shouldLeaveFromSelectedGroup() throws Exception{
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        User user1 = UserProvider.createUserWithAllRoles();
        entityManager.persist(user1);
        User user2 = UserProvider.createUserWithAllRoles();
        entityManager.persist(user2);
        UserGroup userGroup1 = UserGroupProvider.createUserGroupForNormalGroupMember(user1, group);
        UserGroup userGroup2 = UserGroupProvider.createUserGroupForNormalGroupMember(user2, group);
        entityManager.persist(userGroup1);
        entityManager.persist(userGroup2);

        //when
        managedAccessGroupService.leaveFromSelectedGroup(group.getId(), user1.getId());

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup1.getId());
        assertNull(result);

        Group resultGroup = entityManager.find(Group.class, group.getId());
        assertNotNull(resultGroup);


    }

    @Test
    public void shouldLeaveFromSelectedGroupAndDeleteId() throws Exception {
        //given
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user, group);
        entityManager.persist(userGroup);

        //when
        managedAccessGroupService.leaveFromSelectedGroup(group.getId(), user.getId());

        //then
        UserGroup result = entityManager.find(UserGroup.class, userGroup.getId());
        assertNull(result);

        Group resultGroup = entityManager.find(Group.class, group.getId());
        assertNull(resultGroup);
    }
}