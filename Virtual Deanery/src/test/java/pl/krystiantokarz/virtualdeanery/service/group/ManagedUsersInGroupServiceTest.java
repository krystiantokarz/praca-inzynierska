package pl.krystiantokarz.virtualdeanery.service.group;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserGroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.PrivilegeException;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class ManagedUsersInGroupServiceTest {

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private ManagedUsersInGroupService managedUsersInGroupService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldAddAdminAccess() throws Exception{
        //given
        Long userId = 100L;
        Long groupId = 100L;
        Long loggedUserId = 900L;
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        UserGroup userGroup = UserGroupProvider.createUserGroupForNormalGroupMember(user,group);

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(groupRepository.checkLoggedUserIsAdminInSelectedGroup(loggedUserId,groupId)).thenReturn(true);
        when(groupRepository.findUserGroupByUserAndGroup(user,group)).thenReturn(userGroup);
        managedUsersInGroupService.addAdminAccess(userId, groupId, loggedUserId);

        //then
        verify(groupRepository, times(1)).saveUserGroup(any(UserGroup.class));
        assertThat(userGroup.getMemberType()).isEqualTo(MemberType.ADMIN_MEMBER);

    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToAddAdminAccess() throws Exception{
        //given
        Long userId = 100L;
        Long groupId = 100L;
        Long loggedUserId = 900L;

        //when
        when(groupRepository.checkLoggedUserIsAdminInSelectedGroup(loggedUserId, groupId)).thenReturn(false);
        managedUsersInGroupService.addAdminAccess(userId, groupId, loggedUserId);
    }

    @Test
    public void shouldRemoveAdminAccess() throws Exception{
        //given
        Long userId = 100L;
        Long groupId = 100L;
        Long loggedUserId = 900L;
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user,group);

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(groupRepository.checkLoggedUserIsAdminInSelectedGroup(loggedUserId,groupId)).thenReturn(true);
        when(groupRepository.findUserGroupByUserAndGroup(user,group)).thenReturn(userGroup);
        managedUsersInGroupService.removeAdminAccess(userId, groupId, loggedUserId);

        //then
        verify(groupRepository, times(1)).saveUserGroup(any(UserGroup.class));
        assertThat(userGroup.getMemberType()).isEqualTo(MemberType.NORMAL_MEMBER);
    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToRemoveAdminAccess() throws Exception{
        //given
        Long userId = 100L;
        Long groupId = 100L;
        Long loggedUserId = 900L;

        //when
        when(groupRepository.checkLoggedUserIsAdminInSelectedGroup(loggedUserId, groupId)).thenReturn(false);
        managedUsersInGroupService.removeAdminAccess(userId, groupId, loggedUserId);
    }

    @Test
    public void shouldRemoveUserFromGroup() throws Exception{
        //given
        Long userId = 100L;
        Long groupId = 100L;
        Long loggedUserId = 900L;
        User user = UserProvider.createUserWithAllRoles();
        Group group = GroupProvider.createGroup();
        UserGroup userGroup = UserGroupProvider.createUserGroupForAdministratorGroupMember(user,group);

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(groupRepository.checkLoggedUserIsAdminInSelectedGroup(loggedUserId,groupId)).thenReturn(true);
        when(groupRepository.findUserGroupByUserAndGroup(user,group)).thenReturn(userGroup);
        managedUsersInGroupService.removeUserFromGroup(userId, groupId, loggedUserId);

        //then
        verify(groupRepository, times(1)).deleteUserGroup(any(UserGroup.class));
    }

    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowExceptionWhenUserNotHavePrivilegeToRemoveUserFromGroup() throws Exception{
        //given
        Long userId = 100L;
        Long groupId = 100L;
        Long loggedUserId = 900L;

        //when
        when(groupRepository.checkLoggedUserIsAdminInSelectedGroup(loggedUserId, groupId)).thenReturn(false);
        managedUsersInGroupService.removeUserFromGroup(userId, groupId, loggedUserId);

    }

}