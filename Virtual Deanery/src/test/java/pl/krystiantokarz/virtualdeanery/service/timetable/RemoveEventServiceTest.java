package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.TimetableEventRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.TimetableEventProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.PrivilegeException;

import static org.mockito.Mockito.*;

public class RemoveEventServiceTest {

    @Mock
    private User user;

    @Mock
    private TimetableEventRepository timetableEventRepository;

    @Mock
    private PrivilegeService privilegeService;

    @InjectMocks
    private RemoveEventService removeEventService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void shouldRemoveSelectedEventByUser() throws Exception{
        //given
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        Long eventId = 100L;
        Long userId = 100L;

        //when
        when(user.getId()).thenReturn(userId);
        when(timetableEventRepository.findTimetableEventByIdIfExist(eventId)).thenReturn(timetableEvent);
        removeEventService.removeSelectedEventByUser(eventId, userId);

        //then
        verify(timetableEventRepository, times(1)).delete(timetableEvent);
    }


    @Test(expectedExceptions = PrivilegeException.class)
    public void shouldThrowPrivilegeExceptionWhenUserNotHaveAccessToRemoveSelectedGroup() throws Exception{
        //given
        TimetableEvent timetableEvent = TimetableEventProvider.createTimetableEvent(user);
        Long eventId = 100L;
        Long userId = 100L;

        //when
        when(user.getId()).thenReturn(userId);
        when(timetableEventRepository.findTimetableEventByIdIfExist(eventId)).thenReturn(timetableEvent);
        doThrow(PrivilegeException.class).when(privilegeService).checkUserHasAccessToEditEvent(timetableEvent, userId);
        removeEventService.removeSelectedEventByUser(eventId, userId);
    }
}