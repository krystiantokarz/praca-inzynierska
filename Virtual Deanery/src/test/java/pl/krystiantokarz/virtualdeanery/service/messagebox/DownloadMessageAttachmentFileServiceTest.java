package pl.krystiantokarz.virtualdeanery.service.messagebox;

import org.mockito.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.mail.MailAttachmentFileDescription;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.MailMessageRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.MailAttachmentFileDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.service.file.DownloadFileService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.DownloadFileException;
import pl.krystiantokarz.virtualdeanery.service.messagebox.exceptions.DownloadMessageAttachmentFileException;

import java.io.OutputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class DownloadMessageAttachmentFileServiceTest {

    private static final String TEST_REPOSITORY_PATH = "TEST-REPOSITORY-PATH";

    @Mock
    private DownloadFileService downloadFileService;

    @Mock
    private OutputStream outputStream;

    @Mock
    private MailMessageRepository mailMessageRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private User user;

    @Captor
    private ArgumentCaptor<String> savedFileNameCaptor;

    @Captor
    private ArgumentCaptor<String> repositoryPathCaptor;

    @InjectMocks
    private DownloadMessageAttachmentFileService downloadMessageAttachmentFileService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(downloadMessageAttachmentFileService,"repositoryPath",TEST_REPOSITORY_PATH);
    }


    @Test
    public void shouldDownloadFile() throws Exception {
        //given
        MailAttachmentFileDescription mailAttachmentFileDescription = MailAttachmentFileDescriptionProvider.createMailAttachmentFileDescription();
        Long userId = 100L;
        Long attachmentFileId = 100L;

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(mailMessageRepository.findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(attachmentFileId,user)).thenReturn(mailAttachmentFileDescription);
        doNothing().when(downloadFileService).downloadFile(repositoryPathCaptor.capture(), savedFileNameCaptor.capture(), eq(outputStream));

        downloadMessageAttachmentFileService.downloadFile(attachmentFileId,userId, outputStream);

        //then

        assertThat(repositoryPathCaptor.getValue()).isEqualTo(TEST_REPOSITORY_PATH);
        assertThat(savedFileNameCaptor.getValue()).isEqualTo(mailAttachmentFileDescription.getSavedFileName());
        verify(downloadFileService, times(1)).downloadFile(any(String.class),any(String.class),any(OutputStream.class));
        verifyNoMoreInteractions(downloadFileService);
    }

    @Test(expectedExceptions = DownloadMessageAttachmentFileException.class)
    public void shouldThrowException_whenDownloadFileIsImpossible() throws Exception{
        //given
        MailAttachmentFileDescription mailAttachmentFileDescription = MailAttachmentFileDescriptionProvider.createMailAttachmentFileDescription();
        Long userId = 100L;
        Long attachmentFileId = 100L;

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(mailMessageRepository.findMailAttachmentFileDescriptionByIdForSelectedRecipientIfExist(attachmentFileId,user)).thenReturn(mailAttachmentFileDescription);
        doThrow(DownloadFileException.class).when(downloadFileService).downloadFile(TEST_REPOSITORY_PATH,mailAttachmentFileDescription.getSavedFileName(),outputStream);

        downloadMessageAttachmentFileService.downloadFile(attachmentFileId,userId, outputStream);
    }

}