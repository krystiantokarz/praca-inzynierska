package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.RemoveFileException;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.ChangeFileToDownloadTypeException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.FileNotExistException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;
import java.util.Locale;

import static org.assertj.core.api.Java6Assertions.assertThat;


@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class ChangeFileStatusServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private ChangeFileStatusService changeFileStatusService;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String TMP_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/tmp/";
    private static final String ARCHIVE_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/archive/";
    private static final String FORM_REPOSITORY_PATH = "target/test-classes/repository/filetodownload/form/";
    private static final String FILE_NAME = "example-tmp-to-remove.txt";

    @Test
    public void shouldRejectFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        User appUser = UserProvider.createAppUser();
        entityManager.persist(appUser);

        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user,FILE_NAME);
        entityManager.persist(fileEntity);

        //when
        changeFileStatusService.rejectFile(fileEntity.getId(), Locale.getDefault());

        //then
        File removedFile = new File(TMP_REPOSITORY_PATH + FILE_NAME);
        assertThat(removedFile.exists()).isFalse();

        FileToDownloadDescriptionEntity rejectFile = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(rejectFile).isNull();

        removedFile.createNewFile();
    }

    @Test(expectedExceptions = FileNotExistException.class)
    public void shouldThrowFileNotExistException_whenRejectedFileDescriptionNotExist() throws Exception{
        //given
        Long notExistId = -100L;

        //when
        changeFileStatusService.rejectFile(notExistId, Locale.getDefault());
    }

    @Test(expectedExceptions = RemoveFileException.class)
    public void shouldThrowRemoveFileException_whenRejectedFileNotExistInRepository() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user,"NOT-EXIST");
        entityManager.persist(fileEntity);

        //when
        changeFileStatusService.rejectFile(fileEntity.getId(), Locale.getDefault());
    }

    @Test
    public void shouldAcceptArchiveFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        User appUser = UserProvider.createAppUser();
        entityManager.persist(appUser);
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user,FILE_NAME);
        entityManager.persist(fileEntity);

        //when
        changeFileStatusService.acceptFile(fileEntity.getId(), Locale.getDefault());

        //then
        File acceptFileInTmpRepository = new File(TMP_REPOSITORY_PATH + FILE_NAME);
        assertThat(acceptFileInTmpRepository.exists()).isFalse();
        File acceptFileInArchiveRepository = new File(ARCHIVE_REPOSITORY_PATH + FILE_NAME);
        assertThat(acceptFileInArchiveRepository.exists()).isTrue();

        FileToDownloadDescriptionEntity result = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(result).isNotNull();
        assertThat(result.getFileStatus()).isEqualTo(FileStatus.READY_TO_DOWNLOAD);

        acceptFileInTmpRepository.createNewFile();
        acceptFileInArchiveRepository.delete();
    }

    @Test
    public void shouldAcceptFormFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        User appUser = UserProvider.createAppUser();
        entityManager.persist(appUser);
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableFormFileToDownloadDescriptionEntity(user,FILE_NAME);
        entityManager.persist(fileEntity);

        //when
        changeFileStatusService.acceptFile(fileEntity.getId(), Locale.getDefault());

        //then
        File acceptFileInTmpRepository = new File(TMP_REPOSITORY_PATH + FILE_NAME);
        assertThat(acceptFileInTmpRepository.exists()).isFalse();
        File acceptFileInArchiveRepository = new File(FORM_REPOSITORY_PATH + FILE_NAME);
        assertThat(acceptFileInArchiveRepository.exists()).isTrue();

        FileToDownloadDescriptionEntity result = entityManager.find(FileToDownloadDescriptionEntity.class, fileEntity.getId());
        assertThat(result).isNotNull();
        assertThat(result.getFileStatus()).isEqualTo(FileStatus.READY_TO_DOWNLOAD);

        acceptFileInTmpRepository.createNewFile();
        acceptFileInArchiveRepository.delete();
    }

    @Test(expectedExceptions = FileNotExistException.class)
    public void shouldThrowFileNotExistException_whenAcceptFileDescriptionNotExist() throws Exception{
        //given
        Long notExistId = -100L;

        //when
        changeFileStatusService.acceptFile(notExistId, Locale.getDefault());
    }

    @Test(expectedExceptions = ChangeFileToDownloadTypeException.class)
    public void shouldThrowRenameFileException_whenAcceptFileNotExistInRepository() throws Exception{
        //given
        User user = UserProvider.createUserWithEmployeeRole();
        entityManager.persist(user);

        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createWaitingForAcceptableArchiveFileToDownloadDescriptionEntity(user,"NOT-EXIST");
        entityManager.persist(fileEntity);

        //when
        changeFileStatusService.acceptFile(fileEntity.getId(), Locale.getDefault());
    }


}