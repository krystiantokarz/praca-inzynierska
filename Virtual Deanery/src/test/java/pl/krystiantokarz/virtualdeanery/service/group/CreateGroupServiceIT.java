package pl.krystiantokarz.virtualdeanery.service.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.MemberType;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.group.in.CreateGroupDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateGroupDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.GroupProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.group.exceptions.GroupWithSelectedNameExistException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class CreateGroupServiceIT extends AbstractTransactionalTestNGSpringContextTests {


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CreateGroupService createGroupService;


    @Test
    public void shouldCreateGroup() throws Exception {
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        CreateGroupDTO createGroupDTO = CreateGroupDTOProvider.prepareCreateGroupDTO();

        //when
        createGroupService.createGroup(createGroupDTO, user.getId());

        //then
        Group savedGroup = entityManager.createQuery("SELECT g FROM Group g WHERE g.name = :name ", Group.class)
                .setParameter("name", createGroupDTO.getName())
                .getSingleResult();

        assertThat(savedGroup).isNotNull();
        assertThat(savedGroup.getPassword()).isNotNull();
        assertThat(savedGroup.getName()).isEqualTo(createGroupDTO.getName());
        assertThat(savedGroup.getDescription()).isEqualTo(createGroupDTO.getDescription());

        List<UserGroup> userGroupResultList = entityManager.createNativeQuery("SELECT * FROM user_group ug WHERE ug.group_id = :groupId", UserGroup.class)
                .setParameter("groupId", savedGroup.getId())
                .getResultList();

        assertThat(userGroupResultList).isNotEmpty();
        assertThat(userGroupResultList.get(0).getUser()).isEqualTo(user);
        assertThat(userGroupResultList.get(0).getGroup()).isEqualTo(savedGroup);
        assertThat(userGroupResultList.get(0).getMemberType()).isEqualTo(MemberType.ADMIN_MEMBER);
    }

    @Test(expectedExceptions = GroupWithSelectedNameExistException.class)
    public void throwExceptionWhenGroupWithSelectedNameExistInSystem() throws Exception {
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        Group group = GroupProvider.createGroup();
        entityManager.persist(group);

        CreateGroupDTO createGroupDTO = CreateGroupDTOProvider.prepareCreateGroupDTO();
        createGroupDTO.setName(group.getName());

        //when
        createGroupService.createGroup(createGroupDTO, user.getId());
    }
}