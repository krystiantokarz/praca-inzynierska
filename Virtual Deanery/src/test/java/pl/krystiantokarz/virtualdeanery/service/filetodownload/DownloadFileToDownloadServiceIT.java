package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.FileToDownloadDescriptionEntity;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.DownloadFileToDownloadException;
import pl.krystiantokarz.virtualdeanery.utils.CustomFileUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static org.assertj.core.api.Java6Assertions.assertThat;


@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class DownloadFileToDownloadServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    private static final String PATH_TO_DOWNLOAD_FILE = "target/test-classes/download/filetodownload/";
    private static final String FILE_NAME = "example.txt";

    @Autowired
    private DownloadFileToDownloadService downloadFileToDownloadService;

    @PersistenceContext
    private EntityManager entityManager;

    @BeforeClass
    public void init(){
        CustomFileUtils.createFolderIfNotExists(PATH_TO_DOWNLOAD_FILE);
    }

    @Test
    public void shouldDownloadArchiveFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user,"example-archive.txt");
        entityManager.persist(fileEntity);

        File file = new File(PATH_TO_DOWNLOAD_FILE + FILE_NAME);
        file.createNewFile();
        file.deleteOnExit();
        OutputStream outputStream = new FileOutputStream(file);

        //when
        downloadFileToDownloadService.downloadFile(fileEntity.getId(),outputStream);

        //then
        assertThat(file).isNotNull();
    }

    @Test
    public void shouldDownloadFormFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadFormFileToDownloadDescriptionEntity(user,"example-form.txt");
        entityManager.persist(fileEntity);

        File file = new File(PATH_TO_DOWNLOAD_FILE + FILE_NAME);
        file.createNewFile();
        file.deleteOnExit();
        OutputStream outputStream = new FileOutputStream(file);

        //when
        downloadFileToDownloadService.downloadFile(fileEntity.getId(),outputStream);

        //then
        assertThat(file).isNotNull();
    }


    @Test(expectedExceptions = DownloadFileToDownloadException.class)
    public void shouldThrowException_whenDownloadFileIsImpossible() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user,"NOT-EXIST");
        entityManager.persist(fileEntity);

        //when
        downloadFileToDownloadService.downloadFile(fileEntity.getId(), null);
    }


}