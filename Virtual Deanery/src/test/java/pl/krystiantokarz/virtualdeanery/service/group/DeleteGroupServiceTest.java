package pl.krystiantokarz.virtualdeanery.service.group;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.group.Group;
import pl.krystiantokarz.virtualdeanery.domain.group.UserGroup;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.GroupRepository;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.UserRepository;

import static org.mockito.Mockito.*;

public class DeleteGroupServiceTest {

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private Group group;

    @Mock
    private User user;

    @Mock
    private UserGroup userGroup;

    @InjectMocks
    private DeleteGroupService deleteGroupService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void shouldDeleteGroupBySystemAdministrator() throws Exception{
        //given
        Long groupId = 1L;
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);

        //when
        deleteGroupService.deleteGroupBySystemAdministrator(groupId);

        //then
        verify(groupRepository, times(1)).delete(any(Group.class));
        verify(groupRepository, times(1)).deleteAllUserGroupsForSelectedGroup(any(Group.class)); //todo przetestwoac to
    }

    @Test
    public void shouldDeleteGroupByGroupAdministrator() throws Exception{
        //given
        Long groupId = 1L;
        Long userId= 1L;

        //when
        when(userRepository.findUserByIdIfExist(userId)).thenReturn(user);
        when(groupRepository.findGroupByIdIfExist(groupId)).thenReturn(group);
        when(groupRepository.findUserGroupForAdminMember(user,group)).thenReturn(userGroup);

        deleteGroupService.deleteGroupByGroupAdministrator(userId,groupId);

        //then
        verify(groupRepository, times(1)).delete(any(Group.class));
        verify(groupRepository, times(1)).deleteAllUserGroupsForSelectedGroup(any(Group.class));
    }

}