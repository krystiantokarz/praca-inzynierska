package pl.krystiantokarz.virtualdeanery.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.user.in.EditUserDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.EditUserDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserNotExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedEmailExistException;
import pl.krystiantokarz.virtualdeanery.service.user.exceptions.UserWithSelectedLoginExistException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.*;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class EditUserServiceIT extends AbstractTransactionalTestNGSpringContextTests{

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private EditUserService editUserService;

    @Test
    public void shouldEditUser() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();

        //when
        User result = editUserService.editSelectedUser(user.getId(), editUserDTO);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getFirstName()).isEqualTo(editUserDTO.getFirstName());
        assertThat(result.getLastName()).isEqualTo(editUserDTO.getLastName());
        assertThat(result.getEmail()).isEqualTo(editUserDTO.getEmail());
        assertThat(result.getLoginData().getLogin()).isEqualTo(editUserDTO.getLogin());

        User userInDatabase = entityManager.find(User.class, user.getId());

        assertNotNull(userInDatabase);
        assertThat(userInDatabase).isEqualTo(result);

    }

    @Test
    public void shouldEditUserWhenEditedUserEmailAndLoginIsTheSame() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        editUserDTO.setEmail(user.getEmail());
        editUserDTO.setLogin(user.getLoginData().getLogin());


        //when
        User result = editUserService.editSelectedUser(user.getId(), editUserDTO);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getFirstName()).isEqualTo(editUserDTO.getFirstName());
        assertThat(result.getLastName()).isEqualTo(editUserDTO.getLastName());
        assertThat(result.getEmail()).isEqualTo(editUserDTO.getEmail());
        assertThat(result.getLoginData().getLogin()).isEqualTo(editUserDTO.getLogin());

        User userInDatabase = entityManager.find(User.class, user.getId());

        assertNotNull(userInDatabase);
        assertThat(userInDatabase).isEqualTo(result);

    }

    @Test(expectedExceptions = UserNotExistException.class)
    public void shouldNotEditUser_whenThrowUserNotExistException() throws Exception {
        //given
        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        Long notExistUserId = -100L;

        //when
        editUserService.editSelectedUser(notExistUserId, editUserDTO);
    }

    @Test(expectedExceptions = UserWithSelectedLoginExistException.class)
    public void shouldNotEditUser_whenThrowUserWithSelectedLoginExistException() throws Exception{

        //given
        User exampleUser = UserProvider.createUserWithAllRoles();
        entityManager.persist(exampleUser);

        User userToEdit = UserProvider.createUserWithAllRoles();
        entityManager.persist(userToEdit);

        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        editUserDTO.setLogin(exampleUser.getLoginData().getLogin());

        //when
        editUserService.editSelectedUser(userToEdit.getId(), editUserDTO);

    }

    @Test(expectedExceptions = UserWithSelectedEmailExistException.class)
    public void shouldNotEditUser_whenThrowUserWithSelectedEmailExistException() throws Exception{

        //given
        User exampleUser = UserProvider.createUserWithAllRoles();
        entityManager.persist(exampleUser);

        User userToEdit = UserProvider.createUserWithAllRoles();
        entityManager.persist(userToEdit);

        EditUserDTO editUserDTO = EditUserDTOProvider.prepareEditUserDTO();
        editUserDTO.setEmail(exampleUser.getEmail());

        //when
        editUserService.editSelectedUser(userToEdit.getId(), editUserDTO);

    }

}