package pl.krystiantokarz.virtualdeanery.service.filetodownload;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.file.*;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.repository.FileToDownloadDescriptionRepository;
import pl.krystiantokarz.virtualdeanery.provider.entity.FileToDownloadDescriptionProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.file.DownloadFileService;
import pl.krystiantokarz.virtualdeanery.service.file.exceptions.DownloadFileException;
import pl.krystiantokarz.virtualdeanery.service.filetodownload.exceptions.DownloadFileToDownloadException;

import java.io.OutputStream;

import static org.mockito.Mockito.*;

public class DownloadFileToDownloadServiceTest {

    private static final String REPOSITORY_PATH = "REPOSITORY-PATH";

    @Mock
    private DownloadFileService downloadFileService;

    @Mock
    private FileToDownloadDescriptionRepository fileToDownloadDescriptionRepository;

    @Mock
    private FileRepositoryPathComponent fileRepositoryPathComponent;

    @Mock
    private OutputStream outputStream;

    @InjectMocks
    private DownloadFileToDownloadService downloadFileToDownloadService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldDownloadFile() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);

        Long fileId = 100L;
        //when
        when(fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId)).thenReturn(fileEntity);
        when(fileRepositoryPathComponent.getRepositoryPath(fileEntity.getFileStatus(), fileEntity.getFileType())).thenReturn(REPOSITORY_PATH);
        downloadFileToDownloadService.downloadFile(fileId,outputStream);

        //then
        verify(downloadFileService).downloadFile(REPOSITORY_PATH,fileEntity.getSavedFileName(), outputStream);
    }


    @Test(expectedExceptions = DownloadFileToDownloadException.class)
    public void shouldThrowException_whenDownloadFileIsImpossible() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        FileToDownloadDescriptionEntity fileEntity = FileToDownloadDescriptionProvider.createReadyToDownloadArchiveFileToDownloadDescriptionEntity(user);

        Long fileId = 100L;

        //when
        when(fileToDownloadDescriptionRepository.findFileDescriptionByIdIfExist(fileId)).thenReturn(fileEntity);
        when(fileRepositoryPathComponent.getRepositoryPath(fileEntity.getFileStatus(), fileEntity.getFileType())).thenReturn(REPOSITORY_PATH);
        doThrow(DownloadFileException.class).when(downloadFileService).downloadFile(REPOSITORY_PATH, fileEntity.getSavedFileName(), outputStream);
        downloadFileToDownloadService.downloadFile(fileId,outputStream);
    }
}