package pl.krystiantokarz.virtualdeanery.service.timetable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import pl.krystiantokarz.virtualdeanery.domain.timetable.TimetableEvent;
import pl.krystiantokarz.virtualdeanery.domain.user.User;
import pl.krystiantokarz.virtualdeanery.infrastructure.dto.timetable.in.CreateEventDTO;
import pl.krystiantokarz.virtualdeanery.provider.dto.CreateEventDTOProvider;
import pl.krystiantokarz.virtualdeanery.provider.entity.UserProvider;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.EventTimeLimitException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventDateException;
import pl.krystiantokarz.virtualdeanery.service.timetable.exceptions.ValidEventTimeException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class CreateEventServiceIT extends AbstractTransactionalTestNGSpringContextTests {


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CreateEventService createEventService;

    @Test
    public void shouldCorrectCreateEvent() throws Exception {
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();

        //when
        createEventService.createEvent(createEventDTO, user.getId());

        //then
        TimetableEvent timetableEvent = entityManager.createQuery("SELECT t FROM TimetableEvent t WHERE t.message = :msg", TimetableEvent.class)
                .setParameter("msg", createEventDTO.getMessage())
                .getSingleResult();

        assertThat(timetableEvent).isNotNull();
        assertThat(timetableEvent.getMessage()).isEqualTo(createEventDTO.getMessage());
        assertThat(timetableEvent.getTopic()).isEqualTo(createEventDTO.getTopic());
        assertThat(timetableEvent.getCreatingUser()).isEqualTo(user);
        assertThat(timetableEvent.getTimeTo()).isEqualTo(createEventDTO.getEventTimeTo());
        assertThat(timetableEvent.getTimeFrom()).isEqualTo(createEventDTO.getEventTimeFrom());
        assertThat(timetableEvent.getEventDate()).isEqualTo(createEventDTO.getDate());
    }

    @Test(expectedExceptions = EventTimeLimitException.class)
    public void shouldThrowExceptionWhenEventStartTimeIsBeforeCorrectTimes() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setEventTimeFrom(LocalTime.of(1,0,0));

        //when
        createEventService.createEvent(createEventDTO, user.getId());
    }

    @Test(expectedExceptions = EventTimeLimitException.class)
    public void shouldThrowExceptionWhenEventEndTimeIsAfterCorrectTimes() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setEventTimeTo(LocalTime.of(23,0,0));

        //when
        createEventService.createEvent(createEventDTO, user.getId());
    }

    @Test(expectedExceptions = ValidEventTimeException.class)
    public void shouldThrowExceptionWhenStartEventTimeIsAfterEndEventTime() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setEventTimeFrom(LocalTime.of(11,0,0));
        createEventDTO.setEventTimeTo(LocalTime.of(10,0,0));

        //when
        createEventService.createEvent(createEventDTO, user.getId());
    }

    @Test(expectedExceptions = ValidEventDateException.class)
    public void shouldThrowExceptionWhenEventDateIsBeforeNow() throws Exception{
        //given
        User user = UserProvider.createUserWithAllRoles();
        entityManager.persist(user);

        CreateEventDTO createEventDTO = CreateEventDTOProvider.prepareCreateEventDTO();
        createEventDTO.setDate(LocalDate.now().minusDays(10));

        //when
        createEventService.createEvent(createEventDTO, user.getId());
    }

}