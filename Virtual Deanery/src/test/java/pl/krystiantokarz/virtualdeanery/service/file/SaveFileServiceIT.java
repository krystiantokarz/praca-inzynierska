package pl.krystiantokarz.virtualdeanery.service.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.io.File;

import static org.assertj.core.api.Java6Assertions.assertThat;


@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test-service.properties"
)
public class SaveFileServiceIT extends AbstractTransactionalTestNGSpringContextTests {

    private static final String PATH_TO_SAVE_FILE = "target/test-classes/repository/file/";
    private static final String NEW_FILE_NAME = "new-file.txt";
    private static final String PREFIX = "prefix_";


    @Autowired
    private SaveFileService saveFileService;

    @Test
    public void shouldSaveFile() throws Exception{

        //given
        MockMultipartFile mockMultipartFile = new MockMultipartFile(NEW_FILE_NAME, NEW_FILE_NAME, "text/plain", "some xml".getBytes());

        //when
        String savedFileName = saveFileService.saveFile(PATH_TO_SAVE_FILE, PREFIX, mockMultipartFile);

        //then
        File file = new File(PATH_TO_SAVE_FILE + PREFIX+ savedFileName);
        assertThat(file.exists()).isFalse();
        file.createNewFile();

    }
}